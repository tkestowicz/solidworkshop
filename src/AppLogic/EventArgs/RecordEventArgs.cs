﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.EventArgs
{
    /// <summary>
    /// Klasa reprezentująca rekord biorący udział w zdarzeniu
    /// </summary>
    public class RecordEventArgs : Interfaces.Events.IEventArgs
    {
        object _record;

        /// <summary>
        /// Metoda zwraca obiekt rekordu rzutowany na odpowiedni typ.
        /// W przypadku błędu rzutowania generowany wyjątek InvalidCastException
        /// </summary>
        /// <typeparam name="T">Typ na jaki ma być rzutowany rekord</typeparam>
        /// <returns>Rzutowany rekord</returns>
        public T Record<T>() where T: class
        {
            if (_record is T)
                return _record as T;
            else
                throw new InvalidCastException();
        }

        public RecordEventArgs(object record)
        {
            _record = record;
        }
    }
}
