﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data;

namespace AppLogic.Core
{
    /// <summary>
    /// Statyczna klasa, która oferuje szereg pomocnicznych metod ogólnego przeznaczenia.
    /// </summary>
    static class Helpers
    {

        /// <summary>
        /// Metoda umożliwia podmianę źródła danych w pliku konfiguracji
        /// </summary>
        /// <param name="config">Obiekt pliku konfiguracji, w którym należy dokonać zmiany</param>
        /// <param name="connectionStringName">Nazwa, pod którą zapisane są parametry połączenia w pliku konfiguracji</param>
        /// <param name="dataSource">Nowe źródło danych</param>
        public static void ChangeDataSource(Configuration config, string connectionStringName, string dataSource) 
        {
           
            // Pobieramy connection string i providera z App.config
            string conn = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            string provider = ConfigurationManager.ConnectionStrings[connectionStringName].ProviderName;

            // Usuwamy stary connection string z App.config
            config.ConnectionStrings.ConnectionStrings.Remove(connectionStringName);

            // Tworzymy obiekt z łańcucha żeby dotrzeć do poszczególnych atrybutów
            var connBuilder = new EntityConnectionStringBuilder(conn);

            // W ten sposób dotrzemy do konkretnych parametrów połączenia
            var tmpConn = new SqlConnectionStringBuilder(connBuilder.ProviderConnectionString);

            // Zamieniamy źródło danych
            tmpConn.DataSource = dataSource;
            connBuilder.ProviderConnectionString = tmpConn.ConnectionString;

            // Dodajemy nową konfigurację
            config.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings(connectionStringName, connBuilder.ConnectionString, provider));
           
            // Zapisujemy zmiany
            config.Save(ConfigurationSaveMode.Full);

            // Odświeżamy obiekt konfiguracji
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        /// <summary>
        /// For debugging only. Returns the SQL statement that is generated
        /// by LINQ for an IQueryable object. This does NOT execute the query
        /// </summary>
        /// <param name="query">The IQueryable object</param>
        /// <returns>The generated SQL as a string</returns>
        public static string GetGeneratedSQL(IQueryable query)
        {
            string sql = string.Empty;
            var connection = Core.ServiceManager.Database.Connection;
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                sql = (query as ObjectQuery).ToTraceString();
            }
            catch (Exception) { }
            return sql;
        }
    }
}
