﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Core.Utils.Validators
{
    internal class DataValidator : IValidator
    {
        private bool _valid = false;

        public bool Valid
        {
            get { return _valid; }
        }

        public void Validate<T>(T data)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Metoda sprawdza, czy podany rok znajduje się w zadanym przedziale.
        /// </summary>
        /// <param name="from">Dolna granica przedziału</param>
        /// <param name="to">Górna granica przedziału</param>
        /// <param name="year">Rok</param>
        public void ValidateYear(int from, int to, int year)
        {
            if (year >= from && year <= to)
                _valid = true;
        }
    }
}
