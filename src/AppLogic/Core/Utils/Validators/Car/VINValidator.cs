﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Core.Utils.Validators.Car
{
    /// <summary>
    /// Klasa weryfikująca numer identyfikacyjny pojazdu
    /// <see cref="http://pl.wikipedia.org/wiki/Vehicle_Identification_Number"/>
    /// </summary>
    internal class VINValidator : IValidator
    {

        private bool _valid = false;

        public VINValidator(string vin)
        {
            Validate<string>(vin);
        }

        public bool Valid
        {
            get
            {
                return _valid;
            }
        }

        public void Validate<T>(T data)
        {
            var vin = data as string;

            _valid = true;

            if (vin.Length < 17 ||
                vin.ToUpper().Contains('I') ||
                vin.ToUpper().Contains('O') ||
                vin.ToUpper().Contains('Q'))

                _valid = false;

        }
    }
}
