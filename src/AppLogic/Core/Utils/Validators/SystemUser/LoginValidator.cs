﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using AppLogic.Interfaces;

namespace AppLogic.Core.Utils.Validators.SystemUser
{
    internal class LoginValidator : IValidator
    {
        private bool _valid = false;

        public LoginValidator(string login)
        {
            Validate<string>(login);
        }

        public bool Valid
        {
            get
            {
                return _valid;
            }
        }

        /// <summary>
        /// Metoda sprawdza, czy login jest unikalny.
        /// </summary>
        /// <param name="data">Login do walidacji</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public void Validate<T>(T data)
        {
            var login = data as string;

            // Sprawdzamy, czy user z takim loginem już istnieje
            var count = (from u in Core.ServiceManager.Database.Employees where u.Login == login select u).Count();

            if (count == 0)
                _valid = true;
            else
                _valid = false;
        }

    }
}
