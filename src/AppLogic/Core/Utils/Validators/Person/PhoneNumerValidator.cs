﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Core.Utils.Validators.Person
{
    internal class PhoneNumerValidator : IValidator
    {

        private bool _valid = false;

        public PhoneNumerValidator(string phone)
        {
            Validate<string>(phone);
        }
    
        public bool  Valid
        {
	        get {
                return _valid;
            }
        }

        public void  Validate<T>(T data)
        {
            var tmp = data as string;

            try{

                _valid = true;

                // Próbujemy parsować na inta, myślniki zmieniamy na cyfre
                var toint = int.Parse(tmp.Remove('-', '0'));

            }catch(Exception)
            {
                _valid = false;
            }
        }
}
}
