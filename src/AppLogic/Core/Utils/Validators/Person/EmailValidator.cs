﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using AppLogic.Interfaces;

namespace AppLogic.Core.Utils.Validators.Person
{
    internal class EmailValidator : IValidator
    {
        private bool _valid = false;

        public EmailValidator(string email)
        {
            Validate<string>(email);
        }

        public bool Valid
        {
            get
            {
                return _valid;
            }
        }

        /// <summary>
        /// Metoda sprawdza, czy adres email jest poprawny.
        /// </summary>
        /// <see cref="http://msdn.microsoft.com/en-us/library/01escwtf.aspx"/>
        /// <param name="email">Adres email do walidacji</param>
        /// <returns>True - email poprawny, False - email niepoprawny</returns>
        public void Validate<T>(T data)
        {
            var email = data as string;

            _valid = true;

            if (String.IsNullOrEmpty(email))
            {
                _valid = false;
                return;
            }

            // Use IdnMapping class to convert Unicode domain names.
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper);

            if (!Valid)
                return;

            // Return true if email is in valid e-mail format.
            _valid = Regex.IsMatch(email,
                       @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                       @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                       RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Metoda pomocnicza do walidacji adresu email
        /// </summary>
        /// <see cref="http://msdn.microsoft.com/en-us/library/01escwtf.aspx"/>
        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                _valid = false;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}
