﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Core.Utils.Validators.Person
{
    /// <summary>
    /// Pomocnicza klasa dokonująca walidacji numeru PESEL
    /// <see cref="http://www.algorytm.org/numery-identyfikacyjne/pesel/pesel-j.html"/>
    /// </summary>
    internal class PeselValidator : IValidator
    {

        private int[] PESEL;
        private bool _valid = false;

        public PeselValidator(string PESEL)
        {
            Validate<string>(PESEL);
        }

        public bool Valid
        {
            get
            {
                return _valid;
            }
        }

        public void Validate<T>(T data)
        {
            var pesel = data as string;

            if (pesel.Length != 11)
                _valid = false;

            else
            {
                PESEL = new int[11];
                for (int i = 0; i < 11; i++)
                    PESEL[i] = int.Parse(pesel[i].ToString());

                if (CheckSum() && CheckMonth() && CheckDay())
                    _valid = true;
                else
                    _valid = false;
            }
        }

        public int GetBirthYear() {
            
            int year, month;
            
            year = 10 * PESEL[0];
            year += PESEL[1];
            month = 10 * PESEL[2];
            month += PESEL[3];
            
            if (month > 80 && month < 93)
                year += 1800;

            else if (month > 0 && month < 13)
            year += 1900;

            else if (month > 20 && month < 33)
            year += 2000;

            else if (month > 40 && month < 53)
            year += 2100;

            else if (month > 60 && month < 73)
            year += 2200;

            return year;
        }
 
        public int GetBirthMonth() {
            int month;
            
            month = 10 * PESEL[2];
            month += PESEL[3];
            
            if (month > 80 && month < 93)
            month -= 80;

            else if (month > 20 && month < 33)
            month -= 20;

            else if (month > 40 && month < 53)
            month -= 40;

            else if (month > 60 && month < 73)
            month -= 60;

            return month;
        }

        public int GetBirthDay() {
            int day;
            
            day = 10 * PESEL[4];
            day += PESEL[5];
            
            return day;
        }
 
        /// <summary>
        /// Zwraca inforamcje o płci
        /// </summary>
        /// <returns>'m' - mężczyzna, 'k' - kobieta, null - błędny odczyt</returns>
        public char? GetSex() {

            if (_valid) {
                if (PESEL[9] % 2 == 1)
                    return 'm';

                else 
                    return 'f';

            }
            else
                return null;
        }
 
        private bool CheckSum() {

            int sum = 1 * PESEL[0] +
            3 * PESEL[1] +
            7 * PESEL[2] +
            9 * PESEL[3] +
            1 * PESEL[4] +
            3 * PESEL[5] +
            7 * PESEL[6] +
            9 * PESEL[7] +
            1 * PESEL[8] +
            3 * PESEL[9];
            sum %= 10;
            sum = 10 - sum;
            sum %= 10;
 
            if (sum == PESEL[10])
                    return true;
            else
                    return false;

        }
 
        private bool CheckMonth() {

            return (GetBirthMonth() > 0 && GetBirthDay() <= 31);
        }
 
        private bool CheckDay() {
            int year = GetBirthYear();
            int month = GetBirthMonth();
            int day = GetBirthDay();

            if ((day >0 && day < 32) &&
                (month == 1 || month == 3 || month == 5 ||
                month == 7 || month == 8 || month == 10 ||
                month == 12))
                    return true;

            else if ((day >0 && day < 31) &&
                    (month == 4 || month == 6 || month == 9 ||
                    month == 11))
                        return true;

            else if ((day >0 && day < 30 && LeapYear(year)) ||
                    (day >0 && day < 29 && !LeapYear(year)))
                        return true;

            else
                return false;
      
        }
 
        private bool LeapYear(int year) {
    
            return (year % 4 == 0 && year % 100 != 0 || year % 400 == 0);

        }

    }
}
