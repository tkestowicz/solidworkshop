﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Core.Utils
{
    /// <summary>
    /// Klasa dostarcza metody weryfikujące parametry samochodu
    /// </summary>
    public static class CarDataValidator
    {

        /// <summary>
        /// Metoda weryfikuje numer nadwozia pojazdu
        /// </summary>
        /// <param name="vin">Numer nadwozia</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public static bool IsVinValid(string vin)
        {
            return (new Validators.Car.VINValidator(vin)).Valid;
        }

        /// <summary>
        /// Metoda sprawdza, czy rok produkcji zawiera się w przedziale od-obecny
        /// </summary>
        /// <param name="from">Dolna granica roku produkcji</param>
        /// <param name="year">Rok produkcji</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public static bool IsProductionYearValid(int from, int year)
        {
            var validator = new Validators.DataValidator();
            
            validator.ValidateYear(from, DateTime.Now.Year, year);

            return validator.Valid;
        }

    }
}
