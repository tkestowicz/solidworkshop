﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Core.Utils
{
    /// <summary>
    /// Klasa oferuje metody walidacji danych osobowych.
    /// </summary>
    public static class PersonalDataValidator
    {

        /// <summary>
        /// Metoda sprawdza poprawność adresu email
        /// </summary>
        /// <param name="email">Adres email do weryfikacji</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public static bool IsEmailValid(string email)
        {
            return  (new Validators.Person.EmailValidator(email)).Valid;
        }

        /// <summary>
        /// Metoda sprawdza poprawność numeru PESEL
        /// </summary>
        /// <param name="pesel">Numer PESEL do weryfikacji</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public static bool IsPeselValid(string pesel)
        {
            return (new Validators.Person.PeselValidator(pesel)).Valid;
        }

        /// <summary>
        /// Metoda sprawdza poprawność numeru telefonu
        /// </summary>
        /// <param name="phone">Numer telefonu do weryfikacji</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public static bool IsPhoneNumberValid(string phone)
        {
            return (new Validators.Person.PhoneNumerValidator(phone)).Valid;
        }

        /// <summary>
        /// Metoda sprawdza, czy login jest unikalny
        /// </summary>
        /// <param name="login">Nazwa użytkownika do weryfikacji</param>
        /// <returns>True - dane poprawne, False - dane niepoprawne</returns>
        public static bool IsLoginUnique(string login)
        {
            return (new Validators.SystemUser.LoginValidator(login)).Valid;
        }

    }
}
