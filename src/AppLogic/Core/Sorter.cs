﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Linq.Expressions;

namespace AppLogic.Core
{
    public enum SortDirection
    {
        Ascending,
        Descending
    };

    public class GenericSorter<T, PT>
    {
        public IQueryable<T> Sort(IEnumerable source, string sortExpression, SortDirection sortDirection)
        {
            var param = Expression.Parameter(typeof(T), "item");

            var sortLambda = Expression.Lambda<Func<T, PT>>(Expression.Convert(Expression.Property(param, sortExpression), typeof(PT)), param);

            if (sortDirection == SortDirection.Ascending)
            {
                return source.OfType<T>().AsQueryable<T>().OrderBy<T, PT>(sortLambda).AsQueryable<T>();
            }
            else
            {
                return source.OfType<T>().AsQueryable<T>().OrderByDescending<T, PT>(sortLambda).AsQueryable<T>();
            }
        }

        public IQueryable<T> Sort(IQueryable<T> source, string sortExpression, SortDirection sortDirection)
        {
            var param = Expression.Parameter(typeof(T), "item");

            var sortLambda = Expression.Lambda<Func<T, PT>>(Expression.Convert(Expression.Property(param, sortExpression), typeof(PT)), param);

            if (sortDirection == SortDirection.Ascending)
            {
                return source.AsQueryable<T>().OrderBy<T, PT>(sortLambda);
            }
            else
            {
                return source.AsQueryable<T>().OrderByDescending<T, PT>(sortLambda);
            }
        }
    }

    public class LateBoundSorter
    {
        public IQueryable Sort(IQueryable source, string sortExpression, SortDirection sortDirection)
        {
            if (sortDirection == SortDirection.Ascending)
            {
                return source.OfType<object>().OrderBy(item =>
                    GetPropertyValue(item, sortExpression));
            }
            else
            {
                return source.OfType<object>().OrderByDescending(item =>
                    GetPropertyValue(item, sortExpression));
            }
        }

        public static object GetPropertyValue(object item, string propertyName)
        {
            return item.GetType().GetProperty(propertyName).GetValue(item, null);
        }
    }
}
