﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Presenters;
using AppLogic.Interfaces.Events;

namespace AppLogic.Core
{

    /// <summary>
    /// Klasa przechowująca informacje o podpiętych zdarzeniach
    /// </summary>
    public class EventCollection
    {

        #region Types

        public delegate void DisposeEvents();

        public delegate void InitializeEvents();

        public delegate void AddHandler(object sender);

        public delegate void EditHandler(object sender);

        public delegate void EditRecordHandler(object sender, EventArgs.RecordEventArgs record);

        public delegate void PreviewRecordHandler(object sender, EventArgs.RecordEventArgs record);

        public delegate void CancelHandler(object sender);

        public delegate void SaveHandler(object sender);

        public delegate void SaveSucceedHandler(object sender, EventArgs.RecordEventArgs record);

        public delegate void RefreshHandler(object sender);

        public delegate void DataBindingCompleteHandler(object sender);

        public delegate void ClosePopupHandler(object sender);

        public delegate void ReloadDropdownListHandler(object sender, KeyValuePair<int, string> e);

        public delegate void SearchHandler(object sender);

        public delegate void FilterRecordsHandler(object sender, Dictionary<string, object> fields);

        public delegate void ClearHandler(object sender);

        #endregion

        #region Fields

        /// <summary>
        /// Zdarzenie odpięcia metod do obsługi
        /// </summary>
        public event DisposeEvents Disposer;

        /// <summary>
        /// Zdarzenie podpięcia metod do obsługi
        /// </summary>
        public event InitializeEvents Loader;

        #endregion

        #region Methods

        /// <summary>
        /// Metoda sprawdza, czy delegata jest już podpięta
        /// </summary>
        /// <param name="delegates">Lista podpiętych delegat</param>
        /// <param name="callback">Delegata do podpięcia</param>
        /// <returns>Prawda jeżeli delegata jest podpięta</returns>
        private bool CallbackExists(System.Delegate[] delegates, object callback)
        {
            return delegates.Contains(callback as System.Delegate);
        }

        /// <summary>
        /// Wyzwala metod odpinających zdarzenia
        /// </summary>
        public void Dispose()
        {
            if (Disposer != null)
                Disposer();
        }

        /// <summary>
        /// Wyzwala metody przypinające zdarzenia
        /// </summary>
        public void Init()
        {
            if (Loader != null)
                Loader();
        }
        
        #endregion

        #region EditEvent

        /// <summary>
        /// Odpala zdarzenie edycji
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnEdit(object sender)
        {
            if (EditEvent != null)
                EditEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia edycji
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(EditHandler callback)
        {
            if (EditEvent == null)
                EditEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(EditEvent.GetInvocationList(), callback) == false)
                EditEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia edycji
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(EditHandler callback)
        {
            EditEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie edycji
        /// </summary>
        public event EditHandler EditEvent;

        #endregion

        #region EditRecordEvent

        /// <summary>
        /// Odpala zdarzenie edycji
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        /// <param name="record">Rekord dla którego zaszło zdarzenie</param>
        public void OnEditRecord(object sender, EventArgs.RecordEventArgs record)
        {
            if (EditRecordEvent != null)
                EditRecordEvent(sender, record);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia edycji
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(EditRecordHandler callback)
        {
            if (EditRecordEvent == null)
                EditRecordEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(EditRecordEvent.GetInvocationList(), callback) == false)
                EditRecordEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia edycji
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(EditRecordHandler callback)
        {
            EditRecordEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie edycji
        /// </summary>
        public event EditRecordHandler EditRecordEvent;

        #endregion

        #region PreviewRecordEvent

        /// <summary>
        /// Odpala zdarzenie podglądu rekordu
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        /// <param name="record">Rekord dla którego zaszło zdarzenie</param>
        public void OnPreviewRecord(object sender, EventArgs.RecordEventArgs record)
        {
            if (PreviewRecordEvent != null)
                PreviewRecordEvent(sender, record);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia podglądu rekordu
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(PreviewRecordHandler callback)
        {
            if (PreviewRecordEvent == null)
                PreviewRecordEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(PreviewRecordEvent.GetInvocationList(), callback) == false)
                PreviewRecordEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia podglądu rekordu
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(PreviewRecordHandler callback)
        {
            PreviewRecordEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie podglądu rekordu
        /// </summary>
        public event PreviewRecordHandler PreviewRecordEvent;

        #endregion

        #region AddEvent

        /// <summary>
        /// Odpala zdarzenie dodawania
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnAdd(object sender)
        {
            if (AddEvent != null)
                AddEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia dodawania
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(AddHandler callback)
        {
            if(AddEvent == null)
                AddEvent += callback;
            
            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(AddEvent.GetInvocationList(), callback) == false)
                AddEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia edycji
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(AddHandler callback)
        {
            AddEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie dodawania
        /// </summary>
        public event AddHandler AddEvent;

        #endregion

        #region CancelEvent

        /// <summary>
        /// Odpala zdarzenie porzucenia zmian
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnCancel(object sender)
        {
            if (CancelEvent != null)
                CancelEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia porzucenia zmian
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(CancelHandler callback)
        {
            if (CancelEvent == null)
                CancelEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(CancelEvent.GetInvocationList(), callback) == false)
                CancelEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia porzucenia zmian
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(CancelHandler callback)
        {
            CancelEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie porzucenia zmian
        /// </summary>
        public event CancelHandler CancelEvent;

        #endregion

        #region SaveEvent

        /// <summary>
        /// Odpala zdarzenie zapisania zmian
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnSave(object sender)
        {
            if (SaveEvent != null)
                SaveEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia zapisania zmian
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(SaveHandler callback)
        {
            if (SaveEvent == null)
                SaveEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(SaveEvent.GetInvocationList(), callback) == false)
                SaveEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia zapisania zmian
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(SaveHandler callback)
        {
            SaveEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie zapisania zmian
        /// </summary>
        public event SaveHandler SaveEvent;

        #endregion

        #region SaveSucceedEvent

        /// <summary>
        /// Odpala zdarzenie poprawnego zapisania zmian
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        /// /// <param name="record">Rekord dla którego zaszło zdarzenie</param>
        public void OnSaveSucceed(object sender, EventArgs.RecordEventArgs record)
        {
            if (SaveSucceedEvent != null)
                SaveSucceedEvent(sender, record);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia poprawnego zapisania zmian
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(SaveSucceedHandler callback)
        {
            if (SaveSucceedEvent == null)
                SaveSucceedEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(SaveSucceedEvent.GetInvocationList(), callback) == false)
                SaveSucceedEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia poprawnego zapisania zmian
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(SaveSucceedHandler callback)
        {
            SaveSucceedEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie poprawnego zapisania zmian
        /// </summary>
        public event SaveSucceedHandler SaveSucceedEvent;

        #endregion

        #region RefreshEvent

        /// <summary>
        /// Odpala zdarzenie odświeżania
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnRefresh(object sender)
        {
            if (RefreshEvent != null)
                RefreshEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia odświeżania
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(RefreshHandler callback)
        {
            if (RefreshEvent == null)
                RefreshEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(RefreshEvent.GetInvocationList(), callback) == false)
               RefreshEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia odświeżania
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(RefreshHandler callback)
        {
            RefreshEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie odświeżania
        /// </summary>
        public event RefreshHandler RefreshEvent;

        #endregion

        #region DataBindingCompleteEvent

        /// <summary>
        /// Odpala zdarzenie
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnDataBindingComplete(object sender)
        {
            if (DataBindingCompleteEvent != null)
                DataBindingCompleteEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(DataBindingCompleteHandler callback)
        {
            if (DataBindingCompleteEvent == null)
                DataBindingCompleteEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(DataBindingCompleteEvent.GetInvocationList(), callback) == false)
                DataBindingCompleteEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(DataBindingCompleteHandler callback)
        {
            DataBindingCompleteEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie załadowania danych
        /// </summary>
        public event DataBindingCompleteHandler DataBindingCompleteEvent;

        #endregion

        #region ClosePopupEvent

        /// <summary>
        /// Odpala zdarzenie
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnPopupClose(object sender)
        {
            if (ClosePopupEvent != null)
                ClosePopupEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(ClosePopupHandler callback)
        {
            if (ClosePopupEvent == null)
                ClosePopupEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(ClosePopupEvent.GetInvocationList(), callback) == false)
                ClosePopupEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(ClosePopupHandler callback)
        {
            ClosePopupEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie
        /// </summary>
        public event ClosePopupHandler ClosePopupEvent;

        #endregion

        #region SearchEvent

        /// <summary>
        /// Odpala zdarzenie
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnSearch(object sender)
        {
            if (SearchEvent != null)
                SearchEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(SearchHandler callback)
        {
            if (SearchEvent == null)
                SearchEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(SearchEvent.GetInvocationList(), callback) == false)
                SearchEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(SearchHandler callback)
        {
            SearchEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie
        /// </summary>
        public event SearchHandler SearchEvent;

        #endregion

        #region ClearEvent

        /// <summary>
        /// Odpala zdarzenie
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnClear(object sender)
        {
            if (ClearEvent != null)
                ClearEvent(sender);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(ClearHandler callback)
        {
            if (ClearEvent == null)
                ClearEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(ClearEvent.GetInvocationList(), callback) == false)
                ClearEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(ClearHandler callback)
        {
            ClearEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie
        /// </summary>
        public event ClearHandler ClearEvent;

        #endregion

        #region FilterRecordsEvent

        /// <summary>
        /// Odpala zdarzenie
        /// </summary>
        /// <param name="sender">Prezenter dla którego zaszło zdarzenie</param>
        public void OnFilterRecords(object sender, Dictionary<string, object> fields)
        {
            if (FilterRecordsEvent != null)
                FilterRecordsEvent(sender, fields);
        }

        /// <summary>
        /// Przypina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Subscribe(FilterRecordsHandler callback)
        {
            if (FilterRecordsEvent == null)
                FilterRecordsEvent += callback;

            // Zabezpieczenie przed podwójnym podpięciem tej samej metody
            else if (CallbackExists(FilterRecordsEvent.GetInvocationList(), callback) == false)
                FilterRecordsEvent += callback;
        }

        /// <summary>
        /// Odpina obsługę zdarzenia
        /// </summary>
        /// <param name="callback">Metoda obsługująca zdarzenie</param>
        public void Unsubscribe(FilterRecordsHandler callback)
        {
            FilterRecordsEvent -= callback;
        }

        /// <summary>
        /// Zdarzenie
        /// </summary>
        public event FilterRecordsHandler FilterRecordsEvent;

        #endregion

    }
}
