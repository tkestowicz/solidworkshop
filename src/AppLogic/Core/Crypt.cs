﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace AppLogic.Core
{
    /// <summary>
    /// Klasa udostępnia metody hashujące różnymi algorytmami
    /// </summary>
    public static class Crypt
    {
        /// <summary>
        /// Hashowanie algorytmem md5
        /// Info: Źródło wzięte z internetu
        /// </summary>
        /// <param name="input">Dane wejściowe</param>
        /// <returns>Wygenerowany hash</returns>
        public static string GetMD5Hash(string input)
        {
            MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();
            byte[] bs = Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new StringBuilder();

            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }

            return s.ToString();
        }

        /// <summary>
        /// Metoda generuje losowe hasło o podanej długości
        /// </summary>
        /// <see cref="http://madskristensen.net/post/Generate-random-password-in-C.aspx"/>
        /// <param name="length">Długość hasła</param>
        /// <returns>Wygenerowane hasło</returns>
        public static string GeneratePassword(int length)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[length];
            Random rd = new Random();

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
    }
}
