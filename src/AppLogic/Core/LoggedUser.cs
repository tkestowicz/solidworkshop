﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Core
{
    /// <summary>
    /// Klasa przechowuje informacje o zalogowanym użytkowniku, jest singletonem.
    /// </summary>
    public class LoggedUser
    {
        static LoggedUser _instance;

        /// <summary>
        /// Właściwość umożliwia dostęp do obiektu zalogowanego użytkownika
        /// </summary>
        public static LoggedUser Instance
        {
            get
            {
                return _instance ?? (_instance = new LoggedUser());
            }
        }

        /// <summary>
        /// Właściwość umożliwia dostęp do danych zalogowanego użytkownika
        /// </summary>
        public Model.Employee User
        {
            get;
            set;
        }

        /// <summary>
        /// Flaga, która informuje czy użytkownik jest zalogowany
        /// </summary>
        public bool Logged
        {
            set;
            get;
        }

        /// <summary>
        /// Metoda usuwa referencje do obiektu zalogowanego użytkownika
        /// </summary>
        public void Destroy()
        {
            _instance = null;
        }

    }
}
