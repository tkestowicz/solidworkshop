﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AppLogic.Core
{
    /// <summary>
    /// Prosta klasa dająca dostęp do wewnętrznych usług jak baza danych, czy kontroler zdarzeń
    /// </summary>
    public class ServiceManager
    {

        /// <summary>
        /// Dzięki temu wymiana providera = zmiana tylko w tym miejscu, singleton!
        /// </summary>
        public static readonly Model.SWDatabase Database = new Model.SWDatabase(ConfigurationManager.ConnectionStrings[Properties.Settings.Default.ConnectionStringName].ConnectionString);

        /// <summary>
        /// Dostęp do kontrolera wszystkich zarejestrowanych zdarzeń w jednym miejscu, singleton
        /// </summary>
        public static readonly EventCollection Events = new EventCollection();
    }
}
