﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace AppLogic.Exceptions
{
    /// <summary>
    /// Klasa zgłaszająca wyjątek jeżeli występuje problem z połączeniem do bazy danych
    /// </summary>
    public class DatabaseConnectionException : EntityException
    {
        public DatabaseConnectionException(string msg="")
            : base(msg)
        { }
    }
}
