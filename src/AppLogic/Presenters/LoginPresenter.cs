﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using System.Configuration;
using Model;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data;

namespace AppLogic.Presenters
{

    /// <summary>
    /// Klasa logiki okna logowania
    /// </summary>
    public class LoginPresenter
    {

        #region Fields

        private ILoginView _view;

        #endregion

        public LoginPresenter(ILoginView view)
        {
            _view = view;
        }

        /// <summary>
        /// Metoda przeprowadza logowanie do systemu
        /// </summary>
        /// <param name="login">Nazwa użytkownika</param>
        /// <param name="password">Hasło</param>
        public void SignIn(string login, string password)
        {

            try
            {

                // Prosta weryfikacja danych
                if (String.IsNullOrEmpty(login) || String.IsNullOrEmpty(password))
                    throw new ArgumentException(Properties.ApplicationMessages.Default.msgValidationErrorLoginData);


                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Hashujemy wprowadzone hasło algorytmem md5
                string hashedPass = Core.Crypt.GetMD5Hash(password);

                // Pobranie rekordu z bazy danych przy pomocy lambda expression
                Employee user = db.Employees.Single(u => u.Login == login && u.Password == hashedPass);

                #region Alternatywny dostęp do danych
                //var userTmp = (from u in db.Employees 
                //            where u.Login == login && u.Password == password && u.State == true
                //            select u).SingleOrDefault<Employee>();
                //
                #endregion

                Core.LoggedUser.Instance.Logged = true;
                Core.LoggedUser.Instance.User = user;

                _view.LoginSucceed();
                return;
                 
                }// Nie znaleziono usera w bazie
                catch (InvalidOperationException /*e*/)
                {
                    _view.ShowError(Properties.ApplicationMessages.Default.msgLoginIncorrectData, Properties.ApplicationMessages.Default.msgDoLoginErrorCaption);

                }// Nie można otworzyć połączenia do bazy
                catch (EntityException e)
                {
                    var exc = e;
                    _view.ShowError(Properties.ApplicationMessages.Default.msgWrongConnectionParams);

                }// Niepoprawne dane
                catch (ArgumentException e)
                {
                    _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgValidationErrorDefaultCaption);
                }

                _view.LoginFailed();

        }

        /// <summary>
        /// Metoda ładuje nazwę serwera do kontrolki
        /// </summary>
        public void LoadServerName() 
        {
            _view.ServerName = Properties.Settings.Default.ServerName;
        }

        /// <summary>
        /// Metoda zapisuje nazwę serwera w ustawieniach
        /// </summary>
        public void SaveServerName()
        {

            try
            {

                // Weryfikacja danych
                if(String.IsNullOrEmpty(_view.ServerName))
                    throw new ArgumentException(Properties.ApplicationMessages.Default.msgServerNameError);

                Properties.Settings.Default.ServerName = _view.ServerName;
                Properties.Settings.Default.Save();

                Core.Helpers.ChangeDataSource(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None),
                                                    Properties.Settings.Default.ConnectionStringName, _view.ServerName);

            }// Błąd zapisu
            catch (ConfigurationErrorsException /*e*/) 
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgServerNameChangingNotSaved, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            
            }// Nieprawne dane
            catch(ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
            
        }

        /// <summary>
        /// Metoda przeprowadza test połączenia z bazą danych
        /// </summary>
        private bool IsConnectionValid()
        {
            try
            {            
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                if (db.DatabaseExists())
                    return true;
                else
                    return false;
            }
            catch (SqlException)
            {
                return false;
            }
        }

        /// <summary>
        /// Metoda obsługuje zdarzenie przeprowadzenia testu połączenia z bazą danych
        /// </summary>
        public void ConnectionTest()
        {

            if (IsConnectionValid())
                _view.ShowInfo(String.Format(Properties.ApplicationMessages.Default.msgConnectionTestSucceed, Properties.Settings.Default.ServerName), Properties.ApplicationMessages.Default.msgConnectionTestCaption);
            else
                _view.ShowError(String.Format(Properties.ApplicationMessages.Default.msgConnectionTestError, Properties.Settings.Default.ServerName), Properties.ApplicationMessages.Default.msgConnectionTestCaption);

            _view.ResetSettingsPanel();
        }
    }
}
