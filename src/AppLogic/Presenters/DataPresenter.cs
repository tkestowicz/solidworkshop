﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Events;

namespace AppLogic.Presenters
{

    public class OrderBy
    {
            /// <summary>
            /// Kolumna po której rekordy mają być sortowane
            /// </summary>
            public string Column;

            public Core.SortDirection Direction;
    }

    /// <summary>
    /// Prezenter zawierający logikę ładowania tabel z danymi do głównego okna
    /// </summary>
    public abstract partial class DataPresenter : IPresenter,
        IEditEvent, IEventSubscriber, ISaveSucceed, IRefreshEvent, IFilterRecordsEvent
    {

        /// <summary>
        /// Informacje o sortowaniu
        /// </summary>
        public OrderBy Ordering;

        IDictionary<string, object> _filterFields = new Dictionary<string, object>();

        /// <summary>
        /// Domyślny kontruktor dla prezentera
        /// </summary>
        /// <param name="view">Referencja do widoku siatki danych</param>
        public DataPresenter(IDataGridView view)
        {
            _view = view;
            Reset();
        }

        #region IPresenter

        public void Initialize()
        {
            Core.ServiceManager.Events.Disposer += new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader += new Core.EventCollection.InitializeEvents(BindEvents);

            Core.ServiceManager.Events.Init();
        }

        #endregion

        #region Abstract Methods - wykorzystanie wzorca Template Method

        /// <summary>
        /// Metoda ładująca dane
        /// </summary>
        abstract public void LoadData();

        /// <summary>
        /// Obsługa akcji usuwania rekordu
        /// </summary>
        abstract public void DeleteRecord();

        /// <summary>
        /// Prefix indeksu pod jakim będą szukane nazwy kolumn w pliku tłumaczeń
        /// </summary>
        abstract public string GetColumnHeaderPrefixIndex();

        /// <summary>
        /// Metoda dodaje sortowanie do zapytania
        /// </summary>
        /// <typeparam name="T">Typ rekordów w zapytaniu</typeparam>
        /// <param name="query">Zapytanie pierwotne</param>
        /// <returns>Zapytanie z dołączonym sortowaniem</returns>
        abstract protected IQueryable<T> OrderBy<T>(IQueryable<T> query);

        #endregion

        #region IEventSubscriber

        /// <summary>
        /// Podpięcie zdarzeń do kontrolera
        /// </summary>
        public void BindEvents()
        {
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.EditHandler(OnEdit));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.SaveSucceedHandler(OnSaveSucceed));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.RefreshHandler(OnRefresh));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.FilterRecordsHandler(OnFilterRecords));
        }

        /// <summary>
        /// Odpięcie zdarzeń od kontrolera
        /// </summary>
        public void UnbindEvents()
        {
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.EditHandler(OnEdit));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.SaveSucceedHandler(OnSaveSucceed));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.RefreshHandler(OnRefresh));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.FilterRecordsHandler(OnFilterRecords));

            // ZAWSZE! trzeba odpinać loader/disposer zdarzeń dla danego obiektu
            Core.ServiceManager.Events.Disposer -= new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader -= new Core.EventCollection.InitializeEvents(BindEvents);
        }

        #endregion

        #region Fields and Properties

        /// <summary>
        /// Uchwyt do widoku siatki danych
        /// </summary>
        protected IDataGridView _view;

        /// <summary>
        /// Liczba rekordów na jednej stronie tabeli
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Numer aktywnej strony
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Liczba wszystkich stron
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Parametry filtrowania
        /// </summary>
        public IDictionary<string, object> FilterFields 
        { 
            get
            {
                return _filterFields;
            }
            set
            {
                if (value == null)
                {
                    _filterFields = new Dictionary<string, object>();
                    Core.ServiceManager.Events.OnClear(this);
                }
                else
                    _filterFields = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metoda resetuje ustawienia prezentera do stanu pierwotnego
        /// </summary>
        public void Reset()
        {
            CurrentPage = 1;
            TotalPages = 0;
            ItemsPerPage = Properties.Settings.Default.DefaultItemsPerPage;

            _view.ItemsPerPageRange = Properties.Settings.Default.ItemsPerPage;
            _view.ItemsPerPage = ItemsPerPage;
            _view.ResetGrid();
            _view.ResetNavigation();
        }

        /// <summary>
        /// Metoda obcina rekordy do liczby jaka ma zostać wyświetlona.
        /// </summary>
        /// <typeparam name="T">Typ encji, na których będzie wykonana operacja</typeparam>
        /// <param name="query">Zapytanie LINQ, musi zawierać frazę orderby - inaczej zostanie zgłoszony wyjątek</param>
        /// <returns>Zapytanie LINQ z dołączoną paginacją</returns>
        public IQueryable<T> PerPage<T>(IQueryable<T> query)
        {
            // Ilość rekordów jaką należy pominąć
            int skip = (CurrentPage - 1) * ItemsPerPage;
            
            return query.Skip(skip).Take(ItemsPerPage);
        }

        /// <summary>
        /// Metoda ładuje dane do widoku i ustawia nawigację
        /// </summary>
        /// <typeparam name="T">Typ encji, której dotyczą dane</typeparam>
        /// <param name="query">Zapytanie LINQ pobierające dane</param>
        public void LoadData<T>(IQueryable<T> query)
        {

            // Włączamy kontrolke
            _view.Enable = true;

            // Jeżeli nie ma w bazie rekordów to aktywujemy tylko przycisk dodawania
            if (query.Count() == 0)
            {
                _view.ResetGrid();
                _view.ResetNavigation(true);
                return;
            }

            // Obliczamy liczbę wszystkich stron dla danego zapytania
            TotalPages = (int) Math.Ceiling(query.Count()/(double) ItemsPerPage);
            
            // Wsadzamy dane do grida
            _view.LoadData<T>(PerPage(query).ToList());

            // Ustawiamy parametry nawigacji
            _view.TotalPages = TotalPages;
            _view.CurrentPage = CurrentPage;

            OnPreview();
        }

        #endregion

        #region Data navigation

        /// <summary>
        /// Metoda wyświetla następną stronę wyników
        /// </summary>
        public void MoveNextPage()
        {
            
            if (CurrentPage < TotalPages)
            {
                CurrentPage++;
                LoadData();
            }
        }

        /// <summary>
        /// Metoda wyświetla poprzednią stronę wyników
        /// </summary>
        public void MovePrevPage()
        {
            
            if (CurrentPage > 1)
            {
                CurrentPage--;
                LoadData();
            }
        }

        /// <summary>
        /// Metoda ładuje pierwszą stronę danych
        /// </summary>
        public void MoveFirstPage()
        {
            CurrentPage = 1;
            LoadData();
        }

        /// <summary>
        /// Metoda ładuje ostatnią stronę danych
        /// </summary>
        public void MoveLastPage()
        {
            CurrentPage = TotalPages;
            LoadData();
        }

        /// <summary>
        /// Metoda ładuje wybraną stronę danych
        /// </summary>
        public void MoveToPage(int page)
        {
            // Sprawdzamy zakres
            if (page < 1 || page > TotalPages)
                _view.ShowError(String.Format(Properties.ApplicationMessages.Default.msgNavigationWrongRange, 1, TotalPages));

            else
            {
                CurrentPage = page;
                LoadData();
            }
        }

        /// <summary>
        /// Metoda ładuje ponownie rekody do grida w ilości podanej jako parametr.
        /// </summary>
        /// <param name="numberOfItems">Liczba rekordów na stronę</param>
        public void ChangeItemsPerPage(int numberOfItems)
        {
            CurrentPage = 1;
            ItemsPerPage = numberOfItems;
            LoadData();
        }

        #endregion

        #region Events

        public void OnPreview()
        {
            if (TotalPages > 0)
                Core.ServiceManager.Events.OnPreviewRecord(this, new EventArgs.RecordEventArgs(_view.CurrentRecord));
        }

        public void OnEdit(object sender)
        {
            if(TotalPages > 0)
                Core.ServiceManager.Events.OnEditRecord(this, new EventArgs.RecordEventArgs(_view.CurrentRecord));
        }

        public void OnAdd()
        {
            Core.ServiceManager.Events.OnAdd(this);
        }

        public void OnDelete()
        {
            if (TotalPages > 0 && _view.ShowQuestion(Properties.ApplicationMessages.Default.msgDefaultDeleteConfirmation))
                DeleteRecord();
        }

        public void OnSaveSucceed(object sender, EventArgs.RecordEventArgs record)
        {
            // Odświeżenie grida zostanie wykonane tylko, gdy zapisano dane takiego typu, jaki znajduje się w gridzie
            try
            {
                var rec = record.Record<System.Type>();

                if (_view.CurrentRecord == null || rec.Equals(_view.CurrentRecord.GetType()))
                    _view.RefreshGrid();
            }
            catch (Exception)
            {
            }
        }

        public void OnRefresh(object sender)
        {
            OnPreview();
        }

        public void OnFilterRecords(object sender, Dictionary<string, object> fields)
        {
            _view.ClearFiltersEnabled = true;
            FilterFields = fields;
            LoadData();
        }

        #endregion

    }
}
