﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters
{

    /// <summary>
    /// Szablon logiki do obsługi edycji tabel słownikowych
    /// </summary>
    /// <typeparam name="T">Typ encji z którą edytor</typeparam>
    abstract public partial class DictionaryEditorPresenter : EditorPresenter
    {

        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IDictionaryEditorView _View
        {
            get
            {
                return _view as IDictionaryEditorView;
            }
        }

        /// <summary>
        /// Wartość rekordu
        /// </summary>
        public string Data {
            get
            {
                return _View.Data;
            }
            set
            {
                _View.Data = value;
            }
        }

        #endregion

        #region Methods

        public override void Validate()
        {
            if (String.IsNullOrEmpty(Data))
                throw new ArgumentException(Properties.ApplicationMessages.Default.msgValidationDictionaryData);
        }

        public override void Reset()
        {
            base.Reset();
            Data = null;
        }

        #endregion

        }
}
