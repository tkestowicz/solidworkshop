﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Popups;

namespace AppLogic.Presenters
{
    /// <summary>
    /// Kontener na logikę wyszukiwania - implementacja wzorca strategia
    /// </summary>
    /// <typeparam name="T">Typ widoku, z jakim związana jest logika</typeparam>
    public class SearchLogicContainer<T> : ISearchPresenter<T> where T : IPopupView
    {
        /// <summary>
        /// Właściwy obiekt logiki
        /// </summary>
        ISearchPresenter<T> _presenter;

        /// <summary>
        /// Domyślny kontruktor, inicjalizuje logikę oraz widok.
        /// </summary>
        /// <param name="presenter">Konkretna logika wyszukiwana</param>
        public SearchLogicContainer(ISearchPresenter<T> presenter) 
        {
            _presenter = presenter;

            View.Initialize(presenter);
            Initialize();
        }

        /// <summary>
        /// Pobiera / ustawia widok
        /// </summary>
        public T View
        {
            get
            {
                return _presenter.View;
            }
            set
            {
                _presenter.View = value;
            }
        }

        /// <summary>
        /// Wykonanie czynności przypinających zdarzenia
        /// </summary>
        public void Initialize()
        {
            Core.ServiceManager.Events.Disposer += new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader += new Core.EventCollection.InitializeEvents(BindEvents);

            Core.ServiceManager.Events.Init();

            _presenter.Initialize();
        }

        /// <summary>
        /// Przypięcie uchwytów zdarzeń
        /// </summary>
        public void BindEvents()
        {
            // Podpinanie zdarzeń do obsługi
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.ClearHandler(OnClear));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.SearchHandler(OnSearch));

            _presenter.BindEvents();
        }

        /// <summary>
        /// Odpięcie uchwytów zdarzeń
        /// </summary>
        public void UnbindEvents()
        {
            // Podpinanie zdarzeń do obsługi
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.ClearHandler(OnClear));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.SearchHandler(OnSearch));

            _presenter.UnbindEvents();
        }

        public void OnClear(object sender)
        {
            if (_Proxy(sender))
            {
                _presenter.OnClear(sender);
                Core.ServiceManager.Events.OnFilterRecords(this, new Dictionary<string, object>());
            }
        }

        public void OnSearch(object sender)
        {
            if (_Proxy(sender))
            {
                _presenter.OnSearch(sender);

            }
        }

        /// <summary>
        ///  Metoda sprawda, czy zdarzenie jest adresowane do trzymanej w kontenerze logiki
        /// </summary>
        /// <param name="sender">Obiekt, zgłaszający zdarzenie</param>
        /// <returns>True - zdarzenie jest adresowane do logiki w kontenerze, False - odbiorcą ma być inna logika</returns>
        private bool _Proxy(object sender)
        {
            return (sender.GetType().Equals(_presenter.GetType()));
        }
    }
}
