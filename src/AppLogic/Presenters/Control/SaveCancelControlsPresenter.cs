﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Presenters.Control
{
    public class SaveCancelControlsPresenter
    {

        ISaveCancelControlsView _view;

        public SaveCancelControlsPresenter(ISaveCancelControlsView view)
        {
            _view = view;
        }

        public void OnSave()
        {
            Core.ServiceManager.Events.OnSave(_view.Sender ?? this);
        }

        public void OnCancel()
        {
            if (_view.ShowQuestion(Properties.ApplicationMessages.Default.msgCancelConfirmation, Properties.ApplicationMessages.Default.msgConfirmCaption))
                Core.ServiceManager.Events.OnCancel(_view.Sender ?? this);
        }
    }
}
