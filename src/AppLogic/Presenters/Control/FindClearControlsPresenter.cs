﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Presenters.Control
{
    public class FindClearControlsPresenter
    {

        IFindClearControlsView _view;

        public FindClearControlsPresenter(IFindClearControlsView view)
        {
            _view = view;
        }

        public void OnFind()
        {
            Core.ServiceManager.Events.OnSearch(_view.Sender ?? this);
        }

        public void OnClear()
        {
            Core.ServiceManager.Events.OnClear(_view.Sender ?? this);
        }
    }
}
