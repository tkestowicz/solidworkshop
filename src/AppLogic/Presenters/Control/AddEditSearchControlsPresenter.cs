﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Events;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Presenters.Control
{
    public class AddEditSearchControlsPresenter
    {

        /// <summary>
        /// Delegat dla reakcji na zdarzenie wyszukiwania
        /// </summary>
        /// <param name="sender">Obiek, który zgłasza zdarzenie</param>
        public delegate void SearchEventHandler(object sender);

        /// <summary>
        /// Zdarzenie wyszukiwania. W tym przypadku zdarzenie kliknięcia w przycisk "Wyszukaj"
        /// </summary>
        public event SearchEventHandler SearchEvent;

        IAddEditSearchControlsView _view;

        public AddEditSearchControlsPresenter(IAddEditSearchControlsView view)
        {
            _view = view;
        }

        #region Events

        public void OnSearch()
        {
            // Delegacja do konkretnej metody 
            if (SearchEvent != null)
                SearchEvent(_view.Sender ?? this);
        }

        public void OnEdit()
        {
            Core.ServiceManager.Events.OnEdit(this);
        }

        public void OnAdd()
        {
            Core.ServiceManager.Events.OnAdd(this);
        }

        #endregion Events
    }
}
