﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Presenters.Control
{
    /// <summary>
    /// Klasa logiki dla kontrolki samochodu
    /// </summary>
    public class CarControlPresenter
    {

         ICarControlView _view;

        public CarControlPresenter(ICarControlView view)
        {
            _view = view;

            Initialize();
        }

        /// <summary>
        /// Metoda ładuje dane do list rozwijanych
        /// </summary>
        public void Initialize()
        {
            LoadBrands();
            LoadColors();
            LoadFuels();
        }

        #region Initial methods

        /// <summary>
        /// Metoda ładuje modele wybranej marki do listy rozwijanej
        /// </summary>
        /// <param name="brand">Identyfikator wybranej marki</param>
        public void LoadModelsByBrand(int brandId)
        {
            // Reset aktualnie wybranego modelu
            _view.ModelId = 0;

            // Szukamy miast tylko w przypadku podania województwa
            if (brandId <= 0) return;

            _view.Models = GetModelsByBrand(brandId);
        }

        /// <summary>
        /// Metoda ładuje marki samochodów
        /// </summary>
        public void LoadBrands()
        {
            _view.Brands = GetBrands();
        }

        /// <summary>
        /// Metoda ładuje kolory samochodów
        /// </summary>
        public void LoadColors()
        {
            _view.Colors = GetColors();
        }

        /// <summary>
        /// Metoda ładuje rodzaje paliwa
        /// </summary>
        public void LoadFuels()
        {
            _view.Fuels = GetFuels();
        }

        #endregion

        #region Static methods

        /// <summary>
        /// Metoda pobiera z bazy listę marek samochodów i zwraca je w postaci listy
        /// </summary>
        /// <returns>Lista marek samochodów</returns>
        public static List<Model.Projection.ComboBox> GetBrands()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            return (from c in db.CarBrands
                    orderby c.BrandName
                    select new Model.Projection.ComboBox
                    {
                        Id = c.CarBrandId,
                        Value = c.BrandName
                    }).ToList();
        }

        /// <summary>
        /// Metoda pobiera z bazy listę modeli dla danej marki i zwaraca je w postaci listy
        /// </summary>
        /// <param name="brandId">Id marki</param>
        /// <returns>List modeli samochodów</returns>
        public static List<Model.Projection.ComboBox> GetModelsByBrand(int brandId)
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            return (from c in db.CarModels
                    where c.CarBrandId == brandId
                    select new Model.Projection.ComboBox
                    {
                        Id = c.CarModelId,
                        Value = c.ModelName
                    }).ToList();
        }

        /// <summary>
        /// Metoda pobiera z bazy listę kolorów i zwraca je w postaci listy
        /// </summary>
        /// <returns>Lista kolorów</returns>
        public static List<Model.Projection.ComboBox> GetColors()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            return (from c in db.Colors
                    orderby c.ColorName
                    select new Model.Projection.ComboBox
                    {
                        Id = c.ColorId,
                        Value = c.ColorName
                    }).ToList();
        }

        /// <summary>
        /// Metoda pobiera z bazy listę rodzajów paliwa i zwraca je w postaci listy
        /// </summary>
        /// <returns>Lista rodzajów paliwa</returns>
        public static List<Model.Projection.ComboBox> GetFuels()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            return (from c in db.Fuels
                    orderby c.FuelName
                    select new Model.Projection.ComboBox
                    {
                        Id = c.FuelId,
                        Value = c.FuelName
                    }).ToList();

        }


        #endregion

    }
}
