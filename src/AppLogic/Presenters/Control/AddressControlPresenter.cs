﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Events;
using AppLogic.Interfaces.Editors;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Presenters.Control
{
    public class AddressControlPresenter
    {

         IAddressControlView _view;

        public AddressControlPresenter(IAddressControlView view)
        {
            _view = view;

            // Załadowanie województw do widoku 
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var provinces = from p in db.Provinces
                            orderby p.ProvinceId
                            select new Model.Projection.ComboBox
                            {
                                Id = p.ProvinceId,
                                Value = p.Name
                            };

            if (provinces.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgCarBrandsBeforeModels);

            _view.Provinces = provinces.ToList();
        }

        #region Events

        /// <summary>
        /// Metoda ładuje miasta w danym województwie
        /// </summary>
        /// <param name="province">Nazwa województwa</param>
        public void LoadCitiesByProvince(string province)
        {
            // Reset aktualnie wybranego miasta
            _view.CityId = 0;
            _view.Cities = new List<Model.Projection.ComboBox>();

            // Szukamy miast tylko w przypadku podania województwa
            if (province == null) return;

            _view.Cities = GetCitiesByProvince(province);
        }

        /// <summary>
        /// Metoda pobiera z bazy danych miasta we wskazanym województwie
        /// </summary>
        /// <param name="province">województwo</param>
        /// <returns>Lista miast w województwie</returns>
        public static List<Model.Projection.ComboBox> GetCitiesByProvince(string province)
        {

            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            return (from c in db.Cities
                    where c.Province.Name == province
                    select new Model.Projection.ComboBox
                    {
                        Id = c.CityId,
                        Value = c.Name
                    }).ToList();
        }

        #endregion Events
    }
}
