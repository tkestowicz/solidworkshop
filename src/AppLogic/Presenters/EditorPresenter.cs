﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;
using AppLogic.Interfaces.Events;

namespace AppLogic.Presenters
{

    /// <summary>
    /// Szablon logiki do obsługi edytorów
    /// </summary>
    abstract public partial class EditorPresenter : IEditorPresenter, IOpenPopupSearchEvent, IClearEvent
    {

        #region IPresenter

        public virtual void Initialize()
        {
            Core.ServiceManager.Events.Disposer += new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader += new Core.EventCollection.InitializeEvents(BindEvents);

            Core.ServiceManager.Events.Init();
        }

        #endregion

        #region IEventSubscriber

        public virtual void BindEvents()
        {
            // Podpinanie zdarzeń do obsługi
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.EditRecordHandler(OnEdit));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.AddHandler(OnAdd));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.CancelHandler(OnCancel));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.SaveHandler(OnSave));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.PreviewRecordHandler(OnPreview));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.ClearHandler(OnClear));
        }

        public virtual void UnbindEvents()
        {
            // Odpinamy zdarzenia podczas niszczenia obiektu
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.EditRecordHandler(OnEdit));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.AddHandler(OnAdd));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.CancelHandler(OnCancel));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.SaveHandler(OnSave));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.PreviewRecordHandler(OnPreview));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.ClearHandler(OnClear));

            // ZAWSZE! trzeba odpinać loader/disposer zdarzeń dla danego obiektu
            Core.ServiceManager.Events.Disposer -= new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader -= new Core.EventCollection.InitializeEvents(BindEvents);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Właściwość realizująca dostęp do kontrolki
        /// </summary>
        public IEditorView View
        {
            get
            {
                return _view;
            }

            set
            {
                _view = value;
            }
        }

        /// <summary>
        /// Identyfikator rekordu
        /// </summary>
        public Int32 ID { get; protected set; }

        /// <summary>
        /// Prezenter dziedziczący po tej klasie może z różnych powodów wymuszać wyłączenie kontrolki.
        /// Ustawienie flagi na true zapewnia, że kontrolka nie będzie umożliwiała edycji danych.
        /// </summary>
        protected bool ForceOffView { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Referencja do kontrolki widoku
        /// </summary>
        protected IEditorView _view;

        protected string _question = Properties.ApplicationMessages.Default.msgCancelConfirmation;
        protected string _caption = Properties.ApplicationMessages.Default.msgConfirmCaption;

        #endregion

        #region Abstract Methods - wykorzystanie wzorca Template Method

        /// <summary>
        /// Metoda obsługująca rekord przeznaczony do edycji
        /// </summary>
        /// <param name="record">Rekord do edycji</param>
        abstract public void RecordReceived(EventArgs.RecordEventArgs record);

        /// <summary>
        /// Metoda obsługująca zapis zmian do bazy danych
        /// </summary>
        abstract public void SaveChanges();

        #endregion

        #region IEditorPresenter

        public virtual void OnEdit(object sender, EventArgs.RecordEventArgs record)
        {
            if (_view.IsEnabled && _view.ShowQuestion(_question, _caption) == false)
                return;

            Reset();
            // Delegacja do właściwej metody obsługującej rekord
            RecordReceived(record);

            ActivateView();
        }

        public virtual void OnAdd(object sender)
        {
            if (_view.IsEnabled && _view.ShowQuestion(_question, _caption) == false)
                return;

            Reset();
            ActivateView();
        }

        public virtual void OnSave(object sender)
        {
            if(this.GetType().Equals(sender.GetType()))
                SaveChanges();
        }

        public virtual void OnCancel(object sender)
        {
            if (this.GetType().Equals(sender.GetType()) == false)
                return;

            Reset();
            Core.ServiceManager.Events.OnRefresh(this);
        }

        public virtual void OnPreview(object sender, EventArgs.RecordEventArgs record)
        {

            if (_view.IsEnabled && _view.ShowQuestion(_question, _caption) == false)
                return;

            Reset();
            // Delegacja do właściwej metody obsługującej rekord
            RecordReceived(record);
        }

        public virtual void Validate()
        {
            throw new NotImplementedException();
        }

        public virtual void Reset()
        {
            ForceOffView = false;
            _view.Enable = false;
            ID = 0;
        }

        #endregion

        /// <summary>
        /// Metoda odpowiedzialna za aktywację widoku
        /// </summary>
        private void ActivateView()
        {
            if (ForceOffView)
                _view.Enable = false;
            else
                _view.Enable = true;
        }


        public void OnOpenPopupSearch(object sender)
        {
            if (this.GetType().Equals(sender.GetType()))
            {
                try
                {
                    var obj = _view as IOpenPopupSearchEvent;
                    obj.OnOpenPopupSearch(sender);
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// Delegacja do właściwej metody czyszczącej filtry
        /// </summary>
        public virtual void ClearSearchFilters()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Metoda reaguje na zdarzenie "Wyczyść filtrowanie" z grida.
        /// </summary>
        /// <param name="sender"></param>
        public void OnClear(object sender)
        {
            if (sender.GetType().BaseType.Name.Contains("DataPresenter"))
                ClearSearchFilters();
        }
    }
}
