﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Events;

namespace AppLogic.Presenters.Popup
{
    /// <summary>
    /// Klasa adoptująca logikę do działania z widokiem wyskakującego okienka.
    /// Przypina metody obsługujące zdarzenia.
    /// </summary>
    public class EditorPresenterToPopupAdapter : IPopupPresenter, 
        ISaveSucceed, ISaveEvent, ICancelEvent
    {
        #region Fields

        /// <summary>
        /// Zdarzenie przeładowania danych w liście rozwijanej
        /// </summary>
        public event Core.EventCollection.ReloadDropdownListHandler OnReload;

        /// <summary>
        /// Logika kontrolki
        /// </summary>
        IEditorPresenter _presenter;

        /// <summary>
        /// Widok kontrolki
        /// </summary>
        IPopupEditorView _view;

        #endregion

        #region IPopupPresenterAdapter

        /// <summary>
        /// Metoda inicjująca adapter logiki
        /// Uwaga: przed wywołaniem tej metody należy ustawić właściwość <see cref="Presenter"/> bo poleci wyjątek.
        /// </summary>
        public void Initialize()
        {
            
            // Wywołanie na sztywno zdarzenia dodawania - załaduje potrzebne dane i aktywuje kontrolke
            _presenter.OnAdd(this);

            Core.ServiceManager.Events.Disposer += new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader += new Core.EventCollection.InitializeEvents(BindEvents);

            Core.ServiceManager.Events.Init();
        }

        public void BindEvents()
        {
            // Podpinanie zdarzeń do obsługi
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.CancelHandler(OnCancel));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.SaveHandler(OnSave));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.SaveSucceedHandler(OnSaveSucceed));
        }

        public void UnbindEvents()
        {
            // Odpinamy zdarzenia podczas niszczenia obiektu
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.CancelHandler(OnCancel));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.SaveHandler(OnSave));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.SaveSucceedHandler(OnSaveSucceed));

            // ZAWSZE! trzeba odpinać loader/disposer zdarzeń dla danego obiektu
            Core.ServiceManager.Events.Disposer -= new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader -= new Core.EventCollection.InitializeEvents(BindEvents);
        }

        /// <summary>
        /// Pobiera / ustawia logikę dla kontrolki wyświetlonej w dodatkowym okienku
        /// </summary>
        public IEditorPresenter Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                _presenter = value;
            }
        }

        /// <summary>
        /// Pobiera / ustawia widok kontrolki, który jest wyświetlony w dodatkowym okienku
        /// </summary>
        public IPopupEditorView View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        /// <summary>
        /// Przechwycenie zdarzenia naciśnięcia przycisku "Zapisz"
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        public void OnSave(object sender)
        {
            // Blokujemy dane w kontrolce, żeby nie zostały wyczyszczone przez logikę po zapisie.
            View.HoldData = true;
            Presenter.OnSave(sender);
        }

        /// <summary>
        /// Przechwycenie zdarzenia naciśnięcia przycisku "Anuluj"
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        public void OnCancel(object sender)
        {
            Presenter.Reset();
            Core.ServiceManager.Events.OnPopupClose(this);
        }

        /// <summary>
        /// Przechwycenie zdarzenia poprawnego zapisania danych
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        /// <param name="record">Obiekt z typem rekordu, jaki został zapisany do bazy</param>
        public void OnSaveSucceed(object sender, EventArgs.RecordEventArgs record)
        {
            int id;
            try
            {
                id = View.CategoryId ?? 0;
            }
            catch (Exception)
            {
                id = 0;
            }
            // Zgłaszamy żądanie przeładowania listy
            if(OnReload != null)
                OnReload(sender, new KeyValuePair<int, string>(id, View.Value));

            Core.ServiceManager.Events.OnPopupClose(this);
        }

        #endregion

    }
}
