﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;
using AppLogic.Interfaces.Events;

namespace AppLogic.Presenters.Popup
{
    public class ChangePasswordPresenter : IPopupPresenter,
        ISaveEvent, ICancelEvent
    {

        IChangePasswordView _view;

        public ChangePasswordPresenter(IChangePasswordView view)
        {
            _view = view;
        }

        #region IPopupPresenter

        public void Initialize()
        {
            Core.ServiceManager.Events.Disposer += new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader += new Core.EventCollection.InitializeEvents(BindEvents);

            Core.ServiceManager.Events.Init();
        }

        public void BindEvents()
        {
            // Podpinanie zdarzeń do obsługi
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.CancelHandler(OnCancel));
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.SaveHandler(OnSave));
        }

        public void UnbindEvents()
        {
            // Odpinamy zdarzenia podczas niszczenia obiektu
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.CancelHandler(OnCancel));
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.SaveHandler(OnSave));

            // ZAWSZE! trzeba odpinać loader/disposer zdarzeń dla danego obiektu
            Core.ServiceManager.Events.Disposer -= new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader -= new Core.EventCollection.InitializeEvents(BindEvents);
        }

        public void OnSave(object sender)
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pytanie o potwierdzenie
                if (_view.ShowQuestion(Properties.ApplicationMessages.Default.msgPasswordChangingQuestion, Properties.ApplicationMessages.Default.msgConfirmCaption) == false)
                    return;
                
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Pobranie rekordu użytkownika
                var user = (from u in db.Employees where u.EmployeeId == Core.LoggedUser.Instance.User.EmployeeId select u).Single();

                // Wpisujemy nowe hasło
                user.Password = Core.Crypt.GetMD5Hash(_view.NewPassword);

                // Zalogowany user z nowym hasłem
                Core.LoggedUser.Instance.User = user;

                // Zapisujemy zmiany
                db.SaveChanges();

                // Komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgPasswordChangingSucceed);

                // Zamykamy okienko
                Core.ServiceManager.Events.OnPopupClose(this);

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
        }

        public void OnCancel(object sender)
        {
            Core.ServiceManager.Events.OnPopupClose(this);
        }

        #endregion IPopupPresenter

        /// <summary>
        /// Walidacja danych
        /// </summary>
        private void Validate()
        {
            string msg = null;

            // Należy podać bieżące hasło
            if (String.IsNullOrEmpty(_view.ActualPassword) == true)
                msg += Properties.ValidationMessages.Default.emptyActualPassword + "\n";

            // Bieżące hasło musi być prawidłowe
            else if(Core.LoggedUser.Instance.User.Password != Core.Crypt.GetMD5Hash(_view.ActualPassword))
                msg += Properties.ValidationMessages.Default.incorrectActualPassword + "\n";

            // Należy podać bieżące hasło
            if (String.IsNullOrEmpty(_view.NewPassword) == true)
                msg += Properties.ValidationMessages.Default.emptyNewPassword + "\n";

            // Należy powtórzyć hasło
            if (String.IsNullOrEmpty(_view.RepeatPassword) == true)
                msg += Properties.ValidationMessages.Default.emptyRepeatPassword + "\n";

            // Hasła muszą być identyczne
            if (_view.NewPassword != _view.RepeatPassword)
                msg += Properties.ValidationMessages.Default.passwordsNotExact + "\n";

            // Hasło musi mieć odpowiednią długość
            if (_view.NewPassword.Length < Properties.Settings.Default.PasswordMinLength)
                msg += String.Format(Properties.ValidationMessages.Default.tooShortPassword, Properties.Settings.Default.PasswordMinLength) + "\n";

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));

        }
    }
}
