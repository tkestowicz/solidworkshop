﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups.Search;
using AppLogic.Interfaces;
using AppLogic.Presenters.Control;
using AppLogic.Interfaces.Search;

namespace AppLogic.Presenters.Popup.Search
{
    /// <summary>
    /// Logika wyszukiwania dla samochodu
    /// </summary>
    public class CarSearchPresenter : ISearchPresenter<ICarSearchView>
    {
        ICarSearchView _view;

        public CarSearchPresenter(ICarSearchView view)
        {
            _view = view;
        }

        public ICarSearchView View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        public void Initialize()
        {
            View.Brands = CarControlPresenter.GetBrands();
            View.Colors = CarControlPresenter.GetColors();
        }

        public void BindEvents()
        {
            // tutaj mogą być podpięte dodatkowe, specyficzne zdarzenia dla wyszukiwania klienta
        }

        public void UnbindEvents()
        {
            // tutaj mogą być odpiete dodatkowe zdarzenia
        }

        public void OnSearch(object sender)
        {
            Core.ServiceManager.Events.OnFilterRecords(this, _view.SearchParameters);
        }

        public void OnClear(object sender)
        {
            _view.Reset();
        }

        public void LoadModelsByBrand(int brandId)
        {
            // Reset aktualnie wybranego modelu
            _view.ModelId = 0;
            _view.Models = new List<Model.Projection.ComboBox>();

            // Szukamy modeli tylko w przypadku podania marki
            if (brandId <= 0) return;

            View.Models = CarControlPresenter.GetModelsByBrand(brandId);
        }
    }
}
