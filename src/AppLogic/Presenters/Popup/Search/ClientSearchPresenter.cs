﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups.Search;
using AppLogic.Interfaces;
using AppLogic.Presenters.Control;

namespace AppLogic.Presenters.Popup.Search
{
    /// <summary>
    /// Logika wyszukiwania dla klienta
    /// </summary>
    public class ClientSearchPresenter : ISearchPresenter<IClientSearchView>
    {
        IClientSearchView _view;

        public ClientSearchPresenter(IClientSearchView view)
        {
            _view = view;
        }

        public IClientSearchView View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        public void Initialize()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var provinces = from p in db.Provinces
                            orderby p.ProvinceId
                            select new Model.Projection.ComboBox
                            {
                                Id = p.ProvinceId,
                                Value = p.Name
                            };

            if (provinces.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgCarBrandsBeforeModels);

            View.Provinces = provinces.ToList();
        }

        public void BindEvents()
        {
            // tutaj mogą być podpięte dodatkowe, specyficzne zdarzenia dla wyszukiwania klienta
        }

        public void UnbindEvents()
        {
            // tutaj mogą być odpiete dodatkowe zdarzenia
        }

        public void OnSearch(object sender)
        {
            Core.ServiceManager.Events.OnFilterRecords(this, _view.SearchParameters);
        }

        public void OnClear(object sender)
        {
            _view.Reset();
        }

        public void LoadCitiesByProvince(string province)
        {
            // Reset aktualnie wybranego miasta
            _view.CityId = 0;
            _view.Cities = new List<Model.Projection.ComboBox>();

            // Szukamy miast tylko w przypadku podania województwa
            if (province == null) return;

            _view.Cities = AddressControlPresenter.GetCitiesByProvince(province);
        }
    }
}
