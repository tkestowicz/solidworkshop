﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups.Search;
using AppLogic.Interfaces;
using AppLogic.Presenters.Control;
using AppLogic.Interfaces.Search;

namespace AppLogic.Presenters.Popup.Search
{
    /// <summary>
    /// Logika wyszukiwania dla magazynu
    /// </summary>
    public class StoreSearchPresenter : ISearchPresenter<IStoreSearchView>
    {
        IStoreSearchView _view;

        public StoreSearchPresenter(IStoreSearchView view)
        {
            _view = view;
        }

        public IStoreSearchView View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        public void Initialize()
        {
            var db = Core.ServiceManager.Database;
            View.PartClasses = (from o in db.PartClasses
                                orderby o.Name ascending
                               select new Model.Projection.ComboBox
                               {
                                   Id = o.PartClassId,
                                   Value = o.Name
                               }).ToList();
        }

        public void BindEvents()
        {
            // tutaj mogą być podpięte dodatkowe, specyficzne zdarzenia dla wyszukiwania klienta
        }

        public void UnbindEvents()
        {
            // tutaj mogą być odpiete dodatkowe zdarzenia
        }

        public void OnSearch(object sender)
        {
            Core.ServiceManager.Events.OnFilterRecords(this, _view.SearchParameters);
        }

        public void OnClear(object sender)
        {
            _view.Reset();
        }
    }
}
