﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups.Search;
using AppLogic.Interfaces;
using AppLogic.Presenters.Control;
using AppLogic.Interfaces.Search;

namespace AppLogic.Presenters.Popup.Search
{
    /// <summary>
    /// Logika wyszukiwania dla zleceń
    /// </summary>
    public class OrderSearchPresenter : ISearchPresenter<IOrderSearchView>
    {
        IOrderSearchView _view;

        public OrderSearchPresenter(IOrderSearchView view)
        {
            _view = view;
        }

        public IOrderSearchView View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        public void Initialize()
        {
            View.States = GetStates();
            View.Supervisors = GetSupervisors();
        }

        public void BindEvents()
        {
            // tutaj mogą być podpięte dodatkowe, specyficzne zdarzenia dla wyszukiwania klienta
        }

        public void UnbindEvents()
        {
            // tutaj mogą być odpiete dodatkowe zdarzenia
        }

        public void OnSearch(object sender)
        {
            Core.ServiceManager.Events.OnFilterRecords(this, _view.SearchParameters);
        }

        public void OnClear(object sender)
        {
            _view.Reset();
        }

        /// <summary>
        /// Metoda pobiera z bazy statusy zleceń i zwraca w postaci listy
        /// </summary>
        /// <returns>Lista statusów zleceń</returns>
        public static List<Model.Projection.ComboBox> GetStates()
        {
            var db = Core.ServiceManager.Database;

            return (from o in db.OrderStatuses
                    select new Model.Projection.ComboBox
                    {
                        Id = o.OrderStatusId,
                        Value = o.OrderStatusName
                    }).ToList();
        }

        /// <summary>
        /// Pobiera z bazy nazdorców zleceń i zwraca w postaci listy
        /// </summary>
        /// <returns>Lista statusów zleceń</returns>
        public static List<Model.Projection.ComboBox> GetSupervisors()
        {
            var db = Core.ServiceManager.Database;

            return (from o in db.Employees
                    
                    orderby o.Surname, o.Name
                    select new Model.Projection.ComboBox
                    {
                        Id = o.EmployeeId,
                        Value = o.Groups.FirstOrDefault().Name + ": " + o.Surname + " " + o.Name
                    }).OrderBy(o => o.Value).ToList();
        }
    }
}
