﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{
    public class EmployeePresenter : EditorPresenter, IRelationalEditorPresenter
    {
        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IEmployeeEditorView _View
        {
            get
            {
                return _view as IEmployeeEditorView;
            }
        }
        
        /// <summary>
        /// Właściwość przechowuje stary login
        /// </summary>
        protected string OldLogin { get; set; }

        #endregion

        public override void ClearSearchFilters()
        {
            _View.ClearSearchFilters();
        }

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                var employeeRec = record.Record<Model.Projection.Employee>();
                ID = employeeRec.Id;
                _View.EmployeeName = employeeRec.Name;
                _View.EmployeeSurname = employeeRec.Surname;
                _View.EmployeePesel = employeeRec.Pesel;
                _View.EmployeePhoneNumber = employeeRec.PhoneNumber;
                _View.EmployeeMail = employeeRec.Email;
                _View.EmployeeNIP = employeeRec.Nip;

                // Pobieramy dane pracownika z bazy
                var employee = (from e in db.Employees where e.EmployeeId == employeeRec.Id select e).First();

                // Uzupełniamy dane adresowe i resztę danych
                _View.EmployeeAddress.ProvinceId = employee.Address.City.Province.ProvinceId;
                _View.EmployeeAddress.Province = employee.Address.City.Province.Name;
                _View.EmployeeAddress.CityId = employee.Address.City.CityId;
                _View.EmployeeAddress.Postcode = employee.Address.PostCode;
                _View.EmployeeAddress.HouseNumber = employee.Address.HouseNumber;
                _View.EmployeeAddress.ApartmentNumber = employee.Address.ApartmentNumber;
                _View.EmployeeAddress.Street = employee.Address.Street.Name;
                _View.EmployeeQualifications = employee.Qualifications;
                _View.EmployeeHireDate = employee.HireDate;
                _View.EmployeeReleaseDate = employee.ReleaseDate;
                _View.EmployeeGroupId = employee.Groups.Single().GroupId;
                _View.EmployeeLogin = OldLogin = employee.Login;
               
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                var group = (from g in db.Groups where g.GroupId == _View.EmployeeGroupId select g).Single();

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.Employee()
                    {
                        Name = _View.EmployeeName,
                        Surname = _View.EmployeeSurname,
                        EmployeeStateId = _View.EmployeeStateId,
                        Pesel = _View.EmployeePesel,
                        Nip = _View.EmployeeNIP,
                        HireDate = _View.EmployeeHireDate ?? DateTime.Now.Date,
                        ReleaseDate = _View.EmployeeReleaseDate,
                        Qualifications = _View.EmployeeQualifications,
                        PhoneNumber = _View.EmployeePhoneNumber,
                        Email = _View.EmployeeMail,
                        Login = _View.EmployeeLogin,
                        Password = Core.Crypt.GetMD5Hash(_View.EmployeePassword),

                        Address = new Model.Address
                        {
                            CityCityId = _View.EmployeeAddress.CityId,
                            Street = new Model.Street{
                                Name = _View.EmployeeAddress.Street
                            },
                            PostCode = _View.EmployeeAddress.Postcode,
                            HouseNumber = _View.EmployeeAddress.HouseNumber,
                            ApartmentNumber = _View.EmployeeAddress.ApartmentNumber
                        }
                    };

                    record.Groups.Add(group);

                    db.Employees.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Employees where o.EmployeeId == ID select o).Single();

                        record.Name = _View.EmployeeName;
                        record.Surname = _View.EmployeeSurname;
                        record.EmployeeId = _View.EmployeeStateId;
                        record.Pesel = _View.EmployeePesel;
                        record.Nip = _View.EmployeeNIP;
                        record.HireDate = (DateTime)_View.EmployeeHireDate;
                        record.Qualifications = _View.EmployeeQualifications;
                        record.ReleaseDate = _View.EmployeeReleaseDate;
                        record.PhoneNumber = _View.EmployeePhoneNumber;
                        record.Email = _View.EmployeeMail;
                        record.Address.CityCityId = _View.EmployeeAddress.CityId;
                        record.Address.Street.Name = _View.EmployeeAddress.Street;
                        record.Address.PostCode = _View.EmployeeAddress.Postcode;
                        record.Address.HouseNumber = _View.EmployeeAddress.HouseNumber;
                        record.Address.ApartmentNumber = _View.EmployeeAddress.ApartmentNumber;

                        if(record.Login != OldLogin)
                            record.Login = _View.EmployeeLogin;

                        if(String.IsNullOrEmpty(_View.EmployeePassword) == false)
                        record.Password = Core.Crypt.GetMD5Hash(_View.EmployeePassword);

                        record.Groups.Clear();
                        record.Groups.Add(group);

                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.Employee() { }.GetType()));

                

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
        }

        #region IRelationalEditorPresenter Members

        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            string msg = null;
                

            // Pobranie grup użytowników z bazy
            var groups = (from g in db.Groups
                            orderby g.Name
                            select new Model.Projection.ComboBox
                        {
                            Id = g.GroupId,
                            Value = g.Name
                        }).ToList();

            if (groups.Count() == 0)
                msg += Properties.ApplicationMessages.Default.tabGroups + "\n";

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new Core.Exceptions.DataNotFoundException(msg);

            _View.EmployeeGroups = groups;

            // Pobieramy stany pracownika
            // Pobranie uchwytu do bd
            var states = from p in db.EmployeeStates
                            orderby p.EmployeeStateId
                            select new Model.Projection.ComboBox
                            {
                                Id = p.EmployeeStateId,
                                Value = p.Name
                            };

            if (states.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgCarBrandsBeforeModels);

            _View.EmployeeStates = states.ToList();
        }

        #endregion

        public override void Reset()
        {
            try
            {
                _View.EmployeeName = null;
                _View.EmployeeSurname = null;
                _View.EmployeeStateId = 0;
                _View.EmployeePesel = null;
                _View.EmployeeNIP = null;
                _View.EmployeeHireDate = null;
                _View.EmployeeQualifications = null;
                _View.EmployeeReleaseDate = null;
                _View.EmployeePhoneNumber = null;
                _View.EmployeeGroups = new List<Model.Projection.ComboBox>();
                _View.EmployeeMail = null;
                _View.EmployeeGroupId = 0;
                _View.EmployeeAddress.Province = null;
                _View.EmployeeAddress.CityId = 0;
                _View.EmployeeAddress.ApartmentNumber = null;
                _View.EmployeeAddress.HouseNumber = null;
                _View.EmployeeAddress.Postcode = null;
                _View.EmployeeAddress.Street = null;
                _View.EmployeeLogin = null;
                _View.EmployeePassword = null;
                _View.EmployeeRepeatPassword = null;

                base.Reset();

                // Próbujemy załadować kategorie części
                LoadRelatedData();

            }// Brak kategorii w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(String.Format(Properties.ApplicationMessages.Default.msgEmptyTables, e.Message), Properties.ApplicationMessages.Default.msgEmptyTablesErrorCaption);
                ForceOffView = true;
            }
        }


        public override void Validate()
        {
            string msg = null;

            // Imię nie może być puste
            if (String.IsNullOrEmpty(_View.EmployeeName))
                msg += Properties.ValidationMessages.Default.emptyName + "\n";

            // Nazwisko nie może być puste
            if (String.IsNullOrEmpty(_View.EmployeeSurname))
                msg += Properties.ValidationMessages.Default.emptySurname + "\n";

            // Pesel nie może być pusty
            if (String.IsNullOrEmpty(_View.EmployeePesel))
                msg += Properties.ValidationMessages.Default.emptyPesel + "\n";

            // Weryfikacja poprawności numeru
            else if (Core.Utils.PersonalDataValidator.IsPeselValid(_View.EmployeePesel) == false)
                msg += Properties.ValidationMessages.Default.incorrectPesel + "\n";

            // Jeżeli podano email weryfikujemy poprawność
            if (String.IsNullOrEmpty(_View.EmployeeMail) == false && Core.Utils.PersonalDataValidator.IsEmailValid(_View.EmployeeMail) == false)
                msg += Properties.ValidationMessages.Default.incorrectEmail + "\n";

            // Województwo musi być wybrane
            if (String.IsNullOrEmpty(_View.EmployeeAddress.Province))
                msg += Properties.ValidationMessages.Default.emptyProvince + "\n";

            // Miasto musi być wybrane
            if (_View.EmployeeAddress.CityId <= 0)
                msg += Properties.ValidationMessages.Default.emptyCity + "\n";

            // Ulica musi być podana
            if (String.IsNullOrEmpty(_View.EmployeeAddress.Street))
                msg += Properties.ValidationMessages.Default.emptyStreet + "\n";

            // Kod pocztowy musi być podany
            if (String.IsNullOrEmpty(_View.EmployeeAddress.Postcode))
                msg += Properties.ValidationMessages.Default.emptyPostcode + "\n";

            // Grupa musi być wybrana
            if (_View.EmployeeGroupId <= 0)
                msg += Properties.ValidationMessages.Default.emptyUserGroup + "\n";

            // Status zatrudnienia musi być wybrany
            if (_View.EmployeeStateId == 0)
                msg += Properties.ValidationMessages.Default.emptyEmploymentState + "\n";

            if (_View.EmployeeReleaseDate != null)
            {
                // Data zatrudnienia nie może być późniejsza niż zwolnienia
                if(_View.EmployeeHireDate.Value.CompareTo(_View.EmployeeReleaseDate) > 0)
                    msg += Properties.ValidationMessages.Default.incorrectHireDate + "\n";
            }

            // Jeżeli zmienił się login sprawdzamy, czy jest unikalny
            if (ID == 0 || OldLogin != _View.EmployeeLogin)
            {

                // Należy podać login
                if(String.IsNullOrEmpty(_View.EmployeeLogin) == true)
                    msg += Properties.ValidationMessages.Default.emptyLogin + "\n";

                // Login musi być unikalny
                else if(Core.Utils.PersonalDataValidator.IsLoginUnique(_View.EmployeeLogin) == false)
                    msg += String.Format(Properties.ValidationMessages.Default.notUniqueLogin, _View.EmployeeLogin) + "\n";

                // Login musi mieć odpowiednią długość
                else if(_View.EmployeeLogin.Length < Properties.Settings.Default.LoginMinLength)
                    msg += msg += String.Format(Properties.ValidationMessages.Default.tooShortLogin, Properties.Settings.Default.LoginMinLength) + "\n";
            }

            // Jeżeli zmieniamy hasło, bądź dodajemy nowego usera
            if (ID == 0 || String.IsNullOrEmpty(_View.EmployeePassword) == false || String.IsNullOrEmpty(_View.EmployeeRepeatPassword) == false)
            {
                // Należy podać hasło
                if(String.IsNullOrEmpty(_View.EmployeePassword) == true)
                    msg += Properties.ValidationMessages.Default.emptyPassword + "\n";

                // Hasła muszą być identyczne
                else if(_View.EmployeePassword != _View.EmployeeRepeatPassword)
                    msg += Properties.ValidationMessages.Default.passwordsNotExact + "\n";

                // Hasło musi mieć odpowiednią długość
                else if (_View.EmployeePassword.Length < Properties.Settings.Default.PasswordMinLength)
                    msg += String.Format(Properties.ValidationMessages.Default.tooShortPassword, Properties.Settings.Default.PasswordMinLength) + "\n";

            }

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));
        }

        /// <summary>
        /// Metoda generuje losowe hasło
        /// </summary>
        public void GeneratePassword()
        {
            var passwd = Core.Crypt.GeneratePassword(Properties.Settings.Default.PasswordMinLength);

            _View.EmployeePassword = _View.EmployeeRepeatPassword = passwd;
        }

        /// <summary>
        /// Metoda generuje propozycję loginyu jeżeli dodajemy pracownika
        /// </summary>
        public void GenerateLogin()
        {
            if (ID == 0 && String.IsNullOrEmpty(_View.EmployeeName) == false && String.IsNullOrEmpty(_View.EmployeeSurname) == false)
                _View.EmployeeLogin = _View.EmployeeName + '_' + _View.EmployeeSurname;
        }

    }
}
