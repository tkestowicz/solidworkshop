﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{
    public class ClientPresenter : EditorPresenter, IRelationalEditorPresenter
    {
        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IClientEditorView _View
        {
            get
            {
                return _view as IClientEditorView;
            }
        }

        #endregion

        public override void ClearSearchFilters()
        {
            _View.ClearSearchFilters();
        }

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Przechwycenie wysłanego rekordu
                var clientRec = record.Record<Model.Projection.Client>();

                // Ustawienie podstawowych danych klienta
                ID = clientRec.Id;
                _View.ClientName = clientRec.Name;
                _View.ClientSurname = clientRec.Surname;
                _View.ClientPesel = clientRec.Pesel;
                _View.ClientPhoneNumber = clientRec.PhoneNumber;
                _View.ClientMail = clientRec.Email;

                // Pobieramy wszystkie dane klienta z bazy
                var client = (from c in db.Clients where c.ClientId == clientRec.Id select c).First();

                // Uzupełniamy dane adresowe
                _View.ClientAddress.Province = client.Address.City.Province.Name;
                _View.ClientAddress.CityId = client.Address.City.CityId;
                _View.ClientAddress.Postcode = client.Address.PostCode;
                _View.ClientAddress.HouseNumber = client.Address.HouseNumber;
                _View.ClientAddress.ApartmentNumber = client.Address.ApartmentNumber;
                _View.ClientAddress.Street = client.Address.Street.Name;

                List<Model.Interfaces.ICar> cars = new List<Model.Interfaces.ICar>();
                // Dodanie samochodów klienta do kontrolki
                foreach (Model.Car car in client.Cars)
                {
                    cars.Add(new Model.Projection.Car
                    {
                        BodyNumber = car.BodyNumber,
                        BrandId = car.CarModels.CarBrand.CarBrandId,
                        ColorId = car.Colors.ColorId,
                        FuelId = car.Fuels.FuelId,
                        ModelId = car.CarModels.CarModelId,
                        EngineCapacity = car.EngineCapacity,
                        ProductionYear = car.ProductionYear,
                        RegistrationNumber = car.RegistrationNumber
                    });
                }
                _View.ClientCars = cars;

                // Wyświetlenie zniżki
                if(client.Discounts != null)
                    _View.ClientDiscountId = client.Discounts.DiscountId;

                

            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void Validate()
        {
            string msg = null;

            // Imię nie może być puste
            if (String.IsNullOrEmpty(_View.ClientName))
                msg += Properties.ValidationMessages.Default.emptyName + "\n";

            // Nazwisko nie może być puste
            if (String.IsNullOrEmpty(_View.ClientSurname))
                msg += Properties.ValidationMessages.Default.emptySurname + "\n";

            // Pesel nie może być pusty
            if (String.IsNullOrEmpty(_View.ClientPesel))
                msg += Properties.ValidationMessages.Default.emptyPesel + "\n";

            // Weryfikacja poprawności numeru
            else if (Core.Utils.PersonalDataValidator.IsPeselValid(_View.ClientPesel) == false)
                msg += Properties.ValidationMessages.Default.incorrectPesel + "\n";

            // Jeżeli podano email weryfikujemy poprawność
            if(String.IsNullOrEmpty(_View.ClientMail) == false && Core.Utils.PersonalDataValidator.IsEmailValid(_View.ClientMail) == false)
                msg += Properties.ValidationMessages.Default.incorrectEmail + "\n";

            // Województwo musi być wybrane
            if (String.IsNullOrEmpty(_View.ClientAddress.Province))
                msg += Properties.ValidationMessages.Default.emptyProvince + "\n";

            // Miasto musi być wybrane
            if (_View.ClientAddress.CityId <= 0)
                msg += Properties.ValidationMessages.Default.emptyCity + "\n";

            // Ulica musi być podana
            if (String.IsNullOrEmpty(_View.ClientAddress.Street))
                msg += Properties.ValidationMessages.Default.emptyStreet + "\n";

            // Kod pocztowy musi być podany
            if (String.IsNullOrEmpty(_View.ClientAddress.Postcode))
                msg += Properties.ValidationMessages.Default.emptyPostcode + "\n";

            int i = 1;
            string template = Properties.ValidationMessages.Default.carErrorTemplate;
            // Sprawdzamy dane każdego samochodu
            foreach (Model.Interfaces.ICar car in _View.ClientCars)
            {

                // Marka musi być wybrana
                if (car.BrandId <= 0)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyCarBrand + "\n");

                // Model musi być wybrany
                if (car.ModelId <= 0)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyCarModel + "\n");

                // Kolor musi być wybrany
                if (car.ColorId <= 0)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyCarColor + "\n");

                // Paliwo musi być wybrane
                if (car.FuelId <= 0)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyCarFuel + "\n");

                // Numer rejestracyjny musi być podany
                if (String.IsNullOrEmpty(car.RegistrationNumber))
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyRegistrationNumber + "\n");

                // Numer nadwozia musi być podany
                if (String.IsNullOrEmpty(car.BodyNumber))
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyBodyNumber + "\n");
                
                // Weryfikujemy poprawność numeru
                else if(Core.Utils.CarDataValidator.IsVinValid(car.BodyNumber) == false)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.incorrectVIN + "\n");

                // Rok produkcji musi być podany
                if (car.ProductionYear == null)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyProductionYear + "\n");

                // Rok produkcji musi być w odpowiednim przedziale
                else if (Core.Utils.CarDataValidator.IsProductionYearValid(Properties.Settings.Default.ProductionYearFrom, (int)car.ProductionYear) == false)
                    msg += String.Format(template, i, String.Format(Properties.ValidationMessages.Default.incorrectProductionYear, Properties.Settings.Default.ProductionYearFrom, DateTime.Now.Year) + "\n");

                // Pojemność silnika musi być podana
                if (car.EngineCapacity == null)
                    msg += String.Format(template, i, Properties.ValidationMessages.Default.emptyEngineCapacity + "\n");

                i++;
            }

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));
        }

        public override void SaveChanges()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.Client()
                    {
                        Name = _View.ClientName,
                        Surname = _View.ClientSurname,
                        Pesel = _View.ClientPesel,
                        PhoneNumber = _View.ClientPhoneNumber,
                        Email = _View.ClientMail,
                        Address = new Model.Address
                        {
                            CityCityId = _View.ClientAddress.CityId,
                            Street = new Model.Street
                            {
                                Name = _View.ClientAddress.Street
                            },
                            HouseNumber = _View.ClientAddress.HouseNumber,
                            ApartmentNumber = _View.ClientAddress.ApartmentNumber,
                            PostCode = _View.ClientAddress.Postcode
                        }
                    };

                    // Najpierw usuwamy wszystkie samochody
                    record.Cars.Clear();

                    // Dodajemy wszystkie samochody z kontrolki
                    _prepareCarEntities(_View.ClientCars).ForEach(o => { record.Cars.Add(o); });

                    db.Clients.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Clients where o.ClientId == ID select o).Single();

                    record.Name = _View.ClientName;
                    record.Surname = _View.ClientSurname;
                    record.Pesel = _View.ClientPesel;
                    record.PhoneNumber = _View.ClientPhoneNumber;
                    record.Email = _View.ClientMail;

                    record.Address.CityCityId = _View.ClientAddress.CityId;
                    record.Address.PostCode = _View.ClientAddress.Postcode;
                    record.Address.Street.Name = _View.ClientAddress.Street;
                    record.Address.HouseNumber = _View.ClientAddress.HouseNumber;
                    record.Address.ApartmentNumber = _View.ClientAddress.ApartmentNumber;

                    // Najpierw usuwamy wszystkie samochody
                    record.Cars.Clear();
                        
                    // Dodajemy wszystkie samochody z kontrolki
                    _prepareCarEntities(_View.ClientCars).ForEach(o => { record.Cars.Add(o); });

                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.Client() { }.GetType()));

                

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception e)
            {
                var exc = e;
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
        }

        public override void Reset()
        {
            try
            {

                _View.ClientName = null;
                _View.ClientSurname = null;
                _View.ClientDiscountId = 0;
                _View.ClientPesel = null;
                _View.ClientPhoneNumber = null;
                _View.ClientMail = null;
                _View.ClientAddress.Province = null;
                _View.ClientAddress.CityId = 0;
                _View.ClientAddress.Postcode = null;
                _View.ClientAddress.Street = null;
                _View.ClientAddress.HouseNumber = null;
                _View.ClientAddress.ApartmentNumber = null;
                _View.ClientCars = new List<Model.Interfaces.ICar>();
                base.Reset();

                // Próbujemy załadować dane do comboboxów
                LoadRelatedData();

            }// Brak kategorii w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(String.Format(Properties.ApplicationMessages.Default.msgEmptyTables, e.Message), Properties.ApplicationMessages.Default.msgEmptyTablesErrorCaption);
                ForceOffView = true;
            }
        }

        #region IRelationalEditorPresenter Members

        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            // Załadowanie zniżek do widoku
            List<Model.Projection.ComboBox> discounts = new List<Model.Projection.ComboBox>();
            foreach (Model.Discount d in (from c in db.Discounts orderby c.DicountPercent select c))
            {
                discounts.Add(new Model.Projection.ComboBox
                {
                    Id = d.DiscountId,
                    Value = d.DicountPercent.ToString()
                });
            }
            _View.ClientDiscounts = discounts;
            
        }

        #endregion

        /// <summary>
        /// Metoda tworzy encje na podstawie danych przekazanych jako parametr.
        /// </summary>
        /// <param name="cars">Dane samochodów, z których trzeba utworzyć encje.</param>
        /// <returns>Lista encji z danymi samochodów</returns>
        private static List<Model.Car> _prepareCarEntities(List<Model.Interfaces.ICar> cars)
        {
            var list = new List<Model.Car>();

            // Dodajemy wszystkie samochody z kontrolki
            foreach (Model.Interfaces.ICar c in cars)
            {

                var car = new Model.Car
                {
                    RegistrationNumber = c.RegistrationNumber,
                    ProductionYear = c.ProductionYear ?? 0,
                    EngineCapacity = c.EngineCapacity ?? 0,
                    BodyNumber = c.BodyNumber,
                    ColorId = c.ColorId,
                    FuelId = c.FuelId,
                    CarModelId = c.ModelId
                };

                list.Add(car);
            }

            return list;
        }
    }
}
