﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{
    public class GroupPresenter : EditorPresenter
    {
        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IGroupEditorView _View
        {
            get
            {
                return _view as IGroupEditorView;
            }
        }

        /// <summary>
        /// Właściwość przechowuje starą nazwe
        /// </summary>
        protected string OldName { get; set; }

        #endregion

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                var groupRec = record.Record<Model.Projection.Group>();
                ID = groupRec.Id;
                _View.Name = OldName = groupRec.Name;
                _View.Description = groupRec.Description;

                List<Model.Projection.Permission> list = new List<Model.Projection.Permission>();
                
                // Pobieramy grupę z bazy
                var group = (from e in db.Groups where e.GroupId == groupRec.Id select e).First();
                
                // Pobieramy uprawnienia z bazy
                var permissions = (from e in db.Permissions select e);

                foreach (var entry in permissions)
                {
                    Model.Projection.Permission permission = new Model.Projection.Permission();

                    permission.Name = entry.Name;
                    permission.Selected = group.Permissions.Any(p => p.Name == entry.Name);

                    list.Add(permission);
                }

                _View.Permissions = list;

            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void Reset()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            _View.Name = null;
            _View.Description = null;

            List<Model.Projection.Permission> list = new List<Model.Projection.Permission>();

            // Pobieramy uprawnienia z bazy
            var permissions = (from e in db.Permissions select e);

            foreach (var entry in permissions)
            {
                Model.Projection.Permission permission = new Model.Projection.Permission();

                permission.Name = entry.Name;
                permission.Selected = false;

                list.Add(permission);
            }

            _View.Permissions = list;

            base.Reset();
        }

        public override void Validate()
        {
            string msg = null;

            // Nazwa nie może być pusta
            if (String.IsNullOrEmpty(_View.Name))
                msg += Properties.ValidationMessages.Default.emptyGroupName + "\n";

            // Jeżeli zmieniła się nazwa, sprawdzamy czy jest unikalna
            if (ID == 0 || OldName != _View.Name)
            {
                var db = Core.ServiceManager.Database;

                var count = (from o in db.Groups where o.GroupId != ID && o.Name == _View.Name select o).Count();

                if(count > 0)
                    msg += String.Format(Properties.ValidationMessages.Default.notUniqueGroupName, _View.Name) + "\n";

            }

            // sprawdzenie poprawności uprawnień
            // brzydko, bardzo brzydko
            // działa, gdy będzie "czas na poprawki" to wymyśli się coś lepszego
            foreach (var entry in _View.Permissions)
            {
                if (entry.Name.Contains("edycja") && entry.Selected)
                {
                    string temp;
                    temp = entry.Name.Substring(0, entry.Name.IndexOf(':'));
                    temp += ": przeglądanie";
                    if (!_View.Permissions.Any(p => p.Name.Equals(temp) && p.Selected))
                        msg += String.Format(Properties.ValidationMessages.Default.permissionsRequirementNotMeet, entry.Name, temp) + "\n";
                }
            }

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));
        }

        public override void SaveChanges()
        {
            try
            {

                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.Group();

                    record.Name = _View.Name;
                    record.Description = _View.Description;

                    // Pobieramy uprawnienia z bazy
                    var permissions = (from e in db.Permissions select e);

                    foreach (var entry in permissions)
                    {
                        if (_View.Permissions.Any(p => p.Selected && p.Name == entry.Name))
                        {
                            record.Permissions.Add(entry);
                        }
                    }

                    db.Groups.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Groups where o.GroupId == ID select o).Single();

                    record.Name = _View.Name;
                    record.Description = _View.Description;
                    record.Permissions.Clear();

                    // Pobieramy uprawnienia z bazy
                    var permissions = (from e in db.Permissions select e);

                    foreach (var entry in permissions)
                    {
                        if (_View.Permissions.Any(p => p.Selected && p.Name == entry.Name))
                        {
                            record.Permissions.Add(entry);
                        }
                    }
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.Group() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
        }
    }
}
