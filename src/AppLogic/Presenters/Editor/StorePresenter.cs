﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{

    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora
    /// </summary>
    public partial class StorePresenter : EditorPresenter, IRelationalEditorPresenter
    {

        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IStoreEditorView _View
        {
            get
            {
                return _view as IStoreEditorView;
            }
        }

        #endregion

        public override void ClearSearchFilters()
        {
            _View.ClearSearchFilters();
        }

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {

            try
            {
                var rec = record.Record<Model.Projection.Store>();
                ID = rec.Id;
                _View.UnitPrice = rec.UnitPrice;
                _View.PartName = rec.Name;
                _View.PartDescription = rec.Description;
                _View.AvailableAmount = rec.AvailableAmount;
                _View.PartClassName = rec.PartClass; 
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void SaveChanges()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.Store()
                    {
                        Name = _View.PartName,
                        Description = _View.PartDescription,
                        AvailabileAmount = _View.AvailableAmount,
                        UnitPrice = _View.UnitPrice,
                        PartClassPartClassId = (int)_View.PartClassId
                    };
                        
                    db.Stores.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Stores where o.StoreId == ID select o).Single();

                    record.Name = _View.PartName;
                    record.Description = _View.PartDescription;
                    record.AvailabileAmount = _View.AvailableAmount;
                    record.UnitPrice = _View.UnitPrice;
                    record.PartClassPartClassId = (int)_View.PartClassId;

                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.Store() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }


        }

        public override void Validate()
        {

            string msg = null;

            // Nazwa nie może być pusta
            if (String.IsNullOrEmpty(_View.PartName))
                msg += Properties.ValidationMessages.Default.emptyPartName + "\n";

            // Należy wybrać kategorie
            if(_View.PartClassId <= 0)
                msg += Properties.ValidationMessages.Default.emptyPartClass + "\n";

            try
            {
                var value = _View.AvailableAmount;

                if (value < 0)
                    throw new FormatException();

            }// Nieprawidłowy format liczby sztuk
            catch(FormatException)
            {
                msg += Properties.ValidationMessages.Default.incorrectAvailableAmount + "\n";
            }

            try
            {
                var value = _View.UnitPrice;

                if (value < 0)
                    throw new FormatException();

            }// Nieprawidłowy format ceny jednostkowej
            catch (FormatException)
            {
                msg += Properties.ValidationMessages.Default.incorrectUnitPrice + "\n";
            }

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));

        }

        public override void Reset()
        {
            try
            {

                _View.UnitPrice = 0.00;
                _View.PartName = null;
                _View.PartDescription = null;
                _View.AvailableAmount = 0;
                _View.PartClassId = null;
                base.Reset();

                // Próbujemy załadować kategorie części
                LoadRelatedData();

            }// Brak kategorii w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(e.Message, Properties.ApplicationMessages.Default.msgRequiredDataError);
                ForceOffView = true;
            }
        }

        /// <summary>
        /// Metoda ładuje nazwy kategorii do listy rozwijanej
        /// Jeżeli w bazie nie ma kategorii rzucany wyjątek DataNotFoundException
        /// </summary>
        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var classes = from c in db.PartClasses
                            orderby c.Name
                            select new Model.Projection.ComboBox
                            {
                                Id = c.PartClassId,
                                Value = c.Name
                            };

            if (classes.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgEmptyPartClasses);

            _View.PartClasses = classes.ToList();
        }
    }
}
