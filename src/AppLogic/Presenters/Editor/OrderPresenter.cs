﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{
    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora
    /// </summary>
    public partial class OrderPresenter : EditorPresenter, IRelationalEditorPresenter
    {
        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IOrderEditorView _View
        {
            get
            {
                return _view as IOrderEditorView;
            }
        }

        #endregion

        #region IRelationalEditorPresenter Members

        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var statuses = from c in db.ServiceStatuses orderby c.ServiceStatusName select c.ServiceStatusName;

            if (statuses.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgStatusBeforeService);

            _View.OrderStatuses = statuses.ToList<string>();

            var employess = from e in db.Employees orderby e.Name select e.Name;

            if(employess.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgEmployeeBeforeService);

            _View.OrderSupervisores = employess.ToList<string>();
        }

        #endregion

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                var rec = record.Record<Model.Projection.Order>();
                ID = rec.Id;
                _View.OrderStatus = rec.Status;
                _View.Car = rec.Car;
                _View.OrderSupervisor = rec.Employee;
                _View.OrderDescription = rec.Description;
                _View.OrderPrice = rec.Price;
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void Validate()
        {
            string msg = null;

            //TODO funkcja walidacji

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));
        }

        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.Order()
                    {
                        Description = _View.OrderDescription,
                        Price = _View.OrderPrice,

                        OrderStatuses = (from s in db.OrderStatuses where s.OrderStatusName == _View.OrderStatus select s).Single(),

                        //TODO: przemyśleć sposób jednznacznego wyboru rekordu z bazy na podstawie stringa w liście rozwijanej z okna edytora
                        Employees = (from e in db.Employees where e.Name == _View.OrderSupervisor select e).Single(),

                        Cars = (from c in db.Cars where c.CarId == _View.CarId select c).Single()
                    };

                    db.Orders.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Orders where o.OrderId == ID select o).Single();

                    record.Description = _View.OrderDescription;
                    record.Price = _View.OrderPrice;
                        
                    // Pobieramy kategorię do której ma należeć rekord
                    var status = (from s in db.OrderStatuses where s.OrderStatusName == _View.OrderStatus select s).Single();
                    record.OrderStatuses = status;

                    //TODO: przemyśleć sposób jednznacznego wyboru rekordu z bazy na podstawie stringa w liście rozwijanej z okna edytora
                    var supervisor = (from e in db.Employees where e.Name == _View.OrderSupervisor select e).Single();
                    record.Employees = supervisor;

                    record.Cars = (from c in db.Cars where c.CarId == _View.CarId select c).Single();
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.Order() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception e)
            {
                e = null;
                if (e != null)
                    e = null;
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
        }

        public override void Reset()
        {
            try
            {
                _View.OrderStatus = null;
                _View.Car = null;
                _View.OrderSupervisor = null;
                _View.OrderDescription = null;
                _View.OrderPrice = 0.0;
                base.Reset();

                // Próbujemy załadować kategorie części
                LoadRelatedData();

            }// Brak kategorii w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(e.Message, Properties.ApplicationMessages.Default.msgRequiredDataError);
                ForceOffView = true;
            }
        }
    }
}
