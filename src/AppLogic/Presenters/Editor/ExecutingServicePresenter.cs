﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{

    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora
    /// </summary>
    public partial class ExecutingServicePresenterEditor : EditorPresenter, IRelationalEditorPresenter
    {

        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IExecutingServiceEditorView _View
        {
            get
            {
                return _view as IExecutingServiceEditorView;
            }
        }

        #endregion

        public override void ClearSearchFilters()
        {
            _View.ClearSearchFilters();
        }

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {

            try
            {
                var rec = record.Record<Model.Projection.ExecutingService>();
                
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                _View.EmployeeId = rec.EmployeeId;
                _View.OrderId = rec.OrderId;
                _View.StatusesId = rec.StatusId;
                _View.ServiceId = rec.ServiceId;
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void SaveChanges()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.ExecutingService();

                    record.Orders = (from orders in db.Orders where orders.OrderId == _View.OrderId select orders).Single();
                    record.Employees = (from employess in db.Employees where employess.EmployeeId == _View.EmployeeId select employess).Single();
                    record.ServiceStatuses = (from statuses in db.ServiceStatuses where statuses.ServiceStatusId == _View.StatusesId select statuses).Single();
                    record.Services = (from service in db.Services where service.ServiceId == _View.ServiceId select service).Single();

                    db.ExecutingServices.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.ExecutingServices where o.ExecutingServiceId == ID select o).Single();

                    record.Orders = (from orders in db.Orders where orders.OrderId == _View.OrderId select orders).Single();
                    record.Employees = (from employess in db.Employees where employess.EmployeeId == _View.EmployeeId select employess).Single();
                    record.ServiceStatuses = (from statuses in db.ServiceStatuses where statuses.ServiceStatusId == _View.StatusesId select statuses).Single();
                    record.ServiceServiceId = _View.ServiceId;
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.ExecutingService() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception e)
            {
                if (e != null)
                    e = null;
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }


        }

        public override void Validate()
        {

        }

        public override void Reset()
        {
            try
            {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                base.Reset();

                // Próbujemy załadować kategorie części
                LoadRelatedData();

            }// Brak kategorii w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(e.Message, Properties.ApplicationMessages.Default.msgRequiredDataError);
                ForceOffView = true;
            }
        }

        /// <summary>
        /// Metoda ładuje nazwy kategorii do listy rozwijanej
        /// Jeżeli w bazie nie ma kategorii rzucany wyjątek DataNotFoundException
        /// </summary>
        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            _View.EmployeeClasses = (from e in db.Employees
                                     orderby e.Name
                                     select new Model.Projection.ComboBox
                                     {
                                         Id = e.EmployeeId,
                                         Value = e.Name + "," + e.Surname
                                     }
                                     ).ToList();
            if (_View.EmployeeClasses.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgEmployeeBeforeService);

            _View.OrderClasses = (from o in db.Orders
                                  orderby o.Description
                                  select new Model.Projection.ComboBox
                                  {
                                      Id = o.OrderId,
                                      Value = o.Description + "," + o.Cars.BodyNumber
                                  }
                                  ).ToList();
            if (_View.OrderClasses.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgOrderBeforeService);

            _View.StatusesClasses = (from s in db.ServiceStatuses
                                  orderby s.ServiceStatusName
                                  select new Model.Projection.ComboBox
                                  {
                                      Id = s.ServiceStatusId,
                                      Value = s.ServiceStatusName
                                  }
                                  ).ToList();
            if (_View.StatusesClasses.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgStatusBeforeService);

            _View.ServiceClasses = (from s in db.Services
                                     orderby s.Name
                                     select new Model.Projection.ComboBox
                                     {
                                         Id = s.ServiceId,
                                         Value = s.Name + "," + s.Description
                                     }
                                  ).ToList();
            if (_View.ServiceClasses.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgServiceBeforeService);
        }
    }
}
