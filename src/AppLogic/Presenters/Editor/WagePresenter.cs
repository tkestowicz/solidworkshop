﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.Editor
{
    public class WagePresenter : EditorPresenter
    {
        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IWageEditorView _View
        {
            get
            {
                return _view as IWageEditorView;
            }
        }

        #endregion

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                var db = Core.ServiceManager.Database;

                var wageRec = record.Record<Model.Projection.Wage>();
                ID = wageRec.Id;
                _View.Month = wageRec.Month;
                _View.Salary = wageRec.Salary;
                _View.Bonus = wageRec.Bonus;

            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void Validate()
        {
            string msg = null;

            //TODO funkcja walidacji

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));
        }



        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    var record = new Model.Wage()
                    {
                        Month = _View.Month,
                        Salary = _View.Salary,
                        Bonus = _View.Bonus
                    };

                    db.Wages.AddObject(record);
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Wages where o.WageId == ID select o).Single();

                        record.Month = _View.Month;
                        record.Salary = _View.Salary;
                        record.Bonus = _View.Bonus;

                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.Wage() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }
        }

        public override void Reset()
        {
            try
            {

                _View.Month = null;
                _View.Salary = 0.0;
                _View.Bonus = 0.0;
                base.Reset();

            }// Brak kategorii w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(String.Format(Properties.ApplicationMessages.Default.msgEmptyTables, e.Message), Properties.ApplicationMessages.Default.msgEmptyTablesErrorCaption);
            }
        }
    }
}
