﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class StorePresenter : Presenters.DataPresenter, IRelationalDataPresenter
    {
        /// <summary>
        /// Określa, czy zbiór danych jest posortowany
        /// </summary>
        bool _Ordered = false;

        public StorePresenter(IDataGridView view) : base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Store().GetType().Name;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            _Ordered = true;

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "StoreId", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
                case "Description": query = new Core.GenericSorter<T, string>().Sort(query, "Description", Ordering.Direction); break;
                case "AvailableAmount": query = new Core.GenericSorter<T, int>().Sort(query, "AvailabileAmount", Ordering.Direction); break;
                case "UnitPrice": query = new Core.GenericSorter<T, double>().Sort(query, "UnitPrice", Ordering.Direction); break;
                default: _Ordered = false; break;
            }
            
            return query;
        }

        public IQueryable<T> OrderByRelational<T>(IQueryable<T> query)
        {
            if (_Ordered)
                return query;

            switch (Ordering.Column)
            {
                case "PartClass": query = new Core.GenericSorter<T, string>().Sort(query, "PartClass", Ordering.Direction); break;
            }

            return query;
        }

        /// <summary>
        /// Metoda pobiera rekordy i ładuje je do widoku
        /// </summary>
        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = db.Stores.AsQueryable();

            // Jeżeli mamy filtry należy je zastosować
            if (FilterFields.Count > 0)
            {

                foreach (var param in FilterFields)
                {
                    // Bez zmiennej tymczasowej parametry są nadpisywane w każdej iteracji bieżącą wartością
                    object value = param.Value;

                    // Wybrano filtr dla nazwy
                    if (param.Key.Contains("PartName"))
                        query = query.Where(o => o.Name == (string)value);

                    // Wybrano filtr dla kategorii części
                    else if (param.Key.Contains("PartClass"))
                    {
                        var partclass = (Model.Projection.ComboBox)value;
                        query = query.Where(o => o.PartClasses.PartClassId == partclass.Id);
                    }

                    // Wybrano filtr dla liczby dostępnych części
                    else if (param.Key.Contains("AvailableAmountFrom"))
                    {
                        var arg = int.Parse((string)value);
                        query = query.Where(o => o.AvailabileAmount >= arg);
                    }

                    // Wybrano filtr dla liczby dostępnych części
                    else if (param.Key.Contains("AvailableAmountTo"))
                    {
                        var arg = int.Parse((string)value);
                        query = query.Where(o => o.AvailabileAmount <= arg);
                    }

                    // Wybrano filtr dla ceny jednostkowej
                    else if (param.Key.Contains("UnitPriceFrom"))
                    {
                        var arg = double.Parse((string)value);
                        query = query.Where(o => o.UnitPrice >= arg);
                    }

                    // Wybrano filtr dla ceny jednostkowej
                    else if (param.Key.Contains("UnitPriceTo"))
                    {
                        var arg = double.Parse((string)value);
                        query = query.Where(o => o.UnitPrice <= arg);
                    }

                }
            }

            var results = OrderBy(query).Select(o =>
                        new Model.Projection.Store()
                        {
                            Id = o.StoreId,
                            Name = o.Name,
                            Description = o.Description,
                            AvailableAmount = o.AvailabileAmount,
                            UnitPrice = o.UnitPrice,
                            PartClass = o.PartClasses.Name
                        });

                // Jeżeli dane nie są posortowane zostanie doklejone sortowanie po kolumnie z encji relacyjnej
                results = OrderByRelational(results);

            LoadData(results);
        }

        /// <summary>
        /// Usuwanie rekordu
        /// </summary>
        public override void DeleteRecord()
        {
            try
            {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Store;
                var record = (from o in db.Stores where o.StoreId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.Stores.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }
    }
}
