﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class ServicePresenter : Presenters.DataPresenter, IRelationalDataPresenter
    {

        /// <summary>
        /// Określa, czy zbiór danych jest posortowany
        /// </summary>
        bool _Ordered = false;

        public ServicePresenter(IDataGridView view) : base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Service().GetType().Name;
        }

        public IQueryable<T> OrderByRelational<T>(IQueryable<T> query)
        {
            if (_Ordered)
                return query;

            switch (Ordering.Column)
            {
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
            }

            return query;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            _Ordered = true;

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "ServiceId", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
                case "Description": query = new Core.GenericSorter<T, string>().Sort(query, "Description", Ordering.Direction); break;
                case "Price": query = new Core.GenericSorter<T, string>().Sort(query, "Price", Ordering.Direction); break;
                default: _Ordered = false; break;
            }

            return query;
        }

        /// <summary>
        /// Metoda pobiera marki i ładuje je do widoku
        /// </summary>
        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = OrderBy(db.Services).Select(o =>
                        new Model.Projection.Service()
                        {
                            Id = o.ServiceId,
                            Name = o.Name,
                            Description = o.Description,
                            Price = o.Price,
                            ClassName = (from cn in db.ServiceClasses where cn.ServiceClassId == o.ServiceClassServiceClassId select cn).FirstOrDefault().Name
                        });

            // Jeżeli dane nie są posortowane zostanie doklejone sortowanie po kolumnie z encji relacyjnej
            query = OrderByRelational(query);

            LoadData(query);
        }

        /// <summary>
        /// Usuwanie rekordu
        /// </summary>
        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Service;
                var record = (from o in db.Services where o.ServiceId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.Services.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }
    }
}
