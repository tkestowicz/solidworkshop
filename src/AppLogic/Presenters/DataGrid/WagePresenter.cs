﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class WagePresenter : Presenters.DataPresenter, IRelationalDataPresenter
    {
        /// <summary>
        /// Określa, czy zbiór danych jest posortowany
        /// </summary>
        //bool _Ordered = false;

        public WagePresenter(IDataGridView view) : base(view) { }


        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Wage().GetType().Name;
        }

        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = OrderBy(db.Wages).Select(o =>
                        new Model.Projection.Wage()
                        {
                            Id = o.WageId,
                            Month = o.Month,
                            Salary = o.Salary,
                            Bonus = o.Bonus != null ? (double)o.Bonus : 0
                        });
        }
        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Wage;
                var record = (from o in db.Wages where o.WageId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.Wages.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }



        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

           // _Ordered = true;

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "WageId", Ordering.Direction); break;
                case "Month": query = new Core.GenericSorter<T, string>().Sort(query, "Month", Ordering.Direction); break;
                case "Salary": query = new Core.GenericSorter<T, string>().Sort(query, "Salary", Ordering.Direction); break;
                case "Bonus": query = new Core.GenericSorter<T, string>().Sort(query, "Bonus", Ordering.Direction); break;
                default: break;//_Ordered = false; break;
            }

            return query;
        }



        #region IRelationalDataPresenter Members

        public IQueryable<T> OrderByRelational<T>(IQueryable<T> query)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
