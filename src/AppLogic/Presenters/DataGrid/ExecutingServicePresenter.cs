﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class ExecutingServicePresenter : Presenters.DataPresenter, IRelationalDataPresenter
    {
        /// <summary>
        /// Określa, czy zbiór danych jest posortowany
        /// </summary>
        bool _Ordered = false;

        public ExecutingServicePresenter(IDataGridView view) : base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.ExecutingService().GetType().Name;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            _Ordered = true;

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "ExecutingServiceId", Ordering.Direction); break;
                case "EmployeeId": query = new Core.GenericSorter<T, string>().Sort(query, "EmployeeEmployeeId", Ordering.Direction); break;
                case "OrderId": query = new Core.GenericSorter<T, string>().Sort(query, "OrderOrderId", Ordering.Direction); break;
                case "StatusId": query = new Core.GenericSorter<T, int>().Sort(query, "ServiceServiceId", Ordering.Direction); break;
                default: _Ordered = false; break;
            }

            return query;
        }

        public IQueryable<T> OrderByRelational<T>(IQueryable<T> query)
        {
            if (_Ordered)
                return query;

            return query;
        }

        /// <summary>
        /// Metoda pobiera rekordy i ładuje je do widoku
        /// </summary>
        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = db.ExecutingServices.AsQueryable();

            // Jeżeli mamy filtry należy je zastosować
            if (FilterFields.Count > 0)
            {

                foreach (var param in FilterFields)
                {
                    // Bez zmiennej tymczasowej parametry są nadpisywane w każdej iteracji bieżącą wartością
                    object value = param.Value;
                    // TODO filtrowanie
                }
            }

            var results = OrderBy(query).Select(o =>
                        new Model.Projection.ExecutingService()
                        {
                            Id = o.ExecutingServiceId,
                            EmployeeId = o.EmployeeEmployeeId,
                            OrderId = o.OrderOrderId,
                            StatusId = o.ServiceServiceId
                        });

            // Jeżeli dane nie są posortowane zostanie doklejone sortowanie po kolumnie z encji relacyjnej
            results = OrderByRelational(results);

            LoadData(results);
        }

        /// <summary>
        /// Usuwanie rekordu
        /// </summary>
        public override void DeleteRecord()
        {
            try
            {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.ExecutingService;
                var record = (from o in db.ExecutingServices where o.ExecutingServiceId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.ExecutingServices.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }
    }
}
