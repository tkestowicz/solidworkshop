﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class ColorPresenter : Presenters.DataPresenter
    {

        public ColorPresenter(IDataGridView view) : base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Color().GetType().Name;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "ColorId", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "ColorName", Ordering.Direction); break;
            }

            return query;
        }

        /// <summary>
        /// Metoda pobiera kategorie części i ładuje je do widoku
        /// </summary>
        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = OrderBy(db.Colors).Select(o =>
                        new Model.Projection.Color()
                        {
                            Id = o.ColorId,
                            Name = o.ColorName
                        });

            LoadData(query);
        }

        /// <summary>
        /// Usuwanie rekordu
        /// </summary>
        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Color;
                var record = (from o in db.Colors where o.ColorId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.Colors.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }
    }
}
