﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class ClientPresenter : Presenters.DataPresenter
    {

        public ClientPresenter(IDataGridView view) : base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Client().GetType().Name;
        }

        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            // Początek zapytania
            var query = db.Clients.AsQueryable();

            // Jeżeli mamy filtry należy je zastosować
            if (FilterFields.Count > 0)
            {
                bool isCity = false;

                foreach (var param in FilterFields)
                {
                    // Bez zmiennej tymczasowej parametry są nadpisywane w każdej iteracji bieżącą wartością
                    object value = param.Value;

                    // Wybrano filtr dla imienia
                    if (param.Key.Contains("FirstName"))
                        query = query.Where(o => o.Name == (string)value);

                    // Wybrano filtr dla nazwiska
                    else if (param.Key.Contains("Surname"))
                        query = query.Where(o => o.Surname == (string)value);

                    // Wybrano filtr dla peselu
                    else if (param.Key.Contains("Pesel"))
                        query = query.Where(o => o.Pesel == (string)value);

                    // Wybrano filtr dla numeru telefonu
                    else if (param.Key.Contains("Phone"))
                        query = query.Where(o => o.PhoneNumber == (string)value);

                    // Wybrano filtr dla miasta
                    else if (param.Key.Contains("City"))
                    {
                        int cityId = (int)(value as Model.Projection.ComboBox).Id;
                        query = query.Where(o => o.Address.CityCityId == cityId);
                        isCity = true;
                    }

                    // Wybrano filtr dla województwo
                    else if (isCity == false && param.Key.Contains("Province"))
                    {
                        string province = (string)(value as Model.Projection.ComboBox).Value;
                        query = query.Where(o => o.Address.City.Province.Name == province);
                    }

                    // Wybrano filtr dla kodu pocztowego
                    else if (param.Key.Contains("Postcode"))
                        query = query.Where(o => o.Address.PostCode == (string)value);

                    // Wybrano filtr dla ulicy
                    else if (param.Key.Contains("Street"))
                        query = query.Where(o => o.Address.Street.Name == (string)value);

                }
            }

            // Pobieramy i sortujemy wyniki zapytania
            var results = OrderBy(query).Select(o =>
                        new Model.Projection.Client()
                        {
                            Id = o.ClientId,
                            Name = o.Name,
                            Surname = o.Surname,
                            Pesel = o.Pesel,
                            PhoneNumber = o.PhoneNumber,
                            Email = o.Email
                        });

            LoadData(results);
        }

        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Client;
                var record = (from o in db.Clients where o.ClientId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.Clients.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "ClientId", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
                case "Surname": query = new Core.GenericSorter<T, string>().Sort(query, "Surname", Ordering.Direction); break;
                case "Pesel": query = new Core.GenericSorter<T, string>().Sort(query, "Pesel", Ordering.Direction); break;
                case "PhoneNumber": query = new Core.GenericSorter<T, string>().Sort(query, "PhoneNumber", Ordering.Direction); break;
                case "Email": query = new Core.GenericSorter<T, string>().Sort(query, "Email", Ordering.Direction); break;
                default:  break;
            }

            return query;
        }
    }
}
