﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class PartClassPresenter : Presenters.DataPresenter
    {

        public PartClassPresenter(IDataGridView view):base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.PartClass().GetType().Name;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "PartClassId", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
            }

            return query;
        }

        /// <summary>
        /// Metoda pobiera kategorie części i ładuje je do widoku
        /// </summary>
        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = OrderBy(db.PartClasses).Select(o =>
                new Model.Projection.PartClass() { 
                    Id = o.PartClassId,
                    Name = o.Name
                });

            LoadData(query);
        }

        /// <summary>
        /// Usuwanie rekordu
        /// </summary>
        public override void DeleteRecord()
        {
            try
            {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.PartClass;
                var record = (from p in db.PartClasses where p.PartClassId == selectedRecord.Id select p).Single();

                // Usuwamy rekord z kontekstu
                db.PartClasses.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }
    }
}
