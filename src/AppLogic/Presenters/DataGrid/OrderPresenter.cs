﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class OrderPresenter : Presenters.DataPresenter, IRelationalDataPresenter
    {
        /// <summary>
        /// Określa, czy zbiór danych jest posortowany
        /// </summary>
        bool _Ordered = false;

        public OrderPresenter(IDataGridView view) : base(view) { }

        public override void LoadData()
        {
                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                var query = OrderBy(db.Orders).Select(o =>
                            new Model.Projection.Order()
                            {
                                Id = o.OrderId,
                                Description = o.Description,
                                Price = o.Price,
                                Employee = (from e in db.Employees where e.EmployeeId == o.EmployeeEmployeeId select e).FirstOrDefault().Surname + " " +
                                            (from e in db.Employees where e.EmployeeId == o.EmployeeEmployeeId select e).FirstOrDefault().Name,
                                Status = (from os in db.OrderStatuses where os.OrderStatusId == o.OrderStatuses.OrderStatusId select os).FirstOrDefault().OrderStatusName,
                                Car = (from c in db.Cars where c.CarId == o.CarCarId select c).FirstOrDefault().CarModels.CarBrand.BrandName + " " + 
                                        (from c in db.Cars where c.CarId == o.CarCarId select c).FirstOrDefault().CarModels.ModelName + ", " +
                                        (from c in db.Cars where c.CarId == o.CarCarId select c).FirstOrDefault().RegistrationNumber
                            });

                // Jeżeli dane nie są posortowane zostanie doklejone sortowanie po kolumnie z encji relacyjnej
                query = OrderByRelational(query);

                LoadData(query);

        }

        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Order;
                var record = (from o in db.Orders where o.OrderId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                db.Orders.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }// Wszelkie błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Order().GetType().Name;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            _Ordered = true;

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "OrderId", Ordering.Direction); break;
                case "Price": query = new Core.GenericSorter<T, string>().Sort(query, "Price", Ordering.Direction); break;
                case "Description": query = new Core.GenericSorter<T, string>().Sort(query, "Description", Ordering.Direction); break;
                default: _Ordered = false; break;
            }

            return query;
        }

        #region IRelationalDataPresenter Members

        public IQueryable<T> OrderByRelational<T>(IQueryable<T> query)
        {
            if (_Ordered)
                return query;

            switch (Ordering.Column)
            {
                case "Employee": query = new Core.GenericSorter<T, string>().Sort(query, "Employee", Ordering.Direction); break;
                case "Status": query = new Core.GenericSorter<T, string>().Sort(query, "OrderStatus", Ordering.Direction); break;
                case "Car": query = new Core.GenericSorter<T, string>().Sort(query, "Car", Ordering.Direction); break;
            }

            return query;
        }

        #endregion
    }
}
