﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class EmployeePresenter : Presenters.DataPresenter
    {

        public EmployeePresenter(IDataGridView view) : base(view) { }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Employee().GetType().Name;
        }

        /// <summary>
        /// Info: Ciut inna implementacja metody wynika z tego, że jedno z pól trzeba konwertować przed wyświetleniem.
        /// </summary>
        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            // Pobranie wszystkich pracowników
            var query = db.Employees.AsQueryable();

            List<Model.Projection.Employee> list = new List<Model.Projection.Employee>();



            // Jeżeli mamy filtry należy je zastosować
            if (FilterFields.Count > 0)
            {
                bool isCity = false;

                foreach (var param in FilterFields)
                {
                    // Bez zmiennej tymczasowej parametry są nadpisywane w każdej iteracji bieżącą wartością
                    object value = param.Value;

                    // Wybrano filtr dla imienia
                    if (param.Key.Contains("FirstName"))
                        query = query.Where(o => o.Name == (string)value);

                    // Wybrano filtr dla nazwiska
                    else if (param.Key.Contains("Surname"))
                        query = query.Where(o => o.Surname == (string)value);

                    // Wybrano filtr dla peselu
                    else if (param.Key.Contains("Pesel"))
                        query = query.Where(o => o.Pesel == (string)value);

                    // Wybrano filtr dla numeru telefonu
                    else if (param.Key.Contains("Phone"))
                        query = query.Where(o => o.PhoneNumber == (string)value);

                    // Wybrano filtr dla miasta
                    else if (param.Key.Contains("City"))
                    {
                        int cityId = (int)(value as Model.Projection.ComboBox).Id;
                        query = query.Where(o => o.Address.CityCityId == cityId);
                        isCity = true;
                    }

                    // Wybrano filtr dla województwo
                    else if (isCity == false && param.Key.Contains("Province"))
                    {
                        string province = (string)(value as Model.Projection.ComboBox).Value;
                        query = query.Where(o => o.Address.City.Province.Name == province);
                    }

                    // Wybrano filtr dla kodu pocztowego
                    else if (param.Key.Contains("Postcode"))
                        query = query.Where(o => o.Address.PostCode == (string)value);

                    // Wybrano filtr dla ulicy
                    else if (param.Key.Contains("Street"))
                        query = query.Where(o => o.Address.Street.Name == (string)value);

                    // Wybrano filtr dla daty zatrudnienia
                    else if (param.Key.Contains("HireDateFrom"))
                        query = query.Where(o => o.HireDate >= (DateTime)value);

                    // Wybrano filtr dla daty zatrudnienia
                    else if (param.Key.Contains("HireDateTo"))
                        query = query.Where(o => o.HireDate <= (DateTime)value);

                    // Wybrano filtr dla daty zwolnienia
                    else if (param.Key.Contains("ReleaseDateFrom"))
                        query = query.Where(o => o.HireDate >= (DateTime)value);

                    // Wybrano filtr dla daty zwolnienia
                    else if (param.Key.Contains("ReleaseDateTo"))
                        query = query.Where(o => o.HireDate <= (DateTime)value);

                }
            }

            foreach (var record in query.Select(o => o))
            {

                list.Add(new Model.Projection.Employee()
                        {
                            Id = record.EmployeeId,
                            Name = record.Name,
                            Surname = record.Surname,
                            Pesel = record.Pesel,
                            Nip = record.Nip,
                            PhoneNumber = record.PhoneNumber,
                            Email = record.Email,
                            State = record.EmployeeState.Name
                        });
            }

            LoadData(OrderBy(list.AsQueryable()));
        }

        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Employee;
                    
                // Nie można usunąć zalogowanego użytkownika
                if(selectedRecord.Id == Core.LoggedUser.Instance.User.EmployeeId)
                    throw new InvalidOperationException(Properties.ApplicationMessages.Default.msgAttemptToSelfRemove);

                var record = (from o in db.Employees where o.EmployeeId == selectedRecord.Id select o).Single();

                // Usuwamy rekord z kontekstu
                record.Groups.Clear();
                db.Employees.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }catch(InvalidOperationException e){
            
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.captionRemoveError);

            }// Wszelkie inne błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "Id", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
                case "Surname": query = new Core.GenericSorter<T, string>().Sort(query, "Surname", Ordering.Direction); break;
                case "State": query = new Core.GenericSorter<T, string>().Sort(query, "State", Ordering.Direction); break;
                case "Pesel": query = new Core.GenericSorter<T, string>().Sort(query, "Pesel", Ordering.Direction); break;
                case "Nip": query = new Core.GenericSorter<T, string>().Sort(query, "Nip", Ordering.Direction); break;
                case "PhoneNumber": query = new Core.GenericSorter<T, string>().Sort(query, "PhoneNumber", Ordering.Direction); break;
                case "Email": query = new Core.GenericSorter<T, string>().Sort(query, "Email", Ordering.Direction); break;
                default: break;
            }

            return query;
        }
    }
}
