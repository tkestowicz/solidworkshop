﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DataGrid
{
    public class GroupPresenter : Presenters.DataPresenter
    {
        public GroupPresenter(IDataGridView view) : base(view) { }

        public override void LoadData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var query = OrderBy(db.Groups).Select(o =>
                    new Model.Projection.Group()
                    {
                        Id = o.GroupId,
                        Name = o.Name,
                        Description = o.Description
                    });

            LoadData(query);
        }

        public override void DeleteRecord()
        {
            try
            {

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Dlatego, że jest LazyLoading musimy pobrać kontekst bazy
                var selectedRecord = _view.CurrentRecord as Model.Projection.Group;

                // Nie można usunąć grupy SuperAdmin
                if (selectedRecord.Id == 1)
                    throw new InvalidOperationException(Properties.ApplicationMessages.Default.msgAttemptToRemoveSuperAdminsGroup);
                
                var record = (from o in db.Groups where o.GroupId == selectedRecord.Id select o).Single();

                
                // Nie można usunąć niepustej grupy
                if (record.Employees.Count != 0)
                    throw new InvalidOperationException(Properties.ApplicationMessages.Default.msgAttemptToRemoveNonEmptyGroup);

                // Usuwamy rekord z kontekstu
                record.Permissions.Clear();
                db.Groups.DeleteObject(record);

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                // Wyświetlamy komunikat
                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultDeleteSucceed);

                // Odświeżamy widok
                _view.RefreshGrid();

            }
            catch (InvalidOperationException e)
            {

                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.captionRemoveError);

            }// Wszelkie inne błędy napotkane podczas usuwania
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultDeleteError);
            }
        }

        public override string GetColumnHeaderPrefixIndex()
        {
            return new Model.Projection.Group().GetType().Name;
        }

        protected override IQueryable<T> OrderBy<T>(IQueryable<T> query)
        {
            // Domyślne sortowanie
            if (Ordering == null)
                Ordering = new OrderBy() { Column = "Id", Direction = Core.SortDirection.Ascending };

            switch (Ordering.Column)
            {
                case "Id": query = new Core.GenericSorter<T, int>().Sort(query, "GroupId", Ordering.Direction); break;
                case "Name": query = new Core.GenericSorter<T, string>().Sort(query, "Name", Ordering.Direction); break;
                case "Description": query = new Core.GenericSorter<T, string>().Sort(query, "Description", Ordering.Direction); break;
                default: break;
            }

            return query;
        }
    }
}
