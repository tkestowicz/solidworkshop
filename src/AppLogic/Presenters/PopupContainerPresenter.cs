﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Events;
using AppLogic.Interfaces.Popups;

namespace AppLogic.Presenters
{
    /// <summary>
    /// Logika dla kontenera wyskakującego okienka
    /// </summary>
    public class PopupContainerPresenter : IEventSubscriber, IClosePopupEvent
    {

        IPopupContainer _view;

        /// <summary>
        /// Główny kontruktor logiki
        /// </summary>
        /// <param name="view">Widok, z którym ma współpracować logika</param>
        public PopupContainerPresenter(IPopupContainer view)
        {
            _view = view;

            Core.ServiceManager.Events.Disposer += new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader += new Core.EventCollection.InitializeEvents(BindEvents);

            Core.ServiceManager.Events.Init();
        }

        #region IPopupPresenter

        public void BindEvents()
        {
            // Podpinanie zdarzeń do obsługi
            Core.ServiceManager.Events.Subscribe(new Core.EventCollection.ClosePopupHandler(OnPopupClose));
        }

        public void UnbindEvents()
        {
            // Odpinamy zdarzenia podczas niszczenia obiektu
            Core.ServiceManager.Events.Unsubscribe(new Core.EventCollection.ClosePopupHandler(OnPopupClose));

            // ZAWSZE! trzeba odpinać loader/disposer zdarzeń dla danego obiektu
            Core.ServiceManager.Events.Disposer -= new Core.EventCollection.DisposeEvents(UnbindEvents);
            Core.ServiceManager.Events.Loader -= new Core.EventCollection.InitializeEvents(BindEvents);
        }

        #endregion

        /// <summary>
        /// Przechwycenie zdarzenia zamknięcia okienka
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        public void OnPopupClose(object sender)
        {
            _view.ClosePopup();
        }
    }
}
