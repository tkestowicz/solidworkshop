﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters
{
    /// <summary>
    /// Klasa logiki głównego okna aplikacji
    /// </summary>
    public partial class MainPresenter
    {
        public MainPresenter(IMainView view)
        {
            _view = view;
        }

        #region Methods

        /// <summary>
        /// Metoda sprawdza, czy użytkonik jest zalogowany
        /// </summary>
        public void Authorization()
        {

            // Jeżeli user nie jest zalogowany
            if (!Core.LoggedUser.Instance.Logged)
                _view.LoadLoginView();
        }

        #endregion

        #region Fields

        /// <summary>
        /// Referencja do obiektu okna głównego
        /// </summary>
        private IMainView _view;

        #endregion

        #region Events

        /// <summary>
        /// Obsługa wylogowania
        /// </summary>
        public void OnLogout()
        {
            _view.Restart(); 
        }

        /// <summary>
        /// Metoda zamyka połączenie z bazą danych przy wyłączaniu aplikacji
        /// </summary>
        public void OnClosing()
        {
            using (var db = Core.ServiceManager.Database)
            {
                db.Dispose();
            }
        }

        #endregion
    }
}
