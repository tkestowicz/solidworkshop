﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.DicitionaryEditor
{

    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora
    /// </summary>
    public partial class CityPresenter : EditorPresenter, IRelationalEditorPresenter
    {

        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected ICityEditorView _View
        {
            get
            {
                return _view as ICityEditorView;
            }
        }

        #endregion

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            throw new NotImplementedException();
        }

        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy, czy wpis jest unikalny
                var query = from o in db.Cities where o.Name == _View.City && o.Province.Name == _View.Province && o.CityId != ID select o;

                // Jeżeli wpis nie jest unikalny
                if (query.Count() > 0)
                    throw new ArgumentException(String.Format(Properties.ApplicationMessages.Default.msgDictionaryDataRedundant, _View.City));


                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    db.Cities.AddObject(new Model.City() { 
                        Name = _View.City,
                        ProvinceId = _View.ProvinceId
                    });
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Cities where o.CityId == ID select o).Single();

                    record.Name = _View.City;
                    record.ProvinceId = _View.ProvinceId;
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.City() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wpis nie istnieje w bazie
            catch (InvalidOperationException)
            {
                _view.ShowError(String.Format(Properties.ApplicationMessages.Default.msgInvalidRecordId, ID), Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }

            
        }

        public override void Validate()
        {

            string msg = null;

            // Miasto nie może być pusta
            if (String.IsNullOrEmpty(_View.City))
                msg += Properties.ValidationMessages.Default.emptyCity + "\n";

            // Należy wybrać województwo
            if (_View.ProvinceId <= 0)
                msg += Properties.ValidationMessages.Default.emptyProvince + "\n";

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));

        }

        public override void Reset()
        {
            try
            {

                _View.Province = null;
                _View.City = null;
                base.Reset();

                // Próbujemy załadować marki samochodów
                LoadRelatedData();

            }// Brak marek w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(e.Message, Properties.ApplicationMessages.Default.msgRequiredDataError);
                ForceOffView = true;
            }
        }

        /// <summary>
        /// Metoda ładuje województwa do listy rozwijanej
        /// </summary>
        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var provinces = from p in db.Provinces
                         orderby p.ProvinceId
                         select new Model.Projection.ComboBox
                         {
                             Id = p.ProvinceId,
                             Value = p.Name
                         };

            if (provinces.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgCarBrandsBeforeModels);

            _View.Provinces = provinces.ToList();
        }
    }
}
