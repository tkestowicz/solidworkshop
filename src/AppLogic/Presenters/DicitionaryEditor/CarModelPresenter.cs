﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.DicitionaryEditor
{

    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora
    /// </summary>
    public partial class CarModelPresenter : EditorPresenter, IRelationalEditorPresenter
    {

        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected ICarModelEditorView _View
        {
            get
            {
                return _view as ICarModelEditorView;
            }
        }

        #endregion

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                var rec = record.Record<Model.Projection.CarModel>();
                ID = rec.Id;
                _View.ModelName = rec.Name;
                _View.BrandName = rec.BrandName;
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy, czy wpis jest unikalny
                var query = from o in db.CarModels where o.ModelName == _View.ModelName && o.CarBrandId== _View.BrandId && o.CarModelId != ID select o;

                // Jeżeli wpis nie jest unikalny
                if (query.Count() > 0)
                    throw new ArgumentException(String.Format(Properties.ApplicationMessages.Default.msgDictionaryDataRedundant, _View.ModelName));


                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    db.CarModels.AddObject(new Model.CarModel() { 
                        ModelName = _View.ModelName,
                        CarBrandId = (int)_View.BrandId
                    });
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.CarModels where o.CarModelId == ID select o).Single();

                    record.ModelName = _View.ModelName;
                    record.CarBrandId = (int)_View.BrandId;
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.CarModel() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wpis nie istnieje w bazie
            catch (InvalidOperationException)
            {
                _view.ShowError(String.Format(Properties.ApplicationMessages.Default.msgInvalidRecordId, ID), Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }

            
        }

        public override void Validate()
        {

            string msg = null;

            // Nazwa nie może być pusta
            if (String.IsNullOrEmpty(_View.ModelName))
                msg += Properties.ValidationMessages.Default.emptyCarModelName + "\n";

            // Należy wybrać markę
            if (_View.BrandId == null || _View.BrandId <= 0)
                msg += Properties.ValidationMessages.Default.emptyCarBrand + "\n";

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));

        }

        public override void Reset()
        {
            try
            {

                _View.ModelName = null;
                _View.BrandId = null;
                base.Reset();

                // Próbujemy załadować marki samochodów
                LoadRelatedData();

            }// Brak marek w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(e.Message, Properties.ApplicationMessages.Default.msgRequiredDataError);
                ForceOffView = true;
            }
        }

        /// <summary>
        /// Metoda ładuje marki do listy rozwijanej
        /// Jeżeli w bazie nie ma marek rzucany wyjątek DataNotFoundException
        /// </summary>
        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;
            
            var brands = from c in db.CarBrands
                            orderby c.BrandName
                            select new Model.Projection.ComboBox
                            {
                                Id = c.CarBrandId,
                                Value = c.BrandName
                            };

            if (brands.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgCarBrandsBeforeModels);

            _View.CarBrands = brands.ToList();
        }
    }
}
