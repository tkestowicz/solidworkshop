﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace AppLogic.Presenters.DicitionaryEditor
{

    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora usług
    /// </summary>
    public partial class ServicePresenter : EditorPresenter, IRelationalEditorPresenter
    {

        #region Properties

        /// <summary>
        /// Dostęp do właściwego widoku dla logiki
        /// </summary>
        protected IServiceEditorView _View
        {
            get
            {
                return _view as IServiceEditorView;
            }
        }

        #endregion

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                var rec = record.Record<Model.Projection.Service>();
                ID = rec.Id;
                _View.ServiceName = rec.Name;
                _View.Description = rec.Description;
                _View.ClassName = rec.ClassName;
                _View.Price = rec.Price;
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy, czy wpis jest unikalny
                var query = from o in db.Services where o.Name == _View.ServiceName && o.ServiceClasses.ServiceClassId == _View.ClassId select o;

                // Jeżeli wpis nie jest unikalny
                if (query.Count() > 0)
                    throw new ArgumentException(String.Format(Properties.ApplicationMessages.Default.msgDictionaryDataRedundant, _View.ServiceName));


                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    db.Services.AddObject(new Model.Service() {
                        Name = _View.ServiceName,
                        Description = _View.Description,
                        Price = _View.Price,
                        ServiceClassServiceClassId = (int)_View.ClassId
                    });
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from o in db.Services where o.ServiceId == ID select o).Single();

                    record.Name = _View.ServiceName;
                    record.Description = _View.Description;
                    record.Price = _View.Price;
                    record.ServiceId = (int)_View.ClassId;
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.CarModel() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wpis nie istnieje w bazie
            catch (InvalidOperationException)
            {
                _view.ShowError(String.Format(Properties.ApplicationMessages.Default.msgInvalidRecordId, ID), Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }


        }

        public override void Validate()
        {

            string msg = null;

            // Nazwa nie może być pusta
            if (String.IsNullOrEmpty(_View.ServiceName))
                msg += Properties.ValidationMessages.Default.emptyServiceName + "\n";

            // Należy wybrać typ
            if (_View.ClassId == null || _View.ClassId <= 0)
                msg += Properties.ValidationMessages.Default.emptryServiceClass + "\n";

            // Mamy błędy to rzucamy wyjątek
            if (msg != null)
                throw new ArgumentException(String.Format(Properties.ValidationMessages.Default.validationError, msg));

        }

        public override void Reset()
        {
            try
            {

                _View.ServiceName = null;
                _View.Description = null;
                _View.Price = 0;
                _View.ClassId = null;
                base.Reset();

                // Próbujemy załadować typy
                LoadRelatedData();

            }// Typów marek w bazie
            catch (Core.Exceptions.DataNotFoundException e)
            {
                _View.ShowError(e.Message, Properties.ApplicationMessages.Default.msgRequiredDataError);
                ForceOffView = true;
            }
        }

        /// <summary>
        /// Metoda ładuje typy do listy rozwijanej
        /// Jeżeli w bazie nie ma typów rzucany wyjątek DataNotFoundException
        /// </summary>
        public void LoadRelatedData()
        {
            // Pobranie uchwytu do bd
            var db = Core.ServiceManager.Database;

            var classes = from c in db.ServiceClasses
                         orderby c.Name
                         select new Model.Projection.ComboBox
                         {
                             Id = c.ServiceClassId,
                             Value = c.Name
                         };

            if (classes.Count() == 0)
                throw new Core.Exceptions.DataNotFoundException(Properties.ApplicationMessages.Default.msgServicesClassBeforeService);

            _View.Classes = classes.ToList();
        }
    }
}
