﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;

namespace AppLogic.Presenters.DicitionaryEditor
{

    /// <summary>
    /// Klasa logiki do obsługi kontrolki edytora
    /// </summary>
    public partial class ProvincesPresenter : DictionaryEditorPresenter
    {

        public override void RecordReceived(EventArgs.RecordEventArgs record)
        {
            try
            {
                var rec = record.Record<Model.Projection.Provinces>();
                ID = rec.Id;
                Data = rec.Name;
            }
            catch (InvalidCastException)
            {
                Reset();
            }
        }

        public override void SaveChanges()
        {
            try
            {

                // Sprawdzamy poprawność wprowadzonych danych
                Validate();

                // Pobranie uchwytu do bd
                var db = Core.ServiceManager.Database;

                // Sprawdzamy, czy wpis jest unikalny
                var query = from s in db.Provinces where s.Name == Data && s.ProvinceId != ID select s;

                // Jeżeli wpis nie jest unikalny
                if (query.Count() > 0)
                    throw new ArgumentException(String.Format(Properties.ApplicationMessages.Default.msgDictionaryDataRedundant, Data));

                // Dodajemy nowy rekord jeżeli nie mamy w pamięci ID
                if (ID == 0)
                {
                    // Dodajemy nowy wpis do bazy
                    db.Provinces.AddObject(new Model.Province() { Name = Data });
                }
                else
                {
                    // Pobieramy rekord z bazy żeby wprowadzić nowe dane
                    var record = (from s in db.Provinces where s.ProvinceId == ID select s).Single();

                    record.Name = Data;
                }

                // Zapisujemy zmiany w bazie
                db.SaveChanges();

                _view.ShowInfo(Properties.ApplicationMessages.Default.msgDefaultSaveSucceed);

                Reset();

                Core.ServiceManager.Events.OnSaveSucceed(this, new EventArgs.RecordEventArgs(new Model.Projection.ServiceStatus() { }.GetType()));

            }// Błędne dane
            catch (ArgumentException e)
            {
                _view.ShowError(e.Message, Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wpis nie istnieje w bazie
            catch (InvalidOperationException)
            {
                _view.ShowError(String.Format(Properties.ApplicationMessages.Default.msgInvalidRecordId, ID), Properties.ApplicationMessages.Default.msgSaveErrorCaption);

            }// Wszelkie inne błędy napotkane podczas zapisu
            catch (Exception)
            {
                _view.ShowError(Properties.ApplicationMessages.Default.msgDefaultSaveError, Properties.ApplicationMessages.Default.msgSaveErrorCaption);
            }


        }

    }
}
