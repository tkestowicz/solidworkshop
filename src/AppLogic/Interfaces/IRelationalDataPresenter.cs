﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla logiki grida, który prezentuje dane z więcej niż jednej encji
    /// </summary>
    public interface IRelationalDataPresenter: IPresenter
    {

        /// <summary>
        /// Metoda ładuje dane z powiązanych encji
        /// </summary>
        IQueryable<T> OrderByRelational<T>(IQueryable<T> query);

    }
}
