﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Presenters;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla widoku odpowiedzialnego za wyświetlanie danych
    /// </summary>
    public interface IDataGridView : IBaseView, IControlView
    {
        /// <summary>
        /// Aktualnie aktywna strona
        /// </summary>
        int CurrentPage { get; set; }

        /// <summary>
        /// Liczba wszystkich stron
        /// </summary>
        int TotalPages { get; set; }

        /// <summary>
        /// Właściwość do ustawiania listy rozwijanej z ilością wpisów na strone
        /// </summary>
        string ItemsPerPageRange { set;  }

        /// <summary>
        /// Właściwość do ustawiania ilości wpisów do wyświetlenia
        /// </summary>
        int ItemsPerPage { get; set; }

        /// <summary>
        /// Włącza / wyłącza przycisk czyszczenia filtrów danych
        /// </summary>
        bool ClearFiltersEnabled { set; }

        /// <summary>
        /// Zwraca aktualnie zaznaczony rekord
        /// </summary>
        /// <returns>Aktualnie wybrany rekord</returns>
        object CurrentRecord { get; }

        /// <summary>
        /// Metoda inicjuje prezentera
        /// </summary>
        /// <typeparam name="DataPresenter">Prezenter, który będzie obsługiwał kontrolkę</typeparam>
        void Initialize(DataPresenter presenter);

        /// <summary>
        /// Metoda ładuje rekordy do widoku
        /// </summary>
        /// <typeparam name="T">Typ encji</typeparam>
        /// <param name="records">Lista rekordów wyciągniętych z bazy</param>
        void LoadData<T>(IList<T> records);

        /// <summary>
        /// Metoda odświeża siatkę rekordów
        /// </summary>
        void RefreshGrid();

        /// <summary>
        /// Metoda przywraca siatkę rekordów do stanu domyślnego
        /// </summary>
        void ResetGrid();

        /// <summary>
        /// Metoda przywraca do stanu domyślnego nawigację między stronami siatki danych
        /// </summary>
        /// <param name="addRecordEnabled">Flaga określająca, czy przycisk dodawania rekordów ma być aktywny</param>
        void ResetNavigation(bool addRecordEnabled = false);

    }
}
