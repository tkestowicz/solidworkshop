﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs obiektu, który udostępnia dane adresowe.
    /// </summary>
    public interface IAddress
    {

        /// <summary>
        /// Właściwość z miastem
        /// Uwaga: Należy najpierw załadować wojewódtwa a potem ustawiać miasto!
        /// </summary>
        int CityId { get; set; }

        /// <summary>
        /// Ustawia miasto przez jego nazwe a nie Id
        /// </summary>
        string CityByName { set; }

        /// <summary>
        /// Właściwość z województwem (odwołanie przez wartość)
        /// </summary>
        string Province { get; set; }

        /// <summary>
        /// Właściwość z województwem (odwołanie przez id)
        /// </summary>
        int ProvinceId { get; set; }

        /// <summary>
        /// Właściwość z kodem pocztowym
        /// </summary>
        string Postcode { get; set; }

        /// <summary>
        /// Właściwość z ulicą
        /// </summary>
        string Street { get; set; }

        /// <summary>
        /// Właściwość z numerem domu
        /// </summary>
        string HouseNumber { get; set; }

        /// <summary>
        /// Właściwość z numerem mieszkania
        /// </summary>
        string ApartmentNumber { get; set; }

    }
}
