﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla widoku odpowiedzialnego za wyświetlanie komunikatów
    /// </summary>
    public interface INotifier 
    {
    
        /// <summary>
        /// Metoda wyświetlajaca komunikat o charakterze informacyjnym
        /// </summary>
        /// <param name="content">Treść komunikatu</param>
        /// <param name="caption">Tytuł okienka</param>
        void ShowInfo(string content, string caption = null);

        /// <summary>
        /// Metoda wyświetlajaca komunikat z ostrzeżeniem
        /// </summary>
        /// <param name="content">Treść komunikatu</param>
        /// <param name="caption">Tytuł okienka</param>
        void ShowWarning(string content, string caption = null);

        /// <summary>
        /// Metoda wyświetlajaca komunikat o błędzie
        /// </summary>
        /// <param name="content">Treść komunikatu</param>
        /// <param name="caption">Tytuł okienka</param>
        void ShowError(string content, string caption = null);

        /// <summary>
        /// Metoda wyświetlajaca okienko z pytaniem o potwierdzenie
        /// </summary>
        /// <param name="question">Treść pytania</param>
        /// <param name="caption">Tytuł okienka</param>
        /// <returns>Odpowiedź użytkownika, TRUE - tak, FALSE - nie</returns>
        bool ShowQuestion(string question, string caption = null);

        /// <summary>
        /// Metoda wyświetlajaca treść przechwyconego wyjątku
        /// </summary>
        /// <param name="content">Treść komunikatu</param>
        /// <param name="caption">Tytuł okienka</param>
        void ShowException(Exception ex, string caption = null);

    }
}
