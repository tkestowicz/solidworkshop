﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs głównego okna aplikacji
    /// </summary>
    public interface IMainView : IBaseView
    {

        /// <summary>
        /// Metoda ładuje widok logowania
        /// </summary>
        void LoadLoginView();

        /// <summary>
        /// Metoda inicjuje okno aplikacji na podstawie uprawnień użytkownika
        /// </summary>
        void InitMainView();

        /// <summary>
        /// Metoda restartuje aplikacje
        /// </summary>
        void Restart();

    }
}
