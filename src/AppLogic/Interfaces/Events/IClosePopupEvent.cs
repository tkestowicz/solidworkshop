﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie zamknięcia wyskakującego okienka
    /// </summary>
    public interface IClosePopupEvent
    {

        /// <summary>
        /// Metoda obsługi 
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        void OnPopupClose(object sender);
    }
}
