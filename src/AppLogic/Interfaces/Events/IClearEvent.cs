﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{
    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie czyszczenia
    /// </summary>
    public interface IClearEvent
    {

        /// <summary>
        /// Metoda obsługi zdarzenia
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        void OnClear(object sender);

    }
}
