﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie edycji rekordu
    /// </summary>
    public interface IEditRecordEvent
    {

        /// <summary>
        /// Metoda obsługi zdarzenia
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        /// <param name="record">Rekord, którego dotyczy zdarzenie</param>
        void OnEdit(object sender, EventArgs.RecordEventArgs record);

    }
}
