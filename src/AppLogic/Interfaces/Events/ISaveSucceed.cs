﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie poprawnego zapisania zmian
    /// </summary>
    public interface ISaveSucceed
    {

        /// <summary>
        /// Metoda obsługi zdarzenia
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        void OnSaveSucceed(object sender, EventArgs.RecordEventArgs record);
    }
}
