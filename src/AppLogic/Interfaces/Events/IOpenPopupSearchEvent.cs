﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie uruchomienia okienka wyszukiwania
    /// </summary>
    public interface IOpenPopupSearchEvent
    {

        /// <summary>
        /// Metoda obsługi zdarzenia
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        void OnOpenPopupSearch(object sender);
    }
}
