﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie podglądu rekordu
    /// </summary>
    public interface IPreviewRecord
    {
        /// <summary>
        /// Metoda obsługi zdarzenia
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        /// <param name="record">Rekord, którego dotyczy zdarzenie</param>
        void OnPreview(object sender, EventArgs.RecordEventArgs record);
    }
}
