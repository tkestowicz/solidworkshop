﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla klas oczekujących na zdarzenia
    /// </summary>
    public interface IEventSubscriber
    {

        /// <summary>
        /// Metoda podpinająca zdarzenia
        /// </summary>
        void BindEvents();

        /// <summary>
        /// Metoda odpinająca zdarzenia
        /// </summary>
        void UnbindEvents();
    }
}
