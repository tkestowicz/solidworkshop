﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Events
{

    /// <summary>
    /// Interfejs dla obiektów obsługujących zdarzenie filtrowania rekordów
    /// </summary>
    public interface IFilterRecordsEvent
    {

        /// <summary>
        /// Metoda obsługi zdarzenia
        /// </summary>
        /// <param name="sender">Obiekt wywołujący zdarzenie</param>
        /// <param name="fields">Parametry wyszukiwania</param>
        void OnFilterRecords(object sender, Dictionary<string, object> fields);

    }
}
