﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Controls
{
    public interface ISaveCancelControlsView : IControlView
    {
        /// <summary>
        /// Obiekt, w którym znajdują się przyciski - on pojawi się w zdarzeniu.
        /// </summary>
        object Sender { get; set; }

        /// <summary>
        /// Wywołanie akcji anuluj
        /// </summary>
        void Cancel();

        /// <summary>
        /// Wywołanie akcji zapisu
        /// </summary>
        void Save();
    }
}
