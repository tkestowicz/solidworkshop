﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Controls
{
    public interface IFindClearControlsView : IControlView
    {
        /// <summary>
        /// Obiekt, w którym znajdują się przyciski - on pojawi się w zdarzeniu.
        /// </summary>
        object Sender { get; set; }

        /// <summary>
        /// Wywołanie akcji wyczyść
        /// </summary>
        void Clear();

        /// <summary>
        /// Wywołanie akcji znajdź
        /// </summary>
        void Find();
    }
}
