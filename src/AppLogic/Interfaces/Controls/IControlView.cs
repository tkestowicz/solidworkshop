﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Controls
{
    /// <summary>
    /// Interfejs dla kontrolek
    /// </summary>
    public interface IControlView : IBaseView
    {
        /// <summary>
        /// Włączanie / wyłączanie kontrolki
        /// </summary>
        bool Enable { set; }

        /// <summary>
        /// Kontrolka włączona / wyłączona
        /// </summary>
        bool IsEnabled { get; }

    }
}
