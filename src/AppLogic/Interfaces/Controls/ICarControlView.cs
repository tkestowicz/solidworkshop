﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Interfaces;

namespace AppLogic.Interfaces.Controls
{
    public interface ICarControlView: IControlView, ICar
    {
        /// <summary>
        /// Ustawia model przez jego nazwe a nie Id
        /// </summary>
        string ModelByName { set; }

        /// <summary>
        /// Ustawia markę przez jego nazwe a nie Id
        /// </summary>
        string BrandByName { set; }

        /// <summary>
        /// Ustawia kolor przez jego nazwe a nie Id
        /// </summary>
        string ColorByName { set; }

        /// <summary>
        /// Ustawia rodzaj paliwa przez jego nazwe a nie Id
        /// </summary>
        string FuelByName { set; }

        /// <summary>
        /// Właściwość do ładowania listy rozwijanej marek
        /// </summary>
        List<Model.Projection.ComboBox> Brands { set; }

        /// <summary>
        /// Właściwość do ładowania listy rozwijanej modeli
        /// </summary>
        List<Model.Projection.ComboBox> Models { set; }

        /// <summary>
        /// Właściwość do ładowania listy rozwijanej kolorów
        /// </summary>
        List<Model.Projection.ComboBox> Colors { set; }

        /// <summary>
        /// Właściwość do ładowania listy rozwijanej paliw
        /// </summary>
        List<Model.Projection.ComboBox> Fuels { set; }
    }
}
