﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Controls
{
    /// <summary>
    /// Interfejs dla widoku do edycji klienta
    /// </summary>
    public interface IAddressControlView : IControlView , IAddress
    {

        /// <summary>
        /// Właściwość z dostępnymi miastami (dynamicznie ładowane na podstawie wojewódtwa)
        /// </summary>
        List<Model.Projection.ComboBox> Cities { set; }

        /// <summary>
        /// Właściwość z dostępnymi województwami
        /// </summary>
        List<Model.Projection.ComboBox> Provinces { set; }

    }
}
