﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Controls
{

    public interface IAddEditSearchControlsView : IControlView
    {

        /// <summary>
        /// Obiekt, w którym znajdują się przyciski - on pojawi się w zdarzeniu.
        /// </summary>
        object Sender { get; set; }

        /// <summary>
        /// Właściwość określa, czy przycisk wyszukiwania ma być widoczny
        /// </summary>
        bool SearchButtonVisible { get; set; }

        /// <summary>
        /// Właściwość umożliwia podpianie metod reagujących na zdarzenie kliknięcia w przycisk Wyszukaj
        /// </summary>
        Presenters.Control.AddEditSearchControlsPresenter.SearchEventHandler AddHandler { set; }

        /// <summary>
        /// Właściwość umożliwia odpianie metod reagujących na zdarzenie kliknięcia w przycisk Wyszukaj
        /// </summary>
        Presenters.Control.AddEditSearchControlsPresenter.SearchEventHandler RemoveHandler { set; }

    }
}
