﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla okienka logowania
    /// </summary>
    public interface ILoginView : IBaseView
    {
        /// <summary>
        /// Nazwa serwera bazy danych, do którego nastąpi zalogowanie
        /// </summary>
        string ServerName { get; set;}

        /// <summary>
        /// Metoda wykonywana po poprawnym zalogowaniu
        /// </summary>
        void LoginSucceed();

        /// <summary>
        /// Metoda wykonana jeżeli logowanie zakończyło się niepowodzeniem
        /// </summary>
        void LoginFailed();

        /// <summary>
        /// Metoda przywraca zakładke ustawień do stanu domyślnego
        /// </summary>
        void ResetSettingsPanel();

    }
}
