﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Bazowy interfejs dla widoków
    /// </summary>
    public interface IBaseView : INotifier
    {

        /// <summary>
        /// Metoda wyświetla widok
        /// </summary>
        void Show();

        /// <summary>
        /// Metoda zamyka widok
        /// </summary>
        void Close();

    }
}
