﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla kontrolki edytora miasta
    /// </summary>
    public interface ICityEditorView : IEditorView
    {

        /// <summary>
        /// Ustawia / pobiera województwo (przez id)
        /// </summary>
        int ProvinceId { get; set; }

        /// <summary>
        /// Ustawia / pobiera województwo (przez wartość
        /// </summary>
        string Province { get; set; }

        /// <summary>
        /// Ustawia / pobiera miasto
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Ustawia województwa
        /// </summary>
        List<Model.Projection.ComboBox> Provinces { set; }
    }
}
