﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji klienta
    /// </summary>
    public interface IClientEditorView : IEditorView
    {

        /// <summary>
        /// Właściwość z imieniem klienta
        /// </summary>
        string ClientName { get; set; }

        /// <summary>
        /// Właściwość z nazwiskiem klienta
        /// </summary>
        string ClientSurname { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi zniżkami
        /// </summary>
        List<Model.Projection.ComboBox> ClientDiscounts { set; }

        /// <summary>
        /// Właściwość ze zniżką klienta
        /// </summary>
        int? ClientDiscountId { get; set; }

        /// <summary>
        /// Właściwość z peselem klienta
        /// </summary>
        string ClientPesel { get; set; }

        /// <summary>
        /// Właściwość z mailem klienta
        /// </summary>
        string ClientMail { get; set; }

        /// <summary>
        /// Właściwość z nr telefonu klienta
        /// </summary>
        string ClientPhoneNumber { get; set; }

        /// <summary>
        /// Właściwość z samochodami klienta
        /// </summary>
        List<Model.Interfaces.ICar> ClientCars { get; set; }

        /// <summary>
        /// Obiekt z danymi adresowymi klienta
        /// </summary>
        IAddress ClientAddress { get; }

        /// <summary>
        /// Czyści reguły filtrowania zapamiętane w formatce
        /// </summary>
        void ClearSearchFilters();

    }
}
