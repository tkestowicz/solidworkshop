﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji modelu samochodu
    /// </summary>
    public interface ICarModelEditorView : IEditorView 
    {

        /// <summary>
        /// Właściwość z nazwą modelu
        /// </summary>
        string ModelName { get; set; }

        /// <summary>
        /// Właściwość z marką
        /// </summary>
        int? BrandId { get; set; }

        /// <summary>
        /// Właściwość do ustawiania marki w liście rozwijanej przez nazwe
        /// </summary>
        string BrandName { set; }

        /// <summary>
        /// Właściwość do ustawiania marek
        /// </summary>
        List<Model.Projection.ComboBox> CarBrands { set; }
    }
}
