﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji pensji
    /// </summary>
    public interface IWageEditorView : IEditorView
    {
        /// <summary>
        /// Właściwość z okresem pensji
        /// </summary>
        string Month { get; set; }

        /// <summary>
        /// Właściwość z wysokością pensji
        /// </summary>
        double Salary { get; set; }

        /// <summary>
        /// Właściwość z premią do pensji
        /// </summary>
        double Bonus { get; set; }


    }
}
