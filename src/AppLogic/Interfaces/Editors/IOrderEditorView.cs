﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji zlecenia
    /// </summary>
    public interface IOrderEditorView : IEditorView
    {
        /// <summary>
        /// Właściwość ze statusem zlecenia
        /// </summary>
        string OrderStatus { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi statusami zleceń
        /// </summary>
        List<string> OrderStatuses { get; set; }

        /// <summary>
        /// Właściwość z samochodem, ktorego dotyczy zlecenie
        /// </summary>
        int CarId { get; set; }
        string Car { get; set; }

        /// <summary>
        /// Właściwość z nadzorcą zlecenia
        /// </summary>
        string OrderSupervisor { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi osobami nadzorującymi zlecenia
        /// </summary>
        List<string> OrderSupervisores { get; set; }

        /// <summary>
        /// Właściwość z opisem zlecenia
        /// </summary>
        string OrderDescription { get; set; }

        /// <summary>
        /// Właściwość z ceną sumaryczną zlecenia (suma cen usług)
        /// </summary>
        double? OrderPrice { get; set; }
    }
}
