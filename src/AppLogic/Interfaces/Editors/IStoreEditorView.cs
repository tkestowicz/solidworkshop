﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji magazynu
    /// </summary>
    public interface IStoreEditorView : IEditorView 
    {

        /// <summary>
        /// Właściwość z nazwą części
        /// </summary>
        string PartName { get; set; }

        /// <summary>
        /// Właściwość z kategorią części
        /// </summary>
        int? PartClassId { get; set; }

        /// <summary>
        /// Właściwość do ustawiania kategorii częsci w liście rozwijanej przez nazwe
        /// </summary>
        string PartClassName { set; }

        /// <summary>
        /// Właściwość z opisem części
        /// </summary>
        string PartDescription { get; set; }

        /// <summary>
        /// Właściwość z ilością sztuk
        /// </summary>
        int AvailableAmount { get; set;  }

        /// <summary>
        /// Właściwość z ceną jednostkową
        /// </summary>
        double UnitPrice { get; set; }

        /// <summary>
        /// Właściwość do ustawiania kategorii
        /// </summary>
        List<Model.Projection.ComboBox> PartClasses { set; }

        /// <summary>
        /// Czyści reguły filtrowania zapamiętane w formatce
        /// </summary>
        void ClearSearchFilters();

    }
}
