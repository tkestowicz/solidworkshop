﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji tabel słownikowych
    /// </summary>
    public interface IDictionaryEditorView : IEditorView 
    {

        /// <summary>
        /// Dane rekordu
        /// </summary>
        string Data { get; set; }

    }
}
