﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    public interface IServiceEditorView : IEditorView 
    {
        /// <summary>
        /// Właściwość z nazwą modelu
        /// </summary>
        string ServiceName { get; set; }

        /// <summary>
        /// Właściwość z nazwą modelu
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Właściwość z nazwą modelu
        /// </summary>
        double Price { get; set; }

        /// <summary>
        /// Właściwość z marką
        /// </summary>
        int? ClassId { get; set; }

        /// <summary>
        /// Właściwość do ustawiania marki w liście rozwijanej przez nazwe
        /// </summary>
        string ClassName { set; }

        /// <summary>
        /// Właściwość do ustawiania marek
        /// </summary>
        List<Model.Projection.ComboBox> Classes { set; }
    }
}
