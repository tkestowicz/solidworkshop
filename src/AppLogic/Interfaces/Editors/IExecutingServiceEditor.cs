﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Editors
{
    /// <summary>
    /// Interfejs dla widoku do edycji magazynu
    /// </summary>
    public interface IExecutingServiceEditorView : IEditorView
    {

        int StatusesId { set;  get; }
        List<Model.Projection.ComboBox> StatusesClasses { set; get; }

        int EmployeeId { set;  get; }
        List<Model.Projection.ComboBox> EmployeeClasses { set; get; }

        int OrderId { set; get; }
        List<Model.Projection.ComboBox> OrderClasses { set; get; }

        int ServiceId { set; get; }
        List<Model.Projection.ComboBox> ServiceClasses { set; get; }

        /// <summary>
        /// Czyści reguły filtrowania zapamiętane w formatce
        /// </summary>
        void ClearSearchFilters();
    }
}
