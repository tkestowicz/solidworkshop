﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Interfaces.Editors
{
    public interface IGroupEditorView : IEditorView
    {
        /// <summary>
        /// Właściwość z nazwą grupy
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Właściwość z opisem grupy
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Właściwość z uprawnieniami
        /// </summary>
        IList<Model.Projection.Permission> Permissions { get; set; }
    }
}
