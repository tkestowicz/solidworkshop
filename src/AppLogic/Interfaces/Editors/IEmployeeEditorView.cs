﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;

namespace AppLogic.Interfaces.Editors
{
    public interface IEmployeeEditorView : IEditorView
    {
        /// <summary>
        /// Właściwość z imieniem pracownika
        /// </summary>
        string  EmployeeName { get; set; }

        /// <summary>
        /// Właściwość z nazwiskiem pracownika
        /// </summary>
        string  EmployeeSurname { get; set; }

        /// <summary>
        /// Właściwość z nazwą użytkownika
        /// </summary>
        string EmployeeLogin { get; set; }

        /// <summary>
        /// Właściwość z hasłem użytkownika
        /// </summary>
        string EmployeePassword { get; set; }

        /// <summary>
        /// Właściwość z powtórzonym hasłem użytkownika
        /// </summary>
        string EmployeeRepeatPassword { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi grupami
        /// </summary>
        List<Model.Projection.ComboBox> EmployeeGroups { set; }

        /// <summary>
        /// Właściwość z grupą pracownika
        /// </summary>
        int EmployeeGroupId { get; set; }

        /// <summary>
        /// Właściwość z peselem pracownika
        /// </summary>
        string  EmployeePesel { get; set; }

        /// <summary>
        /// Właściwość z NIP-em pracownika
        /// </summary>
        string EmployeeNIP { get; set; }

        /// <summary>
        /// Właściwość z nr telefonu pracownika
        /// </summary>
        string  EmployeePhoneNumber { get; set; }

        /// <summary>
        /// Właściwość z mailem pracownika
        /// </summary>
        string EmployeeMail { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi statusami
        /// </summary>
        List<Model.Projection.ComboBox> EmployeeStates { set; }

        /// <summary>
        /// Właściwość ze statusem pracownika
        /// </summary>
        int EmployeeStateId { get; set; }

        /// <summary>
        /// Właściwość z kwalifikacjami pracownika
        /// </summary>
        string EmployeeQualifications { get; set; }

        /// <summary>
        /// Właściwość z datą zatrudnienia pracownika
        /// </summary>
        DateTime? EmployeeHireDate { get; set; }

        /// <summary>
        /// Właściwość z datą zwolnienia pracownika
        /// </summary>
        DateTime? EmployeeReleaseDate { get; set; }

        /// <summary>
        /// Obiekt z danymi adresowymi
        /// </summary>
        IAddress EmployeeAddress { get; }

        /// <summary>
        /// Czyści reguły filtrowania zapamiętane w formatce
        /// </summary>
        void ClearSearchFilters();

    }
}
