﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla wszystkich kontrolek wyszukiwania danych.
    /// </summary>
    public interface ISearchView
    {

        /// <summary>
        /// Słownik zbiera wybrane przez użytkownika parametry, po których będzie nałożone filtrowanie.
        /// Klucz - identyfikator parametru
        /// Wartość - wartość parametru
        /// </summary>
        Dictionary<string, object> SearchParameters { get; }

        /// <summary>
        /// Przywraca kontrolke wyszukiwania do stanu pierwotnego
        /// </summary>
        void Reset();
    }
}
