﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Popups
{
    /// <summary>
    /// Interfejs dla adaptera widoku edytorów pojawiających się w wyskakującym okienku.
    /// </summary>
    public interface IEditorPopupViewAdapter : IEditorView, IPopupView
    {

        /// <summary>
        /// Wartość rekordu
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Identyfikator kategorii, do której należy rekord np. w przypadku województw ID śląskiego.
        /// </summary>
        int? CategoryId { get; set; }

        /// <summary>
        /// Właściwość do ustawania / pobierania elementu w liście rozwijanej przez nazwe
        /// </summary>
        string ByName { get; set; }

        /// <summary>
        /// Właściwość do ustawiania listy rozwijanej kategorii rekordu
        /// </summary>
        List<Model.Projection.ComboBox> DropdownList { set; }

        /// <summary>
        /// Ustawia etykietę pola tekstowego do wprowadzania wartości
        /// </summary>
        string LabelValue { set; }

        /// <summary>
        /// Ustawia etykietę listy rozwijanej do wybierania kategorii
        /// </summary>
        string LabelDropdownList { set; }

    }
}
