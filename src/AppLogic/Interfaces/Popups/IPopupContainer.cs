﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Popups
{
    /// <summary>
    /// Interfejs dla klasy kontenera wyskakującego okienka
    /// </summary>
    public interface IPopupContainer
    {
        /// <summary>
        /// Metoda inicjalizuje okienko
        /// </summary>
        /// <param name="view">Kontrolka</param>
        /// <param name="presenter">Logiki dla kontrolki</param>
        void Initialize(IPopupView view, IPopupPresenter presenter);

        /// <summary>
        /// Zamyka wyskakujące okienko
        /// </summary>
        void ClosePopup();
    }
}
