﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Popups
{
    /// <summary cref="FormsView.Popups">
    /// Interfejs dla klas adapterów (łączników) między szablonami widoków z przestrzeni FormsView.Popups, a innymi widokami.
    /// </summary>
    public interface IPopupViewAdapter
    {

        /// <summary>
        /// Dostęp do właściwego widoku wyświetlonego w wyskakującym okienku
        /// </summary>
        IPopupEditorView View { get; set; }

        /// <summary>
        /// Dostęp do adaptera logiki dla widoku wyświetlonego w wyskakującym okienku
        /// </summary>
        IPopupPresenter Presenter { get; set; }
    }
}
