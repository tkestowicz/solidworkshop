﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Events;

namespace AppLogic.Interfaces.Popups
{
    /// <summary>
    /// Interfejs adaptera logiki dla wyskakującego okienka
    /// </summary>
    public interface IPopupPresenter : IEventSubscriber
    {
        /// <summary>
        /// Metoda inicjująca adapter logiki
        /// </summary>
        void Initialize();

    }
}
