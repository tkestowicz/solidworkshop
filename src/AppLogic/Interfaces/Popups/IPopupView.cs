﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Popups
{
    /// <summary>
    /// Bazowy interfejs dla kontrolek w wyskakującym okienku
    /// </summary>
    public interface IPopupView : IBaseView
    {
        /// <summary>
        /// Flaga z informacją, czy dane są zablokowane przed wyczyszczeniem. 
        /// Jeżeli 'true' to ustawianie właściwości nie przyniesie efektu.
        /// </summary>
        bool HoldData { get; set; }

        /// <summary>
        /// Metoda inicjuje kontrolkę
        /// </summary>
        /// <param name="presenter">Logika kontrolki</param>
        void Initialize(IPopupPresenter presenter);
    }
}
