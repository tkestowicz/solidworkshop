﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Popups
{
    public interface IChangePasswordView : IPopupView
    {
        /// <summary>
        /// Aktualne hasło
        /// </summary>
        string ActualPassword { get; }

        /// <summary>
        /// Nowe hasło
        /// </summary>
        string NewPassword { get; }

        /// <summary>
        /// Powtórzone hasło
        /// </summary>
        string RepeatPassword { get; }
    }
}
