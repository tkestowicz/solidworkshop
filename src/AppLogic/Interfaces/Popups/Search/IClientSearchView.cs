﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces.Popups.Search
{
    public interface IClientSearchView : IPopupView, ISearchView
    {
        /// <summary>
        /// Właściwość z imieniem klienta
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Właściwość z nazwiskiem klienta
        /// </summary>
        string Surname { get; set; }

        /// <summary>
        /// Właściwość z peselem klienta
        /// </summary>
        string Pesel { get; set; }

        /// <summary>
        /// Właściwość z nr telefonu klienta
        /// </summary>
        string PhoneNumber { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi miastami
        /// </summary>
        List<Model.Projection.ComboBox> Cities { set; }

        /// <summary>
        /// Właściwość z miastem 
        /// </summary>
        int CityId { get; set; }

        /// <summary>
        /// Właściwość z dostępnymi województwami
        /// </summary>
        List<Model.Projection.ComboBox> Provinces { set; }

        /// <summary>
        /// Właściwość z województwem 
        /// </summary>
        string Province { get; set; }

        /// <summary>
        /// Właściwość z kodem pocztowym
        /// </summary>
        string Postcode { get; set; }

        /// <summary>
        /// Właściwość z ulicą
        /// </summary>
        string Street { get; set; }

    }
}
