﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;

namespace AppLogic.Interfaces.Search
{
    public interface IServiceSearchView : IPopupView, ISearchView
    {
        /// <summary>
        /// Właściwość z dostępnymi statusami usług
        /// </summary>
        List <Model.Projection.ComboBox> States { set; }

        /// <summary>
        /// Właściwość z listą dospępnych pracowników
        /// </summary>
        List<Model.Projection.ComboBox> Mechanics { set; }

    }
}
