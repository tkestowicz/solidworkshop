﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;


namespace AppLogic.Interfaces.Search
{
    public interface ICarSearchView : IPopupView, ISearchView
    {

        /// <summary>
        /// Właściwość z dostępnymi markami samochodów
        /// </summary>
        List<Model.Projection.ComboBox> Brands { set; }

        /// <summary>
        /// Właściwość z dostępnymi modelami samochodów
        /// </summary>
        List<Model.Projection.ComboBox> Models { set; }

        /// <summary>
        /// Właściwość z dostępnymi kolorami samochodów
        /// </summary>
        List<Model.Projection.ComboBox> Colors { set; }

        /// <summary>
        /// Właściwość z marką samochodu
        /// </summary>
        int BrandId { get; }

        /// <summary>
        /// Właściwość z modelem samochodu
        /// </summary>
        int ModelId { set; }

    }
}
