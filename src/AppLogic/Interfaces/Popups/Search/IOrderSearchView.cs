﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;

namespace AppLogic.Interfaces.Search
{
    public interface IOrderSearchView : IPopupView, ISearchView
    {
        /// <summary>
        /// Lista statusów zlecenia
        /// </summary>
        List<Model.Projection.ComboBox> States { set; }

        /// <summary>
        /// Lista nadzorców
        /// </summary>
        List<Model.Projection.ComboBox> Supervisors { set; }
    }
}
