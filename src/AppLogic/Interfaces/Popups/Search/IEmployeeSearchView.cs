﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;

namespace AppLogic.Interfaces.Search
{
    public interface IEmployeeSearchView : IPopupView, ISearchView
    {

        /// <summary>
        /// Właściwość z dostępnymi grupami
        /// </summary>
        List<Model.Projection.ComboBox> Groups { set; }

        /// <summary>
        /// Właściwość z dostępnymi miastami
        /// </summary>
        List<Model.Projection.ComboBox> Cities { set; }

        /// <summary>
        /// Właściwość z miastem 
        /// </summary>
        int CityId { set; }

        /// <summary>
        /// Właściwość z dostępnymi województwami
        /// </summary>
        List<Model.Projection.ComboBox> Provinces { set; }

        /// <summary>
        /// Właściwość z województwem 
        /// </summary>
        string Province { get; }

    }
}
