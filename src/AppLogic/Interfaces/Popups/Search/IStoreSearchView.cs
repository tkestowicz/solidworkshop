﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;

namespace AppLogic.Interfaces.Search
{
    public interface IStoreSearchView : IPopupView, ISearchView
    {

        /// <summary>
        /// Właściwość do ustawiania kategorii
        /// </summary>
        List<Model.Projection.ComboBox> PartClasses { set; }

    }
}
