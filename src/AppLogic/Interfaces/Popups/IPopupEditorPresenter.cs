﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Events;

namespace AppLogic.Interfaces.Popups
{
    /// <summary>
    /// Interfejs logiki dla kontrolki edycji danych w wyskakującym okienku - implementacja wzorca strategia
    /// </summary>
    /// <typeparam name="T">Typ widoku, z którym jest związana logika</typeparam>
    public interface IPopupEditorPresenter<T> : IPopupPresenter<T>,
        ISaveEvent, ICancelEvent, ISaveSucceed where T: IPopupView
    {
    }
}
