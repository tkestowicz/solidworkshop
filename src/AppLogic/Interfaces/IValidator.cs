﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla klas walidacyjnych
    /// </summary>
    public interface IValidator
    {

        /// <summary>
        /// Pobiera informację, czy dane poddane weryfikacji są poprawne.
        /// </summary>
        bool Valid{ get; }

        /// <summary>
        /// Metoda weryfikująca dane.
        /// </summary>
        /// <typeparam name="T">Typ walidowanych danych</typeparam>
        /// <param name="data">Dane do weryfikacji</param>
        void Validate<T>(T data);
    }
}
