﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Events;

namespace AppLogic.Interfaces
{

    /// <summary>
    /// Interfejs dla klas będących adapterami logiki dla wyskakujących okienek
    /// </summary>
    public interface IPopupPresenterAdapter : IEventSubscriber
    {
        /// <summary>
        /// Metoda odpowiedzialna za wypięcie logiki z kontekstu aplikacji
        /// </summary>
        void Release();
    }
}
