﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Ogólny interfejs dla prezenterów
    /// </summary>
    public interface IPresenter
    {

        /// <summary>
        /// Metoda inicjująca obiekt
        /// </summary>
        void Initialize();

    }
}
