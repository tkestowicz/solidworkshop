﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla logiki edytorów, które wykorzystują więcej niż jedną encje
    /// </summary>
    public interface IRelationalEditorPresenter: IEditorPresenter
    {

        /// <summary>
        /// Metoda ładuje dane z powiązanych encji
        /// </summary>
        void LoadRelatedData();

    }
}
