﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Popups;
using AppLogic.Interfaces.Events;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla logiki wyszukiwania - implementacja wzorca strategia
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISearchPresenter<T> : IPopupPresenter,
        ISearchEvent, IClearEvent
        where T : IPopupView
    {
        /// <summary>
        /// Widok, z którym związana jest logika
        /// </summary>
        T View { get; set; }
    }
}
