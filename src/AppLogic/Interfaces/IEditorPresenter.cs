﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs dla logiki obsługującej formularze edycji i dodawania danych
    /// </summary>
    public interface IEditorPresenter : IPresenter, Interfaces.Events.IEventSubscriber,
        Events.IEditRecordEvent, Events.IAddEvent, Events.ICancelEvent,
        Events.ISaveEvent, Events.IPreviewRecord
    {

        /// <summary>
        /// Zapis zmian
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Walidacja wprowadzonych danych
        /// </summary>
        void Validate();

        /// <summary>
        /// Metoda przywraca prezentera i kontrolkę do stanu domyślnego
        /// </summary>
        void Reset();
    }
}
