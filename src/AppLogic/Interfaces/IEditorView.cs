﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Controls;
using AppLogic.Presenters;
using AppLogic.Interfaces.Popups.Search;

namespace AppLogic.Interfaces
{
    /// <summary>
    /// Interfejs bazowy dla wszystkich widoków, które umożliwają edycję danych
    /// </summary>
    public interface IEditorView : IBaseView, IControlView
    {
        /// <summary>
        /// Metoda inicjująca kontrolkę
        /// </summary>
        /// <param name="presenter">Logika do obsługi kontrolki</param>
        void Initialize(IEditorPresenter presenter);

        /// <summary>
        /// Metoda ukrywa SaveCancelControls
        /// </summary>
        void HideSaveCancelControls();

        /// <summary>
        /// Metoda pokazuje SaveCancelControls
        /// </summary>
        void ShowSaveCancelControls();
    }
}
