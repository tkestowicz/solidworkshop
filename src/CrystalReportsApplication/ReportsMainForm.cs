﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace CrystalReportsApplication
{
    public partial class ReportsMainForm : Form
    {
        public ReportsMainForm()
        {
            InitializeComponent();
        }

        private void showReportStoreAll()
        {
            foreach (IConnectionInfo connection in CrystalReportStore1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportStore1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportStore1;
        }

        private void showReportStoreAmountFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportStore2.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportStore2.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportStore2;
        }

        private void showReportStorePriceFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportStore3.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportStore3.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportStore3;
        }

        private void showReportStoreAmountAndPriceFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportStore4.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportStore4.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportStore4;
        }

       private void showReportEmployeeSalaryAll()
        {
            foreach (IConnectionInfo connection in CrystalReportEmployeeMonthAll1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportEmployeeMonthAll1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportEmployeeMonthAll1;
        }

       private void showReportEmployeeSalaryMonthFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportEmployee1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportEmployee1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportEmployee1;
        }

        private void showReportEmployeeOrdersAll()
        {
            foreach (IConnectionInfo connection in CrystalReportEmployeeOrdersAll1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportEmployeeOrdersAll1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportEmployeeOrdersAll1;
        }

        private void showReportEmployeeOrdersStatusFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportOrdersStatusFilter1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportOrdersStatusFilter1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportOrdersStatusFilter1;
        }

        private void showReportClientOrdersAll()
        {
            foreach (IConnectionInfo connection in CrystalReportClient1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportClient1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportClient1;
        }

        private void showReportClientOrdersStatusFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportClientOrderStatusFilter2.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportClientOrderStatusFilter2.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportClientOrderStatusFilter2;
        }

        private void showReportClientOrdersPriceFilter()
        {
            foreach (IConnectionInfo connection in CrystalReportClientOrdersPriceFilter1.DataSourceConnections)
            {
                // ustaw parametry połączenia raportowego
                connection.SetConnection(Properties.Settings.Default.ServerName, Properties.Settings.Default.DBName, true);
            }

            foreach (Table table in CrystalReportClientOrdersPriceFilter1.Database.Tables)
            {
                // workaround w celu odświeżenia lokalizacji
                table.Location += String.Empty;
            }

            crystalReportViewer1.ReportSource = CrystalReportClientOrdersPriceFilter1;
        }

        private void buttonShowRaport_Click(object sender, EventArgs e)
        {
            if (comboBoxStore.Text != "")
                switch (comboBoxStore.SelectedIndex)
                {
                    case -1: break;
                    case 0: showReportStoreAll();  break;
                    case 1: showReportStoreAmountFilter(); break;
                    case 2: showReportStorePriceFilter(); break;
                    case 3: showReportStoreAmountAndPriceFilter(); break;
                    default: break;
                }

            if (comboBoxEmployee.Text != "")
                switch (comboBoxEmployee.SelectedIndex)
                {
                    case -1: break;
                    case 0: showReportEmployeeSalaryAll(); break;
                    case 1: showReportEmployeeSalaryMonthFilter(); break;
                    case 2: showReportEmployeeOrdersAll(); break;
                    case 3: showReportEmployeeOrdersStatusFilter(); break;
                    default: break;
                }

            if (comboBoxClients.Text != "")
                switch (comboBoxClients.SelectedIndex)
                {
                    case -1: break;
                    case 0: showReportClientOrdersAll(); break;
                    case 1: showReportClientOrdersStatusFilter(); break;
                    case 2: showReportClientOrdersPriceFilter(); break;
                    default: break;
                }
        }

        private void comboBoxStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            int temp = comboBoxStore.SelectedIndex;
            comboBoxClients.SelectedIndex = -1;
            comboBoxEmployee.SelectedIndex = -1;
            comboBoxStore.SelectedIndex = temp;
        }

        private void comboBoxEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            int temp = comboBoxEmployee.SelectedIndex;
            comboBoxClients.SelectedIndex = -1;
            comboBoxStore.SelectedIndex = -1;
            comboBoxEmployee.SelectedIndex = temp;
        }

        private void comboBoxClients_SelectedIndexChanged(object sender, EventArgs e)
        {
            int temp = comboBoxClients.SelectedIndex;
            comboBoxEmployee.SelectedIndex = -1;
            comboBoxStore.SelectedIndex = -1;
            comboBoxClients.SelectedIndex = temp;
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
