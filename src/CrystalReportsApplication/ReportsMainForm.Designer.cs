﻿namespace CrystalReportsApplication
{
    partial class ReportsMainForm
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.buttonShowRaport = new System.Windows.Forms.Button();
            this.labelEmployee = new System.Windows.Forms.Label();
            this.labelStore = new System.Windows.Forms.Label();
            this.labelClients = new System.Windows.Forms.Label();
            this.comboBoxStore = new System.Windows.Forms.ComboBox();
            this.comboBoxClients = new System.Windows.Forms.ComboBox();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.panelReportSelection = new System.Windows.Forms.Panel();
            this.CrystalReportClient1 = new CrystalReportsApplication.CrystalReportClientOrdersAll();
            this.CrystalReportStore1 = new CrystalReportsApplication.CrystalReportStoreAll();
            this.CrystalReportEmployee1 = new CrystalReportsApplication.CrystalReportEmployeeSalaryMonthFilter();
            this.CrystalReportStore2 = new CrystalReportsApplication.CrystalReportStoreAmountFilter();
            this.CrystalReportStore3 = new CrystalReportsApplication.CrystalReportStorePriceFilter();
            this.CrystalReportStore4 = new CrystalReportsApplication.CrystalReportStoreAmountAndPriceFilter();
            this.CrystalReportEmployeeMonthAll1 = new CrystalReportsApplication.CrystalReportEmployeeSalaryAll();
            this.CrystalReportOrdersStatusFilter1 = new CrystalReportsApplication.CrystalReportEmployeeOrdersStatusFilter();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.CrystalReportClientOrderStatusFilter2 = new CrystalReportsApplication.CrystalReportClientOrdersStatusFilter();
            this.CrystalReportEmployeeOrdersAll1 = new CrystalReportsApplication.CrystalReportEmployeeOrdersAll();
            this.CrystalReportClientOrdersPriceFilter1 = new CrystalReportsApplication.CrystalReportClientOrdersPriceFilter();
            this.panelReportSelection.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonShowRaport
            // 
            this.buttonShowRaport.Location = new System.Drawing.Point(398, 30);
            this.buttonShowRaport.Name = "buttonShowRaport";
            this.buttonShowRaport.Size = new System.Drawing.Size(118, 32);
            this.buttonShowRaport.TabIndex = 1;
            this.buttonShowRaport.Text = "Pokaż raport";
            this.buttonShowRaport.UseVisualStyleBackColor = true;
            this.buttonShowRaport.Click += new System.EventHandler(this.buttonShowRaport_Click);
            // 
            // labelEmployee
            // 
            this.labelEmployee.AutoSize = true;
            this.labelEmployee.Location = new System.Drawing.Point(19, 13);
            this.labelEmployee.Name = "labelEmployee";
            this.labelEmployee.Size = new System.Drawing.Size(65, 13);
            this.labelEmployee.TabIndex = 4;
            this.labelEmployee.Text = "Pracownicy:";
            // 
            // labelStore
            // 
            this.labelStore.AutoSize = true;
            this.labelStore.Location = new System.Drawing.Point(31, 70);
            this.labelStore.Name = "labelStore";
            this.labelStore.Size = new System.Drawing.Size(53, 13);
            this.labelStore.TabIndex = 5;
            this.labelStore.Text = "Magazyn:";
            // 
            // labelClients
            // 
            this.labelClients.AutoSize = true;
            this.labelClients.Location = new System.Drawing.Point(43, 40);
            this.labelClients.Name = "labelClients";
            this.labelClients.Size = new System.Drawing.Size(41, 13);
            this.labelClients.TabIndex = 6;
            this.labelClients.Text = "Klienci:";
            // 
            // comboBoxStore
            // 
            this.comboBoxStore.FormattingEnabled = true;
            this.comboBoxStore.Items.AddRange(new object[] {
            "Części - wszystkie",
            "Części - filtr na ilość",
            "Części - filtr na cenę",
            "Części - filtr na ilość i cenę"});
            this.comboBoxStore.Location = new System.Drawing.Point(90, 67);
            this.comboBoxStore.Name = "comboBoxStore";
            this.comboBoxStore.Size = new System.Drawing.Size(291, 21);
            this.comboBoxStore.TabIndex = 7;
            this.comboBoxStore.SelectedIndexChanged += new System.EventHandler(this.comboBoxStore_SelectedIndexChanged);
            // 
            // comboBoxClients
            // 
            this.comboBoxClients.FormattingEnabled = true;
            this.comboBoxClients.Items.AddRange(new object[] {
            "Zlecenia - wszystkie",
            "Zlecenia - filtr na status",
            "Zlecenia - filtr na cenę"});
            this.comboBoxClients.Location = new System.Drawing.Point(90, 37);
            this.comboBoxClients.Name = "comboBoxClients";
            this.comboBoxClients.Size = new System.Drawing.Size(291, 21);
            this.comboBoxClients.TabIndex = 8;
            this.comboBoxClients.SelectedIndexChanged += new System.EventHandler(this.comboBoxClients_SelectedIndexChanged);
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Items.AddRange(new object[] {
            "Wypłaty - wszystkie",
            "Wypłaty - filtr na okres",
            "Zlecenia - wszystkie",
            "Zlecenia - filtr na status"});
            this.comboBoxEmployee.Location = new System.Drawing.Point(90, 10);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(291, 21);
            this.comboBoxEmployee.TabIndex = 9;
            this.comboBoxEmployee.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmployee_SelectedIndexChanged);
            // 
            // panelReportSelection
            // 
            this.panelReportSelection.Controls.Add(this.labelEmployee);
            this.panelReportSelection.Controls.Add(this.comboBoxEmployee);
            this.panelReportSelection.Controls.Add(this.buttonShowRaport);
            this.panelReportSelection.Controls.Add(this.comboBoxClients);
            this.panelReportSelection.Controls.Add(this.labelStore);
            this.panelReportSelection.Controls.Add(this.comboBoxStore);
            this.panelReportSelection.Controls.Add(this.labelClients);
            this.panelReportSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelReportSelection.Location = new System.Drawing.Point(0, 0);
            this.panelReportSelection.Name = "panelReportSelection";
            this.panelReportSelection.Size = new System.Drawing.Size(799, 99);
            this.panelReportSelection.TabIndex = 10;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 99);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(799, 467);
            this.crystalReportViewer1.TabIndex = 11;
            this.crystalReportViewer1.Load += new System.EventHandler(this.crystalReportViewer1_Load);
            // 
            // ReportsMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 566);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.panelReportSelection);
            this.Name = "ReportsMainForm";
            this.Text = "Raporty";
            this.panelReportSelection.ResumeLayout(false);
            this.panelReportSelection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonShowRaport;
        private System.Windows.Forms.Label labelEmployee;
        private System.Windows.Forms.Label labelStore;
        private System.Windows.Forms.Label labelClients;
        private System.Windows.Forms.ComboBox comboBoxStore;
        private System.Windows.Forms.ComboBox comboBoxClients;
        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Panel panelReportSelection;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private CrystalReportStoreAmountFilter CrystalReportStore2;
        private CrystalReportStoreAll CrystalReportStore1;
        private CrystalReportStoreAmountAndPriceFilter CrystalReportStore4;
        private CrystalReportStorePriceFilter CrystalReportStore3;
        private CrystalReportEmployeeSalaryMonthFilter CrystalReportEmployee1;
        private CrystalReportClientOrdersAll CrystalReportClient1;
        private CrystalReportEmployeeSalaryAll CrystalReportEmployeeMonthAll1;
        private CrystalReportEmployeeOrdersStatusFilter CrystalReportOrdersStatusFilter1;
        private CrystalReportClientOrdersStatusFilter CrystalReportClientOrderStatusFilter2;
        private CrystalReportEmployeeOrdersAll CrystalReportEmployeeOrdersAll1;
        private CrystalReportClientOrdersPriceFilter CrystalReportClientOrdersPriceFilter1;
    }
}

