﻿
USE [SWDb];
GO

-- Create Default Employee State

INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Nieokreślony');
	
INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Umowa o pracę');

INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Umowa zlecenie');

INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Zwolniony');

INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Emerytowany');

INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Praktykant');

INSERT INTO [dbo].[EmployeeStates] (Name)
	VALUES ('Stażysta');

-- Create Default Provinces

INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('dolnośląskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('kujawsko-pomorskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('lubelskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('lubuskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('łódzkie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('małopolskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('mazowieckie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('opolskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('podkarpackie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('podlaskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('pomorskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('śląskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('świętokrzyskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('warmińsko-mazurskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('wielkopolskie');
            
INSERT INTO [dbo].[Provinces] (Name)
	VALUES ('zachodniopomorskie');

-- Create Main: premissions

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('ClientsEdit', 'Klienci: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('ClientsView', 'Klienci: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('EmployeesEdit', 'Pracownicy: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('EmployeesView', 'Pracownicy: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('GroupsEdit', 'Grupy: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('GroupsView', 'Grupy: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('OrdersEdit', 'Zlecenia: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('OrdersView', 'Zlecenia: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('ServicesEdit', 'Usługi: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('ServicesView', 'Usługi: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('StoreEdit', 'Magazyn: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('StoreView', 'Magazyn: przeglądanie');

GO

-- Create Dictionary: premissions

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicPartClassEdit', 'Kategorie części: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicPartClassView', 'Kategorie części: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicServiceClassEdit', 'Kategorie usług: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicServiceClassView', 'Kategorie usług: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicServiceEdit', 'Usługi: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicServiceView', 'Usługi: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicCarBrandEdit', 'Marki samochodów: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicCarBrandView', 'Marki samochodów: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicCarModelEdit', 'Modele samochodów: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicCarModelView', 'Modele samochodów: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicColorEdit', 'Kolory samochodów: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicColorView', 'Kolory samochodów: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicFuelEdit', 'Rodzaje paliwa: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicFuelView', 'Rodzaje paliwa: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicServiceStatusEdit', 'Statusy zleceń: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicServiceStatusView', 'Statusy zleceń: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicProvincesEdit', 'Województwa: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicProvincesView', 'Województwa: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicEmployeeStateEdit', 'Statusy zatrudnień: edycja');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('DicEmployeeStateView', 'Statusy zatrudnień: przeglądanie');

INSERT INTO [dbo].[Permissions] (Identyficator, Name)
	VALUES ('ReportsView', 'Raporty: przeglądanie');

-- Create SuperAdmin group

INSERT INTO [dbo].[Groups] (Name, Description)
	VALUES ('Administratorzy', 'Użytkownicy w tej grupie mają dostęp do wszystkich funkcjonalności w systemie.');

GO

-- Connect SuperAdmin group with All: premissions

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 1);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 2);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 3);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 4);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 5);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 6);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 7);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 8);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 9);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 10);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 11);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 12);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 13);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 14);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 15);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 16);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 17);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 18);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 19);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 20);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 21);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 22);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 23);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 24);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 25);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 26);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 27);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 28);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 29);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 30);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 31);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 32);

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
	VALUES (1, 33);

-- Create Admin account 

INSERT INTO [dbo].[Cities] (Name, ProvinceId)
	VALUES ('Gliwice', 12);
GO
	
INSERT INTO [dbo].[Streets] (Name)
	VALUES ('Akademicka');
GO
	
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
	VALUES (NULL, NULL, 1, 1, '44-122');
GO

INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
	VALUES ('Wieczny', 'Student', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '65070919773', '338-460-84-32', '02-25-2010', NULL, NULL, NULL, NULL, 1);
GO

-- Connect Admin account with SuperAdmin group

INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
	VALUES (1, 1)
GO