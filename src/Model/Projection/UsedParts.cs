﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    [Serializable()]
    public class UsedParts
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Name { get; set; }

        [DataMemberAttribute()]
        public int Count { get; set; }
    }
}
