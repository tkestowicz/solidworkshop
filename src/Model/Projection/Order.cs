﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public class Order
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Description { get; set; }

        [DataMemberAttribute()]
        public double? Price { get; set; }

        [DataMemberAttribute()]
        public string Car { get; set; }

        [DataMemberAttribute()]
        public string Employee { get; set; }

        [DataMemberAttribute()]
        public string Status { get; set; }
    }
}
