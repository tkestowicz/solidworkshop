﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Projection
{

    /// <summary>
    /// Klasa wykorzystwana do prezentacji danych w listach rozwijanych
    /// </summary>
    public class ComboBox : IComparable<ComboBox>
    {
        public ComboBox() { }

        public ComboBox(int id, string value)
        {
            Id = id;
            Value = value;
        }

        /// <summary>
        /// Identyfikator rekordu
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Wartość rekordu
        /// </summary>
        public string Value{ get; set;}

        /// <summary cref="http://msdn.microsoft.com/en-us/library/tfakywbh.aspx">
        /// Metoda dokonuje porównania wartości dwóch obiektów
        /// </summary>
        /// <param name="other">Obiekt z którym będzie porównanie</param>
        /// <returns></returns>
        public int CompareTo(ComboBox other)
        {
            if (other == null)
                return 1;
            else
                return Value.CompareTo(other.Value);
        }
    }
}
