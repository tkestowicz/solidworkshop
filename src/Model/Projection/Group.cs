﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public class Group
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Name { get; set; }

        [DataMemberAttribute()]
        public string Description { get; set; }
    }
}
