﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public class Wage
    {

        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Month { get; set; }

        [DataMemberAttribute()]
        public double Salary { get; set; }

        [DataMemberAttribute()]
        public double Bonus { get; set; }
    }
}
