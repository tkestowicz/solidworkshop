﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public abstract class Dictionary
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Name { get; set; }
    }
}