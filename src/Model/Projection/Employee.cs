﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public class Employee
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Name { get; set; }

        [DataMemberAttribute()]
        public string Surname { get; set; }

        [DataMemberAttribute()]
        public string State { get; set; }

        [DataMemberAttribute()]
        public string Pesel { get; set; }

        [DataMemberAttribute()]
        public string Nip { get; set; }

        [DataMemberAttribute()]
        public string PhoneNumber { get; set; }

        [DataMemberAttribute()]
        public string Email { get; set; }
    }
}
