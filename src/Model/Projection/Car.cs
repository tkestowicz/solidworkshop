﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Interfaces;

namespace Model.Projection
{
    /// <summary>
    /// Klasa pomocnicza - stanowi uproszczony kontener na dane o samochodzie.
    /// </summary>
    public class Car : ICar
    {

        int _brandId, _modelId, _colorId, _fuelId = 0;
        string _regNumber, _bodyNumber = null;
        short? _engineCap, _prodYear = null;

        public int BrandId
        {
            get
            {
                return _brandId;
            }
            set
            {
                _brandId = value;
            }
        }

        public int ModelId
        {
            get
            {
                return _modelId;
            }
            set
            {
                _modelId = value;
            }
        }

        public int ColorId
        {
            get
            {
                return _colorId;
            }
            set
            {
                _colorId = value;
            }
        }

        public int FuelId
        {
            get
            {
                return _fuelId;
            }
            set
            {
                _fuelId = value;
            }
        }

        public string RegistrationNumber
        {
            get
            {
                return _regNumber;
            }
            set
            {
                _regNumber = value;
            }
        }

        public string BodyNumber
        {
            get
            {
                return _bodyNumber;
            }
            set
            {
                _bodyNumber = value;
            }
        }

        public short? EngineCapacity
        {
            get
            {
                return _engineCap;
            }
            set
            {
                _engineCap = value;
            }
        }

        public short? ProductionYear
        {
            get
            {
                return _prodYear;
            }
            set
            {
                _prodYear = value;
            }
        }
    }
}
