﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public class Service
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public string Name { get; set; }

        [DataMemberAttribute()]
        public string Description { get; set; }

        [DataMemberAttribute()]
        public double Price { get; set; }

        [DataMemberAttribute()]
        public string ClassName { get; set; }
    }
}
