﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Projection
{
    /// <summary>
    /// Specyficzny typ wykorzystywany do prezentowania danych w gridzie
    /// </summary>
    [Serializable()]
    public class ExecutingService
    {
        [DataMemberAttribute()]
        public int Id { get; set; }

        [DataMemberAttribute()]
        public int EmployeeId { get; set; }

        [DataMemberAttribute()]
        public int OrderId { get; set; }

        [DataMemberAttribute()]
        public int StatusId { get; set; }

        [DataMemberAttribute()]
        public int ServiceId { get; set; }
    }
}