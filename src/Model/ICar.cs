﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Interfaces
{

    /// <summary>
    /// Interfejs obiektu, który udostępnia dane samochodu.
    /// </summary>
    public interface ICar
    {

        /// <summary>
        /// Właściwość z marką samochodu
        /// </summary>
        int BrandId { get; set; }

        /// <summary>
        /// Właściwość z modelem samochodu
        /// </summary>
        int ModelId { get; set; }

        /// <summary>
        /// Właściwość z kolorem samochodu
        /// </summary>
        int ColorId { get; set; }

        /// <summary>
        /// Właściwość z paliwem samochodu
        /// </summary>
        int FuelId { get; set; }

        /// <summary>
        /// Właściwość z numerem rejestracyjnym samochodu
        /// </summary>
        string RegistrationNumber { get; set; }

        /// <summary>
        /// Właściwość z numerem nadwozia samochodu
        /// </summary>
        string BodyNumber { get; set; }

        /// <summary>
        /// Właściwość z pojemnością silnika samochodu
        /// </summary>
        short? EngineCapacity { get; set; }

        /// <summary>
        /// Właściwość z rokiem produkcji samochodu
        /// </summary>
        short? ProductionYear { get; set; }

    }
}
