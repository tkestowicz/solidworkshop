
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 09/23/2012 15:23:51
-- Generated from EDMX file: C:\Users\Akuzis\Desktop\solidasd\src\Model\SWDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SWDb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_EmployeeGroup_Employees]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EmployeeGroup] DROP CONSTRAINT [FK_EmployeeGroup_Employees];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeGroup_Groups]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EmployeeGroup] DROP CONSTRAINT [FK_EmployeeGroup_Groups];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupPermission_Groups]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupPermission] DROP CONSTRAINT [FK_GroupPermission_Groups];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupPermission_Permissions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupPermission] DROP CONSTRAINT [FK_GroupPermission_Permissions];
GO
IF OBJECT_ID(N'[dbo].[FK_WageEmployee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Wages] DROP CONSTRAINT [FK_WageEmployee];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeAddress]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_EmployeeAddress];
GO
IF OBJECT_ID(N'[dbo].[FK_AddressStreet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Addresses] DROP CONSTRAINT [FK_AddressStreet];
GO
IF OBJECT_ID(N'[dbo].[FK_AddressCity]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Addresses] DROP CONSTRAINT [FK_AddressCity];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientDiscount]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Clients] DROP CONSTRAINT [FK_ClientDiscount];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientCar_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClientCar] DROP CONSTRAINT [FK_ClientCar_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientCar_Car]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClientCar] DROP CONSTRAINT [FK_ClientCar_Car];
GO
IF OBJECT_ID(N'[dbo].[FK_CarCarModel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cars] DROP CONSTRAINT [FK_CarCarModel];
GO
IF OBJECT_ID(N'[dbo].[FK_CarFuel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cars] DROP CONSTRAINT [FK_CarFuel];
GO
IF OBJECT_ID(N'[dbo].[FK_CarColor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cars] DROP CONSTRAINT [FK_CarColor];
GO
IF OBJECT_ID(N'[dbo].[FK_CarOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_CarOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_EmployeeOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderOrderStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_OrderOrderStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderOrderLogs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrdersLogs] DROP CONSTRAINT [FK_OrderOrderLogs];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeOrderLogs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrdersLogs] DROP CONSTRAINT [FK_EmployeeOrderLogs];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeExecutingService]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExecutingServices] DROP CONSTRAINT [FK_EmployeeExecutingService];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderExecutingService]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExecutingServices] DROP CONSTRAINT [FK_OrderExecutingService];
GO
IF OBJECT_ID(N'[dbo].[FK_ExecutingServiceService]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExecutingServices] DROP CONSTRAINT [FK_ExecutingServiceService];
GO
IF OBJECT_ID(N'[dbo].[FK_ServiceServiceClass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Services] DROP CONSTRAINT [FK_ServiceServiceClass];
GO
IF OBJECT_ID(N'[dbo].[FK_ExecutingServiceUsedPart]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsedParts] DROP CONSTRAINT [FK_ExecutingServiceUsedPart];
GO
IF OBJECT_ID(N'[dbo].[FK_UsedPartStore]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsedParts] DROP CONSTRAINT [FK_UsedPartStore];
GO
IF OBJECT_ID(N'[dbo].[FK_StorePartClass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Stores] DROP CONSTRAINT [FK_StorePartClass];
GO
IF OBJECT_ID(N'[dbo].[FK_ExecutingServiceServiceStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExecutingServices] DROP CONSTRAINT [FK_ExecutingServiceServiceStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_ExecutingServiceServiceLogs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServicesLogs] DROP CONSTRAINT [FK_ExecutingServiceServiceLogs];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeServiceLogs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServicesLogs] DROP CONSTRAINT [FK_EmployeeServiceLogs];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientAddress]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Clients] DROP CONSTRAINT [FK_ClientAddress];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderStatusOrderLogs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrdersLogs] DROP CONSTRAINT [FK_OrderStatusOrderLogs];
GO
IF OBJECT_ID(N'[dbo].[FK_ServiceStatusServiceLogs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServicesLogs] DROP CONSTRAINT [FK_ServiceStatusServiceLogs];
GO
IF OBJECT_ID(N'[dbo].[FK_CarBrandCarModel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CarModels] DROP CONSTRAINT [FK_CarBrandCarModel];
GO
IF OBJECT_ID(N'[dbo].[FK_ProvinceCity]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cities] DROP CONSTRAINT [FK_ProvinceCity];
GO
IF OBJECT_ID(N'[dbo].[FK_StateEmployee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_StateEmployee];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Employees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employees];
GO
IF OBJECT_ID(N'[dbo].[Groups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Groups];
GO
IF OBJECT_ID(N'[dbo].[Permissions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Permissions];
GO
IF OBJECT_ID(N'[dbo].[Wages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Wages];
GO
IF OBJECT_ID(N'[dbo].[Addresses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Addresses];
GO
IF OBJECT_ID(N'[dbo].[Streets]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Streets];
GO
IF OBJECT_ID(N'[dbo].[Cities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cities];
GO
IF OBJECT_ID(N'[dbo].[Clients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Clients];
GO
IF OBJECT_ID(N'[dbo].[Discounts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Discounts];
GO
IF OBJECT_ID(N'[dbo].[Cars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cars];
GO
IF OBJECT_ID(N'[dbo].[CarBrands]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CarBrands];
GO
IF OBJECT_ID(N'[dbo].[CarModels]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CarModels];
GO
IF OBJECT_ID(N'[dbo].[Fuels]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Fuels];
GO
IF OBJECT_ID(N'[dbo].[Colors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Colors];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[OrderStatuses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderStatuses];
GO
IF OBJECT_ID(N'[dbo].[OrdersLogs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrdersLogs];
GO
IF OBJECT_ID(N'[dbo].[ExecutingServices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExecutingServices];
GO
IF OBJECT_ID(N'[dbo].[Services]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Services];
GO
IF OBJECT_ID(N'[dbo].[ServiceClasses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServiceClasses];
GO
IF OBJECT_ID(N'[dbo].[UsedParts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsedParts];
GO
IF OBJECT_ID(N'[dbo].[Stores]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Stores];
GO
IF OBJECT_ID(N'[dbo].[PartClasses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PartClasses];
GO
IF OBJECT_ID(N'[dbo].[ServiceStatuses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServiceStatuses];
GO
IF OBJECT_ID(N'[dbo].[ServicesLogs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServicesLogs];
GO
IF OBJECT_ID(N'[dbo].[Provinces]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Provinces];
GO
IF OBJECT_ID(N'[dbo].[EmployeeStates]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmployeeStates];
GO
IF OBJECT_ID(N'[dbo].[EmployeeGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmployeeGroup];
GO
IF OBJECT_ID(N'[dbo].[GroupPermission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupPermission];
GO
IF OBJECT_ID(N'[dbo].[ClientCar]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientCar];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [EmployeeId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Surname] nvarchar(50)  NOT NULL,
    [Login] nvarchar(25)  NULL,
    [Password] nchar(32)  NULL,
    [Pesel] nchar(11)  NOT NULL,
    [Nip] nchar(13)  NOT NULL,
    [HireDate] datetime  NOT NULL,
    [Qualifications] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(15)  NULL,
    [Email] nvarchar(40)  NULL,
    [ReleaseDate] datetime  NULL,
    [EmployeeStateId] int  NOT NULL,
    [Address_AddressId] int  NOT NULL
);
GO

-- Creating table 'Groups'
CREATE TABLE [dbo].[Groups] (
    [GroupId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'Permissions'
CREATE TABLE [dbo].[Permissions] (
    [PermissionId] int IDENTITY(1,1) NOT NULL,
    [Identyficator] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Wages'
CREATE TABLE [dbo].[Wages] (
    [WageId] int IDENTITY(1,1) NOT NULL,
    [Month] nvarchar(20)  NOT NULL,
    [Salary] float  NOT NULL,
    [Bonus] float  NULL,
    [EmployeeEmployeeId] int  NOT NULL
);
GO

-- Creating table 'Addresses'
CREATE TABLE [dbo].[Addresses] (
    [AddressId] int IDENTITY(1,1) NOT NULL,
    [HouseNumber] nvarchar(4)  NULL,
    [ApartmentNumber] nvarchar(4)  NULL,
    [StreetStreetId] int  NOT NULL,
    [CityCityId] int  NOT NULL,
    [PostCode] nchar(6)  NOT NULL
);
GO

-- Creating table 'Streets'
CREATE TABLE [dbo].[Streets] (
    [StreetId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(40)  NOT NULL
);
GO

-- Creating table 'Cities'
CREATE TABLE [dbo].[Cities] (
    [CityId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(40)  NOT NULL,
    [ProvinceId] int  NOT NULL
);
GO

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [ClientId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Surname] nvarchar(50)  NOT NULL,
    [Pesel] nchar(11)  NOT NULL,
    [PhoneNumber] nvarchar(15)  NULL,
    [Email] nvarchar(40)  NULL,
    [DiscountId] int  NULL,
    [Address_AddressId] int  NOT NULL
);
GO

-- Creating table 'Discounts'
CREATE TABLE [dbo].[Discounts] (
    [DiscountId] int IDENTITY(1,1) NOT NULL,
    [DicountPercent] smallint  NOT NULL
);
GO

-- Creating table 'Cars'
CREATE TABLE [dbo].[Cars] (
    [CarId] int IDENTITY(1,1) NOT NULL,
    [RegistrationNumber] nvarchar(7)  NOT NULL,
    [BodyNumber] nvarchar(17)  NOT NULL,
    [EngineCapacity] smallint  NOT NULL,
    [ProductionYear] smallint  NOT NULL,
    [CarModelId] int  NOT NULL,
    [FuelId] int  NOT NULL,
    [ColorId] int  NOT NULL
);
GO

-- Creating table 'CarBrands'
CREATE TABLE [dbo].[CarBrands] (
    [CarBrandId] int IDENTITY(1,1) NOT NULL,
    [BrandName] nvarchar(25)  NOT NULL
);
GO

-- Creating table 'CarModels'
CREATE TABLE [dbo].[CarModels] (
    [CarModelId] int IDENTITY(1,1) NOT NULL,
    [ModelName] nvarchar(30)  NOT NULL,
    [CarBrandId] int  NOT NULL
);
GO

-- Creating table 'Fuels'
CREATE TABLE [dbo].[Fuels] (
    [FuelId] int IDENTITY(1,1) NOT NULL,
    [FuelName] nvarchar(15)  NOT NULL
);
GO

-- Creating table 'Colors'
CREATE TABLE [dbo].[Colors] (
    [ColorId] int IDENTITY(1,1) NOT NULL,
    [ColorName] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [OrderId] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Price] float  NULL,
    [CarCarId] int  NOT NULL,
    [EmployeeEmployeeId] int  NOT NULL,
    [OrderStatuses_OrderStatusId] int  NOT NULL
);
GO

-- Creating table 'OrderStatuses'
CREATE TABLE [dbo].[OrderStatuses] (
    [OrderStatusId] int IDENTITY(1,1) NOT NULL,
    [OrderStatusName] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'OrdersLogs'
CREATE TABLE [dbo].[OrdersLogs] (
    [OrderLogsId] int IDENTITY(1,1) NOT NULL,
    [ChangeTime] datetime  NOT NULL,
    [OrderOrderId] int  NOT NULL,
    [OrderEmployeeEmployeeId] int  NOT NULL,
    [OrderCarCarId] int  NOT NULL,
    [EmployeeEmployeeId] int  NOT NULL,
    [OrderStatuses_OrderStatusId] int  NOT NULL
);
GO

-- Creating table 'ExecutingServices'
CREATE TABLE [dbo].[ExecutingServices] (
    [ExecutingServiceId] int IDENTITY(1,1) NOT NULL,
    [EmployeeEmployeeId] int  NOT NULL,
    [OrderOrderId] int  NOT NULL,
    [ServiceServiceId] int  NOT NULL,
    [ServiceStatuses_ServiceStatusId] int  NOT NULL
);
GO

-- Creating table 'Services'
CREATE TABLE [dbo].[Services] (
    [ServiceId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(25)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Price] float  NOT NULL,
    [ServiceClassServiceClassId] int  NOT NULL
);
GO

-- Creating table 'ServiceClasses'
CREATE TABLE [dbo].[ServiceClasses] (
    [ServiceClassId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(25)  NOT NULL
);
GO

-- Creating table 'UsedParts'
CREATE TABLE [dbo].[UsedParts] (
    [UsedPartId] int IDENTITY(1,1) NOT NULL,
    [UsedPartsNumber] int  NOT NULL,
    [ExecutingServiceExecutingServiceId] int  NOT NULL,
    [StoreStoreId] int  NOT NULL
);
GO

-- Creating table 'Stores'
CREATE TABLE [dbo].[Stores] (
    [StoreId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(25)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [AvailabileAmount] int  NOT NULL,
    [UnitPrice] float  NOT NULL,
    [PartClassPartClassId] int  NOT NULL
);
GO

-- Creating table 'PartClasses'
CREATE TABLE [dbo].[PartClasses] (
    [PartClassId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(25)  NOT NULL
);
GO

-- Creating table 'ServiceStatuses'
CREATE TABLE [dbo].[ServiceStatuses] (
    [ServiceStatusId] int IDENTITY(1,1) NOT NULL,
    [ServiceStatusName] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'ServicesLogs'
CREATE TABLE [dbo].[ServicesLogs] (
    [ServiceLogsId] int IDENTITY(1,1) NOT NULL,
    [ChangeTime] datetime  NOT NULL,
    [ExecutingServiceExecutingServiceId] int  NOT NULL,
    [EmployeeEmployeeId] int  NOT NULL,
    [ServiceStatuses_ServiceStatusId] int  NOT NULL
);
GO

-- Creating table 'Provinces'
CREATE TABLE [dbo].[Provinces] (
    [ProvinceId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'EmployeeStates'
CREATE TABLE [dbo].[EmployeeStates] (
    [EmployeeStateId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'EmployeeGroup'
CREATE TABLE [dbo].[EmployeeGroup] (
    [Employees_EmployeeId] int  NOT NULL,
    [Groups_GroupId] int  NOT NULL
);
GO

-- Creating table 'GroupPermission'
CREATE TABLE [dbo].[GroupPermission] (
    [Groups_GroupId] int  NOT NULL,
    [Permissions_PermissionId] int  NOT NULL
);
GO

-- Creating table 'ClientCar'
CREATE TABLE [dbo].[ClientCar] (
    [Clients_ClientId] int  NOT NULL,
    [Cars_CarId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [EmployeeId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([EmployeeId] ASC);
GO

-- Creating primary key on [GroupId] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [PK_Groups]
    PRIMARY KEY CLUSTERED ([GroupId] ASC);
GO

-- Creating primary key on [PermissionId] in table 'Permissions'
ALTER TABLE [dbo].[Permissions]
ADD CONSTRAINT [PK_Permissions]
    PRIMARY KEY CLUSTERED ([PermissionId] ASC);
GO

-- Creating primary key on [WageId] in table 'Wages'
ALTER TABLE [dbo].[Wages]
ADD CONSTRAINT [PK_Wages]
    PRIMARY KEY CLUSTERED ([WageId] ASC);
GO

-- Creating primary key on [AddressId] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [PK_Addresses]
    PRIMARY KEY CLUSTERED ([AddressId] ASC);
GO

-- Creating primary key on [StreetId] in table 'Streets'
ALTER TABLE [dbo].[Streets]
ADD CONSTRAINT [PK_Streets]
    PRIMARY KEY CLUSTERED ([StreetId] ASC);
GO

-- Creating primary key on [CityId] in table 'Cities'
ALTER TABLE [dbo].[Cities]
ADD CONSTRAINT [PK_Cities]
    PRIMARY KEY CLUSTERED ([CityId] ASC);
GO

-- Creating primary key on [ClientId] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([ClientId] ASC);
GO

-- Creating primary key on [DiscountId] in table 'Discounts'
ALTER TABLE [dbo].[Discounts]
ADD CONSTRAINT [PK_Discounts]
    PRIMARY KEY CLUSTERED ([DiscountId] ASC);
GO

-- Creating primary key on [CarId] in table 'Cars'
ALTER TABLE [dbo].[Cars]
ADD CONSTRAINT [PK_Cars]
    PRIMARY KEY CLUSTERED ([CarId] ASC);
GO

-- Creating primary key on [CarBrandId] in table 'CarBrands'
ALTER TABLE [dbo].[CarBrands]
ADD CONSTRAINT [PK_CarBrands]
    PRIMARY KEY CLUSTERED ([CarBrandId] ASC);
GO

-- Creating primary key on [CarModelId] in table 'CarModels'
ALTER TABLE [dbo].[CarModels]
ADD CONSTRAINT [PK_CarModels]
    PRIMARY KEY CLUSTERED ([CarModelId] ASC);
GO

-- Creating primary key on [FuelId] in table 'Fuels'
ALTER TABLE [dbo].[Fuels]
ADD CONSTRAINT [PK_Fuels]
    PRIMARY KEY CLUSTERED ([FuelId] ASC);
GO

-- Creating primary key on [ColorId] in table 'Colors'
ALTER TABLE [dbo].[Colors]
ADD CONSTRAINT [PK_Colors]
    PRIMARY KEY CLUSTERED ([ColorId] ASC);
GO

-- Creating primary key on [OrderId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([OrderId] ASC);
GO

-- Creating primary key on [OrderStatusId] in table 'OrderStatuses'
ALTER TABLE [dbo].[OrderStatuses]
ADD CONSTRAINT [PK_OrderStatuses]
    PRIMARY KEY CLUSTERED ([OrderStatusId] ASC);
GO

-- Creating primary key on [OrderLogsId] in table 'OrdersLogs'
ALTER TABLE [dbo].[OrdersLogs]
ADD CONSTRAINT [PK_OrdersLogs]
    PRIMARY KEY CLUSTERED ([OrderLogsId] ASC);
GO

-- Creating primary key on [ExecutingServiceId] in table 'ExecutingServices'
ALTER TABLE [dbo].[ExecutingServices]
ADD CONSTRAINT [PK_ExecutingServices]
    PRIMARY KEY CLUSTERED ([ExecutingServiceId] ASC);
GO

-- Creating primary key on [ServiceId] in table 'Services'
ALTER TABLE [dbo].[Services]
ADD CONSTRAINT [PK_Services]
    PRIMARY KEY CLUSTERED ([ServiceId] ASC);
GO

-- Creating primary key on [ServiceClassId] in table 'ServiceClasses'
ALTER TABLE [dbo].[ServiceClasses]
ADD CONSTRAINT [PK_ServiceClasses]
    PRIMARY KEY CLUSTERED ([ServiceClassId] ASC);
GO

-- Creating primary key on [UsedPartId] in table 'UsedParts'
ALTER TABLE [dbo].[UsedParts]
ADD CONSTRAINT [PK_UsedParts]
    PRIMARY KEY CLUSTERED ([UsedPartId] ASC);
GO

-- Creating primary key on [StoreId] in table 'Stores'
ALTER TABLE [dbo].[Stores]
ADD CONSTRAINT [PK_Stores]
    PRIMARY KEY CLUSTERED ([StoreId] ASC);
GO

-- Creating primary key on [PartClassId] in table 'PartClasses'
ALTER TABLE [dbo].[PartClasses]
ADD CONSTRAINT [PK_PartClasses]
    PRIMARY KEY CLUSTERED ([PartClassId] ASC);
GO

-- Creating primary key on [ServiceStatusId] in table 'ServiceStatuses'
ALTER TABLE [dbo].[ServiceStatuses]
ADD CONSTRAINT [PK_ServiceStatuses]
    PRIMARY KEY CLUSTERED ([ServiceStatusId] ASC);
GO

-- Creating primary key on [ServiceLogsId] in table 'ServicesLogs'
ALTER TABLE [dbo].[ServicesLogs]
ADD CONSTRAINT [PK_ServicesLogs]
    PRIMARY KEY CLUSTERED ([ServiceLogsId] ASC);
GO

-- Creating primary key on [ProvinceId] in table 'Provinces'
ALTER TABLE [dbo].[Provinces]
ADD CONSTRAINT [PK_Provinces]
    PRIMARY KEY CLUSTERED ([ProvinceId] ASC);
GO

-- Creating primary key on [EmployeeStateId] in table 'EmployeeStates'
ALTER TABLE [dbo].[EmployeeStates]
ADD CONSTRAINT [PK_EmployeeStates]
    PRIMARY KEY CLUSTERED ([EmployeeStateId] ASC);
GO

-- Creating primary key on [Employees_EmployeeId], [Groups_GroupId] in table 'EmployeeGroup'
ALTER TABLE [dbo].[EmployeeGroup]
ADD CONSTRAINT [PK_EmployeeGroup]
    PRIMARY KEY NONCLUSTERED ([Employees_EmployeeId], [Groups_GroupId] ASC);
GO

-- Creating primary key on [Groups_GroupId], [Permissions_PermissionId] in table 'GroupPermission'
ALTER TABLE [dbo].[GroupPermission]
ADD CONSTRAINT [PK_GroupPermission]
    PRIMARY KEY NONCLUSTERED ([Groups_GroupId], [Permissions_PermissionId] ASC);
GO

-- Creating primary key on [Clients_ClientId], [Cars_CarId] in table 'ClientCar'
ALTER TABLE [dbo].[ClientCar]
ADD CONSTRAINT [PK_ClientCar]
    PRIMARY KEY NONCLUSTERED ([Clients_ClientId], [Cars_CarId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Employees_EmployeeId] in table 'EmployeeGroup'
ALTER TABLE [dbo].[EmployeeGroup]
ADD CONSTRAINT [FK_EmployeeGroup_Employees]
    FOREIGN KEY ([Employees_EmployeeId])
    REFERENCES [dbo].[Employees]
        ([EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Groups_GroupId] in table 'EmployeeGroup'
ALTER TABLE [dbo].[EmployeeGroup]
ADD CONSTRAINT [FK_EmployeeGroup_Groups]
    FOREIGN KEY ([Groups_GroupId])
    REFERENCES [dbo].[Groups]
        ([GroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeGroup_Groups'
CREATE INDEX [IX_FK_EmployeeGroup_Groups]
ON [dbo].[EmployeeGroup]
    ([Groups_GroupId]);
GO

-- Creating foreign key on [Groups_GroupId] in table 'GroupPermission'
ALTER TABLE [dbo].[GroupPermission]
ADD CONSTRAINT [FK_GroupPermission_Groups]
    FOREIGN KEY ([Groups_GroupId])
    REFERENCES [dbo].[Groups]
        ([GroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Permissions_PermissionId] in table 'GroupPermission'
ALTER TABLE [dbo].[GroupPermission]
ADD CONSTRAINT [FK_GroupPermission_Permissions]
    FOREIGN KEY ([Permissions_PermissionId])
    REFERENCES [dbo].[Permissions]
        ([PermissionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupPermission_Permissions'
CREATE INDEX [IX_FK_GroupPermission_Permissions]
ON [dbo].[GroupPermission]
    ([Permissions_PermissionId]);
GO

-- Creating foreign key on [EmployeeEmployeeId] in table 'Wages'
ALTER TABLE [dbo].[Wages]
ADD CONSTRAINT [FK_WageEmployee]
    FOREIGN KEY ([EmployeeEmployeeId])
    REFERENCES [dbo].[Employees]
        ([EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_WageEmployee'
CREATE INDEX [IX_FK_WageEmployee]
ON [dbo].[Wages]
    ([EmployeeEmployeeId]);
GO

-- Creating foreign key on [Address_AddressId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_EmployeeAddress]
    FOREIGN KEY ([Address_AddressId])
    REFERENCES [dbo].[Addresses]
        ([AddressId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeAddress'
CREATE INDEX [IX_FK_EmployeeAddress]
ON [dbo].[Employees]
    ([Address_AddressId]);
GO

-- Creating foreign key on [StreetStreetId] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [FK_AddressStreet]
    FOREIGN KEY ([StreetStreetId])
    REFERENCES [dbo].[Streets]
        ([StreetId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AddressStreet'
CREATE INDEX [IX_FK_AddressStreet]
ON [dbo].[Addresses]
    ([StreetStreetId]);
GO

-- Creating foreign key on [CityCityId] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [FK_AddressCity]
    FOREIGN KEY ([CityCityId])
    REFERENCES [dbo].[Cities]
        ([CityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AddressCity'
CREATE INDEX [IX_FK_AddressCity]
ON [dbo].[Addresses]
    ([CityCityId]);
GO

-- Creating foreign key on [DiscountId] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [FK_ClientDiscount]
    FOREIGN KEY ([DiscountId])
    REFERENCES [dbo].[Discounts]
        ([DiscountId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientDiscount'
CREATE INDEX [IX_FK_ClientDiscount]
ON [dbo].[Clients]
    ([DiscountId]);
GO

-- Creating foreign key on [Clients_ClientId] in table 'ClientCar'
ALTER TABLE [dbo].[ClientCar]
ADD CONSTRAINT [FK_ClientCar_Client]
    FOREIGN KEY ([Clients_ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Cars_CarId] in table 'ClientCar'
ALTER TABLE [dbo].[ClientCar]
ADD CONSTRAINT [FK_ClientCar_Car]
    FOREIGN KEY ([Cars_CarId])
    REFERENCES [dbo].[Cars]
        ([CarId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientCar_Car'
CREATE INDEX [IX_FK_ClientCar_Car]
ON [dbo].[ClientCar]
    ([Cars_CarId]);
GO

-- Creating foreign key on [CarModelId] in table 'Cars'
ALTER TABLE [dbo].[Cars]
ADD CONSTRAINT [FK_CarCarModel]
    FOREIGN KEY ([CarModelId])
    REFERENCES [dbo].[CarModels]
        ([CarModelId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarCarModel'
CREATE INDEX [IX_FK_CarCarModel]
ON [dbo].[Cars]
    ([CarModelId]);
GO

-- Creating foreign key on [FuelId] in table 'Cars'
ALTER TABLE [dbo].[Cars]
ADD CONSTRAINT [FK_CarFuel]
    FOREIGN KEY ([FuelId])
    REFERENCES [dbo].[Fuels]
        ([FuelId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarFuel'
CREATE INDEX [IX_FK_CarFuel]
ON [dbo].[Cars]
    ([FuelId]);
GO

-- Creating foreign key on [ColorId] in table 'Cars'
ALTER TABLE [dbo].[Cars]
ADD CONSTRAINT [FK_CarColor]
    FOREIGN KEY ([ColorId])
    REFERENCES [dbo].[Colors]
        ([ColorId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarColor'
CREATE INDEX [IX_FK_CarColor]
ON [dbo].[Cars]
    ([ColorId]);
GO

-- Creating foreign key on [CarCarId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_CarOrder]
    FOREIGN KEY ([CarCarId])
    REFERENCES [dbo].[Cars]
        ([CarId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarOrder'
CREATE INDEX [IX_FK_CarOrder]
ON [dbo].[Orders]
    ([CarCarId]);
GO

-- Creating foreign key on [EmployeeEmployeeId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_EmployeeOrder]
    FOREIGN KEY ([EmployeeEmployeeId])
    REFERENCES [dbo].[Employees]
        ([EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeOrder'
CREATE INDEX [IX_FK_EmployeeOrder]
ON [dbo].[Orders]
    ([EmployeeEmployeeId]);
GO

-- Creating foreign key on [OrderStatuses_OrderStatusId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_OrderOrderStatus]
    FOREIGN KEY ([OrderStatuses_OrderStatusId])
    REFERENCES [dbo].[OrderStatuses]
        ([OrderStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderOrderStatus'
CREATE INDEX [IX_FK_OrderOrderStatus]
ON [dbo].[Orders]
    ([OrderStatuses_OrderStatusId]);
GO

-- Creating foreign key on [OrderOrderId] in table 'OrdersLogs'
ALTER TABLE [dbo].[OrdersLogs]
ADD CONSTRAINT [FK_OrderOrderLogs]
    FOREIGN KEY ([OrderOrderId])
    REFERENCES [dbo].[Orders]
        ([OrderId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderOrderLogs'
CREATE INDEX [IX_FK_OrderOrderLogs]
ON [dbo].[OrdersLogs]
    ([OrderOrderId]);
GO

-- Creating foreign key on [EmployeeEmployeeId] in table 'OrdersLogs'
ALTER TABLE [dbo].[OrdersLogs]
ADD CONSTRAINT [FK_EmployeeOrderLogs]
    FOREIGN KEY ([EmployeeEmployeeId])
    REFERENCES [dbo].[Employees]
        ([EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeOrderLogs'
CREATE INDEX [IX_FK_EmployeeOrderLogs]
ON [dbo].[OrdersLogs]
    ([EmployeeEmployeeId]);
GO

-- Creating foreign key on [EmployeeEmployeeId] in table 'ExecutingServices'
ALTER TABLE [dbo].[ExecutingServices]
ADD CONSTRAINT [FK_EmployeeExecutingService]
    FOREIGN KEY ([EmployeeEmployeeId])
    REFERENCES [dbo].[Employees]
        ([EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeExecutingService'
CREATE INDEX [IX_FK_EmployeeExecutingService]
ON [dbo].[ExecutingServices]
    ([EmployeeEmployeeId]);
GO

-- Creating foreign key on [OrderOrderId] in table 'ExecutingServices'
ALTER TABLE [dbo].[ExecutingServices]
ADD CONSTRAINT [FK_OrderExecutingService]
    FOREIGN KEY ([OrderOrderId])
    REFERENCES [dbo].[Orders]
        ([OrderId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderExecutingService'
CREATE INDEX [IX_FK_OrderExecutingService]
ON [dbo].[ExecutingServices]
    ([OrderOrderId]);
GO

-- Creating foreign key on [ServiceServiceId] in table 'ExecutingServices'
ALTER TABLE [dbo].[ExecutingServices]
ADD CONSTRAINT [FK_ExecutingServiceService]
    FOREIGN KEY ([ServiceServiceId])
    REFERENCES [dbo].[Services]
        ([ServiceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExecutingServiceService'
CREATE INDEX [IX_FK_ExecutingServiceService]
ON [dbo].[ExecutingServices]
    ([ServiceServiceId]);
GO

-- Creating foreign key on [ServiceClassServiceClassId] in table 'Services'
ALTER TABLE [dbo].[Services]
ADD CONSTRAINT [FK_ServiceServiceClass]
    FOREIGN KEY ([ServiceClassServiceClassId])
    REFERENCES [dbo].[ServiceClasses]
        ([ServiceClassId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceServiceClass'
CREATE INDEX [IX_FK_ServiceServiceClass]
ON [dbo].[Services]
    ([ServiceClassServiceClassId]);
GO

-- Creating foreign key on [ExecutingServiceExecutingServiceId] in table 'UsedParts'
ALTER TABLE [dbo].[UsedParts]
ADD CONSTRAINT [FK_ExecutingServiceUsedPart]
    FOREIGN KEY ([ExecutingServiceExecutingServiceId])
    REFERENCES [dbo].[ExecutingServices]
        ([ExecutingServiceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExecutingServiceUsedPart'
CREATE INDEX [IX_FK_ExecutingServiceUsedPart]
ON [dbo].[UsedParts]
    ([ExecutingServiceExecutingServiceId]);
GO

-- Creating foreign key on [StoreStoreId] in table 'UsedParts'
ALTER TABLE [dbo].[UsedParts]
ADD CONSTRAINT [FK_UsedPartStore]
    FOREIGN KEY ([StoreStoreId])
    REFERENCES [dbo].[Stores]
        ([StoreId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsedPartStore'
CREATE INDEX [IX_FK_UsedPartStore]
ON [dbo].[UsedParts]
    ([StoreStoreId]);
GO

-- Creating foreign key on [PartClassPartClassId] in table 'Stores'
ALTER TABLE [dbo].[Stores]
ADD CONSTRAINT [FK_StorePartClass]
    FOREIGN KEY ([PartClassPartClassId])
    REFERENCES [dbo].[PartClasses]
        ([PartClassId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_StorePartClass'
CREATE INDEX [IX_FK_StorePartClass]
ON [dbo].[Stores]
    ([PartClassPartClassId]);
GO

-- Creating foreign key on [ServiceStatuses_ServiceStatusId] in table 'ExecutingServices'
ALTER TABLE [dbo].[ExecutingServices]
ADD CONSTRAINT [FK_ExecutingServiceServiceStatus]
    FOREIGN KEY ([ServiceStatuses_ServiceStatusId])
    REFERENCES [dbo].[ServiceStatuses]
        ([ServiceStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExecutingServiceServiceStatus'
CREATE INDEX [IX_FK_ExecutingServiceServiceStatus]
ON [dbo].[ExecutingServices]
    ([ServiceStatuses_ServiceStatusId]);
GO

-- Creating foreign key on [ExecutingServiceExecutingServiceId] in table 'ServicesLogs'
ALTER TABLE [dbo].[ServicesLogs]
ADD CONSTRAINT [FK_ExecutingServiceServiceLogs]
    FOREIGN KEY ([ExecutingServiceExecutingServiceId])
    REFERENCES [dbo].[ExecutingServices]
        ([ExecutingServiceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ExecutingServiceServiceLogs'
CREATE INDEX [IX_FK_ExecutingServiceServiceLogs]
ON [dbo].[ServicesLogs]
    ([ExecutingServiceExecutingServiceId]);
GO

-- Creating foreign key on [EmployeeEmployeeId] in table 'ServicesLogs'
ALTER TABLE [dbo].[ServicesLogs]
ADD CONSTRAINT [FK_EmployeeServiceLogs]
    FOREIGN KEY ([EmployeeEmployeeId])
    REFERENCES [dbo].[Employees]
        ([EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeServiceLogs'
CREATE INDEX [IX_FK_EmployeeServiceLogs]
ON [dbo].[ServicesLogs]
    ([EmployeeEmployeeId]);
GO

-- Creating foreign key on [Address_AddressId] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [FK_ClientAddress]
    FOREIGN KEY ([Address_AddressId])
    REFERENCES [dbo].[Addresses]
        ([AddressId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientAddress'
CREATE INDEX [IX_FK_ClientAddress]
ON [dbo].[Clients]
    ([Address_AddressId]);
GO

-- Creating foreign key on [OrderStatuses_OrderStatusId] in table 'OrdersLogs'
ALTER TABLE [dbo].[OrdersLogs]
ADD CONSTRAINT [FK_OrderStatusOrderLogs]
    FOREIGN KEY ([OrderStatuses_OrderStatusId])
    REFERENCES [dbo].[OrderStatuses]
        ([OrderStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderStatusOrderLogs'
CREATE INDEX [IX_FK_OrderStatusOrderLogs]
ON [dbo].[OrdersLogs]
    ([OrderStatuses_OrderStatusId]);
GO

-- Creating foreign key on [ServiceStatuses_ServiceStatusId] in table 'ServicesLogs'
ALTER TABLE [dbo].[ServicesLogs]
ADD CONSTRAINT [FK_ServiceStatusServiceLogs]
    FOREIGN KEY ([ServiceStatuses_ServiceStatusId])
    REFERENCES [dbo].[ServiceStatuses]
        ([ServiceStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceStatusServiceLogs'
CREATE INDEX [IX_FK_ServiceStatusServiceLogs]
ON [dbo].[ServicesLogs]
    ([ServiceStatuses_ServiceStatusId]);
GO

-- Creating foreign key on [CarBrandId] in table 'CarModels'
ALTER TABLE [dbo].[CarModels]
ADD CONSTRAINT [FK_CarBrandCarModel]
    FOREIGN KEY ([CarBrandId])
    REFERENCES [dbo].[CarBrands]
        ([CarBrandId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarBrandCarModel'
CREATE INDEX [IX_FK_CarBrandCarModel]
ON [dbo].[CarModels]
    ([CarBrandId]);
GO

-- Creating foreign key on [ProvinceId] in table 'Cities'
ALTER TABLE [dbo].[Cities]
ADD CONSTRAINT [FK_ProvinceCity]
    FOREIGN KEY ([ProvinceId])
    REFERENCES [dbo].[Provinces]
        ([ProvinceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProvinceCity'
CREATE INDEX [IX_FK_ProvinceCity]
ON [dbo].[Cities]
    ([ProvinceId]);
GO

-- Creating foreign key on [EmployeeStateId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_StateEmployee]
    FOREIGN KEY ([EmployeeStateId])
    REFERENCES [dbo].[EmployeeStates]
        ([EmployeeStateId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_StateEmployee'
CREATE INDEX [IX_FK_StateEmployee]
ON [dbo].[Employees]
    ([EmployeeStateId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------