USE [SWDb];
GO

DELETE FROM [dbo].[UsedParts];
DELETE FROM [dbo].[ExecutingServices];
DELETE FROM [dbo].[Orders];
DELETE FROM [dbo].[Services];
DELETE FROM [dbo].[ServiceClasses];
DELETE FROM [dbo].[Stores];
DELETE FROM [dbo].[PartClasses];
DELETE FROM [dbo].[Wages];
DELETE FROM [dbo].[ServiceStatuses];
DELETE FROM [dbo].[OrderStatuses];
DELETE FROM [dbo].[ClientCar];
DELETE FROM [dbo].[Cars];
DELETE FROM [dbo].[Clients];
DELETE FROM [dbo].[CarModels];
DELETE FROM [dbo].[CarBrands];
DELETE FROM [dbo].[Fuels];
DELETE FROM [dbo].[Colors];
DELETE FROM [dbo].[Discounts];

INSERT INTO [dbo].[Discounts]
VALUES (5);
INSERT INTO [dbo].[Discounts]
VALUES (10);
INSERT INTO [dbo].[Discounts]
VALUES (15);

INSERT INTO [dbo].[Colors]
VALUES ('bia�y');
INSERT INTO [dbo].[Colors]
VALUES ('br�zowy');
INSERT INTO [dbo].[Colors]
VALUES ('czarny');
INSERT INTO [dbo].[Colors]
VALUES ('czerwony');
INSERT INTO [dbo].[Colors]
VALUES ('fioletowy');
INSERT INTO [dbo].[Colors]
VALUES ('niebieski');
INSERT INTO [dbo].[Colors]
VALUES ('pomara�czowy');
INSERT INTO [dbo].[Colors]
VALUES ('r�owy');
INSERT INTO [dbo].[Colors]
VALUES ('srebrny');
INSERT INTO [dbo].[Colors]
VALUES ('szary');
INSERT INTO [dbo].[Colors]
VALUES ('zielony');
INSERT INTO [dbo].[Colors]
VALUES ('z�oty');
INSERT INTO [dbo].[Colors]
VALUES ('��ty');

INSERT INTO [dbo].[Fuels]
VALUES ('benzyna');
INSERT INTO [dbo].[Fuels]
VALUES ('diesel');
INSERT INTO [dbo].[Fuels]
VALUES ('benzyna + LPG');

INSERT INTO [dbo].[CarBrands]
VALUES ('Alfa Romeo');
INSERT INTO [dbo].[CarBrands]
VALUES ('Audi');
INSERT INTO [dbo].[CarBrands]
VALUES ('BMW');
INSERT INTO [dbo].[CarBrands]
VALUES ('Chevrolet');
INSERT INTO [dbo].[CarBrands]
VALUES ('Citroen');
INSERT INTO [dbo].[CarBrands]
VALUES ('Daewoo');
INSERT INTO [dbo].[CarBrands]
VALUES ('Fiat');
INSERT INTO [dbo].[CarBrands]
VALUES ('Ford');
INSERT INTO [dbo].[CarBrands]
VALUES ('Honda');
INSERT INTO [dbo].[CarBrands]
VALUES ('Hyundai');
INSERT INTO [dbo].[CarBrands]
VALUES ('Kia');
INSERT INTO [dbo].[CarBrands]
VALUES ('Mazda');
INSERT INTO [dbo].[CarBrands]
VALUES ('Mercedes');
INSERT INTO [dbo].[CarBrands]
VALUES ('Mini');
INSERT INTO [dbo].[CarBrands]
VALUES ('Mitsubishi');
INSERT INTO [dbo].[CarBrands]
VALUES ('Nissan');
INSERT INTO [dbo].[CarBrands]
VALUES ('Opel');
INSERT INTO [dbo].[CarBrands]
VALUES ('Peugeot');
INSERT INTO [dbo].[CarBrands]
VALUES ('Renault');
INSERT INTO [dbo].[CarBrands]
VALUES ('Seat');
INSERT INTO [dbo].[CarBrands]
VALUES ('Skoda');
INSERT INTO [dbo].[CarBrands]
VALUES ('Toyota');
INSERT INTO [dbo].[CarBrands]
VALUES ('Volkswagen');

INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('159', 1);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A1', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A3', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A4', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A5', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A6', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A7', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('A8', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Q3', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Q5', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Q7', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('TT', 2);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Seria 1', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Seria 3', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Seria 5', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Seria 6', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Seria 7', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('X1', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('X3', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('X5', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('X6', 3);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Spark', 4);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Aveo', 4);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Cruze', 4);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Lacetti', 4);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Berlingo', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('C1', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('C3', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('C4', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('C5', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Saxo', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Xsara', 5);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Lanos', 6);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Matiz', 6);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Nexia', 6);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Tico', 6);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Bravo', 7);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Panda', 7);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Punto', 7);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Albea', 7);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Siena', 7);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Stilo', 7);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('C-Max', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Escort', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Fiesta', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Focus', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Ka', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Mondeo', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('S-Max', 8);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Accord', 9);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('City', 9);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Civic', 9);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('CR-V', 9);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Jazz', 9);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Legend', 9);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Accent', 10);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('i10', 10);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('i20', 10);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('i30', 10);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('i40', 10);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Santa Fe', 10);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Ceed', 11);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Picanto', 11);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Rio', 11);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Sportage', 11);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Venga', 11);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('2', 12);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('3', 12);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('5', 12);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('6', 12);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa A', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa B', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa C', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa E', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa GL', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa M', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Klasa S', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Sprinter', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Vito', 13);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Cooper', 14);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('One', 14);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Colt', 15);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Lancer', 15);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Outlander', 15);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Almera', 16);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Micra', 16);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Note', 16);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Qashqai', 16);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Primera', 16);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Astra', 17);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Corsa', 17);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Insignia', 17);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Meriva', 17);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Vectra', 17);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Zafira', 17);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('206', 18);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('207', 18);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('307', 18);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('406', 18);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('407', 18);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('607', 18);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Clio', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Fluence', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Kangoo', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Koleos', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Laguna', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Megane', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Scenic', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Twingo', 19);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Arosa', 20);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Ibiza', 20);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Leon', 20);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Toledo', 20);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Fabia', 21);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Felicia', 21);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Octavia', 21);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Roomster', 21);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Superb', 21);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Yeti', 21);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Auris', 22);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Avensis', 22);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Corolla', 22);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Land Cruiser', 22);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Prius', 22);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Yaris', 22);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Golf', 23);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Jetta', 23);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Lupo', 23);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Passat', 23);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Polo', 23);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Tiguan', 23);
INSERT INTO [dbo].[CarModels] (ModelName, CarBrandId)
VALUES ('Touareg', 23);

INSERT INTO [dbo].[Cities]
VALUES ('Bytom', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Chorz�w', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Zabrze', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Ruda �l�ska', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Siemianowice', 12);
INSERT INTO [dbo].[Cities]
VALUES ('�wi�toch�owice', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Katowice', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Tarnowskie G�ry', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Miko��w', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Piekary �l�skie', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Sosnowiec', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Czelad�', 12);
INSERT INTO [dbo].[Cities]
VALUES ('B�dzin', 12);
INSERT INTO [dbo].[Cities]
VALUES ('D�browa G�rnicza', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Jaworzno', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Mys�owice', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Tychy', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Bielsko-Bia�a', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Rybnik', 12);
INSERT INTO [dbo].[Cities]
VALUES ('�ory', 12);
INSERT INTO [dbo].[Cities]
VALUES ('Pszczyna', 12);

INSERT INTO [dbo].[Streets]
VALUES ('Mickiewicza');
INSERT INTO [dbo].[Streets]
VALUES ('Wolno�ci');
INSERT INTO [dbo].[Streets]
VALUES ('D�browskiego');
INSERT INTO [dbo].[Streets]
VALUES ('Wroc�awska');
INSERT INTO [dbo].[Streets]
VALUES ('Konstytucji');
INSERT INTO [dbo].[Streets]
VALUES ('Kopernika');
INSERT INTO [dbo].[Streets]
VALUES ('Bankowa');
INSERT INTO [dbo].[Streets]
VALUES ('Chopina');
INSERT INTO [dbo].[Streets]
VALUES ('3-go Maja');
INSERT INTO [dbo].[Streets]
VALUES ('Dworcowa');
INSERT INTO [dbo].[Streets]
VALUES ('Pszczy�ska');
INSERT INTO [dbo].[Streets]
VALUES ('Zwyci�stwa');
INSERT INTO [dbo].[Streets]
VALUES ('Korfantego');
INSERT INTO [dbo].[Streets]
VALUES ('Wyzwolenia');
INSERT INTO [dbo].[Streets]
VALUES ('Orzeszkowej');
INSERT INTO [dbo].[Streets]
VALUES ('Kolejowa');
INSERT INTO [dbo].[Streets]
VALUES ('Armii Krajowej');
INSERT INTO [dbo].[Streets]
VALUES ('Ko�ciuszki');
INSERT INTO [dbo].[Streets]
VALUES ('Francuska');
INSERT INTO [dbo].[Streets]
VALUES ('Miarki');
INSERT INTO [dbo].[Streets]
VALUES ('Pi�sudskiego');
INSERT INTO [dbo].[Streets]
VALUES ('Sobieskiego');
INSERT INTO [dbo].[Streets]
VALUES ('Roosvelta');
INSERT INTO [dbo].[Streets]
VALUES ('Goethego');
INSERT INTO [dbo].[Streets]
VALUES ('1-go Maja');
INSERT INTO [dbo].[Streets]
VALUES ('Batorego');
INSERT INTO [dbo].[Streets]
VALUES ('Graniczna');
INSERT INTO [dbo].[Streets]
VALUES ('Goduli');
INSERT INTO [dbo].[Streets]
VALUES ('Legion�w');
INSERT INTO [dbo].[Streets]
VALUES ('Powsta�c�w �l�skich');
INSERT INTO [dbo].[Streets]
VALUES ('Szpitalna');
INSERT INTO [dbo].[Streets]
VALUES ('Sienkiewicza');
INSERT INTO [dbo].[Streets]
VALUES ('S�owackiego');
INSERT INTO [dbo].[Streets]
VALUES ('Jagie��y');
INSERT INTO [dbo].[Streets]
VALUES ('Grunwaldzka');
INSERT INTO [dbo].[Streets]
VALUES ('Pocztowa');
INSERT INTO [dbo].[Streets]
VALUES ('Nowa');
INSERT INTO [dbo].[Streets]
VALUES ('Chrobrego');
INSERT INTO [dbo].[Streets]
VALUES ('Sk�odowskiej-Curie');

INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (25, 7, 21, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (18, NULL, 11, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (120, 8, 12, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (3, NULL, 29, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (45, NULL, 4, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (74, NULL, 22, 8, '40-005');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (37, 12, 20, 2, '41-900');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (93, 2, 1, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (38, 4, 16, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (19, NULL, 9, 2, '41-900');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (14, NULL, 16, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (28, NULL, 19, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (17, 9, 32, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (208, NULL, 2, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (167, NULL, 31, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (16, 22, 13, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (23, NULL, 28, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (52, NULL, 9, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (5, 10, 1, 8, '40-005');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (17, NULL, 6, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (77, NULL, 23, 2, '41-900');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (44, 13, 18, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (40, 7, 28, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (12, 24, 3, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (12, 31, 24, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (29, NULL, 14, 2, '41-900');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (34, NULL, 31, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (67, 16, 18, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (24, NULL, 5, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (24, 8, 20, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (14, NULL, 9, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (31, NULL, 14, 8, '40-005');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (7, NULL, 30, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (8, 5, 25, 2, '41-900');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (10, 15, 8, 1, '44-100');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (2, NULL, 25, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (46, NULL, 3, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (9, 17, 15, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (10, 22, 27, 2, '41-900');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (80, NULL, 17, 3, '41-500');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (6, 27, 7, 4, '41-800');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (1, NULL, 26, 5, '41-700');
INSERT INTO [dbo].[Addresses] (HouseNumber, ApartmentNumber, StreetStreetId, CityCityId, PostCode)
VALUES (14, NULL, 10, 8, '40-005');

INSERT INTO [dbo].[Groups] (Name, Description)
VALUES ('Serwisanci', 'Przyjmuj� zlecenia');
INSERT INTO [dbo].[Groups] (Name, Description)
VALUES ('Pracownicy warsztatu', 'Dokonuj� napraw');

INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 1);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 2);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 4);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 6);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 7);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 8);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 9);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 10);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 11);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 12);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 14);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 16);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 18);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 19);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 20);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 21);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 22);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 23);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 24);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 25);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 26);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 28);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 29);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 30);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (2, 32);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 2);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 4);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 6);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 8);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 9);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 10);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 11);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 12);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 14);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 16);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 18);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 20);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 22);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 24);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 26);
INSERT INTO [dbo].[GroupPermission] (Groups_GroupId, Permissions_PermissionId)
VALUES (3, 28);

INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Tomasz', 'Nowak', 'nowakt', 'tomasz', 2, '56051260911', '233-204-38-97', '02-25-2010', NULL, '695732145', NULL, NULL, 2);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Piotr', 'Wi�niewski', 'wisnia', 'piotr', 2, '72110298819', '534-719-98-68', '02-25-2010', NULL, '504321456', NULL, NULL, 3);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Micha�', 'Wojcik', 'wojcik', 'michal', 2, '74022365517', '328-128-99-90', '02-25-2010', NULL, '778654493', NULL, NULL, 4);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('�ukasz', 'Kowalczyk', 'kowal', 'lukasz', 2, '78071276013', '537-456-46-39', '03-17-2010', NULL, '788246012', NULL, NULL, 5);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Marcin', 'Kami�ski', 'marcink', 'marcin', 2, '84121448019', '233-427-30-12', '04-04-2010', NULL, '663852169', NULL, NULL, 6);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Wojciech', 'Lewandowski', 'lewy', 'wojciech', 2, '71062118415', '219-064-27-76', '09-25-2010', NULL, '514128924', NULL, NULL, 7);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Marek', 'Zieli�ski', 'marekz', 'marek', 3, '89100328713', '512-023-17-56', '06-28-2012', NULL, '888134692', NULL, NULL, 8);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Pawe�', 'Wo�niak', 'pawelw', 'pawel', 6, '91041093315', '784-685-93-67', '07-02-2012', NULL, '501206442', NULL, NULL, 9);
INSERT INTO [dbo].[Employees] (Name, Surname, Login, Password, EmployeeStateId, Pesel, Nip, HireDate, Qualifications, PhoneNumber, Email, ReleaseDate, Address_AddressId)
VALUES ('Robert', 'Szyma�ski', 'roberts', 'robert', 7, '91020359418', '569-724-68-64', '07-14-2012', NULL, '603597168', NULL, NULL, 10);

INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (2, 2);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (2, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (3, 2);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (3, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (4, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (5, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (6, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (7, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (8, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (9, 3);
INSERT INTO [dbo].[EmployeeGroup] (Employees_EmployeeId, Groups_GroupId)
VALUES (10, 3);

INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Adam', 'D�browski', '66080363211', '695125486', NULL, NULL, 11);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('J�zef', 'Koz�owski', '52120254632', '595634158', NULL, NULL, 12);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Kszysztof', 'Jankowski', '59112043173', '565333214', NULL, NULL, 13);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Aleksandra', 'Mazur', '78010865962', '687235149', NULL, 3, 14);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Katarzyna', 'Kwiatkowska', '83031185766', '728569874', NULL, 1, 15);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Dawid', 'Wojciechowski', '69051728127', '834569833', NULL, NULL, 16);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Barbara', 'Krawczyk', '87022254325', '502465654', NULL, NULL, 17);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Wiktor', 'Kaczmarek', '40022037767', '506895666', NULL, NULL, 18);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Agata', 'Piotrowska', '56013113244', '604189733', NULL, NULL, 19);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Marcin', 'Zaj�c', '61061437949', '888256947', NULL, NULL, 20);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Janusz', 'Grabowski', '57101255727', '503507980', NULL, NULL, 21);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Jakub', 'Paw�owski', '66102097186', '694008451', NULL, NULL, 22);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Magdalena', 'Kr�l', '71020534765', '663985012', NULL, NULL, 23);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Piotr', 'Wr�bel', '77042874274', '654987258', NULL, 1, 24);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Monika', 'Wieczorek', '50100351166', '509658733', NULL, NULL, 25);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Tomasz', 'Jab�o�ski', '79071828744', '504496537', NULL, 2, 26);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Maciej', 'Nowakowski', '75120546495', '500167984', NULL, NULL, 27);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Stanis�aw', 'Majewski', '58052479873', '691548060', NULL, NULL, 28);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Beata', 'St�pie�', '66010325566', '783659048', NULL, NULL, 29);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Patryk', 'Dudek', '83122745377', '845761050', NULL, NULL, 30);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Joanna', 'Malinowska', '50012096946', '694890310', NULL, NULL, 31);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Karol', 'Adamczyk', '83052422781', '721456999', NULL, 1, 32);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Rafa�', 'G�rski', '62071357892', '506340087', NULL, NULL, 33);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Piotr', 'Nowicki', '86090359275', '694521067', NULL, NULL, 34);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Zofia', 'Sikora', '84040989482', '776056307', NULL, NULL, 35);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Marta', 'Walczak', '67082485525', '695722469', NULL, NULL, 36);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Batrosz', 'Rutkowski', '88020947156', '662195847', NULL, NULL, 37);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Daniel', 'Baran', '80021091667', '691480567', NULL, 1, 38);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Przemys�aw', 'Szewczyk', '66012315967', '501468720', NULL, NULL, 39);
INSERT INTO [dbo].[Clients] (Name, Surname, Pesel, PhoneNumber, Email, DiscountId, Address_AddressId)
VALUES ('Ryszard', 'Wr�blewski', '43081217688', '506456320', NULL, NULL, 40);

INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY35902', '69F7AXRKE98ZD1SSP', 1330, 2006, 52, 1, 4);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH86420', 'VUEHUYWCNDLCR78PC', 1390, 1999, 107, 2, 3);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ69541', '5R0L04KYTNP953UN7', 1360, 2000, 90, 2, 11);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG27402', '26VM98RKA5G8KK409', 1000, 1997, 34, 3, 1);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG35446', 'ER4PDL3TLZL4CHX93', 1998, 2007, 48, 2, 9);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SL18373', '882NJ3JS2U1WJ000V', 998, 1998, 86, 1, 11);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SL90944', 'E358LFYXA04XZYVXP', 1998, 2009, 121, 1, 3);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ46488', '86GHM82CVB3K1MRVH', 1360, 2003, 28, 3, 6);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH28647', 'P5PBSP1LAR9WCPV8X', 1399, 2004, 23, 2, 1);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SK53678', '2KWUNEVGPLV552J1F', 1390, 2005, 114, 1, 9);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG38649', 'KHVBVWSUFU71TUT3E', 1598, 2001, 126, 1, 4);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY29488', 'BFJXNZ87U4Z3V4C2P', 1199, 2001, 91, 1, 13);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ35575', 'EMAW5GE37YMRRSTX1', 1498, 2003, 33, 2, 9);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ01940', 'E9PC686V2SP4J49W4', 1241, 1997, 39, 2, 1);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SL75861', 'WSB0K8NXN3D6P4ANJ', 1896, 2001, 3, 2, 9);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG56981', 'DKH959SK40E3L8N29', 1781, 2003, 4, 1, 6);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY32005', 'DB8SZMJ2HZXJC6MMD', 1390, 2006, 102, 2, 3);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH04958', 'MHC3T7UX04WCXWGYU', 973, 2002, 91, 2, 12);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SRS4685', 'YM67DDX1NZX0B0BJ3', 1781, 2004, 4, 2, 13);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG46895', '420NPPG9T3W0SEM3A', 1360, 1999, 90, 2, 10);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ05608', 'K7E6YTVJGX9J62DYS', 1390, 2004, 114, 3, 1);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG08954', 'SC6YBPRYD72S031X7', 1896, 2002, 3, 2, 11);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SK46983', 'XJPGTX3S5ZJ8M1PPX', 1360, 2001, 90, 2, 7);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SL16854', 'CSBUDER5XEZM1W8AA', 1199, 2006, 91, 1, 3);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY46899', 'EUX7ATXJS895MX83Y', 1241, 1998, 39, 1, 5);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG19344', 'S7363PBS23B03C5M0', 1390, 1999, 107, 2, 4);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH16845', '3MGRXYNXU1HSV68S0', 1781, 1994, 126, 1, 3);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH46755', '4KE616Z3XT5T4VLF2', 1998, 2006, 48, 3, 6);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ91600', '13RDP85YRPUP6JJFN', 1390, 2008, 102, 2, 13);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY09766', 'E6K307KVPEE0K6RCL', 1388, 2000, 45, 1, 9);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SG16455', 'VTKRE15YE17P7KU4W', 1995, 2004, 14, 3, 2);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH49961', 'MUTYD62HXBECESXX2', 2979, 2005, 20, 1, 4);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY47506', 'C2U7RZZMCWYWPH780', 1896, 2007, 111, 2, 3);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SY38764', 'VBWLKHNRKU67FZ97G', 1587, 2008, 96, 2, 6);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SL30992', '6M5VZ7ELUVUSTJBU5', 1388, 2003, 45, 1, 9);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SZ47677', 'N4C2JMKEJS4HNAE16', 1753, 2003, 45, 3, 11);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SK48720', 'ZP441MPK46U6RNJJS', 1995, 2006, 14, 1, 13);
INSERT INTO [dbo].[Cars] (RegistrationNumber, BodyNumber, EngineCapacity, ProductionYear, CarModelId, FuelId, ColorId)
VALUES ('SH21076', 'AT3LYC2SLFSR6NELG', 1242, 2011, 38, 1, 4);

INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (1, 1);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (2, 2);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (3, 3);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (4, 4);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (5, 5);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (6, 6);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (7, 7);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (8, 8);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (9, 9);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (10, 10);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (11, 11);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (12, 12);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (13, 13);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (14, 14);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (15, 15);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (16, 16);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (17, 17);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (18, 18);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (19, 19);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (20, 20);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (21, 21);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (22, 22);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (23, 23);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (24, 24);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (25, 25);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (26, 26);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (27, 27);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (28, 28);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (29, 29);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (30, 30);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (4, 31);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (18, 32);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (12, 33);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (25, 34);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (7, 35);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (21, 36);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (10, 37);
INSERT INTO [dbo].[ClientCar] (Clients_ClientId, Cars_CarId)
VALUES (27, 38);

INSERT INTO [dbo].[OrderStatuses]
VALUES ('nowe');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('przyj�te');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('w toku');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('zawieszone');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('do weryfikacji');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('do odbioru');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('wydane');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('reklamowane');
INSERT INTO [dbo].[OrderStatuses]
VALUES ('zako�czone');

INSERT INTO [dbo].[ServiceStatuses]
VALUES ('nowa');
INSERT INTO [dbo].[ServiceStatuses]
VALUES ('w toku');
INSERT INTO [dbo].[ServiceStatuses]
VALUES ('zawieszona');
INSERT INTO [dbo].[ServiceStatuses]
VALUES ('zako�czona');

INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2700.0, NULL, 1);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2600.0, NULL, 2);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2400.0, NULL, 3);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2400.0, NULL, 4);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2700.0, NULL, 5);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2300.0, NULL, 6);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2400.0, NULL, 7);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 2000.0, NULL, 8);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 1800.0, NULL, 9);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('03/2012', 1200.0, NULL, 10);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2800.0, NULL, 1);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2600.0, NULL, 2);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2400.0, NULL, 3);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2400.0, NULL, 4);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2800.0, NULL, 5);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2300.0, NULL, 6);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2400.0, NULL, 7);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 2000.0, NULL, 8);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 1800.0, NULL, 9);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('04/2012', 1200.0, NULL, 10);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2800.0, NULL, 1);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2700.0, 200.0, 2);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2400.0, NULL, 3);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2400.0, NULL, 4);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2800.0, 100.0, 5);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2300.0, NULL, 6);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2400.0, NULL, 7);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 2000.0, NULL, 8);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 1800.0, NULL, 9);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('05/2012', 1200.0, NULL, 10);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2800.0, 100.0, 1);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2700.0, NULL, 2);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2400.0, NULL, 3);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2400.0, NULL, 4);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2800.0, NULL, 5);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2300.0, NULL, 6);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2400.0, 100.0, 7);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 2000.0, NULL, 8);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 1800.0, NULL, 9);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('06/2012', 1200.0, NULL, 10);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2800.0, NULL, 1);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2700.0, NULL, 2);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2400.0, 200.0, 3);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2400.0, NULL, 4);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2800.0, NULL, 5);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2300.0, 100.0, 6);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2400.0, NULL, 7);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 2000.0, NULL, 8);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 1800.0, NULL, 9);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('07/2012', 1200.0, NULL, 10);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2800.0, 100.0, 1);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2700.0, NULL, 2);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2400.0, NULL, 3);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2400.0, 300.0, 4);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2800.0, NULL, 5);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2400.0, 200.0, 6);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2400.0, NULL, 7);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 2000.0, NULL, 8);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 1800.0, NULL, 9);
INSERT INTO [dbo].[Wages] (Month, Salary, Bonus, EmployeeEmployeeId)
VALUES ('08/2012', 1200.0, NULL, 10);

INSERT INTO [dbo].[PartClasses]
VALUES ('Opony i felgi');
INSERT INTO [dbo].[PartClasses]
VALUES ('Uk�ad nap�dowy i silnik');
INSERT INTO [dbo].[PartClasses]
VALUES ('Uk�ad hamulcowy');
INSERT INTO [dbo].[PartClasses]
VALUES ('Uk�ad kierowniczy');
INSERT INTO [dbo].[PartClasses]
VALUES ('Zawieszenie');
INSERT INTO [dbo].[PartClasses]
VALUES ('Elektryka i elektronika');
INSERT INTO [dbo].[PartClasses]
VALUES ('Uk�ad wydechowy');
INSERT INTO [dbo].[PartClasses]
VALUES ('Elementy karoserii');
INSERT INTO [dbo].[PartClasses]
VALUES ('O�wietlenie');
INSERT INTO [dbo].[PartClasses]
VALUES ('Szyby i lusterka');

INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga Dotz Dakar R15', 'srebrna felga aluminiowa, R15', 8, 85.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga Dotz Dakar R16', 'srebrna felga aluminiowa, R16', 8, 85.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga Enzo H R14', 'srebrna felga aluminiowa, R14', 8, 95.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga Enzo H R15', 'srebrna felga aluminiowa, R15', 8, 95.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga Enzo H R16', 'srebrna felga aluminiowa, R16', 4, 95.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga stalowa czarna R13', NULL, 12, 45.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga stalowa czarna R14', NULL, 10, 45.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga stalowa czarna R15', NULL, 10, 45.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga stalowa czarna R16', NULL, 4, 45.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Felga OZ Adrenalina R15', 'czarna felga aluminiowa, matowa R15', 6, 160.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('D�bica Passio 175/65R14', 'opona letnia 175/65R14', 12, 120.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('D�bica Passio 195/65R15', 'opona letnia 195/65R15', 12, 150.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Toyo Proxes 205/55R16', 'opona letnia 205/55R16', 8, 280.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Pirelli P3000 185/60R14', 'opona letnia 185/60R14', 8, 160.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Fulda Kristal 185/65R15', 'opona zimowa 185/65R15', 12, 210.0, 1);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Olej Castrol Magnatec', 'olej silnikowy uniwersalny mineralny 1l', 15, 30.0, 2);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Olej Castrol Edge', 'olej silnikowy uniwersalny syntetyczny 1l', 7, 60.0, 2);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Olej Mobil 1', 'olej silnikowy uniwersalny p�syntetyczny 1l', 11, 25.0, 2);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Elf Turbo Diessel', 'olej silnikowy diessel mineralny 1l', 13, 25.0, 2);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Cewka zap�onowa', NULL, 16, 80.0, 6);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Klocki hamulcowe prz�d', NULL, 9, 105.0, 3);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Tarcze hamulcowe', NULL, 6, 190.0, 3);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('P�yn hamulcowy DOT-4 0.5l', NULL, 10, 15.0, 3);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Pompa wspomagania', NULL, 2, 450.0, 4);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Amortyzator prz�d', NULL, 4, 140.0, 5);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Amortyzator ty�', NULL, 4, 100.0, 5);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Wahacz', NULL, 2, 180.0, 5);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Akumulator Bosch', NULL, 7, 260.0, 6);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('T�umik', NULL, 6, 50.0, 7);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Kolektor wydechowy', NULL, 1, 560.0, 7);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Katalizator', NULL, 1, 630.0, 7);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('�ar�wka H4', '�wiat�a drogowe i mijania', 20, 20.0, 9);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('�ar�wka R5W', '�wiat�a pozycyjne', 17, 9.0, 9);
INSERT INTO [dbo].[Stores] (Name, Description, AvailabileAmount, UnitPrice, PartClassPartClassId)
VALUES ('Filtr paliwa', NULL, 5, 75.0, 2);

INSERT INTO [dbo].[ServiceClasses]
VALUES ('Blacharstwo');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Lakiernictwo');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Mechanika');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Wulkanizacja');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Elektryka, elektronika');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Diagnostyka');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Auto szyby');
INSERT INTO [dbo].[ServiceClasses]
VALUES ('Klimatyzacja');

INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Usuwanie wgniecenia', NULL, 200.0, 1);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Lakierowanie ma�ego el.', NULL, 120.0, 2);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Lakierowanie du�ego el.', NULL, 450.0, 2);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Polerowanie karoserii', NULL, 300.0, 2);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana oleju silnikowego', NULL, 15.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana filtra paliwowego', NULL, 20.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana klock�w ham.', NULL, 60.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana tarcz ham.', NULL, 70.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana p�ynu hamulcowego', NULL, 30.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana amortyzatora prz.', NULL, 85.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana amortyzatora ty�', NULL, 40.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana wahacza', NULL, 70.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana cewki zap�on.', NULL, 15.0, 5);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana �ar�wki', NULL, 20.0, 5);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Sprawdzenie ust. �wiate�', NULL, 20.0, 5);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana p�ynu ch�odn.', NULL, 30.0, 3);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana kompletu opon', NULL, 55.0, 4);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Regeneracja opony', NULL, 35.0, 4);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Prostowanie felgi alu.', NULL, 75.0, 4);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Prostowanie felgi stal.', NULL, 40.0, 4);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Geometria k�', NULL, 100.0, 6);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Przegl�d techniczny', NULL, 100.0, 6);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Diagnostyka komputerowa', NULL, 80.0, 6);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Wymiana szyby przedniej', NULL, 490.0, 7);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Naprawa szyby', NULL, 70.0, 7);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Serwis klimatyzacji', NULL, 120.0, 8);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Odgrzybianie klimatyzacji', NULL, 90.0, 8);
INSERT INTO [dbo].[Services] (Name, Description, Price, ServiceClassServiceClassId)
VALUES ('Zakup cz�ci', NULL, 0.0, 3);

INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 45.0, 1, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 100.0, 2, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 390.0, 3, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 40.0, 4, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 535.0, 5, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 950.0, 6, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 95.0, 7, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 210.0, 8, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 90.0, 9, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 100.0, 10, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 100.0, 11, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 820.0, 12, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 490.0, 13, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 95.0, 14, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 95.0, 15, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 470.0, 16, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 120.0, 17, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 895.0, 18, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 75.0, 19, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 70.0, 20, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 58.0, 21, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 365.0, 22, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 35.0, 23, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 140.0, 24, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 980.0, 25, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 120.0, 26, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 250.0, 27, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 80.0, 28, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 45.0, 29, 2, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 320.0, 30, 3, 9);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 300.0, 31, 2, 6);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 140.0, 32, 2, 6);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 40.0, 33, 3, 6);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 250.0, 34, 3, 5);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 195.0, 35, 2, 3);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 120.0, 36, 3, 3);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 158.0, 37, 3, 3);
INSERT INTO [dbo].[Orders] (Description, Price, CarCarId, EmployeeEmployeeId, OrderStatuses_OrderStatusId)
VALUES (NULL, 100.0, 38, 2, 2);

INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 1, 5, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 2, 23, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 2, 15, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 3, 11, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 3, 12, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 4, 14, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 5, 17, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 6, 1, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 6, 3, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 6, 4, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 7, 13, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 8, 7, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 8, 9, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 9, 27, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 10, 21, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 11, 22, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (3, 12, 28, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 13, 24, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 14, 17, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 14, 20, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 15, 6, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 16, 7, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 16, 8, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 16, 9, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 17, 26, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 18, 17, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 19, 5, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (3, 20, 25, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (3, 21, 28, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 22, 10, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 22, 11, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 23, 18, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 24, 22, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 24, 14, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (3, 25, 28, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 26, 26, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (7, 27, 12, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 28, 23, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (8, 29, 5, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 30, 1, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (7, 30, 2, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 31, 4, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 32, 11, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 33, 20, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (8, 34, 12, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (7, 35, 6, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (4, 35, 22, 2);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (6, 36, 2, 2);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (8, 37, 14, 4);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (7, 37, 26, 2);
INSERT INTO [dbo].[ExecutingServices] (EmployeeEmployeeId, OrderOrderId, ServiceServiceId, ServiceStatuses_ServiceStatusId)
VALUES (5, 38, 21, 1);

INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 1, 16);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 4, 26);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 5, 27);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 6, 32);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (4, 7, 11);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 11, 20);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 12, 21);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 13, 23);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (4, 17, 10);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 17, 28);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 21, 34);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 22, 21);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 23, 22);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 24, 23);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (4, 26, 15);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 27, 17);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (2, 29, 32);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (2, 29, 33);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 30, 25);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 31, 26);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 34, 32);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (4, 35, 4);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (4, 35, 12);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 37, 27);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 39, 16);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 43, 26);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 45, 27);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (1, 46, 34);
INSERT INTO [dbo].[UsedParts] (UsedPartsNumber, ExecutingServiceExecutingServiceId, StoreStoreId)
VALUES (2, 49, 33);