﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsView.Helpers
{
    public partial class CarChooseForm : Form
    {
        public CarChooseForm()
        {
            InitializeComponent();

            this.CancelButton = this.buttonCancel;
            this.AcceptButton = this.buttonOK;

            // Pobranie uchwytu do bd
            var db = AppLogic.Core.ServiceManager.Database;

            var cars = (from c in db.Cars select c);

            dataGrid.DataSource = cars;
        }

        public int Run(IWin32Window owner)
        {
            id_ = 0;
            ShowDialog(owner);
            return id_;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            var r = dataGrid.CurrentRow;

            if(r != null)
                id_ = (r.DataBoundItem as Model.Car).CarId;

            DestroyHandle();
        }

        private int id_ = 0;
    }
}
