﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsView.Helpers
{
    /// <summary>
    /// Wewnętrzna klasa, odpowiada za wyświetlanie komunikatów na ekranie
    /// </summary>
    internal class Notifier
    {
        internal static void  ShowInfo(string content, string caption = null)
        {
            MessageBox.Show(content, caption ?? TranslationsStatic.Default.msgInfo, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        internal static void ShowWarning(string content, string caption = null)
        {
            MessageBox.Show(content, caption ?? TranslationsStatic.Default.msgWarning, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        internal static void ShowError(string content, string caption = null)
        {
            MessageBox.Show(content, caption ?? TranslationsStatic.Default.msgError, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal static void ShowException(Exception ex, string caption = null)
        {
            MessageBox.Show(ex.ToString(), caption ?? TranslationsStatic.Default.msgException);
        }

        internal static bool ShowQuestion(string question, string caption = null)
        {
            if (MessageBox.Show(question, caption ?? TranslationsStatic.Default.msgQuestion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                return true;
            else
                return false;
        }
    }
}
