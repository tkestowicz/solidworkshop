﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsView.Helpers
{
    public static class ControlContainerHelper
    {
        /// <summary>
        /// Metoda zbiera wszystkie aktywne parametry wyszukiwania.
        /// Kontrolka parametru wyszukiwania musi zawierać Tag "searchParam" i musi być aktywna Enabled = true.
        /// </summary>
        /// <param name="groupBox">Obiekt kontrolki, w której będą szukane parametry</param>
        /// <returns>Słownik z parametrami wyszukiwania - klucz nazwa pola, wartość - wartość parametru</returns>
        public static Dictionary<string, object> GetSearchParams(this GroupBox groupBox)
        {
            return GetSearchParams(groupBox.Controls);
        }

        /// <summary>
        /// Metoda zbiera wszystkie aktywne parametry wyszukiwania.
        /// Kontrolka parametru wyszukiwania musi zawierać Tag = "searchParam" i musi być aktywna Enabled = true.
        /// </summary>
        /// <param name="controls">Kolekcja kontrolek, w której będą szukane parametry</param>
        /// <returns>Słownik z parametrami wyszukiwania - klucz nazwa pola, wartość - wartość parametru</returns>
        private static Dictionary<string, object> GetSearchParams(System.Windows.Forms.Control.ControlCollection controls)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();

            foreach (Control ctrl in controls)
            {
                if (ctrl.Tag != null && ctrl.Tag.ToString() == "searchParam" && ctrl.Enabled)
                {
                    if (ctrl.GetType().Name.Contains("TextBox"))
                    {
                        args.Add(ctrl.Name, ctrl.Text);
                    }

                    else if (ctrl.GetType().Name.Contains("ComboBox"))
                        args.Add(ctrl.Name, (ctrl as ComboBox).SelectedItem);

                    else if (ctrl.GetType().Name.Contains("DateTimePicker"))
                        args.Add(ctrl.Name, (ctrl as DateTimePicker).Value);
                }
            }
            return args;
        }
    }
}
