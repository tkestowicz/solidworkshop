﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsView.Helpers
{
    public static class ComboBoxHelper
    {

        /// <summary>
        /// Metoda znajduje indeks elementu o zadanym identyfikatorze
        /// </summary>
        /// <param name="list">Lista elementów w liście rozwijanej</param>
        /// <param name="id">Identyfikator elementu, którego szukamy</param>
        /// <returns>Znaleziony indeks</returns>
        public static int FindIndex(this ComboBox list, int id)
        {
            // Szukanie elementu, który ma być aktywny
            for (int i = 0; i < list.Items.Count; i++)
            {

                Model.Projection.ComboBox item = list.Items[i] as Model.Projection.ComboBox;

                if (item.Id == id)
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// Metoda znajduje indeks elementu o zadanej wartości
        /// </summary>
        /// <param name="list">Lista elementów w liście rozwijanej</param>
        /// <param name="value">Wartość elementu, którego szukamy</param>
        /// <returns>Znaleziony indeks</returns>
        public static int FindIndex(this ComboBox list, string value)
        {
            // Szukanie elementu, który ma być aktywny
            for (int i = 0; i < list.Items.Count; i++)
            {

                Model.Projection.ComboBox item = list.Items[i] as Model.Projection.ComboBox;

                if (item.Value == value)
                    return i;
            }
            return -1;
        }
    }
}
