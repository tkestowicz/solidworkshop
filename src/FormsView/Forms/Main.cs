﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces;
using AppLogic.Presenters;
using AppLogic;
using FormsView.Editors;
using AppLogic.Presenters.Popup;

namespace FormsView.Forms
{
    /// <summary>
    /// Klasa reprezentująca główne okno aplikacji
    /// </summary>
    public partial class Main : Base.BaseForm, IMainView
    {

        public Main()
        {
            InitializeComponent();

            // Utworzenie obiektu prezentera i powiązanie go z widokiem
            _presenter = new MainPresenter(this);
        }

        #region Methods

        /// <summary>
        /// Metoda odpowiednio  modyfikuje kontrolki GUI aby były zgodne
        /// z uprawnieniami nadanymi grupie
        /// </summary>
        private void PostInitializeComponent()
        {
            // Pobranie uchwytu do bd
            var db = AppLogic.Core.ServiceManager.Database;

            // Ukryj wszystko
            foreach (System.Windows.Forms.ToolStripItem item in mainMenuItemDictionaries.DropDownItems)
                if(!item.Name.Contains("toolStripSeparator"))
                    item.Visible = false;

            foreach (System.Windows.Forms.ToolStripItem item in toolStripMenuItemMainMenu.DropDownItems)
                if(!item.Name.Contains("toolStripSeparator"))
                    item.Visible = false;

            toolStripMenuItemReports.Visible = false;

            // Pokaż tylko te, do których mamy uprawnienia
            var employee = (from e in db.Employees where e.EmployeeId == AppLogic.Core.LoggedUser.Instance.User.EmployeeId select e).First();
            foreach (var group in employee.Groups)
            {
                foreach (var item in group.Permissions)
                {
                    if (item.Identyficator.Contains("View"))
                    {
                        string temp = item.Identyficator.Substring(0, item.Identyficator.IndexOf("View"));

                        if (temp.Contains("Dic"))
                        {
                            string asdsd = "menuItem" + temp;
                            mainMenuItemDictionaries.DropDownItems.Find("menuItem" + temp, true).First().Visible = true;
                        }
                        else if (temp.Contains("Reports"))
                        {
                            toolStripMenuItemReports.Visible = true;
                        }
                        else
                        {
                            string asdsd = "toolStripMenuItem" + temp;
                            toolStripMenuItemMainMenu.DropDownItems.Find("toolStripMenuItem" + temp, true).First().Visible = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Metoda ładuje odpowiednie kontrolki do GUI
        /// </summary>
        /// <param name="dPresenter">Logika dla głównego grida z danymi</param>
        /// <param name="ePresenter">Logika dla kontrolki edytora</param>
        /// <param name="editorView">Kontrolka edytora</param>
        private void _InitGui(DataPresenter dPresenter, IEditorPresenter ePresenter, IEditorView editorView)
        {

            // Zwalniamy podpięte zdarzenia dla starych - BARDZO WAŻNE!
            AppLogic.Core.ServiceManager.Events.Dispose();

            (editorView as Base.BaseUserControl).Dock = DockStyle.Fill;

            // Inicjujemy edytor dodając odpowiednią logikę
            editorView.Initialize(ePresenter);

            panelEditor.Controls.Clear();

            // Podpinamy widok podglądu do kontenera
            panelEditor.Controls.Add(editorView as Base.BaseUserControl);

			dataGrid.ResetGrid();
            dataGrid.ResetNavigation();
            /// Inicjujemy grid dodając odpowiednią logikę
            dataGrid.Initialize(dPresenter);
        }

        #endregion

        #region Events

        /// <summary>
        /// Metoda wywoływana podczas ładowania formularza, 
        /// odpowiada za inicjację autoryzacji.
        /// </summary>
        private void OnLoadEvent(object sender, EventArgs e)
        {
            _presenter.Authorization();

            // Ustawienie pionowego splittera w odpowiednim miejscu
            int collapsed = splitterHorizontal.Size.Width - splitterHorizontal.SplitterDistance - 25;
            splitterHorizontal.SplitterDistance -= 319;
            splitterHorizontal.SplitterDistance += collapsed;

        }

        /// <summary>
        /// Obsługa zwijania fragmentu okna w pionie
        /// </summary>
        private void buttonCollapseVertical_Click(object sender, EventArgs e)
        {
            //int collapsed = splitterVertical.Size.Height - splitterVertical.Panel2MinSize;

            //if (splitterVertical.SplitterDistance == collapsed - 4)
            //{
            //    splitterVertical.SplitterDistance = splitterVertical.Size.Height / 2;
            //    buttonCollapseVertical.Text = "V";
            //}
            //else
            //{
            //    splitterVertical.SplitterDistance = collapsed;
            //    buttonCollapseVertical.Text = "^";
            //}
        }

        /// <summary>
        /// Obsługa zwijania fragmentu okna w poziomie
        /// </summary>
        private void buttonCollapseHorizontal_Click(object sender, EventArgs e)
        {

            int collapsed = splitterHorizontal.Size.Width - splitterHorizontal.SplitterDistance - 25;

         
            if(collapsed  < 40)
            {
                splitterHorizontal.SplitterDistance -= 319;
                buttonCollapseHorizontal.Text = ">";
            }
            else
            {
                splitterHorizontal.SplitterDistance += collapsed;
                buttonCollapseHorizontal.Text = "<";
            }
        }

        /// <summary>
        /// Obsługa wylogowania
        /// </summary>
        private void buttonLogout_Click(object sender, EventArgs e)
        {
            _presenter.OnLogout();
        }

        private void buttonChangePassword_Click(object sender, EventArgs e)
        {
            var view = new PasswordEditor();
            var logic = new ChangePasswordPresenter(view);

            logic.Initialize();
            view.Initialize(logic);

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(view, logic);
            popup.Text = TranslationsStatic.Default.titlePasswordChanging;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia
        }

        #endregion

        #region Fields

        /// <summary>
        /// Obiekt prezentera dla głównego okna aplikacji
        /// </summary>
        MainPresenter _presenter;

        /// <summary>
        /// Aktualnie wybrana opcja z menu
        /// </summary>
        ToolStripMenuItem _currentItem;

        #endregion 

        #region IMainView

        public void LoadLoginView()
        {
            Visible = false;

            // Wyświetlamy okienko logowania
            var login = new Login(this).ShowDialog();
        }

        public void InitMainView()
        {
            Visible = true;

            string name =  AppLogic.Core.LoggedUser.Instance.User.Name;
            string surname = AppLogic.Core.LoggedUser.Instance.User.Surname;
            string login = AppLogic.Core.LoggedUser.Instance.User.Login;

            toolStripLabelUserInfo.Text = String.Format(FormsView.TranslationsStatic.Default.labelLoggedAs, name, surname, login);

            PostInitializeComponent();
        }

        public void Restart()
        {
            System.Windows.Forms.Application.Restart();
        }

        #endregion

        #region Main menu events

        private void BindControlToPanelEditor(Base.BaseUserControl control)
        {
            control.Dock = DockStyle.Fill;

            panelEditor.Controls.Clear();

            // Podpinamy widok podglądu do kontenera
            panelEditor.Controls.Add(control);

        }

        /// <summary>
        /// Metoda aktualizuje tytuł dla aktywnej opcji z menu i zaznacza wybraną opcję
        /// </summary>
        /// <param name="e">Wybrana opcja z menu</param>
        private void mainMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (_currentItem != null)
                _currentItem.Checked = false;

            _currentItem = (e.ClickedItem as ToolStripMenuItem);

            _currentItem.Checked = true;

            // Ustawiamy wytuł okienka
            Title = e.ClickedItem.Text;
        }

        private void OnClosingEvent(object sender, FormClosingEventArgs e)
        {
            _presenter.OnClosing();
        }


        #endregion

        #region Dictionary Items

        private void menuItemDicPartClass_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.PartClassPresenter(dataGrid),
                                                   new AppLogic.Presenters.DicitionaryEditor.PartClassPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicPartClass"));

        }

        private void mainItemDicServiceClass_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.ServiceClassPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.ServiceClassPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicServiceClass"));
        }

        private void menuItemDicService_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.ServicePresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.ServicePresenter(),
                                                   ProceedEditor(new Editors.ServiceEditor(), "DicService"));
        }

        private void menuItemDicFuel_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.FuelPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.FuelPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicFuel"));
        }

        private void menuItemDicColor_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.ColorPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.ColorPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicColor"));
        }

        private void menuItemDicCarBrand_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.CarBrandPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.CarBrandPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicCarBrand"));
        }

        private void menuItemDicCarModel_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.CarModelPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.CarModelPresenter(),
                                                   ProceedEditor(new Editors.CarModelEditor(), "DicCarModel"));
        }

        private void menuItemDicServiceStatus_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.ServiceStatusPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.ServiceStatusPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicServiceStatus"));
        }

        private void menuItemDicEmployeeStatus_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.EmployeeStatePresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.EmployeeStatePresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicEmployeeState"));
        }

        private void województwaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.ProvincesPresenter(dataGrid),
                                                    new AppLogic.Presenters.DicitionaryEditor.ProvincesPresenter(),
                                                   ProceedEditor(new Editors.DictionaryEditor(), "DicProvinces"));
        }

        #endregion

        #region Main Items 

        private void toolStripMenuItemClients_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.ClientPresenter(dataGrid),
                    new AppLogic.Presenters.Editor.ClientPresenter(),
                    ProceedEditor(new Editors.ClientEditor(), "Clients"));
        }

        private void toolStripMenuItemEmployees_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.EmployeePresenter(dataGrid),
                    new AppLogic.Presenters.Editor.EmployeePresenter(),
                    ProceedEditor(new Editors.EmployeeEditor(), "Employees"));
        }

        private void toolStripMenuItemGroups_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.GroupPresenter(dataGrid),
                    new AppLogic.Presenters.Editor.GroupPresenter(),
                    ProceedEditor(new Editors.GroupEditor(), "Groups"));
        }

        private void toolStripMenuItemOrders_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.OrderPresenter(dataGrid),
                    new AppLogic.Presenters.Editor.OrderPresenter(),
                    ProceedEditor(new Editors.OrderEditor(), "Orders"));

        }

        private void toolStripMenuItemServices_Click(object sender, EventArgs e)
        {
            //BindControlToPanelEditor(new Editors.ExecutingServiceEditor());
            _InitGui(new AppLogic.Presenters.DataGrid.ExecutingServicePresenter(dataGrid),
                    new AppLogic.Presenters.Editor.ExecutingServicePresenterEditor(),
                    ProceedEditor(new Editors.ExecutingServiceEditor(), "Services"));
        }

        private void toolStripMenuItemStore_Click(object sender, EventArgs e)
        {
            _InitGui(new AppLogic.Presenters.DataGrid.StorePresenter(dataGrid), 
                    new AppLogic.Presenters.Editor.StorePresenter(), 
                    ProceedEditor(new Editors.StoreEditor(), "Store"));
        }

        private void toolStripMenuItemWages_Click(object sender, EventArgs e)
        {
            BindControlToPanelEditor(new Editors.WageEditor());
        }

        #endregion

        private IEditorView ProceedEditor(IEditorView editor, string identificator)
        {
            editor.HideSaveCancelControls();

            // Pobranie uchwytu do bd
            var db = AppLogic.Core.ServiceManager.Database;

            // Pokaż tylko te, do których mamy uprawnienia
            var employee = (from emp in db.Employees where emp.EmployeeId == AppLogic.Core.LoggedUser.Instance.User.EmployeeId select emp).First();
            foreach (var group in employee.Groups)
            {
                if (group.Permissions.Any(p => p.Identyficator.Contains(identificator + "Edit")))
                    editor.ShowSaveCancelControls();
            }

            return editor;
        }

        private void toolStripMenuItemReports_Click(object sender, EventArgs e)
        {
            new CrystalReportsApplication.ReportsMainForm().ShowDialog();
        }

    }
}
