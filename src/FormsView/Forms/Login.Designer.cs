﻿namespace FormsView.Forms
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.workerLogin = new System.ComponentModel.BackgroundWorker();
            this.workerConnectionTest = new System.ComponentModel.BackgroundWorker();
            this.tabSignIn = new System.Windows.Forms.TabPage();
            this.labelAccount = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.textboxPassword = new System.Windows.Forms.MaskedTextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxAccount = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.buttonTestConnection = new System.Windows.Forms.Button();
            this.labelDbServerName = new System.Windows.Forms.Label();
            this.textBoxDbServerName = new System.Windows.Forms.TextBox();
            this.tabSignIn.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // workerLogin
            // 
            this.workerLogin.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerLogin_DoWork);
            // 
            // workerConnectionTest
            // 
            this.workerConnectionTest.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerConnectionTest_DoWork);
            // 
            // tabSignIn
            // 
            this.tabSignIn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabSignIn.Controls.Add(this.labelAccount);
            this.tabSignIn.Controls.Add(this.buttonLogin);
            this.tabSignIn.Controls.Add(this.textboxPassword);
            this.tabSignIn.Controls.Add(this.labelPassword);
            this.tabSignIn.Controls.Add(this.textBoxAccount);
            this.tabSignIn.Location = new System.Drawing.Point(4, 22);
            this.tabSignIn.Name = "tabSignIn";
            this.tabSignIn.Padding = new System.Windows.Forms.Padding(3);
            this.tabSignIn.Size = new System.Drawing.Size(305, 135);
            this.tabSignIn.TabIndex = 0;
            this.tabSignIn.Text = global::FormsView.TranslationsStatic.Default.tabSignIn;
            // 
            // labelAccount
            // 
            this.labelAccount.AutoSize = true;
            this.labelAccount.Location = new System.Drawing.Point(12, 33);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(105, 13);
            this.labelAccount.TabIndex = 10;
            this.labelAccount.Text = global::FormsView.TranslationsStatic.Default.labelLogin;
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(146, 82);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(117, 34);
            this.buttonLogin.TabIndex = 3;
            this.buttonLogin.Text = global::FormsView.TranslationsStatic.Default.buttonLogin;
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonSignIn_Click);
            // 
            // textboxPassword
            // 
            this.textboxPassword.Location = new System.Drawing.Point(123, 56);
            this.textboxPassword.Name = "textboxPassword";
            this.textboxPassword.PasswordChar = '*';
            this.textboxPassword.Size = new System.Drawing.Size(161, 20);
            this.textboxPassword.TabIndex = 2;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(78, 59);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(39, 13);
            this.labelPassword.TabIndex = 11;
            this.labelPassword.Text = global::FormsView.TranslationsStatic.Default.labelPassword;
            // 
            // textBoxAccount
            // 
            this.textBoxAccount.Location = new System.Drawing.Point(123, 30);
            this.textBoxAccount.Name = "textBoxAccount";
            this.textBoxAccount.Size = new System.Drawing.Size(161, 20);
            this.textBoxAccount.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabSignIn);
            this.tabControl.Controls.Add(this.tabSettings);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(313, 161);
            this.tabControl.TabIndex = 12;
            // 
            // tabSettings
            // 
            this.tabSettings.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabSettings.Controls.Add(this.buttonTestConnection);
            this.tabSettings.Controls.Add(this.labelDbServerName);
            this.tabSettings.Controls.Add(this.textBoxDbServerName);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabSettings.Size = new System.Drawing.Size(305, 135);
            this.tabSettings.TabIndex = 1;
            this.tabSettings.Text = global::FormsView.TranslationsStatic.Default.tabSettings;
            this.tabSettings.Enter += new System.EventHandler(this.LoadConfig);
            // 
            // buttonTestConnection
            // 
            this.buttonTestConnection.Location = new System.Drawing.Point(141, 80);
            this.buttonTestConnection.Name = "buttonTestConnection";
            this.buttonTestConnection.Size = new System.Drawing.Size(127, 23);
            this.buttonTestConnection.TabIndex = 2;
            this.buttonTestConnection.Text = global::FormsView.TranslationsStatic.Default.buttonTestConnection;
            this.buttonTestConnection.UseVisualStyleBackColor = true;
            this.buttonTestConnection.Click += new System.EventHandler(this.buttonTestConnection_Click);
            // 
            // labelDbServerName
            // 
            this.labelDbServerName.AutoSize = true;
            this.labelDbServerName.Location = new System.Drawing.Point(37, 34);
            this.labelDbServerName.Name = "labelDbServerName";
            this.labelDbServerName.Size = new System.Drawing.Size(83, 13);
            this.labelDbServerName.TabIndex = 1;
            this.labelDbServerName.Text = "Nazwa serwera:";
            // 
            // textBoxDbServerName
            // 
            this.textBoxDbServerName.Location = new System.Drawing.Point(126, 31);
            this.textBoxDbServerName.Name = "textBoxDbServerName";
            this.textBoxDbServerName.Size = new System.Drawing.Size(142, 20);
            this.textBoxDbServerName.TabIndex = 0;
            this.textBoxDbServerName.Leave += new System.EventHandler(this.SaveConfigEvent);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 187);
            this.Controls.Add(this.tabControl);
            this.MaximumSize = new System.Drawing.Size(355, 225);
            this.MinimumSize = new System.Drawing.Size(355, 225);
            this.Name = "Login";
            this.Text = global::FormsView.TranslationsStatic.Default.formTitleLogin;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnCloseEvent);
            this.tabSignIn.ResumeLayout(false);
            this.tabSignIn.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.tabSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker workerLogin;
        private System.ComponentModel.BackgroundWorker workerConnectionTest;
        private System.Windows.Forms.TabPage tabSignIn;
        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.MaskedTextBox textboxPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxAccount;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.Button buttonTestConnection;
        private System.Windows.Forms.Label labelDbServerName;
        private System.Windows.Forms.TextBox textBoxDbServerName;
    }
}