﻿namespace FormsView.Forms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.splitterHorizontal = new System.Windows.Forms.SplitContainer();
            this.splitterVertical = new System.Windows.Forms.SplitContainer();
            this.panelWithTable = new System.Windows.Forms.Panel();
            this.dataGrid = new FormsView.Controls.DataGrid();
            this.panelAdditionalData = new System.Windows.Forms.Panel();
            this.tableHorizontal = new System.Windows.Forms.TableLayoutPanel();
            this.buttonCollapseHorizontal = new System.Windows.Forms.Button();
            this.panelEditor = new System.Windows.Forms.Panel();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItemMainMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemClients = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemEmployees = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOrders = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemServices = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemStore = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuItemDictionaries = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicPartClass = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicCarBrand = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicCarModel = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicColor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicFuel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemDicServiceClass = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicService = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDicServiceStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemDicEmployeeState = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemDicProvinces = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemReports = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.buttonLogout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonChangePassword = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelUserInfo = new System.Windows.Forms.ToolStripLabel();
            ((System.ComponentModel.ISupportInitialize)(this.splitterHorizontal)).BeginInit();
            this.splitterHorizontal.Panel1.SuspendLayout();
            this.splitterHorizontal.Panel2.SuspendLayout();
            this.splitterHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitterVertical)).BeginInit();
            this.splitterVertical.Panel1.SuspendLayout();
            this.splitterVertical.Panel2.SuspendLayout();
            this.splitterVertical.SuspendLayout();
            this.panelWithTable.SuspendLayout();
            this.tableHorizontal.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.MaximumSize = new System.Drawing.Size(0, 38);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(150, 150);
            // 
            // splitterHorizontal
            // 
            this.splitterHorizontal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitterHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitterHorizontal.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitterHorizontal.IsSplitterFixed = true;
            this.splitterHorizontal.Location = new System.Drawing.Point(0, 24);
            this.splitterHorizontal.Name = "splitterHorizontal";
            // 
            // splitterHorizontal.Panel1
            // 
            this.splitterHorizontal.Panel1.Controls.Add(this.splitterVertical);
            this.splitterHorizontal.Panel1MinSize = 150;
            // 
            // splitterHorizontal.Panel2
            // 
            this.splitterHorizontal.Panel2.Controls.Add(this.tableHorizontal);
            this.splitterHorizontal.Panel2MinSize = 0;
            this.splitterHorizontal.Size = new System.Drawing.Size(1016, 650);
            this.splitterHorizontal.SplitterDistance = 454;
            this.splitterHorizontal.SplitterWidth = 7;
            this.splitterHorizontal.TabIndex = 4;
            // 
            // splitterVertical
            // 
            this.splitterVertical.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitterVertical.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitterVertical.Location = new System.Drawing.Point(0, 0);
            this.splitterVertical.Margin = new System.Windows.Forms.Padding(0);
            this.splitterVertical.MinimumSize = new System.Drawing.Size(0, 17);
            this.splitterVertical.Name = "splitterVertical";
            this.splitterVertical.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitterVertical.Panel1
            // 
            this.splitterVertical.Panel1.Controls.Add(this.panelWithTable);
            this.splitterVertical.Panel1MinSize = 150;
            // 
            // splitterVertical.Panel2
            // 
            this.splitterVertical.Panel2.Controls.Add(this.panelAdditionalData);
            this.splitterVertical.Panel2Collapsed = true;
            this.splitterVertical.Panel2MinSize = 17;
            this.splitterVertical.Size = new System.Drawing.Size(454, 650);
            this.splitterVertical.SplitterDistance = 150;
            this.splitterVertical.SplitterWidth = 7;
            this.splitterVertical.TabIndex = 0;
            // 
            // panelWithTable
            // 
            this.panelWithTable.Controls.Add(this.dataGrid);
            this.panelWithTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWithTable.Location = new System.Drawing.Point(0, 0);
            this.panelWithTable.Name = "panelWithTable";
            this.panelWithTable.Size = new System.Drawing.Size(452, 648);
            this.panelWithTable.TabIndex = 0;
            // 
            // dataGrid
            // 
            this.dataGrid.CurrentPage = 0;
            this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid.ItemsPerPage = 0;
            this.dataGrid.Location = new System.Drawing.Point(0, 0);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(452, 648);
            this.dataGrid.TabIndex = 0;
            this.dataGrid.TotalPages = 0;
            // 
            // panelAdditionalData
            // 
            this.panelAdditionalData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAdditionalData.Location = new System.Drawing.Point(0, 0);
            this.panelAdditionalData.Name = "panelAdditionalData";
            this.panelAdditionalData.Size = new System.Drawing.Size(148, 44);
            this.panelAdditionalData.TabIndex = 0;
            // 
            // tableHorizontal
            // 
            this.tableHorizontal.ColumnCount = 2;
            this.tableHorizontal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableHorizontal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableHorizontal.Controls.Add(this.buttonCollapseHorizontal, 0, 0);
            this.tableHorizontal.Controls.Add(this.panelEditor, 1, 0);
            this.tableHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableHorizontal.Location = new System.Drawing.Point(0, 0);
            this.tableHorizontal.MinimumSize = new System.Drawing.Size(0, 590);
            this.tableHorizontal.Name = "tableHorizontal";
            this.tableHorizontal.RowCount = 1;
            this.tableHorizontal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableHorizontal.Size = new System.Drawing.Size(553, 648);
            this.tableHorizontal.TabIndex = 0;
            // 
            // buttonCollapseHorizontal
            // 
            this.buttonCollapseHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCollapseHorizontal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCollapseHorizontal.Location = new System.Drawing.Point(0, 0);
            this.buttonCollapseHorizontal.Margin = new System.Windows.Forms.Padding(0);
            this.buttonCollapseHorizontal.Name = "buttonCollapseHorizontal";
            this.buttonCollapseHorizontal.Size = new System.Drawing.Size(15, 648);
            this.buttonCollapseHorizontal.TabIndex = 1;
            this.buttonCollapseHorizontal.Text = ">";
            this.buttonCollapseHorizontal.UseMnemonic = false;
            this.buttonCollapseHorizontal.UseVisualStyleBackColor = true;
            this.buttonCollapseHorizontal.Click += new System.EventHandler(this.buttonCollapseHorizontal_Click);
            // 
            // panelEditor
            // 
            this.panelEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEditor.Location = new System.Drawing.Point(18, 3);
            this.panelEditor.Name = "panelEditor";
            this.panelEditor.Size = new System.Drawing.Size(532, 642);
            this.panelEditor.TabIndex = 2;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemMainMenu,
            this.mainMenuItemDictionaries,
            this.toolStripMenuItemReports});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1016, 24);
            this.mainMenu.TabIndex = 5;
            this.mainMenu.Text = "mainMenu";
            // 
            // toolStripMenuItemMainMenu
            // 
            this.toolStripMenuItemMainMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemClients,
            this.toolStripMenuItemGroups,
            this.toolStripMenuItemEmployees,
            this.toolStripMenuItemOrders,
            this.toolStripMenuItemServices,
            this.toolStripMenuItemStore});
            this.toolStripMenuItemMainMenu.Name = "toolStripMenuItemMainMenu";
            this.toolStripMenuItemMainMenu.Size = new System.Drawing.Size(83, 20);
            this.toolStripMenuItemMainMenu.Text = "Menu główne";
            this.toolStripMenuItemMainMenu.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mainMenuItem_DropDownItemClicked);
            // 
            // toolStripMenuItemClients
            // 
            this.toolStripMenuItemClients.Name = "toolStripMenuItemClients";
            this.toolStripMenuItemClients.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemClients.Text = "Klienci";
            this.toolStripMenuItemClients.Click += new System.EventHandler(this.toolStripMenuItemClients_Click);
            // 
            // toolStripMenuItemGroups
            // 
            this.toolStripMenuItemGroups.Name = "toolStripMenuItemGroups";
            this.toolStripMenuItemGroups.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemGroups.Text = "Grupy";
            this.toolStripMenuItemGroups.Click += new System.EventHandler(this.toolStripMenuItemGroups_Click);
            // 
            // toolStripMenuItemEmployees
            // 
            this.toolStripMenuItemEmployees.Name = "toolStripMenuItemEmployees";
            this.toolStripMenuItemEmployees.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemEmployees.Text = "Pracownicy";
            this.toolStripMenuItemEmployees.Click += new System.EventHandler(this.toolStripMenuItemEmployees_Click);
            // 
            // toolStripMenuItemOrders
            // 
            this.toolStripMenuItemOrders.Name = "toolStripMenuItemOrders";
            this.toolStripMenuItemOrders.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemOrders.Text = "Zlecenia";
            this.toolStripMenuItemOrders.Click += new System.EventHandler(this.toolStripMenuItemOrders_Click);
            // 
            // toolStripMenuItemServices
            // 
            this.toolStripMenuItemServices.Name = "toolStripMenuItemServices";
            this.toolStripMenuItemServices.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemServices.Text = "Usługi";
            this.toolStripMenuItemServices.Click += new System.EventHandler(this.toolStripMenuItemServices_Click);
            // 
            // toolStripMenuItemStore
            // 
            this.toolStripMenuItemStore.Name = "toolStripMenuItemStore";
            this.toolStripMenuItemStore.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemStore.Text = "Magazyn";
            this.toolStripMenuItemStore.Click += new System.EventHandler(this.toolStripMenuItemStore_Click);
            // 
            // mainMenuItemDictionaries
            // 
            this.mainMenuItemDictionaries.CheckOnClick = true;
            this.mainMenuItemDictionaries.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemDicPartClass,
            this.menuItemDicCarBrand,
            this.menuItemDicCarModel,
            this.menuItemDicColor,
            this.menuItemDicFuel,
            this.toolStripSeparator3,
            this.menuItemDicServiceClass,
            this.menuItemDicService,
            this.menuItemDicServiceStatus,
            this.toolStripSeparator4,
            this.menuItemDicEmployeeState,
            this.toolStripSeparator5,
            this.menuItemDicProvinces});
            this.mainMenuItemDictionaries.Name = "mainMenuItemDictionaries";
            this.mainMenuItemDictionaries.Size = new System.Drawing.Size(57, 20);
            this.mainMenuItemDictionaries.Text = "Słowniki";
            this.mainMenuItemDictionaries.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mainMenuItem_DropDownItemClicked);
            // 
            // menuItemDicPartClass
            // 
            this.menuItemDicPartClass.Name = "menuItemDicPartClass";
            this.menuItemDicPartClass.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicPartClass.Text = "Kategorie części";
            this.menuItemDicPartClass.Click += new System.EventHandler(this.menuItemDicPartClass_Click);
            // 
            // menuItemDicCarBrand
            // 
            this.menuItemDicCarBrand.Name = "menuItemDicCarBrand";
            this.menuItemDicCarBrand.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicCarBrand.Text = "Marki samochodów";
            this.menuItemDicCarBrand.Click += new System.EventHandler(this.menuItemDicCarBrand_Click);
            // 
            // menuItemDicCarModel
            // 
            this.menuItemDicCarModel.Name = "menuItemDicCarModel";
            this.menuItemDicCarModel.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicCarModel.Text = "Modele samochodów";
            this.menuItemDicCarModel.Click += new System.EventHandler(this.menuItemDicCarModel_Click);
            // 
            // menuItemDicColor
            // 
            this.menuItemDicColor.Name = "menuItemDicColor";
            this.menuItemDicColor.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicColor.Text = "Kolory samochodów";
            this.menuItemDicColor.Click += new System.EventHandler(this.menuItemDicColor_Click);
            // 
            // menuItemDicFuel
            // 
            this.menuItemDicFuel.Name = "menuItemDicFuel";
            this.menuItemDicFuel.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicFuel.Tag = "SolidWorkshop";
            this.menuItemDicFuel.Text = "Rodzaje paliwa";
            this.menuItemDicFuel.Click += new System.EventHandler(this.menuItemDicFuel_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(170, 6);
            // 
            // menuItemDicServiceClass
            // 
            this.menuItemDicServiceClass.Name = "menuItemDicServiceClass";
            this.menuItemDicServiceClass.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicServiceClass.Text = "Kategorie usług";
            this.menuItemDicServiceClass.Click += new System.EventHandler(this.mainItemDicServiceClass_Click);
            // 
            // menuItemDicService
            // 
            this.menuItemDicService.Name = "menuItemDicService";
            this.menuItemDicService.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicService.Text = "Typy usług";
            this.menuItemDicService.Click += new System.EventHandler(this.menuItemDicService_Click);
            // 
            // menuItemDicServiceStatus
            // 
            this.menuItemDicServiceStatus.Name = "menuItemDicServiceStatus";
            this.menuItemDicServiceStatus.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicServiceStatus.Text = "Statusy zleceń";
            this.menuItemDicServiceStatus.Click += new System.EventHandler(this.menuItemDicServiceStatus_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(170, 6);
            // 
            // menuItemDicEmployeeState
            // 
            this.menuItemDicEmployeeState.Name = "menuItemDicEmployeeState";
            this.menuItemDicEmployeeState.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicEmployeeState.Text = "Rodzaje zatrudnień";
            this.menuItemDicEmployeeState.Click += new System.EventHandler(this.menuItemDicEmployeeStatus_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(170, 6);
            // 
            // menuItemDicProvinces
            // 
            this.menuItemDicProvinces.Name = "menuItemDicProvinces";
            this.menuItemDicProvinces.Size = new System.Drawing.Size(173, 22);
            this.menuItemDicProvinces.Text = "Województwa";
            this.menuItemDicProvinces.Click += new System.EventHandler(this.województwaToolStripMenuItem_Click);
            // 
            // toolStripMenuItemReports
            // 
            this.toolStripMenuItemReports.Name = "toolStripMenuItemReports";
            this.toolStripMenuItemReports.Size = new System.Drawing.Size(58, 20);
            this.toolStripMenuItemReports.Text = global::FormsView.TranslationsMainMenu.Default.mainItemReports;
            this.toolStripMenuItemReports.Click += new System.EventHandler(this.toolStripMenuItemReports_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonLogout,
            this.toolStripSeparator1,
            this.buttonChangePassword,
            this.toolStripSeparator2,
            this.toolStripLabelUserInfo});
            this.toolStrip.Location = new System.Drawing.Point(0, 674);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip.Size = new System.Drawing.Size(1016, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip";
            // 
            // buttonLogout
            // 
            this.buttonLogout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonLogout.Image = ((System.Drawing.Image)(resources.GetObject("buttonLogout.Image")));
            this.buttonLogout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(50, 22);
            this.buttonLogout.Text = "&Wyloguj";
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // buttonChangePassword
            // 
            this.buttonChangePassword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonChangePassword.Image = ((System.Drawing.Image)(resources.GetObject("buttonChangePassword.Image")));
            this.buttonChangePassword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonChangePassword.Name = "buttonChangePassword";
            this.buttonChangePassword.Size = new System.Drawing.Size(68, 22);
            this.buttonChangePassword.Text = global::FormsView.TranslationsStatic.Default.buttonChangePassword;
            this.buttonChangePassword.Click += new System.EventHandler(this.buttonChangePassword_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabelUserInfo
            // 
            this.toolStripLabelUserInfo.Name = "toolStripLabelUserInfo";
            this.toolStripLabelUserInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripLabelUserInfo.Size = new System.Drawing.Size(157, 22);
            this.toolStripLabelUserInfo.Text = "Zalogowany jako: {0} {1} ({2})";
            this.toolStripLabelUserInfo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 699);
            this.Controls.Add(this.splitterHorizontal);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "Main";
            this.Text = "Okno główne";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosingEvent);
            this.Load += new System.EventHandler(this.OnLoadEvent);
            this.splitterHorizontal.Panel1.ResumeLayout(false);
            this.splitterHorizontal.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitterHorizontal)).EndInit();
            this.splitterHorizontal.ResumeLayout(false);
            this.splitterVertical.Panel1.ResumeLayout(false);
            this.splitterVertical.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitterVertical)).EndInit();
            this.splitterVertical.ResumeLayout(false);
            this.panelWithTable.ResumeLayout(false);
            this.tableHorizontal.ResumeLayout(false);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitterHorizontal;
        private System.Windows.Forms.SplitContainer splitterVertical;
        private System.Windows.Forms.Panel panelWithTable;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicPartClass;
        private System.Windows.Forms.TableLayoutPanel tableHorizontal;
        private System.Windows.Forms.Button buttonCollapseHorizontal;
        private FormsView.Controls.DataGrid dataGrid;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicServiceClass;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicCarBrand;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicCarModel;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicColor;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicFuel;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicServiceStatus;
        private System.Windows.Forms.ToolStripMenuItem mainMenuItemDictionaries;
        private System.Windows.Forms.Panel panelEditor;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMainMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemClients;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEmployees;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemGroups;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOrders;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemServices;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemStore;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton buttonLogout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabelUserInfo;
        private System.Windows.Forms.ToolStripButton buttonChangePassword;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicService;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicEmployeeState;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem menuItemDicProvinces;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemReports;
        private System.Windows.Forms.Panel panelAdditionalData;

    }
}