﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters.Editor;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Popups;
using AppLogic.Presenters;

namespace FormsView.Forms
{
    public partial class PopupContainer : Base.BaseForm, IPopupContainer
    {
        /// <summary>
        /// Logika kontenera
        /// </summary>
        PopupContainerPresenter _popupPresenter;

        /// <summary>
        /// Adapter z widokiem głównej kontrolki
        /// </summary>
        IPopupView _view;

        /// <summary>
        /// Adapter z logiką głównej kontrolki
        /// </summary>
        IPopupPresenter _presenter;

        public PopupContainer()
        {
            InitializeComponent();
            _popupPresenter = new PopupContainerPresenter(this);
        }

        #region IPopupContainer

        public void Initialize(IPopupView view, IPopupPresenter presenter)
        {
            _view = view;
            _presenter = presenter;

            var control = _view as Control;

            _ResizeWindow(control.Width, control.Height);
            _BindControl(control);
        }

        public void ClosePopup()
        {
            Close();
        }

        #endregion IPopupContainer

        /// <summary>
        /// Przypięcie kontrolki do okineka
        /// </summary>
        /// <param name="control">Kontrolka, która ma zostać wsadzona do kontenera</param>
        private void _BindControl(Control control)
        {
            control.Dock = DockStyle.Fill;
            Controls.Add(control);
        }

        /// <summary>
        /// Metoda ustawia rozmiar okienka kontenera
        /// </summary>
        /// <param name="width">Nowa szerokość</param>
        /// <param name="height">Nowa wysokość</param>
        private void _ResizeWindow(int width, int height)
        {
            Width =  width + 30;
            Height = height + 30;
        }

        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            // Odpięcie zdarzeń
            _presenter.UnbindEvents();
        }
    }
}
