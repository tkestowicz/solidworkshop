﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces;
using AppLogic.Presenters;

namespace FormsView.Forms
{
    /// <summary>
    /// Klasa reprezentująca okienko logowania
    /// </summary>
    public partial class Login : Base.BaseForm, ILoginView
    {

        #region Fields

        /// <summary>
        /// Referencja do obiektu okna głównego
        /// </summary>
        IMainView _view;

        /// <summary>
        /// Obiekt prezentera okienka logowania
        /// </summary>
        LoginPresenter _presenter;

        #endregion

        #region Properties

        public string ServerName
        {
            get
            {
                return textBoxDbServerName.Text;
            }
            set
            {
                textBoxDbServerName.Text = value;
            }
        }

        #endregion

        /// <summary>
        /// Konstruktor okienka logowania
        /// </summary>
        /// <param name="view">Referencja do głównego okna aplikacji. Jeżeli logowanie się nie uda można zamknąć okienko.</param>
        public Login(IMainView view)
        {
            InitializeComponent();

            _view = view;

            // Tworzymy obiekt prezentera i wiążemy go z widokiem
            _presenter = new LoginPresenter(this);
        }

        #region Events

        /// <summary>
        /// Metoda zamyka cała aplikację po kliknięciu w przycisk zamknij.
        /// W przypadku poprawnego zalogowania do systemu zdarzenie zostaje odpięte.
        /// </summary>
        private void OnCloseEvent(object sender, FormClosingEventArgs e)
        {
            _view.Close();
        }

        /// <summary>
        /// Reakcja na kliknięcie przycisku logowania
        /// </summary>
        private void buttonSignIn_Click(object sender, EventArgs e)
        {
            buttonLogin.Enabled = false;
            workerLogin.RunWorkerAsync();
        }

        /// <summary>
        /// Metoda ładuje nazwę serwera po przełączeniu zakładki
        /// </summary>
        private void LoadConfig(object sender, EventArgs e)
        {
            _presenter.LoadServerName();
        }

        /// <summary>
        /// Metoda zapisuje nową nazwę serwera BD
        /// </summary>
        private void SaveConfigEvent(object sender, EventArgs e)
        {
            _presenter.SaveServerName();
        }

        /// <summary>
        /// Wykonanie testu połączenia za bazą
        /// </summary>
        private void buttonTestConnection_Click(object sender, EventArgs e)
        {
            buttonTestConnection.Enabled = false;
            workerConnectionTest.RunWorkerAsync();
        }

        #endregion

        #region ILoginView

        public void LoginSucceed()
        {
            this.Invoke(new MethodInvoker(delegate {

                // Odpinamy zdarzenie zamknięcia okna głównego
                FormClosing -= new System.Windows.Forms.FormClosingEventHandler(this.OnCloseEvent);

                _view.InitMainView();

                // Zamknięcie okna logowania
                Close();
            
            }));
        }

        public void LoginFailed()
        {
            // Tutaj użycie typu anonimowego z rzutowaniem
            this.Invoke((MethodInvoker) delegate { buttonLogin.Enabled = true; });
        }

        public void ResetSettingsPanel()
        {
            // Wykorzystano Action w ramach podejścia alternatywnego, nie wiem czy jest jakaś znacząca różnica.
            this.Invoke(new Action(() => buttonTestConnection.Enabled = true ));
        }

        #endregion

        /// <summary>
        /// Odpalenie logowania w osobnym wątku
        /// </summary>
        private void workerLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            _presenter.SignIn(textBoxAccount.Text, textboxPassword.Text);
        }

        /// <summary>
        /// Odpalenie testu połączenia w osobnym wątku
        /// </summary>
        private void workerConnectionTest_DoWork(object sender, DoWorkEventArgs e)
        {
            _presenter.ConnectionTest();
        }

    }
}
