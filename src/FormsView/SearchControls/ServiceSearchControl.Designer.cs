﻿namespace FormsView.SearchControls
{
    partial class ServiceSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxServiceSearch = new System.Windows.Forms.GroupBox();
            this.checkBoxServiceState = new System.Windows.Forms.CheckBox();
            this.checkBoxServiceName = new System.Windows.Forms.CheckBox();
            this.checkBoxServiceMechanic = new System.Windows.Forms.CheckBox();
            this.checkBoxPriceTo = new System.Windows.Forms.CheckBox();
            this.checkBoxPriceFrom = new System.Windows.Forms.CheckBox();
            this.labelPriceTo = new System.Windows.Forms.Label();
            this.labelPriceFrom = new System.Windows.Forms.Label();
            this.textBoxServiceName = new System.Windows.Forms.TextBox();
            this.labelServiceName = new System.Windows.Forms.Label();
            this.comboBoxServiceState = new System.Windows.Forms.ComboBox();
            this.labelServiceState = new System.Windows.Forms.Label();
            this.comboBoxServiceMechanic = new System.Windows.Forms.ComboBox();
            this.labelServiceMechanic = new System.Windows.Forms.Label();
            this.labelServicePrice = new System.Windows.Forms.Label();
            this.findClearControls1 = new FormsView.Controls.FindClearControls();
            this.textBoxPriceFrom = new System.Windows.Forms.MaskedTextBox();
            this.textBoxPriceTo = new System.Windows.Forms.MaskedTextBox();
            this.groupBoxServiceSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxServiceSearch
            // 
            this.groupBoxServiceSearch.Controls.Add(this.textBoxPriceTo);
            this.groupBoxServiceSearch.Controls.Add(this.textBoxPriceFrom);
            this.groupBoxServiceSearch.Controls.Add(this.checkBoxServiceState);
            this.groupBoxServiceSearch.Controls.Add(this.checkBoxServiceName);
            this.groupBoxServiceSearch.Controls.Add(this.checkBoxServiceMechanic);
            this.groupBoxServiceSearch.Controls.Add(this.checkBoxPriceTo);
            this.groupBoxServiceSearch.Controls.Add(this.checkBoxPriceFrom);
            this.groupBoxServiceSearch.Controls.Add(this.labelPriceTo);
            this.groupBoxServiceSearch.Controls.Add(this.labelPriceFrom);
            this.groupBoxServiceSearch.Controls.Add(this.textBoxServiceName);
            this.groupBoxServiceSearch.Controls.Add(this.labelServiceName);
            this.groupBoxServiceSearch.Controls.Add(this.comboBoxServiceState);
            this.groupBoxServiceSearch.Controls.Add(this.labelServiceState);
            this.groupBoxServiceSearch.Controls.Add(this.comboBoxServiceMechanic);
            this.groupBoxServiceSearch.Controls.Add(this.labelServiceMechanic);
            this.groupBoxServiceSearch.Controls.Add(this.labelServicePrice);
            this.groupBoxServiceSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxServiceSearch.Location = new System.Drawing.Point(0, 0);
            this.groupBoxServiceSearch.Name = "groupBoxServiceSearch";
            this.groupBoxServiceSearch.Size = new System.Drawing.Size(314, 198);
            this.groupBoxServiceSearch.TabIndex = 0;
            this.groupBoxServiceSearch.TabStop = false;
            this.groupBoxServiceSearch.Text = global::FormsView.TranslationsStatic.Default.titleService;
            // 
            // checkBoxServiceState
            // 
            this.checkBoxServiceState.AutoSize = true;
            this.checkBoxServiceState.Location = new System.Drawing.Point(86, 31);
            this.checkBoxServiceState.Name = "checkBoxServiceState";
            this.checkBoxServiceState.Size = new System.Drawing.Size(15, 14);
            this.checkBoxServiceState.TabIndex = 132;
            this.checkBoxServiceState.UseVisualStyleBackColor = true;
            this.checkBoxServiceState.CheckedChanged += new System.EventHandler(this.checkBoxServiceState_CheckedChanged);
            // 
            // checkBoxServiceName
            // 
            this.checkBoxServiceName.AutoSize = true;
            this.checkBoxServiceName.Location = new System.Drawing.Point(86, 72);
            this.checkBoxServiceName.Name = "checkBoxServiceName";
            this.checkBoxServiceName.Size = new System.Drawing.Size(15, 14);
            this.checkBoxServiceName.TabIndex = 131;
            this.checkBoxServiceName.UseVisualStyleBackColor = true;
            this.checkBoxServiceName.CheckedChanged += new System.EventHandler(this.checkBoxServiceName_CheckedChanged);
            // 
            // checkBoxServiceMechanic
            // 
            this.checkBoxServiceMechanic.AutoSize = true;
            this.checkBoxServiceMechanic.Location = new System.Drawing.Point(86, 151);
            this.checkBoxServiceMechanic.Name = "checkBoxServiceMechanic";
            this.checkBoxServiceMechanic.Size = new System.Drawing.Size(15, 14);
            this.checkBoxServiceMechanic.TabIndex = 130;
            this.checkBoxServiceMechanic.UseVisualStyleBackColor = true;
            this.checkBoxServiceMechanic.CheckedChanged += new System.EventHandler(this.checkBoxServiceMechanic_CheckedChanged);
            // 
            // checkBoxPriceTo
            // 
            this.checkBoxPriceTo.AutoSize = true;
            this.checkBoxPriceTo.Location = new System.Drawing.Point(187, 112);
            this.checkBoxPriceTo.Name = "checkBoxPriceTo";
            this.checkBoxPriceTo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPriceTo.TabIndex = 128;
            this.checkBoxPriceTo.UseVisualStyleBackColor = true;
            this.checkBoxPriceTo.CheckedChanged += new System.EventHandler(this.checkBoxPriceTo_CheckedChanged);
            // 
            // checkBoxPriceFrom
            // 
            this.checkBoxPriceFrom.AutoSize = true;
            this.checkBoxPriceFrom.Location = new System.Drawing.Point(86, 112);
            this.checkBoxPriceFrom.Name = "checkBoxPriceFrom";
            this.checkBoxPriceFrom.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPriceFrom.TabIndex = 127;
            this.checkBoxPriceFrom.UseVisualStyleBackColor = true;
            this.checkBoxPriceFrom.CheckedChanged += new System.EventHandler(this.checkBoxPriceFrom_CheckedChanged);
            // 
            // labelPriceTo
            // 
            this.labelPriceTo.AutoSize = true;
            this.labelPriceTo.Location = new System.Drawing.Point(206, 112);
            this.labelPriceTo.Name = "labelPriceTo";
            this.labelPriceTo.Size = new System.Drawing.Size(24, 13);
            this.labelPriceTo.TabIndex = 126;
            this.labelPriceTo.Text = "Do:";
            // 
            // labelPriceFrom
            // 
            this.labelPriceFrom.AutoSize = true;
            this.labelPriceFrom.Location = new System.Drawing.Point(104, 112);
            this.labelPriceFrom.Name = "labelPriceFrom";
            this.labelPriceFrom.Size = new System.Drawing.Size(24, 13);
            this.labelPriceFrom.TabIndex = 125;
            this.labelPriceFrom.Text = "Od:";
            // 
            // textBoxServiceName
            // 
            this.textBoxServiceName.Enabled = false;
            this.textBoxServiceName.Location = new System.Drawing.Point(107, 69);
            this.textBoxServiceName.Name = "textBoxServiceName";
            this.textBoxServiceName.Size = new System.Drawing.Size(180, 20);
            this.textBoxServiceName.TabIndex = 50;
            this.textBoxServiceName.Tag = "searchParam";
            // 
            // labelServiceName
            // 
            this.labelServiceName.AutoSize = true;
            this.labelServiceName.Location = new System.Drawing.Point(17, 69);
            this.labelServiceName.Name = "labelServiceName";
            this.labelServiceName.Size = new System.Drawing.Size(43, 13);
            this.labelServiceName.TabIndex = 49;
            this.labelServiceName.Text = global::FormsView.TranslationsStatic.Default.labelName;
            // 
            // comboBoxServiceState
            // 
            this.comboBoxServiceState.DisplayMember = "Value";
            this.comboBoxServiceState.Enabled = false;
            this.comboBoxServiceState.FormattingEnabled = true;
            this.comboBoxServiceState.Location = new System.Drawing.Point(107, 28);
            this.comboBoxServiceState.Name = "comboBoxServiceState";
            this.comboBoxServiceState.Size = new System.Drawing.Size(180, 21);
            this.comboBoxServiceState.TabIndex = 48;
            this.comboBoxServiceState.Tag = "searchParam";
            this.comboBoxServiceState.ValueMember = "Value";
            // 
            // labelServiceState
            // 
            this.labelServiceState.AutoSize = true;
            this.labelServiceState.Location = new System.Drawing.Point(17, 28);
            this.labelServiceState.Name = "labelServiceState";
            this.labelServiceState.Size = new System.Drawing.Size(40, 13);
            this.labelServiceState.TabIndex = 47;
            this.labelServiceState.Text = global::FormsView.TranslationsStatic.Default.labelState;
            // 
            // comboBoxServiceMechanic
            // 
            this.comboBoxServiceMechanic.DisplayMember = "Value";
            this.comboBoxServiceMechanic.Enabled = false;
            this.comboBoxServiceMechanic.FormattingEnabled = true;
            this.comboBoxServiceMechanic.Location = new System.Drawing.Point(107, 148);
            this.comboBoxServiceMechanic.Name = "comboBoxServiceMechanic";
            this.comboBoxServiceMechanic.Size = new System.Drawing.Size(180, 21);
            this.comboBoxServiceMechanic.TabIndex = 46;
            this.comboBoxServiceMechanic.Tag = "searchParam";
            this.comboBoxServiceMechanic.ValueMember = "Value";
            // 
            // labelServiceMechanic
            // 
            this.labelServiceMechanic.AutoSize = true;
            this.labelServiceMechanic.Location = new System.Drawing.Point(17, 148);
            this.labelServiceMechanic.Name = "labelServiceMechanic";
            this.labelServiceMechanic.Size = new System.Drawing.Size(60, 13);
            this.labelServiceMechanic.TabIndex = 42;
            this.labelServiceMechanic.Text = global::FormsView.TranslationsStatic.Default.labelServceMechanic;
            // 
            // labelServicePrice
            // 
            this.labelServicePrice.AutoSize = true;
            this.labelServicePrice.Location = new System.Drawing.Point(17, 112);
            this.labelServicePrice.Name = "labelServicePrice";
            this.labelServicePrice.Size = new System.Drawing.Size(35, 13);
            this.labelServicePrice.TabIndex = 41;
            this.labelServicePrice.Text = global::FormsView.TranslationsStatic.Default.labelPrice;
            // 
            // findClearControls1
            // 
            this.findClearControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.findClearControls1.Location = new System.Drawing.Point(0, 209);
            this.findClearControls1.Name = "findClearControls1";
            this.findClearControls1.Sender = null;
            this.findClearControls1.Size = new System.Drawing.Size(314, 25);
            this.findClearControls1.TabIndex = 1;
            // 
            // textBoxPriceFrom
            // 
            this.textBoxPriceFrom.Enabled = false;
            this.textBoxPriceFrom.Location = new System.Drawing.Point(126, 109);
            this.textBoxPriceFrom.Mask = "00000.00";
            this.textBoxPriceFrom.Name = "textBoxPriceFrom";
            this.textBoxPriceFrom.Size = new System.Drawing.Size(55, 20);
            this.textBoxPriceFrom.TabIndex = 147;
            this.textBoxPriceFrom.Tag = "searchParam";
            // 
            // textBoxPriceTo
            // 
            this.textBoxPriceTo.Enabled = false;
            this.textBoxPriceTo.Location = new System.Drawing.Point(232, 109);
            this.textBoxPriceTo.Mask = "00000.00";
            this.textBoxPriceTo.Name = "textBoxPriceTo";
            this.textBoxPriceTo.Size = new System.Drawing.Size(55, 20);
            this.textBoxPriceTo.TabIndex = 148;
            this.textBoxPriceTo.Tag = "searchParam";
            // 
            // ServiceSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.findClearControls1);
            this.Controls.Add(this.groupBoxServiceSearch);
            this.Name = "ServiceSearchControl";
            this.Size = new System.Drawing.Size(314, 234);
            this.groupBoxServiceSearch.ResumeLayout(false);
            this.groupBoxServiceSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxServiceSearch;
        private Controls.FindClearControls findClearControls1;
        private System.Windows.Forms.TextBox textBoxServiceName;
        private System.Windows.Forms.Label labelServiceName;
        private System.Windows.Forms.ComboBox comboBoxServiceState;
        private System.Windows.Forms.Label labelServiceState;
        private System.Windows.Forms.ComboBox comboBoxServiceMechanic;
        private System.Windows.Forms.Label labelServiceMechanic;
        private System.Windows.Forms.Label labelServicePrice;
        private System.Windows.Forms.CheckBox checkBoxServiceState;
        private System.Windows.Forms.CheckBox checkBoxServiceName;
        private System.Windows.Forms.CheckBox checkBoxServiceMechanic;
        private System.Windows.Forms.CheckBox checkBoxPriceTo;
        private System.Windows.Forms.CheckBox checkBoxPriceFrom;
        private System.Windows.Forms.Label labelPriceTo;
        private System.Windows.Forms.Label labelPriceFrom;
        private System.Windows.Forms.MaskedTextBox textBoxPriceTo;
        private System.Windows.Forms.MaskedTextBox textBoxPriceFrom;
    }
}
