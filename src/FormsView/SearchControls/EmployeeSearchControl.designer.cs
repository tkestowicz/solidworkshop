﻿namespace FormsView.SearchControls
{
    partial class EmployeeSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.dateTimeReleaseDateTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimeReleaseDateFrom = new System.Windows.Forms.DateTimePicker();
            this.checkBoxReleaseDateTo = new System.Windows.Forms.CheckBox();
            this.checkBoxReleaseDateFrom = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimeHireDateTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimeHireDateFrom = new System.Windows.Forms.DateTimePicker();
            this.checkBoxHireTimeTo = new System.Windows.Forms.CheckBox();
            this.checkBoxHireTimeFrom = new System.Windows.Forms.CheckBox();
            this.checkBoxGroup = new System.Windows.Forms.CheckBox();
            this.labelHireTimeTo = new System.Windows.Forms.Label();
            this.labelHireTimeFrom = new System.Windows.Forms.Label();
            this.labelEmployeeReleaseTime = new System.Windows.Forms.Label();
            this.labelEmployeeHireTime = new System.Windows.Forms.Label();
            this.comboBoxGroup = new System.Windows.Forms.ComboBox();
            this.labelEmployeeGroup = new System.Windows.Forms.Label();
            this.checkBoxPhoneNumber = new System.Windows.Forms.CheckBox();
            this.checkBoxPesel = new System.Windows.Forms.CheckBox();
            this.checkBoxSurname = new System.Windows.Forms.CheckBox();
            this.checkBoxName = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxPesel = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxPhone = new System.Windows.Forms.MaskedTextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.labelClientFirstName = new System.Windows.Forms.Label();
            this.labelClientSurname = new System.Windows.Forms.Label();
            this.labelClientPhone = new System.Windows.Forms.Label();
            this.labelClientPesel = new System.Windows.Forms.Label();
            this.checkBoxStreet = new System.Windows.Forms.CheckBox();
            this.checkBoxPostcode = new System.Windows.Forms.CheckBox();
            this.checkBoxCity = new System.Windows.Forms.CheckBox();
            this.checkBoxProvince = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxAddressPostcode = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxAddressProvince = new System.Windows.Forms.ComboBox();
            this.labelAddressProvince = new System.Windows.Forms.Label();
            this.textBoxAddressStreet = new System.Windows.Forms.TextBox();
            this.labelAddressStreet = new System.Windows.Forms.Label();
            this.labelAddressPostcode = new System.Windows.Forms.Label();
            this.comboBoxAddressCity = new System.Windows.Forms.ComboBox();
            this.labelAddressCity = new System.Windows.Forms.Label();
            this.findClearControls1 = new FormsView.Controls.FindClearControls();
            this.groupBoxEmployee.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.Controls.Add(this.dateTimeReleaseDateTo);
            this.groupBoxEmployee.Controls.Add(this.dateTimeReleaseDateFrom);
            this.groupBoxEmployee.Controls.Add(this.checkBoxReleaseDateTo);
            this.groupBoxEmployee.Controls.Add(this.checkBoxReleaseDateFrom);
            this.groupBoxEmployee.Controls.Add(this.label1);
            this.groupBoxEmployee.Controls.Add(this.label2);
            this.groupBoxEmployee.Controls.Add(this.dateTimeHireDateTo);
            this.groupBoxEmployee.Controls.Add(this.dateTimeHireDateFrom);
            this.groupBoxEmployee.Controls.Add(this.checkBoxHireTimeTo);
            this.groupBoxEmployee.Controls.Add(this.checkBoxHireTimeFrom);
            this.groupBoxEmployee.Controls.Add(this.checkBoxGroup);
            this.groupBoxEmployee.Controls.Add(this.labelHireTimeTo);
            this.groupBoxEmployee.Controls.Add(this.labelHireTimeFrom);
            this.groupBoxEmployee.Controls.Add(this.labelEmployeeReleaseTime);
            this.groupBoxEmployee.Controls.Add(this.labelEmployeeHireTime);
            this.groupBoxEmployee.Controls.Add(this.comboBoxGroup);
            this.groupBoxEmployee.Controls.Add(this.labelEmployeeGroup);
            this.groupBoxEmployee.Controls.Add(this.checkBoxPhoneNumber);
            this.groupBoxEmployee.Controls.Add(this.checkBoxPesel);
            this.groupBoxEmployee.Controls.Add(this.checkBoxSurname);
            this.groupBoxEmployee.Controls.Add(this.checkBoxName);
            this.groupBoxEmployee.Controls.Add(this.maskedTextBoxPesel);
            this.groupBoxEmployee.Controls.Add(this.maskedTextBoxPhone);
            this.groupBoxEmployee.Controls.Add(this.textBoxFirstName);
            this.groupBoxEmployee.Controls.Add(this.textBoxSurname);
            this.groupBoxEmployee.Controls.Add(this.labelClientFirstName);
            this.groupBoxEmployee.Controls.Add(this.labelClientSurname);
            this.groupBoxEmployee.Controls.Add(this.labelClientPhone);
            this.groupBoxEmployee.Controls.Add(this.labelClientPesel);
            this.groupBoxEmployee.Controls.Add(this.checkBoxStreet);
            this.groupBoxEmployee.Controls.Add(this.checkBoxPostcode);
            this.groupBoxEmployee.Controls.Add(this.checkBoxCity);
            this.groupBoxEmployee.Controls.Add(this.checkBoxProvince);
            this.groupBoxEmployee.Controls.Add(this.maskedTextBoxAddressPostcode);
            this.groupBoxEmployee.Controls.Add(this.comboBoxAddressProvince);
            this.groupBoxEmployee.Controls.Add(this.labelAddressProvince);
            this.groupBoxEmployee.Controls.Add(this.textBoxAddressStreet);
            this.groupBoxEmployee.Controls.Add(this.labelAddressStreet);
            this.groupBoxEmployee.Controls.Add(this.labelAddressPostcode);
            this.groupBoxEmployee.Controls.Add(this.comboBoxAddressCity);
            this.groupBoxEmployee.Controls.Add(this.labelAddressCity);
            this.groupBoxEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEmployee.Location = new System.Drawing.Point(0, 0);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(398, 498);
            this.groupBoxEmployee.TabIndex = 1;
            this.groupBoxEmployee.TabStop = false;
            this.groupBoxEmployee.Text = "Pracownik";
            // 
            // dateTimeReleaseDateTo
            // 
            this.dateTimeReleaseDateTo.CustomFormat = "";
            this.dateTimeReleaseDateTo.Enabled = false;
            this.dateTimeReleaseDateTo.Location = new System.Drawing.Point(246, 313);
            this.dateTimeReleaseDateTo.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTimeReleaseDateTo.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimeReleaseDateTo.Name = "dateTimeReleaseDateTo";
            this.dateTimeReleaseDateTo.Size = new System.Drawing.Size(137, 20);
            this.dateTimeReleaseDateTo.TabIndex = 127;
            this.dateTimeReleaseDateTo.Tag = "searchParam";
            // 
            // dateTimeReleaseDateFrom
            // 
            this.dateTimeReleaseDateFrom.CustomFormat = "";
            this.dateTimeReleaseDateFrom.Enabled = false;
            this.dateTimeReleaseDateFrom.Location = new System.Drawing.Point(65, 313);
            this.dateTimeReleaseDateFrom.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTimeReleaseDateFrom.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimeReleaseDateFrom.Name = "dateTimeReleaseDateFrom";
            this.dateTimeReleaseDateFrom.Size = new System.Drawing.Size(127, 20);
            this.dateTimeReleaseDateFrom.TabIndex = 126;
            this.dateTimeReleaseDateFrom.Tag = "searchParam";
            // 
            // checkBoxReleaseDateTo
            // 
            this.checkBoxReleaseDateTo.AutoSize = true;
            this.checkBoxReleaseDateTo.Location = new System.Drawing.Point(225, 316);
            this.checkBoxReleaseDateTo.Name = "checkBoxReleaseDateTo";
            this.checkBoxReleaseDateTo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxReleaseDateTo.TabIndex = 125;
            this.checkBoxReleaseDateTo.UseVisualStyleBackColor = true;
            this.checkBoxReleaseDateTo.CheckedChanged += new System.EventHandler(this.checkBoxReleaseDateTo_CheckedChanged);
            // 
            // checkBoxReleaseDateFrom
            // 
            this.checkBoxReleaseDateFrom.AutoSize = true;
            this.checkBoxReleaseDateFrom.Location = new System.Drawing.Point(44, 315);
            this.checkBoxReleaseDateFrom.Name = "checkBoxReleaseDateFrom";
            this.checkBoxReleaseDateFrom.Size = new System.Drawing.Size(15, 14);
            this.checkBoxReleaseDateFrom.TabIndex = 124;
            this.checkBoxReleaseDateFrom.UseVisualStyleBackColor = true;
            this.checkBoxReleaseDateFrom.CheckedChanged += new System.EventHandler(this.checkBoxReleaseDateFrom_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Location = new System.Drawing.Point(198, 316);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 123;
            this.label1.Text = global::FormsView.TranslationsStatic.Default.labelTo;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Location = new System.Drawing.Point(17, 315);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 122;
            this.label2.Text = global::FormsView.TranslationsStatic.Default.labelFrom;
            // 
            // dateTimeHireDateTo
            // 
            this.dateTimeHireDateTo.CustomFormat = "";
            this.dateTimeHireDateTo.Enabled = false;
            this.dateTimeHireDateTo.Location = new System.Drawing.Point(246, 250);
            this.dateTimeHireDateTo.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTimeHireDateTo.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimeHireDateTo.Name = "dateTimeHireDateTo";
            this.dateTimeHireDateTo.Size = new System.Drawing.Size(137, 20);
            this.dateTimeHireDateTo.TabIndex = 121;
            this.dateTimeHireDateTo.Tag = "searchParam";
            // 
            // dateTimeHireDateFrom
            // 
            this.dateTimeHireDateFrom.CustomFormat = "";
            this.dateTimeHireDateFrom.Enabled = false;
            this.dateTimeHireDateFrom.Location = new System.Drawing.Point(65, 250);
            this.dateTimeHireDateFrom.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTimeHireDateFrom.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimeHireDateFrom.Name = "dateTimeHireDateFrom";
            this.dateTimeHireDateFrom.Size = new System.Drawing.Size(127, 20);
            this.dateTimeHireDateFrom.TabIndex = 119;
            this.dateTimeHireDateFrom.Tag = "searchParam";
            // 
            // checkBoxHireTimeTo
            // 
            this.checkBoxHireTimeTo.AutoSize = true;
            this.checkBoxHireTimeTo.Location = new System.Drawing.Point(225, 253);
            this.checkBoxHireTimeTo.Name = "checkBoxHireTimeTo";
            this.checkBoxHireTimeTo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxHireTimeTo.TabIndex = 116;
            this.checkBoxHireTimeTo.UseVisualStyleBackColor = true;
            this.checkBoxHireTimeTo.CheckedChanged += new System.EventHandler(this.checkBoxHireTimeTo_CheckedChanged);
            // 
            // checkBoxHireTimeFrom
            // 
            this.checkBoxHireTimeFrom.AutoSize = true;
            this.checkBoxHireTimeFrom.Location = new System.Drawing.Point(44, 252);
            this.checkBoxHireTimeFrom.Name = "checkBoxHireTimeFrom";
            this.checkBoxHireTimeFrom.Size = new System.Drawing.Size(15, 14);
            this.checkBoxHireTimeFrom.TabIndex = 114;
            this.checkBoxHireTimeFrom.UseVisualStyleBackColor = true;
            this.checkBoxHireTimeFrom.CheckedChanged += new System.EventHandler(this.checkBoxHireTimeFrom_CheckedChanged);
            // 
            // checkBoxGroup
            // 
            this.checkBoxGroup.AutoSize = true;
            this.checkBoxGroup.Location = new System.Drawing.Point(154, 107);
            this.checkBoxGroup.Name = "checkBoxGroup";
            this.checkBoxGroup.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGroup.TabIndex = 113;
            this.checkBoxGroup.UseVisualStyleBackColor = true;
            this.checkBoxGroup.CheckedChanged += new System.EventHandler(this.checkBoxGroup_CheckedChanged);
            // 
            // labelHireTimeTo
            // 
            this.labelHireTimeTo.AutoSize = true;
            this.labelHireTimeTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelHireTimeTo.Location = new System.Drawing.Point(198, 253);
            this.labelHireTimeTo.Name = "labelHireTimeTo";
            this.labelHireTimeTo.Size = new System.Drawing.Size(21, 13);
            this.labelHireTimeTo.TabIndex = 111;
            this.labelHireTimeTo.Text = global::FormsView.TranslationsStatic.Default.labelTo;
            // 
            // labelHireTimeFrom
            // 
            this.labelHireTimeFrom.AutoSize = true;
            this.labelHireTimeFrom.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelHireTimeFrom.Location = new System.Drawing.Point(17, 252);
            this.labelHireTimeFrom.Name = "labelHireTimeFrom";
            this.labelHireTimeFrom.Size = new System.Drawing.Size(21, 13);
            this.labelHireTimeFrom.TabIndex = 109;
            this.labelHireTimeFrom.Text = global::FormsView.TranslationsStatic.Default.labelFrom;
            // 
            // labelEmployeeReleaseTime
            // 
            this.labelEmployeeReleaseTime.AutoSize = true;
            this.labelEmployeeReleaseTime.Location = new System.Drawing.Point(17, 285);
            this.labelEmployeeReleaseTime.Name = "labelEmployeeReleaseTime";
            this.labelEmployeeReleaseTime.Size = new System.Drawing.Size(132, 13);
            this.labelEmployeeReleaseTime.TabIndex = 105;
            this.labelEmployeeReleaseTime.Text = global::FormsView.TranslationsStatic.Default.labelReleaseTime;
            // 
            // labelEmployeeHireTime
            // 
            this.labelEmployeeHireTime.AutoSize = true;
            this.labelEmployeeHireTime.Location = new System.Drawing.Point(17, 218);
            this.labelEmployeeHireTime.Name = "labelEmployeeHireTime";
            this.labelEmployeeHireTime.Size = new System.Drawing.Size(123, 13);
            this.labelEmployeeHireTime.TabIndex = 103;
            this.labelEmployeeHireTime.Text = global::FormsView.TranslationsStatic.Default.labelHireTime;
            // 
            // comboBoxGroup
            // 
            this.comboBoxGroup.DisplayMember = "Value";
            this.comboBoxGroup.Enabled = false;
            this.comboBoxGroup.FormattingEnabled = true;
            this.comboBoxGroup.Location = new System.Drawing.Point(175, 104);
            this.comboBoxGroup.Name = "comboBoxGroup";
            this.comboBoxGroup.Size = new System.Drawing.Size(166, 21);
            this.comboBoxGroup.TabIndex = 102;
            this.comboBoxGroup.Tag = "searchParam";
            this.comboBoxGroup.ValueMember = "Value";
            // 
            // labelEmployeeGroup
            // 
            this.labelEmployeeGroup.AutoSize = true;
            this.labelEmployeeGroup.Location = new System.Drawing.Point(17, 104);
            this.labelEmployeeGroup.Name = "labelEmployeeGroup";
            this.labelEmployeeGroup.Size = new System.Drawing.Size(39, 13);
            this.labelEmployeeGroup.TabIndex = 101;
            this.labelEmployeeGroup.Text = global::FormsView.TranslationsStatic.Default.labelGroup;
            // 
            // checkBoxPhoneNumber
            // 
            this.checkBoxPhoneNumber.AutoSize = true;
            this.checkBoxPhoneNumber.Location = new System.Drawing.Point(154, 185);
            this.checkBoxPhoneNumber.Name = "checkBoxPhoneNumber";
            this.checkBoxPhoneNumber.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPhoneNumber.TabIndex = 100;
            this.checkBoxPhoneNumber.UseVisualStyleBackColor = true;
            this.checkBoxPhoneNumber.CheckedChanged += new System.EventHandler(this.checkBoxPhoneNumber_CheckedChanged);
            // 
            // checkBoxPesel
            // 
            this.checkBoxPesel.AutoSize = true;
            this.checkBoxPesel.Location = new System.Drawing.Point(154, 145);
            this.checkBoxPesel.Name = "checkBoxPesel";
            this.checkBoxPesel.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPesel.TabIndex = 99;
            this.checkBoxPesel.UseVisualStyleBackColor = true;
            this.checkBoxPesel.CheckedChanged += new System.EventHandler(this.checkBoxPesel_CheckedChanged);
            // 
            // checkBoxSurname
            // 
            this.checkBoxSurname.AutoSize = true;
            this.checkBoxSurname.Location = new System.Drawing.Point(154, 69);
            this.checkBoxSurname.Name = "checkBoxSurname";
            this.checkBoxSurname.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSurname.TabIndex = 98;
            this.checkBoxSurname.UseVisualStyleBackColor = true;
            this.checkBoxSurname.CheckedChanged += new System.EventHandler(this.checkBoxSurname_CheckedChanged);
            // 
            // checkBoxName
            // 
            this.checkBoxName.AutoSize = true;
            this.checkBoxName.Location = new System.Drawing.Point(154, 32);
            this.checkBoxName.Name = "checkBoxName";
            this.checkBoxName.Size = new System.Drawing.Size(15, 14);
            this.checkBoxName.TabIndex = 97;
            this.checkBoxName.UseVisualStyleBackColor = true;
            this.checkBoxName.CheckedChanged += new System.EventHandler(this.checkBoxName_CheckedChanged);
            // 
            // maskedTextBoxPesel
            // 
            this.maskedTextBoxPesel.Enabled = false;
            this.maskedTextBoxPesel.Location = new System.Drawing.Point(175, 142);
            this.maskedTextBoxPesel.Mask = "00000000000";
            this.maskedTextBoxPesel.Name = "maskedTextBoxPesel";
            this.maskedTextBoxPesel.Size = new System.Drawing.Size(72, 20);
            this.maskedTextBoxPesel.TabIndex = 96;
            this.maskedTextBoxPesel.Tag = "searchParam";
            // 
            // maskedTextBoxPhone
            // 
            this.maskedTextBoxPhone.Enabled = false;
            this.maskedTextBoxPhone.Location = new System.Drawing.Point(175, 182);
            this.maskedTextBoxPhone.Mask = "000-000-000";
            this.maskedTextBoxPhone.Name = "maskedTextBoxPhone";
            this.maskedTextBoxPhone.Size = new System.Drawing.Size(72, 20);
            this.maskedTextBoxPhone.TabIndex = 95;
            this.maskedTextBoxPhone.Tag = "searchParam";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Enabled = false;
            this.textBoxFirstName.Location = new System.Drawing.Point(175, 29);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(166, 20);
            this.textBoxFirstName.TabIndex = 93;
            this.textBoxFirstName.Tag = "searchParam";
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Enabled = false;
            this.textBoxSurname.Location = new System.Drawing.Point(175, 66);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxSurname.Size = new System.Drawing.Size(166, 20);
            this.textBoxSurname.TabIndex = 94;
            this.textBoxSurname.Tag = "searchParam";
            // 
            // labelClientFirstName
            // 
            this.labelClientFirstName.AutoSize = true;
            this.labelClientFirstName.Location = new System.Drawing.Point(17, 29);
            this.labelClientFirstName.Name = "labelClientFirstName";
            this.labelClientFirstName.Size = new System.Drawing.Size(29, 13);
            this.labelClientFirstName.TabIndex = 89;
            this.labelClientFirstName.Text = global::FormsView.TranslationsStatic.Default.labelPersonName;
            // 
            // labelClientSurname
            // 
            this.labelClientSurname.AutoSize = true;
            this.labelClientSurname.Location = new System.Drawing.Point(17, 66);
            this.labelClientSurname.Name = "labelClientSurname";
            this.labelClientSurname.Size = new System.Drawing.Size(56, 13);
            this.labelClientSurname.TabIndex = 90;
            this.labelClientSurname.Text = global::FormsView.TranslationsStatic.Default.labelPersonSurname;
            // 
            // labelClientPhone
            // 
            this.labelClientPhone.AutoSize = true;
            this.labelClientPhone.Location = new System.Drawing.Point(17, 182);
            this.labelClientPhone.Name = "labelClientPhone";
            this.labelClientPhone.Size = new System.Drawing.Size(82, 13);
            this.labelClientPhone.TabIndex = 92;
            this.labelClientPhone.Text = global::FormsView.TranslationsStatic.Default.labelTelephone;
            // 
            // labelClientPesel
            // 
            this.labelClientPesel.AutoSize = true;
            this.labelClientPesel.Location = new System.Drawing.Point(17, 145);
            this.labelClientPesel.Name = "labelClientPesel";
            this.labelClientPesel.Size = new System.Drawing.Size(44, 13);
            this.labelClientPesel.TabIndex = 91;
            this.labelClientPesel.Text = global::FormsView.TranslationsStatic.Default.labelPesel;
            // 
            // checkBoxStreet
            // 
            this.checkBoxStreet.AutoSize = true;
            this.checkBoxStreet.Location = new System.Drawing.Point(154, 465);
            this.checkBoxStreet.Name = "checkBoxStreet";
            this.checkBoxStreet.Size = new System.Drawing.Size(15, 14);
            this.checkBoxStreet.TabIndex = 88;
            this.checkBoxStreet.UseVisualStyleBackColor = true;
            this.checkBoxStreet.CheckedChanged += new System.EventHandler(this.checkBoxStreet_CheckedChanged);
            // 
            // checkBoxPostcode
            // 
            this.checkBoxPostcode.AutoSize = true;
            this.checkBoxPostcode.Location = new System.Drawing.Point(154, 431);
            this.checkBoxPostcode.Name = "checkBoxPostcode";
            this.checkBoxPostcode.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPostcode.TabIndex = 87;
            this.checkBoxPostcode.UseVisualStyleBackColor = true;
            this.checkBoxPostcode.CheckedChanged += new System.EventHandler(this.checkBoxPostcode_CheckedChanged);
            // 
            // checkBoxCity
            // 
            this.checkBoxCity.AutoSize = true;
            this.checkBoxCity.Location = new System.Drawing.Point(154, 394);
            this.checkBoxCity.Name = "checkBoxCity";
            this.checkBoxCity.Size = new System.Drawing.Size(15, 14);
            this.checkBoxCity.TabIndex = 86;
            this.checkBoxCity.UseVisualStyleBackColor = true;
            this.checkBoxCity.CheckedChanged += new System.EventHandler(this.checkBoxCity_CheckedChanged);
            // 
            // checkBoxProvince
            // 
            this.checkBoxProvince.AutoSize = true;
            this.checkBoxProvince.Location = new System.Drawing.Point(154, 357);
            this.checkBoxProvince.Name = "checkBoxProvince";
            this.checkBoxProvince.Size = new System.Drawing.Size(15, 14);
            this.checkBoxProvince.TabIndex = 85;
            this.checkBoxProvince.UseVisualStyleBackColor = true;
            this.checkBoxProvince.CheckedChanged += new System.EventHandler(this.checkBoxProvince_CheckedChanged);
            // 
            // maskedTextBoxAddressPostcode
            // 
            this.maskedTextBoxAddressPostcode.Enabled = false;
            this.maskedTextBoxAddressPostcode.Location = new System.Drawing.Point(175, 428);
            this.maskedTextBoxAddressPostcode.Mask = "00-000";
            this.maskedTextBoxAddressPostcode.Name = "maskedTextBoxAddressPostcode";
            this.maskedTextBoxAddressPostcode.Size = new System.Drawing.Size(43, 20);
            this.maskedTextBoxAddressPostcode.TabIndex = 84;
            this.maskedTextBoxAddressPostcode.Tag = "searchParam";
            // 
            // comboBoxAddressProvince
            // 
            this.comboBoxAddressProvince.DisplayMember = "Value";
            this.comboBoxAddressProvince.Enabled = false;
            this.comboBoxAddressProvince.FormattingEnabled = true;
            this.comboBoxAddressProvince.Location = new System.Drawing.Point(175, 354);
            this.comboBoxAddressProvince.Name = "comboBoxAddressProvince";
            this.comboBoxAddressProvince.Size = new System.Drawing.Size(166, 21);
            this.comboBoxAddressProvince.TabIndex = 83;
            this.comboBoxAddressProvince.Tag = "searchParam";
            this.comboBoxAddressProvince.ValueMember = "Value";
            this.comboBoxAddressProvince.SelectedIndexChanged += new System.EventHandler(this.LoadCitiesByProvince);
            // 
            // labelAddressProvince
            // 
            this.labelAddressProvince.AutoSize = true;
            this.labelAddressProvince.Location = new System.Drawing.Point(17, 354);
            this.labelAddressProvince.Name = "labelAddressProvince";
            this.labelAddressProvince.Size = new System.Drawing.Size(77, 13);
            this.labelAddressProvince.TabIndex = 82;
            this.labelAddressProvince.Text = global::FormsView.TranslationsStatic.Default.labelProvince;
            // 
            // textBoxAddressStreet
            // 
            this.textBoxAddressStreet.Enabled = false;
            this.textBoxAddressStreet.Location = new System.Drawing.Point(175, 462);
            this.textBoxAddressStreet.Name = "textBoxAddressStreet";
            this.textBoxAddressStreet.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddressStreet.Size = new System.Drawing.Size(166, 20);
            this.textBoxAddressStreet.TabIndex = 81;
            this.textBoxAddressStreet.Tag = "searchParam";
            // 
            // labelAddressStreet
            // 
            this.labelAddressStreet.AutoSize = true;
            this.labelAddressStreet.Location = new System.Drawing.Point(17, 462);
            this.labelAddressStreet.Name = "labelAddressStreet";
            this.labelAddressStreet.Size = new System.Drawing.Size(34, 13);
            this.labelAddressStreet.TabIndex = 80;
            this.labelAddressStreet.Text = global::FormsView.TranslationsStatic.Default.labelStreet;
            // 
            // labelAddressPostcode
            // 
            this.labelAddressPostcode.AutoSize = true;
            this.labelAddressPostcode.Location = new System.Drawing.Point(17, 428);
            this.labelAddressPostcode.Name = "labelAddressPostcode";
            this.labelAddressPostcode.Size = new System.Drawing.Size(77, 13);
            this.labelAddressPostcode.TabIndex = 79;
            this.labelAddressPostcode.Text = global::FormsView.TranslationsStatic.Default.labelPostcode;
            // 
            // comboBoxAddressCity
            // 
            this.comboBoxAddressCity.DisplayMember = "Value";
            this.comboBoxAddressCity.Enabled = false;
            this.comboBoxAddressCity.FormattingEnabled = true;
            this.comboBoxAddressCity.Location = new System.Drawing.Point(175, 391);
            this.comboBoxAddressCity.Name = "comboBoxAddressCity";
            this.comboBoxAddressCity.Size = new System.Drawing.Size(166, 21);
            this.comboBoxAddressCity.TabIndex = 78;
            this.comboBoxAddressCity.Tag = "searchParam";
            this.comboBoxAddressCity.ValueMember = "Value";
            // 
            // labelAddressCity
            // 
            this.labelAddressCity.AutoSize = true;
            this.labelAddressCity.Location = new System.Drawing.Point(17, 391);
            this.labelAddressCity.Name = "labelAddressCity";
            this.labelAddressCity.Size = new System.Drawing.Size(41, 13);
            this.labelAddressCity.TabIndex = 77;
            this.labelAddressCity.Text = global::FormsView.TranslationsStatic.Default.labelCity;
            // 
            // findClearControls1
            // 
            this.findClearControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.findClearControls1.Location = new System.Drawing.Point(0, 518);
            this.findClearControls1.Name = "findClearControls1";
            this.findClearControls1.Sender = null;
            this.findClearControls1.Size = new System.Drawing.Size(398, 25);
            this.findClearControls1.TabIndex = 0;
            // 
            // EmployeeSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxEmployee);
            this.Controls.Add(this.findClearControls1);
            this.Name = "EmployeeSearchControl";
            this.Size = new System.Drawing.Size(398, 543);
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.FindClearControls findClearControls1;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        private System.Windows.Forms.CheckBox checkBoxStreet;
        private System.Windows.Forms.CheckBox checkBoxPostcode;
        private System.Windows.Forms.CheckBox checkBoxCity;
        private System.Windows.Forms.CheckBox checkBoxProvince;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxAddressPostcode;
        private System.Windows.Forms.ComboBox comboBoxAddressProvince;
        private System.Windows.Forms.Label labelAddressProvince;
        private System.Windows.Forms.TextBox textBoxAddressStreet;
        private System.Windows.Forms.Label labelAddressStreet;
        private System.Windows.Forms.Label labelAddressPostcode;
        private System.Windows.Forms.ComboBox comboBoxAddressCity;
        private System.Windows.Forms.Label labelAddressCity;
        private System.Windows.Forms.CheckBox checkBoxPhoneNumber;
        private System.Windows.Forms.CheckBox checkBoxPesel;
        private System.Windows.Forms.CheckBox checkBoxSurname;
        private System.Windows.Forms.CheckBox checkBoxName;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPesel;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPhone;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.Label labelClientFirstName;
        private System.Windows.Forms.Label labelClientSurname;
        private System.Windows.Forms.Label labelClientPhone;
        private System.Windows.Forms.Label labelClientPesel;
        private System.Windows.Forms.ComboBox comboBoxGroup;
        private System.Windows.Forms.Label labelEmployeeGroup;
        private System.Windows.Forms.Label labelHireTimeTo;
        private System.Windows.Forms.Label labelHireTimeFrom;
        private System.Windows.Forms.Label labelEmployeeReleaseTime;
        private System.Windows.Forms.Label labelEmployeeHireTime;
        private System.Windows.Forms.CheckBox checkBoxHireTimeTo;
        private System.Windows.Forms.CheckBox checkBoxHireTimeFrom;
        private System.Windows.Forms.CheckBox checkBoxGroup;
        private System.Windows.Forms.DateTimePicker dateTimeReleaseDateTo;
        private System.Windows.Forms.DateTimePicker dateTimeReleaseDateFrom;
        private System.Windows.Forms.CheckBox checkBoxReleaseDateTo;
        private System.Windows.Forms.CheckBox checkBoxReleaseDateFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimeHireDateTo;
        private System.Windows.Forms.DateTimePicker dateTimeHireDateFrom;
    }
}
