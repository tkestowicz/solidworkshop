﻿namespace FormsView.SearchControls
{
    partial class OrderSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findClearControls1 = new FormsView.Controls.FindClearControls();
            this.groupBoxOrderSearch = new System.Windows.Forms.GroupBox();
            this.checkBoxOrderSupervisor = new System.Windows.Forms.CheckBox();
            this.checkBoxState = new System.Windows.Forms.CheckBox();
            this.comboBoxOrderState = new System.Windows.Forms.ComboBox();
            this.labelOrderState = new System.Windows.Forms.Label();
            this.comboBoxOrderSupervisor = new System.Windows.Forms.ComboBox();
            this.labelOrderSupervisor = new System.Windows.Forms.Label();
            this.groupBoxOrderSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // findClearControls1
            // 
            this.findClearControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.findClearControls1.Location = new System.Drawing.Point(0, 137);
            this.findClearControls1.Name = "findClearControls1";
            this.findClearControls1.Sender = null;
            this.findClearControls1.Size = new System.Drawing.Size(314, 25);
            this.findClearControls1.TabIndex = 0;
            // 
            // groupBoxOrderSearch
            // 
            this.groupBoxOrderSearch.Controls.Add(this.checkBoxOrderSupervisor);
            this.groupBoxOrderSearch.Controls.Add(this.checkBoxState);
            this.groupBoxOrderSearch.Controls.Add(this.comboBoxOrderState);
            this.groupBoxOrderSearch.Controls.Add(this.labelOrderState);
            this.groupBoxOrderSearch.Controls.Add(this.comboBoxOrderSupervisor);
            this.groupBoxOrderSearch.Controls.Add(this.labelOrderSupervisor);
            this.groupBoxOrderSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxOrderSearch.Location = new System.Drawing.Point(0, 0);
            this.groupBoxOrderSearch.Name = "groupBoxOrderSearch";
            this.groupBoxOrderSearch.Size = new System.Drawing.Size(314, 121);
            this.groupBoxOrderSearch.TabIndex = 1;
            this.groupBoxOrderSearch.TabStop = false;
            this.groupBoxOrderSearch.Text = global::FormsView.TranslationsStatic.Default.titleOrder;
            // 
            // checkBoxOrderSupervisor
            // 
            this.checkBoxOrderSupervisor.AutoSize = true;
            this.checkBoxOrderSupervisor.Location = new System.Drawing.Point(78, 77);
            this.checkBoxOrderSupervisor.Name = "checkBoxOrderSupervisor";
            this.checkBoxOrderSupervisor.Size = new System.Drawing.Size(15, 14);
            this.checkBoxOrderSupervisor.TabIndex = 129;
            this.checkBoxOrderSupervisor.UseVisualStyleBackColor = true;
            this.checkBoxOrderSupervisor.CheckedChanged += new System.EventHandler(this.checkBoxOrderSupervisor_CheckedChanged);
            // 
            // checkBoxState
            // 
            this.checkBoxState.AutoSize = true;
            this.checkBoxState.Location = new System.Drawing.Point(78, 34);
            this.checkBoxState.Name = "checkBoxState";
            this.checkBoxState.Size = new System.Drawing.Size(15, 14);
            this.checkBoxState.TabIndex = 128;
            this.checkBoxState.UseVisualStyleBackColor = true;
            this.checkBoxState.CheckedChanged += new System.EventHandler(this.checkBoxState_CheckedChanged);
            // 
            // comboBoxOrderState
            // 
            this.comboBoxOrderState.DisplayMember = "Value";
            this.comboBoxOrderState.Enabled = false;
            this.comboBoxOrderState.FormattingEnabled = true;
            this.comboBoxOrderState.Location = new System.Drawing.Point(99, 31);
            this.comboBoxOrderState.Name = "comboBoxOrderState";
            this.comboBoxOrderState.Size = new System.Drawing.Size(187, 21);
            this.comboBoxOrderState.TabIndex = 18;
            this.comboBoxOrderState.Tag = "searchParam";
            this.comboBoxOrderState.ValueMember = "Value";
            // 
            // labelOrderState
            // 
            this.labelOrderState.AutoSize = true;
            this.labelOrderState.Location = new System.Drawing.Point(17, 31);
            this.labelOrderState.Name = "labelOrderState";
            this.labelOrderState.Size = new System.Drawing.Size(40, 13);
            this.labelOrderState.TabIndex = 17;
            this.labelOrderState.Text = global::FormsView.TranslationsStatic.Default.labelState;
            // 
            // comboBoxOrderSupervisor
            // 
            this.comboBoxOrderSupervisor.DisplayMember = "Value";
            this.comboBoxOrderSupervisor.Enabled = false;
            this.comboBoxOrderSupervisor.FormattingEnabled = true;
            this.comboBoxOrderSupervisor.Location = new System.Drawing.Point(99, 74);
            this.comboBoxOrderSupervisor.Name = "comboBoxOrderSupervisor";
            this.comboBoxOrderSupervisor.Size = new System.Drawing.Size(187, 21);
            this.comboBoxOrderSupervisor.TabIndex = 16;
            this.comboBoxOrderSupervisor.Tag = "searchParam";
            this.comboBoxOrderSupervisor.ValueMember = "Value";
            // 
            // labelOrderSupervisor
            // 
            this.labelOrderSupervisor.AutoSize = true;
            this.labelOrderSupervisor.Location = new System.Drawing.Point(17, 74);
            this.labelOrderSupervisor.Name = "labelOrderSupervisor";
            this.labelOrderSupervisor.Size = new System.Drawing.Size(56, 13);
            this.labelOrderSupervisor.TabIndex = 13;
            this.labelOrderSupervisor.Text = global::FormsView.TranslationsStatic.Default.labelSupervisor;
            // 
            // OrderSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxOrderSearch);
            this.Controls.Add(this.findClearControls1);
            this.Name = "OrderSearchControl";
            this.Size = new System.Drawing.Size(314, 162);
            this.groupBoxOrderSearch.ResumeLayout(false);
            this.groupBoxOrderSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.FindClearControls findClearControls1;
        private System.Windows.Forms.GroupBox groupBoxOrderSearch;
        private System.Windows.Forms.ComboBox comboBoxOrderState;
        private System.Windows.Forms.Label labelOrderState;
        private System.Windows.Forms.ComboBox comboBoxOrderSupervisor;
        private System.Windows.Forms.Label labelOrderSupervisor;
        private System.Windows.Forms.CheckBox checkBoxOrderSupervisor;
        private System.Windows.Forms.CheckBox checkBoxState;
    }
}
