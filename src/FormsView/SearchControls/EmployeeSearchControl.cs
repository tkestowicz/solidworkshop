﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormsView.Helpers;
using AppLogic.Interfaces.Search;
using AppLogic.Presenters.Popup.Search;

namespace FormsView.SearchControls
{
    public partial class EmployeeSearchControl : Base.BaseUserControl, IEmployeeSearchView
    {

        EmployeeSearchPresenter _presenter;

        public EmployeeSearchControl()
        {
            InitializeComponent();
        }

        public List<Model.Projection.ComboBox> Groups
        {
            set
            {
                comboBoxGroup.Items.Clear();
                comboBoxGroup.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Cities
        {
            set
            {
                comboBoxAddressCity.Items.Clear();
                comboBoxAddressCity.Items.AddRange(value.ToArray());
            }
        }

        public int CityId
        {
            set
            {
                comboBoxAddressCity.SelectedIndex = comboBoxAddressCity.FindIndex(value);
            }
        }

        public List<Model.Projection.ComboBox> Provinces
        {
            set
            {
                comboBoxAddressProvince.Items.Clear();
                comboBoxAddressProvince.Items.AddRange(value.ToArray());
            }
        }

        public string Province
        {
            get
            {
                try
                {
                    return (comboBoxAddressCity.SelectedItem as Model.Projection.ComboBox).Value;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(AppLogic.Interfaces.Popups.IPopupPresenter presenter)
        {
            findClearControls1.Enable = true;
            findClearControls1.Sender = presenter;
            _presenter = presenter as EmployeeSearchPresenter;
        }

        public Dictionary<string, object> SearchParameters
        {
            get
            {
                return groupBoxEmployee.GetSearchParams();
            }
        }

        public void Reset()
        {
            checkBoxCity.Checked = false;
            checkBoxName.Checked = false;
            checkBoxPesel.Checked = false;
            checkBoxPhoneNumber.Checked = false;
            checkBoxPostcode.Checked = false;
            checkBoxProvince.Checked = false;
            checkBoxStreet.Checked = false;
            checkBoxSurname.Checked = false;
            checkBoxHireTimeFrom.Checked = false;
            checkBoxHireTimeTo.Checked = false;
            checkBoxGroup.Checked = false;
            checkBoxReleaseDateFrom.Checked = false;
            checkBoxReleaseDateTo.Checked = false;

            comboBoxAddressCity.SelectedIndex = -1;
            comboBoxAddressProvince.SelectedIndex = -1;
            comboBoxGroup.SelectedIndex = -1;
            textBoxFirstName.Text = null;
            textBoxSurname.Text = null;
            maskedTextBoxPesel.Text = null;
            maskedTextBoxPhone.Text = null;
            maskedTextBoxAddressPostcode.Text = null;
            textBoxAddressStreet.Text = null;
            dateTimeHireDateFrom.Value = DateTime.Now;
            dateTimeHireDateTo.Value = DateTime.Now;
            dateTimeReleaseDateFrom.Value = DateTime.Now;
            dateTimeReleaseDateTo.Value = DateTime.Now;

        }

        private void checkBoxName_CheckedChanged(object sender, EventArgs e)
        {
            textBoxFirstName.Enabled = checkBoxName.Checked;
        }

        private void checkBoxSurname_CheckedChanged(object sender, EventArgs e)
        {
            textBoxSurname.Enabled = checkBoxSurname.Checked;
        }

        private void checkBoxGroup_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxGroup.Enabled = checkBoxGroup.Checked;
        }

        private void checkBoxPesel_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxPesel.Enabled = checkBoxPesel.Checked;
        }

        private void checkBoxPhoneNumber_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxPhone.Enabled = checkBoxPhoneNumber.Checked;
        }

        private void checkBoxHireTimeFrom_CheckedChanged(object sender, EventArgs e)
        {
            dateTimeHireDateFrom.Enabled = checkBoxHireTimeFrom.Checked;
        }

        private void checkBoxHireTimeTo_CheckedChanged(object sender, EventArgs e)
        {
            dateTimeHireDateTo.Enabled = checkBoxHireTimeTo.Checked;
        }

        private void checkBoxReleaseDateTo_CheckedChanged(object sender, EventArgs e)
        {
            dateTimeReleaseDateTo.Enabled = checkBoxReleaseDateTo.Checked;
        }

        private void checkBoxReleaseDateFrom_CheckedChanged(object sender, EventArgs e)
        {
            dateTimeReleaseDateFrom.Enabled = checkBoxReleaseDateFrom.Checked;
        }

        private void checkBoxProvince_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxAddressProvince.Enabled = checkBoxProvince.Checked;
        }

        private void checkBoxCity_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxAddressCity.Enabled = checkBoxCity.Checked;
        }

        private void checkBoxPostcode_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxAddressPostcode.Enabled = checkBoxPostcode.Checked;
        }

        private void checkBoxStreet_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAddressStreet.Enabled = checkBoxStreet.Checked;
        }

        private void LoadCitiesByProvince(object sender, EventArgs e)
        {
            _presenter.LoadCitiesByProvince(Province);
        }
    }
}
