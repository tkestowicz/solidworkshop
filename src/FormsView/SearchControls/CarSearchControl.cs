﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Search;
using FormsView.Helpers;
using AppLogic.Presenters.Popup.Search;

namespace FormsView.SearchControls
{
    public partial class CarSearchControl : Base.BaseUserControl, ICarSearchView
    {

        CarSearchPresenter _presenter;

        public CarSearchControl()
        {
            InitializeComponent();
        }

        private void checkBoxBrand_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxCarBrand.Enabled = checkBoxBrand.Checked;
        }

        private void checkBoxModel_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxCarModel.Enabled = checkBoxModel.Checked;
        }

        private void checkBoxColor_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxCarColor.Enabled = checkBoxColor.Checked;
        }

        private void checkBoxRegistrationNumber_CheckedChanged(object sender, EventArgs e)
        {
            textBoxRegistrationNumber.Enabled = checkBoxRegistrationNumber.Checked;
        }

        private void checkBoxProductionYearFrom_CheckedChanged(object sender, EventArgs e)
        {
            maskedProductionYearFrom.Enabled = checkBoxProductionYearFrom.Checked;
        }

        private void checkBoxProductionYearTo_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxProductionYearTo.Enabled = checkBoxProductionYearTo.Checked;
        }

        public List<Model.Projection.ComboBox> Brands
        {
            set 
            {
                comboBoxCarBrand.Items.Clear();
                comboBoxCarBrand.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Models
        {
            set 
            {
                comboBoxCarModel.Items.Clear();
                comboBoxCarModel.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Colors
        {
            set 
            {
                comboBoxCarColor.Items.Clear();
                comboBoxCarColor.Items.AddRange(value.ToArray());
            }
        }

        public int BrandId
        {
            get
            {
                try
                {
                    return (comboBoxCarBrand.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        public int ModelId
        {
            set
            {
                comboBoxCarModel.SelectedIndex = comboBoxCarModel.FindIndex(value);
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(AppLogic.Interfaces.Popups.IPopupPresenter presenter)
        {
            findClearControls1.Enable = true;
            findClearControls1.Sender = presenter;
            _presenter = presenter as CarSearchPresenter;
        }

        public Dictionary<string, object> SearchParameters
        {
            get 
            {
                return groupBoxCarSearch.GetSearchParams();
            }
        }

        public void Reset()
        {
            checkBoxRegistrationNumber.Checked = false;
            checkBoxProductionYearTo.Checked = false;
            checkBoxProductionYearFrom.Checked = false;
            checkBoxModel.Checked = false;
            checkBoxColor.Checked = false;
            checkBoxBrand.Checked = false;

            maskedProductionYearFrom.Text = null;
            maskedTextBoxProductionYearTo.Text = null;
            textBoxRegistrationNumber.Text = null;
            comboBoxCarBrand.SelectedIndex = -1;
            comboBoxCarColor.SelectedIndex = -1;
            comboBoxCarModel.SelectedIndex = -1;
        }

        private void LoadModelsByBrand(object sender, EventArgs e)
        {
            _presenter.LoadModelsByBrand(BrandId);
        }
    }
}
