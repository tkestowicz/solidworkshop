﻿namespace FormsView.SearchControls
{
    partial class ClientSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findClearControls1 = new FormsView.Controls.FindClearControls();
            this.groupBoxClientSearch = new System.Windows.Forms.GroupBox();
            this.checkBoxStreet = new System.Windows.Forms.CheckBox();
            this.checkBoxPostcode = new System.Windows.Forms.CheckBox();
            this.checkBoxCity = new System.Windows.Forms.CheckBox();
            this.checkBoxProvince = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxAddressPostcode = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxAddressProvince = new System.Windows.Forms.ComboBox();
            this.textBoxAddressStreet = new System.Windows.Forms.TextBox();
            this.comboBoxAddressCity = new System.Windows.Forms.ComboBox();
            this.checkBoxPhoneNumber = new System.Windows.Forms.CheckBox();
            this.checkBoxPesel = new System.Windows.Forms.CheckBox();
            this.checkBoxSurname = new System.Windows.Forms.CheckBox();
            this.checkBoxName = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxClientPhone = new System.Windows.Forms.MaskedTextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxClientSurname = new System.Windows.Forms.TextBox();
            this.labelClientPesel = new System.Windows.Forms.Label();
            this.labelAddressProvince = new System.Windows.Forms.Label();
            this.labelAddressStreet = new System.Windows.Forms.Label();
            this.labelAddressPostcode = new System.Windows.Forms.Label();
            this.labelAddressCity = new System.Windows.Forms.Label();
            this.maskedTextBoxClientPesel = new System.Windows.Forms.MaskedTextBox();
            this.labelClientFirstName = new System.Windows.Forms.Label();
            this.labelClientSurname = new System.Windows.Forms.Label();
            this.labelClientPhone = new System.Windows.Forms.Label();
            this.groupBoxClientSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // findClearControls1
            // 
            this.findClearControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.findClearControls1.Location = new System.Drawing.Point(0, 344);
            this.findClearControls1.Name = "findClearControls1";
            this.findClearControls1.Sender = null;
            this.findClearControls1.Size = new System.Drawing.Size(314, 25);
            this.findClearControls1.TabIndex = 2;
            // 
            // groupBoxClientSearch
            // 
            this.groupBoxClientSearch.Controls.Add(this.checkBoxStreet);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxPostcode);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxCity);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxProvince);
            this.groupBoxClientSearch.Controls.Add(this.maskedTextBoxAddressPostcode);
            this.groupBoxClientSearch.Controls.Add(this.comboBoxAddressProvince);
            this.groupBoxClientSearch.Controls.Add(this.labelAddressProvince);
            this.groupBoxClientSearch.Controls.Add(this.textBoxAddressStreet);
            this.groupBoxClientSearch.Controls.Add(this.labelAddressStreet);
            this.groupBoxClientSearch.Controls.Add(this.labelAddressPostcode);
            this.groupBoxClientSearch.Controls.Add(this.comboBoxAddressCity);
            this.groupBoxClientSearch.Controls.Add(this.labelAddressCity);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxPhoneNumber);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxPesel);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxSurname);
            this.groupBoxClientSearch.Controls.Add(this.checkBoxName);
            this.groupBoxClientSearch.Controls.Add(this.maskedTextBoxClientPesel);
            this.groupBoxClientSearch.Controls.Add(this.maskedTextBoxClientPhone);
            this.groupBoxClientSearch.Controls.Add(this.textBoxFirstName);
            this.groupBoxClientSearch.Controls.Add(this.textBoxClientSurname);
            this.groupBoxClientSearch.Controls.Add(this.labelClientFirstName);
            this.groupBoxClientSearch.Controls.Add(this.labelClientSurname);
            this.groupBoxClientSearch.Controls.Add(this.labelClientPhone);
            this.groupBoxClientSearch.Controls.Add(this.labelClientPesel);
            this.groupBoxClientSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxClientSearch.Location = new System.Drawing.Point(0, 0);
            this.groupBoxClientSearch.Name = "groupBoxClientSearch";
            this.groupBoxClientSearch.Size = new System.Drawing.Size(314, 326);
            this.groupBoxClientSearch.TabIndex = 1;
            this.groupBoxClientSearch.TabStop = false;
            this.groupBoxClientSearch.Text = global::FormsView.TranslationsStatic.Default.groupBoxClientData;
            // 
            // checkBoxStreet
            // 
            this.checkBoxStreet.AutoSize = true;
            this.checkBoxStreet.Location = new System.Drawing.Point(109, 291);
            this.checkBoxStreet.Name = "checkBoxStreet";
            this.checkBoxStreet.Size = new System.Drawing.Size(15, 14);
            this.checkBoxStreet.TabIndex = 76;
            this.checkBoxStreet.UseVisualStyleBackColor = true;
            this.checkBoxStreet.CheckedChanged += new System.EventHandler(this.checkBoxStreet_CheckedChanged);
            // 
            // checkBoxPostcode
            // 
            this.checkBoxPostcode.AutoSize = true;
            this.checkBoxPostcode.Location = new System.Drawing.Point(109, 257);
            this.checkBoxPostcode.Name = "checkBoxPostcode";
            this.checkBoxPostcode.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPostcode.TabIndex = 75;
            this.checkBoxPostcode.UseVisualStyleBackColor = true;
            this.checkBoxPostcode.CheckedChanged += new System.EventHandler(this.checkBoxPostcode_CheckedChanged);
            // 
            // checkBoxCity
            // 
            this.checkBoxCity.AutoSize = true;
            this.checkBoxCity.Location = new System.Drawing.Point(109, 220);
            this.checkBoxCity.Name = "checkBoxCity";
            this.checkBoxCity.Size = new System.Drawing.Size(15, 14);
            this.checkBoxCity.TabIndex = 74;
            this.checkBoxCity.UseVisualStyleBackColor = true;
            this.checkBoxCity.CheckedChanged += new System.EventHandler(this.checkBoxCity_CheckedChanged);
            // 
            // checkBoxProvince
            // 
            this.checkBoxProvince.AutoSize = true;
            this.checkBoxProvince.Location = new System.Drawing.Point(109, 183);
            this.checkBoxProvince.Name = "checkBoxProvince";
            this.checkBoxProvince.Size = new System.Drawing.Size(15, 14);
            this.checkBoxProvince.TabIndex = 73;
            this.checkBoxProvince.UseVisualStyleBackColor = true;
            this.checkBoxProvince.CheckedChanged += new System.EventHandler(this.checkBoxProvince_CheckedChanged);
            // 
            // maskedTextBoxAddressPostcode
            // 
            this.maskedTextBoxAddressPostcode.Enabled = false;
            this.maskedTextBoxAddressPostcode.Location = new System.Drawing.Point(130, 254);
            this.maskedTextBoxAddressPostcode.Mask = "00-000";
            this.maskedTextBoxAddressPostcode.Name = "maskedTextBoxAddressPostcode";
            this.maskedTextBoxAddressPostcode.Size = new System.Drawing.Size(43, 20);
            this.maskedTextBoxAddressPostcode.TabIndex = 72;
            this.maskedTextBoxAddressPostcode.Tag = "searchParam";
            // 
            // comboBoxAddressProvince
            // 
            this.comboBoxAddressProvince.DisplayMember = "Value";
            this.comboBoxAddressProvince.Enabled = false;
            this.comboBoxAddressProvince.FormattingEnabled = true;
            this.comboBoxAddressProvince.Location = new System.Drawing.Point(130, 180);
            this.comboBoxAddressProvince.Name = "comboBoxAddressProvince";
            this.comboBoxAddressProvince.Size = new System.Drawing.Size(166, 21);
            this.comboBoxAddressProvince.TabIndex = 71;
            this.comboBoxAddressProvince.Tag = "searchParam";
            this.comboBoxAddressProvince.ValueMember = "Value";
            this.comboBoxAddressProvince.SelectedIndexChanged += new System.EventHandler(this.LoadCitiesByProvince);
            // 
            // textBoxAddressStreet
            // 
            this.textBoxAddressStreet.Enabled = false;
            this.textBoxAddressStreet.Location = new System.Drawing.Point(130, 288);
            this.textBoxAddressStreet.Name = "textBoxAddressStreet";
            this.textBoxAddressStreet.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddressStreet.Size = new System.Drawing.Size(166, 20);
            this.textBoxAddressStreet.TabIndex = 69;
            this.textBoxAddressStreet.Tag = "searchParam";
            // 
            // comboBoxAddressCity
            // 
            this.comboBoxAddressCity.DisplayMember = "Value";
            this.comboBoxAddressCity.Enabled = false;
            this.comboBoxAddressCity.FormattingEnabled = true;
            this.comboBoxAddressCity.Location = new System.Drawing.Point(130, 217);
            this.comboBoxAddressCity.Name = "comboBoxAddressCity";
            this.comboBoxAddressCity.Size = new System.Drawing.Size(166, 21);
            this.comboBoxAddressCity.TabIndex = 66;
            this.comboBoxAddressCity.Tag = "searchParam";
            this.comboBoxAddressCity.ValueMember = "Value";
            // 
            // checkBoxPhoneNumber
            // 
            this.checkBoxPhoneNumber.AutoSize = true;
            this.checkBoxPhoneNumber.Location = new System.Drawing.Point(109, 146);
            this.checkBoxPhoneNumber.Name = "checkBoxPhoneNumber";
            this.checkBoxPhoneNumber.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPhoneNumber.TabIndex = 64;
            this.checkBoxPhoneNumber.UseVisualStyleBackColor = true;
            this.checkBoxPhoneNumber.CheckedChanged += new System.EventHandler(this.checkBoxPhoneNumber_CheckedChanged);
            // 
            // checkBoxPesel
            // 
            this.checkBoxPesel.AutoSize = true;
            this.checkBoxPesel.Location = new System.Drawing.Point(109, 106);
            this.checkBoxPesel.Name = "checkBoxPesel";
            this.checkBoxPesel.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPesel.TabIndex = 63;
            this.checkBoxPesel.UseVisualStyleBackColor = true;
            this.checkBoxPesel.CheckedChanged += new System.EventHandler(this.checkBoxPesel_CheckedChanged);
            // 
            // checkBoxSurname
            // 
            this.checkBoxSurname.AutoSize = true;
            this.checkBoxSurname.Location = new System.Drawing.Point(109, 69);
            this.checkBoxSurname.Name = "checkBoxSurname";
            this.checkBoxSurname.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSurname.TabIndex = 62;
            this.checkBoxSurname.UseVisualStyleBackColor = true;
            this.checkBoxSurname.CheckedChanged += new System.EventHandler(this.checkBoxSurname_CheckedChanged);
            // 
            // checkBoxName
            // 
            this.checkBoxName.AutoSize = true;
            this.checkBoxName.Location = new System.Drawing.Point(109, 32);
            this.checkBoxName.Name = "checkBoxName";
            this.checkBoxName.Size = new System.Drawing.Size(15, 14);
            this.checkBoxName.TabIndex = 61;
            this.checkBoxName.UseVisualStyleBackColor = true;
            this.checkBoxName.CheckedChanged += new System.EventHandler(this.checkBoxName_CheckedChanged);
            // 
            // maskedTextBoxClientPhone
            // 
            this.maskedTextBoxClientPhone.Enabled = false;
            this.maskedTextBoxClientPhone.Location = new System.Drawing.Point(130, 143);
            this.maskedTextBoxClientPhone.Mask = "000-000-000";
            this.maskedTextBoxClientPhone.Name = "maskedTextBoxClientPhone";
            this.maskedTextBoxClientPhone.Size = new System.Drawing.Size(72, 20);
            this.maskedTextBoxClientPhone.TabIndex = 59;
            this.maskedTextBoxClientPhone.Tag = "searchParam";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Enabled = false;
            this.textBoxFirstName.Location = new System.Drawing.Point(130, 29);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(166, 20);
            this.textBoxFirstName.TabIndex = 55;
            this.textBoxFirstName.Tag = "searchParam";
            // 
            // textBoxClientSurname
            // 
            this.textBoxClientSurname.Enabled = false;
            this.textBoxClientSurname.Location = new System.Drawing.Point(130, 66);
            this.textBoxClientSurname.Name = "textBoxClientSurname";
            this.textBoxClientSurname.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxClientSurname.Size = new System.Drawing.Size(166, 20);
            this.textBoxClientSurname.TabIndex = 56;
            this.textBoxClientSurname.Tag = "searchParam";
            // 
            // labelClientPesel
            // 
            this.labelClientPesel.AutoSize = true;
            this.labelClientPesel.Location = new System.Drawing.Point(17, 106);
            this.labelClientPesel.Name = "labelClientPesel";
            this.labelClientPesel.Size = new System.Drawing.Size(44, 13);
            this.labelClientPesel.TabIndex = 53;
            this.labelClientPesel.Text = "PESEL:";
            // 
            // labelAddressProvince
            // 
            this.labelAddressProvince.AutoSize = true;
            this.labelAddressProvince.Location = new System.Drawing.Point(17, 180);
            this.labelAddressProvince.Name = "labelAddressProvince";
            this.labelAddressProvince.Size = new System.Drawing.Size(77, 13);
            this.labelAddressProvince.TabIndex = 70;
            this.labelAddressProvince.Text = global::FormsView.TranslationsStatic.Default.labelProvince;
            // 
            // labelAddressStreet
            // 
            this.labelAddressStreet.AutoSize = true;
            this.labelAddressStreet.Location = new System.Drawing.Point(17, 288);
            this.labelAddressStreet.Name = "labelAddressStreet";
            this.labelAddressStreet.Size = new System.Drawing.Size(34, 13);
            this.labelAddressStreet.TabIndex = 68;
            this.labelAddressStreet.Text = global::FormsView.TranslationsStatic.Default.labelStreet;
            // 
            // labelAddressPostcode
            // 
            this.labelAddressPostcode.AutoSize = true;
            this.labelAddressPostcode.Location = new System.Drawing.Point(17, 254);
            this.labelAddressPostcode.Name = "labelAddressPostcode";
            this.labelAddressPostcode.Size = new System.Drawing.Size(77, 13);
            this.labelAddressPostcode.TabIndex = 67;
            this.labelAddressPostcode.Text = global::FormsView.TranslationsStatic.Default.labelPostcode;
            // 
            // labelAddressCity
            // 
            this.labelAddressCity.AutoSize = true;
            this.labelAddressCity.Location = new System.Drawing.Point(17, 217);
            this.labelAddressCity.Name = "labelAddressCity";
            this.labelAddressCity.Size = new System.Drawing.Size(41, 13);
            this.labelAddressCity.TabIndex = 65;
            this.labelAddressCity.Text = global::FormsView.TranslationsStatic.Default.labelCity;
            // 
            // maskedTextBoxClientPesel
            // 
            this.maskedTextBoxClientPesel.Enabled = false;
            this.maskedTextBoxClientPesel.Location = new System.Drawing.Point(130, 103);
            this.maskedTextBoxClientPesel.Mask = "00000000000";
            this.maskedTextBoxClientPesel.Name = "maskedTextBoxClientPesel";
            this.maskedTextBoxClientPesel.Size = new System.Drawing.Size(72, 20);
            this.maskedTextBoxClientPesel.TabIndex = 60;
            this.maskedTextBoxClientPesel.Tag = "searchParam";
            this.maskedTextBoxClientPesel.Text = global::FormsView.TranslationsStatic.Default.labelPesel;
            // 
            // labelClientFirstName
            // 
            this.labelClientFirstName.AutoSize = true;
            this.labelClientFirstName.Location = new System.Drawing.Point(17, 29);
            this.labelClientFirstName.Name = "labelClientFirstName";
            this.labelClientFirstName.Size = new System.Drawing.Size(29, 13);
            this.labelClientFirstName.TabIndex = 51;
            this.labelClientFirstName.Text = global::FormsView.TranslationsStatic.Default.labelFirstName;
            // 
            // labelClientSurname
            // 
            this.labelClientSurname.AutoSize = true;
            this.labelClientSurname.Location = new System.Drawing.Point(17, 66);
            this.labelClientSurname.Name = "labelClientSurname";
            this.labelClientSurname.Size = new System.Drawing.Size(56, 13);
            this.labelClientSurname.TabIndex = 52;
            this.labelClientSurname.Text = global::FormsView.TranslationsStatic.Default.labelSurname;
            // 
            // labelClientPhone
            // 
            this.labelClientPhone.AutoSize = true;
            this.labelClientPhone.Location = new System.Drawing.Point(17, 143);
            this.labelClientPhone.Name = "labelClientPhone";
            this.labelClientPhone.Size = new System.Drawing.Size(82, 13);
            this.labelClientPhone.TabIndex = 54;
            this.labelClientPhone.Text = global::FormsView.TranslationsStatic.Default.labelTelephone;
            // 
            // ClientSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.findClearControls1);
            this.Controls.Add(this.groupBoxClientSearch);
            this.Name = "ClientSearchControl";
            this.Size = new System.Drawing.Size(314, 369);
            this.groupBoxClientSearch.ResumeLayout(false);
            this.groupBoxClientSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxClientSearch;
        private System.Windows.Forms.CheckBox checkBoxPhoneNumber;
        private System.Windows.Forms.CheckBox checkBoxPesel;
        private System.Windows.Forms.CheckBox checkBoxSurname;
        private System.Windows.Forms.CheckBox checkBoxName;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxClientPesel;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxClientPhone;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.TextBox textBoxClientSurname;
        private System.Windows.Forms.Label labelClientFirstName;
        private System.Windows.Forms.Label labelClientSurname;
        private System.Windows.Forms.Label labelClientPhone;
        private System.Windows.Forms.Label labelClientPesel;
        private System.Windows.Forms.CheckBox checkBoxStreet;
        private System.Windows.Forms.CheckBox checkBoxPostcode;
        private System.Windows.Forms.CheckBox checkBoxCity;
        private System.Windows.Forms.CheckBox checkBoxProvince;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxAddressPostcode;
        private System.Windows.Forms.ComboBox comboBoxAddressProvince;
        private System.Windows.Forms.Label labelAddressProvince;
        private System.Windows.Forms.TextBox textBoxAddressStreet;
        private System.Windows.Forms.Label labelAddressStreet;
        private System.Windows.Forms.Label labelAddressPostcode;
        private System.Windows.Forms.ComboBox comboBoxAddressCity;
        private System.Windows.Forms.Label labelAddressCity;
        private Controls.FindClearControls findClearControls1;
    }
}
