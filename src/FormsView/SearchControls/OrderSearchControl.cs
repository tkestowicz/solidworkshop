﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Search;
using FormsView.Helpers;
using AppLogic.Presenters.Popup.Search;

namespace FormsView.SearchControls
{
    public partial class OrderSearchControl : Base.BaseUserControl, IOrderSearchView
    {

        OrderSearchPresenter _presenter;

        public OrderSearchControl()
        {
            InitializeComponent();
        }

        private void checkBoxState_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxOrderState.Enabled = checkBoxState.Checked;
        }

        private void checkBoxOrderSupervisor_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxOrderSupervisor.Enabled = checkBoxOrderSupervisor.Checked;
        }

        public List<Model.Projection.ComboBox> States
        {
            set 
            {
                comboBoxOrderState.Items.Clear();
                comboBoxOrderState.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Supervisors
        {
            set 
            {
                comboBoxOrderSupervisor.Items.Clear();
                comboBoxOrderSupervisor.Items.AddRange(value.ToArray());
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(AppLogic.Interfaces.Popups.IPopupPresenter presenter)
        {
            findClearControls1.Enable = true;
            findClearControls1.Sender = presenter;
            _presenter = presenter as OrderSearchPresenter;
        }

        public Dictionary<string, object> SearchParameters
        {
            get 
            {
                return groupBoxOrderSearch.GetSearchParams();
            }
        }

        public void Reset()
        {
            checkBoxOrderSupervisor.Checked = false;
            checkBoxState.Checked = false;

            comboBoxOrderState.SelectedIndex = -1;
            comboBoxOrderSupervisor.SelectedIndex = -1;
        }
    }
}
