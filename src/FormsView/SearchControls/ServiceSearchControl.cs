﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Search;
using FormsView.Helpers;
using AppLogic.Presenters.Popup.Search;

namespace FormsView.SearchControls
{
    public partial class ServiceSearchControl : Base.BaseUserControl, IServiceSearchView
    {

        ServiceSearchPresenter _presenter;

        public ServiceSearchControl()
        {
            InitializeComponent();
        }

        private void checkBoxServiceState_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxServiceState.Enabled = checkBoxServiceState.Checked;
        }

        private void checkBoxServiceName_CheckedChanged(object sender, EventArgs e)
        {
            textBoxServiceName.Enabled = checkBoxServiceName.Checked;
        }

        private void checkBoxPriceFrom_CheckedChanged(object sender, EventArgs e)
        {
            textBoxPriceFrom.Enabled = checkBoxPriceFrom.Checked;
        }

        private void checkBoxPriceTo_CheckedChanged(object sender, EventArgs e)
        {
            textBoxPriceTo.Enabled = checkBoxPriceTo.Checked;
        }

        private void checkBoxServiceMechanic_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxServiceMechanic.Enabled = checkBoxServiceMechanic.Checked;
        }

        public List<Model.Projection.ComboBox> States
        {
            set 
            {
                comboBoxServiceState.Items.Clear();
                comboBoxServiceState.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Mechanics
        {
            set 
            {
                comboBoxServiceMechanic.Items.Clear();
                comboBoxServiceMechanic.Items.AddRange(value.ToArray());
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(AppLogic.Interfaces.Popups.IPopupPresenter presenter)
        {
            findClearControls1.Enable = true;
            findClearControls1.Sender = presenter;
            _presenter = presenter as ServiceSearchPresenter;
        }

        public Dictionary<string, object> SearchParameters
        {
            get 
            {
                return groupBoxServiceSearch.GetSearchParams();
            }
        }

        public void Reset()
        {
            checkBoxPriceFrom.Checked = false;
            checkBoxPriceTo.Checked = false;
            checkBoxServiceMechanic.Checked = false;
            checkBoxServiceName.Checked = false;
            checkBoxServiceState.Checked = false;

            comboBoxServiceMechanic.SelectedIndex = -1;
            comboBoxServiceState.SelectedIndex = -1;
            textBoxPriceFrom.Text = null;
            textBoxPriceTo.Text = null;
            textBoxServiceName.Text = null;
        }
    }
}
