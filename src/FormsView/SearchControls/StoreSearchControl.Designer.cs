﻿namespace FormsView.SearchControls
{
    partial class StoreSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findClearControls1 = new FormsView.Controls.FindClearControls();
            this.groupBoxStoreSearch = new System.Windows.Forms.GroupBox();
            this.textBoxUnitPriceTo = new System.Windows.Forms.MaskedTextBox();
            this.textBoxUnitPriceFrom = new System.Windows.Forms.MaskedTextBox();
            this.textBoxAvailableAmountTo = new System.Windows.Forms.MaskedTextBox();
            this.textBoxAvailableAmountFrom = new System.Windows.Forms.MaskedTextBox();
            this.checkBoxPartName = new System.Windows.Forms.CheckBox();
            this.checkBoxStorePartClass = new System.Windows.Forms.CheckBox();
            this.checkBoxAvailableAmountTo = new System.Windows.Forms.CheckBox();
            this.checkBoxAvailableAmountFrom = new System.Windows.Forms.CheckBox();
            this.labelAvailableAmountTo = new System.Windows.Forms.Label();
            this.labelAvailableAmountFrom = new System.Windows.Forms.Label();
            this.checkBoxUnitPriceTo = new System.Windows.Forms.CheckBox();
            this.checkBoxUnitPriceFrom = new System.Windows.Forms.CheckBox();
            this.labelUnitPriceTo = new System.Windows.Forms.Label();
            this.labelUnitPriceFrom = new System.Windows.Forms.Label();
            this.comboBoxStorePartClass = new System.Windows.Forms.ComboBox();
            this.labelStorePartClass = new System.Windows.Forms.Label();
            this.labelPartName = new System.Windows.Forms.Label();
            this.textBoxPartName = new System.Windows.Forms.TextBox();
            this.labelPartUnitPrice = new System.Windows.Forms.Label();
            this.labelPartAvailableAmount = new System.Windows.Forms.Label();
            this.groupBoxStoreSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // findClearControls1
            // 
            this.findClearControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.findClearControls1.Location = new System.Drawing.Point(0, 236);
            this.findClearControls1.Name = "findClearControls1";
            this.findClearControls1.Sender = null;
            this.findClearControls1.Size = new System.Drawing.Size(314, 25);
            this.findClearControls1.TabIndex = 0;
            // 
            // groupBoxStoreSearch
            // 
            this.groupBoxStoreSearch.Controls.Add(this.textBoxUnitPriceTo);
            this.groupBoxStoreSearch.Controls.Add(this.textBoxUnitPriceFrom);
            this.groupBoxStoreSearch.Controls.Add(this.textBoxAvailableAmountTo);
            this.groupBoxStoreSearch.Controls.Add(this.textBoxAvailableAmountFrom);
            this.groupBoxStoreSearch.Controls.Add(this.checkBoxPartName);
            this.groupBoxStoreSearch.Controls.Add(this.checkBoxStorePartClass);
            this.groupBoxStoreSearch.Controls.Add(this.checkBoxAvailableAmountTo);
            this.groupBoxStoreSearch.Controls.Add(this.checkBoxAvailableAmountFrom);
            this.groupBoxStoreSearch.Controls.Add(this.labelAvailableAmountTo);
            this.groupBoxStoreSearch.Controls.Add(this.labelAvailableAmountFrom);
            this.groupBoxStoreSearch.Controls.Add(this.checkBoxUnitPriceTo);
            this.groupBoxStoreSearch.Controls.Add(this.checkBoxUnitPriceFrom);
            this.groupBoxStoreSearch.Controls.Add(this.labelUnitPriceTo);
            this.groupBoxStoreSearch.Controls.Add(this.labelUnitPriceFrom);
            this.groupBoxStoreSearch.Controls.Add(this.comboBoxStorePartClass);
            this.groupBoxStoreSearch.Controls.Add(this.labelStorePartClass);
            this.groupBoxStoreSearch.Controls.Add(this.labelPartName);
            this.groupBoxStoreSearch.Controls.Add(this.textBoxPartName);
            this.groupBoxStoreSearch.Controls.Add(this.labelPartUnitPrice);
            this.groupBoxStoreSearch.Controls.Add(this.labelPartAvailableAmount);
            this.groupBoxStoreSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxStoreSearch.Location = new System.Drawing.Point(0, 0);
            this.groupBoxStoreSearch.Name = "groupBoxStoreSearch";
            this.groupBoxStoreSearch.Size = new System.Drawing.Size(314, 219);
            this.groupBoxStoreSearch.TabIndex = 1;
            this.groupBoxStoreSearch.TabStop = false;
            this.groupBoxStoreSearch.Text = global::FormsView.TranslationsStatic.Default.groupBoxStoreTitle;
            // 
            // textBoxUnitPriceTo
            // 
            this.textBoxUnitPriceTo.Enabled = false;
            this.textBoxUnitPriceTo.Location = new System.Drawing.Point(236, 166);
            this.textBoxUnitPriceTo.Mask = "00000.00";
            this.textBoxUnitPriceTo.Name = "textBoxUnitPriceTo";
            this.textBoxUnitPriceTo.Size = new System.Drawing.Size(55, 20);
            this.textBoxUnitPriceTo.TabIndex = 147;
            this.textBoxUnitPriceTo.Tag = "searchParam";
            // 
            // textBoxUnitPriceFrom
            // 
            this.textBoxUnitPriceFrom.Enabled = false;
            this.textBoxUnitPriceFrom.Location = new System.Drawing.Point(149, 166);
            this.textBoxUnitPriceFrom.Mask = "00000.00";
            this.textBoxUnitPriceFrom.Name = "textBoxUnitPriceFrom";
            this.textBoxUnitPriceFrom.Size = new System.Drawing.Size(55, 20);
            this.textBoxUnitPriceFrom.TabIndex = 146;
            this.textBoxUnitPriceFrom.Tag = "searchParam";
            // 
            // textBoxAvailableAmountTo
            // 
            this.textBoxAvailableAmountTo.Enabled = false;
            this.textBoxAvailableAmountTo.Location = new System.Drawing.Point(252, 112);
            this.textBoxAvailableAmountTo.Mask = "00000";
            this.textBoxAvailableAmountTo.Name = "textBoxAvailableAmountTo";
            this.textBoxAvailableAmountTo.Size = new System.Drawing.Size(39, 20);
            this.textBoxAvailableAmountTo.TabIndex = 145;
            this.textBoxAvailableAmountTo.Tag = "searchParam";
            this.textBoxAvailableAmountTo.ValidatingType = typeof(int);
            // 
            // textBoxAvailableAmountFrom
            // 
            this.textBoxAvailableAmountFrom.Enabled = false;
            this.textBoxAvailableAmountFrom.Location = new System.Drawing.Point(165, 113);
            this.textBoxAvailableAmountFrom.Mask = "00000";
            this.textBoxAvailableAmountFrom.Name = "textBoxAvailableAmountFrom";
            this.textBoxAvailableAmountFrom.Size = new System.Drawing.Size(39, 20);
            this.textBoxAvailableAmountFrom.TabIndex = 144;
            this.textBoxAvailableAmountFrom.Tag = "searchParam";
            this.textBoxAvailableAmountFrom.ValidatingType = typeof(int);
            // 
            // checkBoxPartName
            // 
            this.checkBoxPartName.AutoSize = true;
            this.checkBoxPartName.Location = new System.Drawing.Point(122, 34);
            this.checkBoxPartName.Name = "checkBoxPartName";
            this.checkBoxPartName.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPartName.TabIndex = 143;
            this.checkBoxPartName.UseVisualStyleBackColor = true;
            this.checkBoxPartName.CheckedChanged += new System.EventHandler(this.checkBoxPartName_CheckedChanged);
            // 
            // checkBoxStorePartClass
            // 
            this.checkBoxStorePartClass.AutoSize = true;
            this.checkBoxStorePartClass.Location = new System.Drawing.Point(122, 78);
            this.checkBoxStorePartClass.Name = "checkBoxStorePartClass";
            this.checkBoxStorePartClass.Size = new System.Drawing.Size(15, 14);
            this.checkBoxStorePartClass.TabIndex = 142;
            this.checkBoxStorePartClass.UseVisualStyleBackColor = true;
            this.checkBoxStorePartClass.CheckedChanged += new System.EventHandler(this.checkBoxStorePartClass_CheckedChanged);
            // 
            // checkBoxAvailableAmountTo
            // 
            this.checkBoxAvailableAmountTo.AutoSize = true;
            this.checkBoxAvailableAmountTo.Location = new System.Drawing.Point(211, 116);
            this.checkBoxAvailableAmountTo.Name = "checkBoxAvailableAmountTo";
            this.checkBoxAvailableAmountTo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAvailableAmountTo.TabIndex = 140;
            this.checkBoxAvailableAmountTo.UseVisualStyleBackColor = true;
            this.checkBoxAvailableAmountTo.CheckedChanged += new System.EventHandler(this.checkBoxAvailableAmountTo_CheckedChanged);
            // 
            // checkBoxAvailableAmountFrom
            // 
            this.checkBoxAvailableAmountFrom.AutoSize = true;
            this.checkBoxAvailableAmountFrom.Location = new System.Drawing.Point(122, 116);
            this.checkBoxAvailableAmountFrom.Name = "checkBoxAvailableAmountFrom";
            this.checkBoxAvailableAmountFrom.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAvailableAmountFrom.TabIndex = 139;
            this.checkBoxAvailableAmountFrom.UseVisualStyleBackColor = true;
            this.checkBoxAvailableAmountFrom.CheckedChanged += new System.EventHandler(this.checkBoxAvailableAmountFrom_CheckedChanged);
            // 
            // labelAvailableAmountTo
            // 
            this.labelAvailableAmountTo.AutoSize = true;
            this.labelAvailableAmountTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelAvailableAmountTo.Location = new System.Drawing.Point(227, 116);
            this.labelAvailableAmountTo.Name = "labelAvailableAmountTo";
            this.labelAvailableAmountTo.Size = new System.Drawing.Size(24, 13);
            this.labelAvailableAmountTo.TabIndex = 138;
            this.labelAvailableAmountTo.Text = global::FormsView.TranslationsStatic.Default.labelTo;
            // 
            // labelAvailableAmountFrom
            // 
            this.labelAvailableAmountFrom.AutoSize = true;
            this.labelAvailableAmountFrom.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelAvailableAmountFrom.Location = new System.Drawing.Point(140, 116);
            this.labelAvailableAmountFrom.Name = "labelAvailableAmountFrom";
            this.labelAvailableAmountFrom.Size = new System.Drawing.Size(24, 13);
            this.labelAvailableAmountFrom.TabIndex = 137;
            this.labelAvailableAmountFrom.Text = global::FormsView.TranslationsStatic.Default.labelFrom;
            // 
            // checkBoxUnitPriceTo
            // 
            this.checkBoxUnitPriceTo.AutoSize = true;
            this.checkBoxUnitPriceTo.Location = new System.Drawing.Point(215, 169);
            this.checkBoxUnitPriceTo.Name = "checkBoxUnitPriceTo";
            this.checkBoxUnitPriceTo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUnitPriceTo.TabIndex = 134;
            this.checkBoxUnitPriceTo.UseVisualStyleBackColor = true;
            this.checkBoxUnitPriceTo.CheckedChanged += new System.EventHandler(this.checkBoxUnitPriceTo_CheckedChanged);
            // 
            // checkBoxUnitPriceFrom
            // 
            this.checkBoxUnitPriceFrom.AutoSize = true;
            this.checkBoxUnitPriceFrom.Location = new System.Drawing.Point(122, 169);
            this.checkBoxUnitPriceFrom.Name = "checkBoxUnitPriceFrom";
            this.checkBoxUnitPriceFrom.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUnitPriceFrom.TabIndex = 133;
            this.checkBoxUnitPriceFrom.UseVisualStyleBackColor = true;
            this.checkBoxUnitPriceFrom.CheckedChanged += new System.EventHandler(this.checkBoxUnitPriceFrom_CheckedChanged);
            // 
            // labelUnitPriceTo
            // 
            this.labelUnitPriceTo.AutoSize = true;
            this.labelUnitPriceTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelUnitPriceTo.Location = new System.Drawing.Point(249, 150);
            this.labelUnitPriceTo.Name = "labelUnitPriceTo";
            this.labelUnitPriceTo.Size = new System.Drawing.Size(24, 13);
            this.labelUnitPriceTo.TabIndex = 132;
            this.labelUnitPriceTo.Text = global::FormsView.TranslationsStatic.Default.labelTo;
            // 
            // labelUnitPriceFrom
            // 
            this.labelUnitPriceFrom.AutoSize = true;
            this.labelUnitPriceFrom.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelUnitPriceFrom.Location = new System.Drawing.Point(162, 150);
            this.labelUnitPriceFrom.Name = "labelUnitPriceFrom";
            this.labelUnitPriceFrom.Size = new System.Drawing.Size(24, 13);
            this.labelUnitPriceFrom.TabIndex = 131;
            this.labelUnitPriceFrom.Text = global::FormsView.TranslationsStatic.Default.labelFrom;
            // 
            // comboBoxStorePartClass
            // 
            this.comboBoxStorePartClass.DisplayMember = "Value";
            this.comboBoxStorePartClass.Enabled = false;
            this.comboBoxStorePartClass.FormattingEnabled = true;
            this.comboBoxStorePartClass.Location = new System.Drawing.Point(143, 75);
            this.comboBoxStorePartClass.Name = "comboBoxStorePartClass";
            this.comboBoxStorePartClass.Size = new System.Drawing.Size(148, 21);
            this.comboBoxStorePartClass.TabIndex = 35;
            this.comboBoxStorePartClass.Tag = "searchParam";
            this.comboBoxStorePartClass.ValueMember = "Value";
            // 
            // labelStorePartClass
            // 
            this.labelStorePartClass.AutoSize = true;
            this.labelStorePartClass.Location = new System.Drawing.Point(17, 75);
            this.labelStorePartClass.Name = "labelStorePartClass";
            this.labelStorePartClass.Size = new System.Drawing.Size(88, 13);
            this.labelStorePartClass.TabIndex = 34;
            this.labelStorePartClass.Text = global::FormsView.TranslationsStatic.Default.labelPartClass;
            // 
            // labelPartName
            // 
            this.labelPartName.AutoSize = true;
            this.labelPartName.Location = new System.Drawing.Point(17, 31);
            this.labelPartName.Name = "labelPartName";
            this.labelPartName.Size = new System.Drawing.Size(43, 13);
            this.labelPartName.TabIndex = 26;
            this.labelPartName.Text = global::FormsView.TranslationsStatic.Default.labelName;
            // 
            // textBoxPartName
            // 
            this.textBoxPartName.Enabled = false;
            this.textBoxPartName.Location = new System.Drawing.Point(143, 31);
            this.textBoxPartName.Name = "textBoxPartName";
            this.textBoxPartName.Size = new System.Drawing.Size(148, 20);
            this.textBoxPartName.TabIndex = 30;
            this.textBoxPartName.Tag = "searchParam";
            // 
            // labelPartUnitPrice
            // 
            this.labelPartUnitPrice.AutoSize = true;
            this.labelPartUnitPrice.Location = new System.Drawing.Point(17, 166);
            this.labelPartUnitPrice.Name = "labelPartUnitPrice";
            this.labelPartUnitPrice.Size = new System.Drawing.Size(98, 13);
            this.labelPartUnitPrice.TabIndex = 29;
            this.labelPartUnitPrice.Text = global::FormsView.TranslationsStatic.Default.labelUnitPrice;
            // 
            // labelPartAvailableAmount
            // 
            this.labelPartAvailableAmount.AutoSize = true;
            this.labelPartAvailableAmount.Location = new System.Drawing.Point(17, 116);
            this.labelPartAvailableAmount.Name = "labelPartAvailableAmount";
            this.labelPartAvailableAmount.Size = new System.Drawing.Size(80, 13);
            this.labelPartAvailableAmount.TabIndex = 28;
            this.labelPartAvailableAmount.Text = global::FormsView.TranslationsStatic.Default.labelAvailableAmount;
            // 
            // StoreSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxStoreSearch);
            this.Controls.Add(this.findClearControls1);
            this.Name = "StoreSearchControl";
            this.Size = new System.Drawing.Size(314, 261);
            this.groupBoxStoreSearch.ResumeLayout(false);
            this.groupBoxStoreSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.FindClearControls findClearControls1;
        private System.Windows.Forms.GroupBox groupBoxStoreSearch;
        private System.Windows.Forms.ComboBox comboBoxStorePartClass;
        private System.Windows.Forms.Label labelStorePartClass;
        private System.Windows.Forms.Label labelPartName;
        private System.Windows.Forms.TextBox textBoxPartName;
        private System.Windows.Forms.Label labelPartUnitPrice;
        private System.Windows.Forms.Label labelPartAvailableAmount;
        private System.Windows.Forms.CheckBox checkBoxPartName;
        private System.Windows.Forms.CheckBox checkBoxStorePartClass;
        private System.Windows.Forms.CheckBox checkBoxAvailableAmountTo;
        private System.Windows.Forms.CheckBox checkBoxAvailableAmountFrom;
        private System.Windows.Forms.Label labelAvailableAmountTo;
        private System.Windows.Forms.Label labelAvailableAmountFrom;
        private System.Windows.Forms.CheckBox checkBoxUnitPriceTo;
        private System.Windows.Forms.CheckBox checkBoxUnitPriceFrom;
        private System.Windows.Forms.Label labelUnitPriceTo;
        private System.Windows.Forms.Label labelUnitPriceFrom;
        private System.Windows.Forms.MaskedTextBox textBoxAvailableAmountFrom;
        private System.Windows.Forms.MaskedTextBox textBoxAvailableAmountTo;
        private System.Windows.Forms.MaskedTextBox textBoxUnitPriceFrom;
        private System.Windows.Forms.MaskedTextBox textBoxUnitPriceTo;
    }
}
