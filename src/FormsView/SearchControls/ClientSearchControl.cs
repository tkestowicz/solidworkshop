﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Popups.Search;
using AppLogic.Presenters.Popup.Search;
using FormsView.Helpers;

namespace FormsView.SearchControls
{
    public partial class ClientSearchControl : Base.BaseUserControl, IClientSearchView
    {
        ClientSearchPresenter _presenter;

        public ClientSearchControl()
        {
            InitializeComponent();
        }

        public string FirstName
        {
            get
            {
                return textBoxFirstName.Text;
            }
            set
            {
                textBoxFirstName.Text = value;
            }
        }

        public string Surname
        {
            get
            {
                return textBoxClientSurname.Text;
            }
            set
            {
                textBoxClientSurname.Text = value;
            }
        }

        public string Pesel
        {
            get
            {
                return maskedTextBoxClientPesel.Text;
            }
            set
            {
                maskedTextBoxClientPesel.Text = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return maskedTextBoxClientPhone.Text;
            }
            set
            {
                maskedTextBoxClientPhone.Text = value;
            }
        }

        public List<Model.Projection.ComboBox> Cities
        {
            set
            {
                comboBoxAddressCity.Items.Clear();
                comboBoxAddressCity.Items.AddRange(value.ToArray());
            }
        }

        public int CityId
        {
            get
            {
                try
                {
                    return (comboBoxAddressCity.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                comboBoxAddressCity.SelectedIndex = comboBoxAddressCity.FindIndex(value);
            }
        }

        public List<Model.Projection.ComboBox> Provinces
        {
            set
            {
                comboBoxAddressProvince.Items.Clear();
                comboBoxAddressProvince.Items.AddRange(value.ToArray());
            }
        }

        public string Province
        {
            get
            {
                try
                {
                    return (comboBoxAddressProvince.SelectedItem as Model.Projection.ComboBox).Value;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                try
                {
                    comboBoxAddressProvince.SelectedIndex = comboBoxAddressProvince.FindIndex(value);

                    // Ładujemy miasta dla ustawionego wojewódtwa
                    LoadCitiesByProvince(this, new EventArgs());

                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    comboBoxAddressProvince.SelectedIndex = -1;
                }
            }
        }

        public string Postcode
        {
            get
            {
                return maskedTextBoxAddressPostcode.Text;
            }
            set
            {
                maskedTextBoxAddressPostcode.Text = value;
            }
        }

        public string Street
        {
            get
            {
                return textBoxAddressStreet.Text;
            }
            set
            {
                textBoxAddressStreet.Text = value;
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(AppLogic.Interfaces.Popups.IPopupPresenter presenter)
        {
            findClearControls1.Enable = true;
            findClearControls1.Sender = presenter;
            _presenter = presenter as ClientSearchPresenter;
        }

        public Dictionary<string, object> SearchParameters
        {
            get 
            {
                return groupBoxClientSearch.GetSearchParams();
            }
        }

        private void checkBoxName_CheckedChanged(object sender, EventArgs e)
        {
            textBoxFirstName.Enabled = checkBoxName.Checked;
        }

        private void checkBoxSurname_CheckedChanged(object sender, EventArgs e)
        {
            textBoxClientSurname.Enabled = checkBoxSurname.Checked;
        }

        private void checkBoxPesel_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxClientPesel.Enabled = checkBoxPesel.Checked;
        }

        private void checkBoxPhoneNumber_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxClientPhone.Enabled = checkBoxPhoneNumber.Checked;
        }

        private void checkBoxProvince_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxAddressProvince.Enabled = checkBoxProvince.Checked;
        }

        private void checkBoxCity_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxAddressCity.Enabled = checkBoxCity.Checked;
        }

        private void checkBoxPostcode_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBoxAddressPostcode.Enabled = checkBoxPostcode.Checked;
        }

        private void checkBoxStreet_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAddressStreet.Enabled = checkBoxStreet.Checked;
        }

        private void LoadCitiesByProvince(object sender, EventArgs e)
        {
            _presenter.LoadCitiesByProvince(Province);
        }


        public void Reset()
        {
            checkBoxCity.Checked = false;
            checkBoxName.Checked = false;
            checkBoxPesel.Checked = false;
            checkBoxPhoneNumber.Checked = false;
            checkBoxPostcode.Checked = false;
            checkBoxProvince.Checked = false;
            checkBoxStreet.Checked = false;
            checkBoxSurname.Checked = false;

            CityId = 0;
            Province = null;
            FirstName = null;
            Surname = null;
            Street = null;
            Postcode = null;
            Pesel = null;
            PhoneNumber = null;
        }
    }
}
