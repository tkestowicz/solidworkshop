﻿namespace FormsView.SearchControls
{
    partial class CarSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findClearControls1 = new FormsView.Controls.FindClearControls();
            this.groupBoxCarSearch = new System.Windows.Forms.GroupBox();
            this.checkBoxBrand = new System.Windows.Forms.CheckBox();
            this.checkBoxModel = new System.Windows.Forms.CheckBox();
            this.checkBoxColor = new System.Windows.Forms.CheckBox();
            this.checkBoxRegistrationNumber = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxProductionYearTo = new System.Windows.Forms.MaskedTextBox();
            this.checkBoxProductionYearTo = new System.Windows.Forms.CheckBox();
            this.checkBoxProductionYearFrom = new System.Windows.Forms.CheckBox();
            this.maskedProductionYearFrom = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxCarColor = new System.Windows.Forms.ComboBox();
            this.comboBoxCarModel = new System.Windows.Forms.ComboBox();
            this.comboBoxCarBrand = new System.Windows.Forms.ComboBox();
            this.textBoxRegistrationNumber = new System.Windows.Forms.TextBox();
            this.labelProductionYearTo = new System.Windows.Forms.Label();
            this.labelProductionYearFrom = new System.Windows.Forms.Label();
            this.labelProductionYear = new System.Windows.Forms.Label();
            this.labelCarColor = new System.Windows.Forms.Label();
            this.labelCarBrand = new System.Windows.Forms.Label();
            this.labelCarModel = new System.Windows.Forms.Label();
            this.labelCarRegistrationNumber = new System.Windows.Forms.Label();
            this.groupBoxCarSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // findClearControls1
            // 
            this.findClearControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.findClearControls1.Location = new System.Drawing.Point(0, 248);
            this.findClearControls1.Name = "findClearControls1";
            this.findClearControls1.Sender = null;
            this.findClearControls1.Size = new System.Drawing.Size(314, 25);
            this.findClearControls1.TabIndex = 0;
            // 
            // groupBoxCarSearch
            // 
            this.groupBoxCarSearch.Controls.Add(this.checkBoxBrand);
            this.groupBoxCarSearch.Controls.Add(this.checkBoxModel);
            this.groupBoxCarSearch.Controls.Add(this.checkBoxColor);
            this.groupBoxCarSearch.Controls.Add(this.checkBoxRegistrationNumber);
            this.groupBoxCarSearch.Controls.Add(this.maskedTextBoxProductionYearTo);
            this.groupBoxCarSearch.Controls.Add(this.checkBoxProductionYearTo);
            this.groupBoxCarSearch.Controls.Add(this.checkBoxProductionYearFrom);
            this.groupBoxCarSearch.Controls.Add(this.labelProductionYearTo);
            this.groupBoxCarSearch.Controls.Add(this.labelProductionYearFrom);
            this.groupBoxCarSearch.Controls.Add(this.maskedProductionYearFrom);
            this.groupBoxCarSearch.Controls.Add(this.labelProductionYear);
            this.groupBoxCarSearch.Controls.Add(this.comboBoxCarColor);
            this.groupBoxCarSearch.Controls.Add(this.comboBoxCarModel);
            this.groupBoxCarSearch.Controls.Add(this.comboBoxCarBrand);
            this.groupBoxCarSearch.Controls.Add(this.textBoxRegistrationNumber);
            this.groupBoxCarSearch.Controls.Add(this.labelCarColor);
            this.groupBoxCarSearch.Controls.Add(this.labelCarBrand);
            this.groupBoxCarSearch.Controls.Add(this.labelCarModel);
            this.groupBoxCarSearch.Controls.Add(this.labelCarRegistrationNumber);
            this.groupBoxCarSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxCarSearch.Location = new System.Drawing.Point(0, 0);
            this.groupBoxCarSearch.Name = "groupBoxCarSearch";
            this.groupBoxCarSearch.Size = new System.Drawing.Size(314, 228);
            this.groupBoxCarSearch.TabIndex = 1;
            this.groupBoxCarSearch.TabStop = false;
            this.groupBoxCarSearch.Text = global::FormsView.TranslationsStatic.Default.groupBoxCarTitle;
            // 
            // checkBoxBrand
            // 
            this.checkBoxBrand.AutoSize = true;
            this.checkBoxBrand.Location = new System.Drawing.Point(124, 39);
            this.checkBoxBrand.Name = "checkBoxBrand";
            this.checkBoxBrand.Size = new System.Drawing.Size(15, 14);
            this.checkBoxBrand.TabIndex = 127;
            this.checkBoxBrand.UseVisualStyleBackColor = true;
            this.checkBoxBrand.CheckedChanged += new System.EventHandler(this.checkBoxBrand_CheckedChanged);
            // 
            // checkBoxModel
            // 
            this.checkBoxModel.AutoSize = true;
            this.checkBoxModel.Location = new System.Drawing.Point(124, 76);
            this.checkBoxModel.Name = "checkBoxModel";
            this.checkBoxModel.Size = new System.Drawing.Size(15, 14);
            this.checkBoxModel.TabIndex = 126;
            this.checkBoxModel.UseVisualStyleBackColor = true;
            this.checkBoxModel.CheckedChanged += new System.EventHandler(this.checkBoxModel_CheckedChanged);
            // 
            // checkBoxColor
            // 
            this.checkBoxColor.AutoSize = true;
            this.checkBoxColor.Location = new System.Drawing.Point(124, 112);
            this.checkBoxColor.Name = "checkBoxColor";
            this.checkBoxColor.Size = new System.Drawing.Size(15, 14);
            this.checkBoxColor.TabIndex = 125;
            this.checkBoxColor.UseVisualStyleBackColor = true;
            this.checkBoxColor.CheckedChanged += new System.EventHandler(this.checkBoxColor_CheckedChanged);
            // 
            // checkBoxRegistrationNumber
            // 
            this.checkBoxRegistrationNumber.AutoSize = true;
            this.checkBoxRegistrationNumber.Location = new System.Drawing.Point(124, 151);
            this.checkBoxRegistrationNumber.Name = "checkBoxRegistrationNumber";
            this.checkBoxRegistrationNumber.Size = new System.Drawing.Size(15, 14);
            this.checkBoxRegistrationNumber.TabIndex = 124;
            this.checkBoxRegistrationNumber.UseVisualStyleBackColor = true;
            this.checkBoxRegistrationNumber.CheckedChanged += new System.EventHandler(this.checkBoxRegistrationNumber_CheckedChanged);
            // 
            // maskedTextBoxProductionYearTo
            // 
            this.maskedTextBoxProductionYearTo.Enabled = false;
            this.maskedTextBoxProductionYearTo.Location = new System.Drawing.Point(260, 186);
            this.maskedTextBoxProductionYearTo.Mask = "0000";
            this.maskedTextBoxProductionYearTo.Name = "maskedTextBoxProductionYearTo";
            this.maskedTextBoxProductionYearTo.Size = new System.Drawing.Size(34, 20);
            this.maskedTextBoxProductionYearTo.TabIndex = 123;
            this.maskedTextBoxProductionYearTo.Tag = "searchParam";
            // 
            // checkBoxProductionYearTo
            // 
            this.checkBoxProductionYearTo.AutoSize = true;
            this.checkBoxProductionYearTo.Location = new System.Drawing.Point(214, 189);
            this.checkBoxProductionYearTo.Name = "checkBoxProductionYearTo";
            this.checkBoxProductionYearTo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxProductionYearTo.TabIndex = 122;
            this.checkBoxProductionYearTo.UseVisualStyleBackColor = true;
            this.checkBoxProductionYearTo.CheckedChanged += new System.EventHandler(this.checkBoxProductionYearTo_CheckedChanged);
            // 
            // checkBoxProductionYearFrom
            // 
            this.checkBoxProductionYearFrom.AutoSize = true;
            this.checkBoxProductionYearFrom.Location = new System.Drawing.Point(124, 189);
            this.checkBoxProductionYearFrom.Name = "checkBoxProductionYearFrom";
            this.checkBoxProductionYearFrom.Size = new System.Drawing.Size(15, 14);
            this.checkBoxProductionYearFrom.TabIndex = 121;
            this.checkBoxProductionYearFrom.UseVisualStyleBackColor = true;
            this.checkBoxProductionYearFrom.CheckedChanged += new System.EventHandler(this.checkBoxProductionYearFrom_CheckedChanged);
            // 
            // maskedProductionYearFrom
            // 
            this.maskedProductionYearFrom.Enabled = false;
            this.maskedProductionYearFrom.Location = new System.Drawing.Point(168, 186);
            this.maskedProductionYearFrom.Mask = "0000";
            this.maskedProductionYearFrom.Name = "maskedProductionYearFrom";
            this.maskedProductionYearFrom.Size = new System.Drawing.Size(34, 20);
            this.maskedProductionYearFrom.TabIndex = 84;
            this.maskedProductionYearFrom.Tag = "searchParam";
            // 
            // comboBoxCarColor
            // 
            this.comboBoxCarColor.DisplayMember = "Value";
            this.comboBoxCarColor.Enabled = false;
            this.comboBoxCarColor.FormattingEnabled = true;
            this.comboBoxCarColor.Location = new System.Drawing.Point(145, 109);
            this.comboBoxCarColor.Name = "comboBoxCarColor";
            this.comboBoxCarColor.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarColor.TabIndex = 77;
            this.comboBoxCarColor.TabStop = false;
            this.comboBoxCarColor.Tag = "searchParam";
            this.comboBoxCarColor.ValueMember = "Value";
            // 
            // comboBoxCarModel
            // 
            this.comboBoxCarModel.DisplayMember = "Value";
            this.comboBoxCarModel.Enabled = false;
            this.comboBoxCarModel.FormattingEnabled = true;
            this.comboBoxCarModel.Location = new System.Drawing.Point(145, 73);
            this.comboBoxCarModel.Name = "comboBoxCarModel";
            this.comboBoxCarModel.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarModel.TabIndex = 76;
            this.comboBoxCarModel.Tag = "searchParam";
            this.comboBoxCarModel.ValueMember = "Value";
            // 
            // comboBoxCarBrand
            // 
            this.comboBoxCarBrand.DisplayMember = "Value";
            this.comboBoxCarBrand.Enabled = false;
            this.comboBoxCarBrand.FormattingEnabled = true;
            this.comboBoxCarBrand.Location = new System.Drawing.Point(145, 36);
            this.comboBoxCarBrand.Name = "comboBoxCarBrand";
            this.comboBoxCarBrand.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarBrand.TabIndex = 75;
            this.comboBoxCarBrand.Tag = "searchParam";
            this.comboBoxCarBrand.ValueMember = "Value";
            this.comboBoxCarBrand.SelectedIndexChanged += new System.EventHandler(this.LoadModelsByBrand);
            // 
            // textBoxRegistrationNumber
            // 
            this.textBoxRegistrationNumber.Enabled = false;
            this.textBoxRegistrationNumber.Location = new System.Drawing.Point(145, 148);
            this.textBoxRegistrationNumber.MaxLength = 7;
            this.textBoxRegistrationNumber.Name = "textBoxRegistrationNumber";
            this.textBoxRegistrationNumber.Size = new System.Drawing.Size(57, 20);
            this.textBoxRegistrationNumber.TabIndex = 69;
            this.textBoxRegistrationNumber.Tag = "searchParam";
            // 
            // labelProductionYearTo
            // 
            this.labelProductionYearTo.AutoSize = true;
            this.labelProductionYearTo.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelTo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelProductionYearTo.Location = new System.Drawing.Point(233, 189);
            this.labelProductionYearTo.Name = "labelProductionYearTo";
            this.labelProductionYearTo.Size = new System.Drawing.Size(24, 13);
            this.labelProductionYearTo.TabIndex = 120;
            this.labelProductionYearTo.Text = global::FormsView.TranslationsStatic.Default.labelTo;
            // 
            // labelProductionYearFrom
            // 
            this.labelProductionYearFrom.AutoSize = true;
            this.labelProductionYearFrom.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelProductionYearFrom.Location = new System.Drawing.Point(142, 189);
            this.labelProductionYearFrom.Name = "labelProductionYearFrom";
            this.labelProductionYearFrom.Size = new System.Drawing.Size(24, 13);
            this.labelProductionYearFrom.TabIndex = 119;
            this.labelProductionYearFrom.Text = global::FormsView.TranslationsStatic.Default.labelFrom;
            // 
            // labelProductionYear
            // 
            this.labelProductionYear.AutoSize = true;
            this.labelProductionYear.Location = new System.Drawing.Point(17, 190);
            this.labelProductionYear.Name = "labelProductionYear";
            this.labelProductionYear.Size = new System.Drawing.Size(76, 13);
            this.labelProductionYear.TabIndex = 83;
            this.labelProductionYear.Text = global::FormsView.TranslationsStatic.Default.labelProductionYear;
            // 
            // labelCarColor
            // 
            this.labelCarColor.AutoSize = true;
            this.labelCarColor.Location = new System.Drawing.Point(17, 109);
            this.labelCarColor.Name = "labelCarColor";
            this.labelCarColor.Size = new System.Drawing.Size(34, 13);
            this.labelCarColor.TabIndex = 71;
            this.labelCarColor.Text = global::FormsView.TranslationsStatic.Default.labelColor;
            // 
            // labelCarBrand
            // 
            this.labelCarBrand.AutoSize = true;
            this.labelCarBrand.Location = new System.Drawing.Point(17, 36);
            this.labelCarBrand.Name = "labelCarBrand";
            this.labelCarBrand.Size = new System.Drawing.Size(40, 13);
            this.labelCarBrand.TabIndex = 65;
            this.labelCarBrand.Text = global::FormsView.TranslationsStatic.Default.labelBrand;
            // 
            // labelCarModel
            // 
            this.labelCarModel.AutoSize = true;
            this.labelCarModel.Location = new System.Drawing.Point(17, 73);
            this.labelCarModel.Name = "labelCarModel";
            this.labelCarModel.Size = new System.Drawing.Size(39, 13);
            this.labelCarModel.TabIndex = 66;
            this.labelCarModel.Text = global::FormsView.TranslationsStatic.Default.labelModel;
            // 
            // labelCarRegistrationNumber
            // 
            this.labelCarRegistrationNumber.AutoSize = true;
            this.labelCarRegistrationNumber.Location = new System.Drawing.Point(17, 148);
            this.labelCarRegistrationNumber.Name = "labelCarRegistrationNumber";
            this.labelCarRegistrationNumber.Size = new System.Drawing.Size(102, 13);
            this.labelCarRegistrationNumber.TabIndex = 67;
            this.labelCarRegistrationNumber.Text = global::FormsView.TranslationsStatic.Default.labelRegistrationNumber;
            // 
            // CarSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxCarSearch);
            this.Controls.Add(this.findClearControls1);
            this.Name = "CarSearchControl";
            this.Size = new System.Drawing.Size(314, 273);
            this.groupBoxCarSearch.ResumeLayout(false);
            this.groupBoxCarSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.FindClearControls findClearControls1;
        private System.Windows.Forms.GroupBox groupBoxCarSearch;
        private System.Windows.Forms.MaskedTextBox maskedProductionYearFrom;
        private System.Windows.Forms.Label labelProductionYear;
        private System.Windows.Forms.ComboBox comboBoxCarColor;
        private System.Windows.Forms.ComboBox comboBoxCarModel;
        private System.Windows.Forms.ComboBox comboBoxCarBrand;
        private System.Windows.Forms.TextBox textBoxRegistrationNumber;
        private System.Windows.Forms.Label labelCarColor;
        private System.Windows.Forms.Label labelCarBrand;
        private System.Windows.Forms.Label labelCarModel;
        private System.Windows.Forms.Label labelCarRegistrationNumber;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxProductionYearTo;
        private System.Windows.Forms.CheckBox checkBoxProductionYearTo;
        private System.Windows.Forms.CheckBox checkBoxProductionYearFrom;
        private System.Windows.Forms.Label labelProductionYearTo;
        private System.Windows.Forms.Label labelProductionYearFrom;
        private System.Windows.Forms.CheckBox checkBoxBrand;
        private System.Windows.Forms.CheckBox checkBoxModel;
        private System.Windows.Forms.CheckBox checkBoxColor;
        private System.Windows.Forms.CheckBox checkBoxRegistrationNumber;
    }
}
