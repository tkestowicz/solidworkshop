﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Search;
using FormsView.Helpers;
using AppLogic.Presenters.Popup.Search;

namespace FormsView.SearchControls
{
    public partial class StoreSearchControl : Base.BaseUserControl, IStoreSearchView
    {

        StoreSearchPresenter _presenter;

        public StoreSearchControl()
        {
            InitializeComponent();
        }

        private void checkBoxPartName_CheckedChanged(object sender, EventArgs e)
        {
            textBoxPartName.Enabled = checkBoxPartName.Checked;
        }

        private void checkBoxStorePartClass_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxStorePartClass.Enabled = checkBoxStorePartClass.Checked;
        }

        private void checkBoxAvailableAmountFrom_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAvailableAmountFrom.Enabled = checkBoxAvailableAmountFrom.Checked;
        }
        
        private void checkBoxAvailableAmountTo_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAvailableAmountTo.Enabled = checkBoxAvailableAmountTo.Checked;
        }

        private void checkBoxUnitPriceFrom_CheckedChanged(object sender, EventArgs e)
        {
            textBoxUnitPriceFrom.Enabled = checkBoxUnitPriceFrom.Enabled;
        }

        private void checkBoxUnitPriceTo_CheckedChanged(object sender, EventArgs e)
        {
            textBoxUnitPriceTo.Enabled = checkBoxUnitPriceTo.Enabled;
        }

        public List<Model.Projection.ComboBox> PartClasses
        {
            set 
            {
                comboBoxStorePartClass.Items.Clear();
                comboBoxStorePartClass.Items.AddRange(value.ToArray());
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(AppLogic.Interfaces.Popups.IPopupPresenter presenter)
        {
            findClearControls1.Enable = true;
            findClearControls1.Sender = presenter;
            _presenter = presenter as StoreSearchPresenter;
        }

        public Dictionary<string, object> SearchParameters
        {
            get 
            {
                return groupBoxStoreSearch.GetSearchParams();
            }
        }

        public void Reset()
        {
            checkBoxAvailableAmountFrom.Checked = false;
            checkBoxAvailableAmountTo.Checked = false;
            checkBoxPartName.Checked = false;
            checkBoxStorePartClass.Checked = false;
            checkBoxUnitPriceFrom.Checked = false;
            checkBoxUnitPriceTo.Checked = false;

            comboBoxStorePartClass.SelectedIndex = -1;
            textBoxAvailableAmountFrom.Text = null;
            textBoxAvailableAmountTo.Text = null;
            textBoxPartName.Text = null;
            textBoxUnitPriceFrom.Text = null;
            textBoxUnitPriceTo.Text = null;
        }
    }
}
