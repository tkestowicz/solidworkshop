﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FormsView
{
    static class RunApp
    {
        /// <summary>
        /// Wejście do aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Forms.Main());
        }
    }
}
