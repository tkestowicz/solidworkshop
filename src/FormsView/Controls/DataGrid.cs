﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces;
using AppLogic.Presenters;

namespace FormsView.Controls
{

    /// <summary>
    /// Klasa implementuje funkcjonalności związane z siatką danych
    /// </summary>
    public partial class DataGrid : Base.BaseUserControl, IDataGridView
    {

        DataPresenter _dataPresenter;

        public DataGrid()
        {
            InitializeComponent();
        }

        #region Implementacja interfejsu IDataGridView

        #region Properties

        public bool Enable
        {
            set
            {
                Enabled = value;
                bindingNavigatorCountItem.Enabled = value;
                bindingNavigatorAddNewItem.Enabled = value;
                bindingNavigatorDeleteItem.Enabled = value;
                bindingNavigatorMoveFirstItem.Enabled = value;
                bindingNavigatorMoveLastItem.Enabled = value;
                bindingNavigatorMoveNextItem.Enabled = value;
                bindingNavigatorMovePreviousItem.Enabled = value;
                bindingNavigatorPositionItem.Enabled = value;
                bindingNavigatorPageLabel.Enabled = value;
                bindingNavigatorItemsPerPageLabel.Enabled = value;
                bindingNavigatorItemsPerPage.Enabled = value;
                bindingNavigatorEditItem.Enabled = value;

                pagination.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get { return Enabled; }
        }

        public int CurrentPage
        {
            get
            {
                try
                {
                    return Int32.Parse(bindingNavigatorPositionItem.Text);
                } // Input string was not in a correct format
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                bindingNavigatorPositionItem.Text = value.ToString();
            }
        }

        public int TotalPages
        {
            get
            {
                try
                {
                    return Int32.Parse(bindingNavigatorCountItem.Text);
                } // Input string was not in a correct format
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                bindingNavigatorCountItem.Text = String.Format(TranslationsStatic.Default.paginationTotalCount, value.ToString());
            }
        }

        public string ItemsPerPageRange
        {
            set
            {
                var options = value.Split(',').ToArray<string>();

                bindingNavigatorItemsPerPage.Items.Clear();
                bindingNavigatorItemsPerPage.Items.AddRange(options);
            }
        }

        public int ItemsPerPage
        {
            get
            {
                try
                {
                    return Int32.Parse(bindingNavigatorItemsPerPage.SelectedItem.ToString());
                } // Input string was not in a correct format
                catch (Exception)
                {
                    return 0;
                }
            }

            set
            {
                try
                {
                    bindingNavigatorItemsPerPage.SelectedIndex = bindingNavigatorItemsPerPage.Items.IndexOf(value.ToString());
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    bindingNavigatorItemsPerPage.SelectedIndex = -1;
                }
            }
        }

        public object CurrentRecord
        {
            get
            {
                return bindingSource.Current as object;
            }
        }

        public bool ClearFiltersEnabled
        {
            set 
            {
                buttonClearFilters.Enabled = value;
            }
        }

        #endregion

        #region Methods

        public void Initialize(DataPresenter presenter)
        {

            _dataPresenter = presenter;

            _dataPresenter.Initialize();

            // Resetujemy ustawienia prezentera
            _dataPresenter.Reset();

            // Załadowanie pierwszej strony danych
            _dataPresenter.LoadData();
        }

        public void LoadData<T>(IList<T> records)
        {
            //bindingSource.DataSource = new AppLogic.Core.SortableBindingList<T>(records.ToList());
            bindingSource.DataSource = records;
            dataGridView.AutoGenerateColumns = true;
            bindingSource.ResetBindings(false);
        }

        public void ResetGrid()
        {
            bindingSource.DataSource = null;
            bindingSource.ResetBindings(false);
        }

        public void RefreshGrid()
        {
            _dataPresenter.LoadData();
        }

        public void ResetNavigation(bool addRecordEnabled = false)
        {
            CurrentPage = 0;
            TotalPages = 0;
            Enable = false;

            bindingNavigatorAddNewItem.Enabled = addRecordEnabled;
            pagination.Enabled = addRecordEnabled;
            Enabled = addRecordEnabled;
        }

        #endregion

        #endregion

        #region Navigation events

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            _dataPresenter.MoveNextPage();
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
           _dataPresenter.MovePrevPage();
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
           _dataPresenter.MoveFirstPage();
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
           _dataPresenter.MoveLastPage();
        }

        private void bindingNavigatorPositionItem_TextChanged(object sender, EventArgs e)
        {

            if (bindingNavigatorPositionItem.Focused)
                _dataPresenter.MoveToPage(Int32.Parse(bindingNavigatorPositionItem.Text));
        }

        private void bindingNavigatorItemsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_dataPresenter != null && bindingNavigatorItemsPerPage.Focused)
                _dataPresenter.ChangeItemsPerPage(ItemsPerPage);
        }

        #endregion

        #region Datagrid Events

        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            _dataPresenter.OnPreview();
        }

        private void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            _dataPresenter.OnPreview();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            _dataPresenter.OnAdd();
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            _dataPresenter.OnDelete();
        }

        private void bindingNavigatorEditItem_Click(object sender, EventArgs e)
        {
            _dataPresenter.OnEdit(this as IDataGridView);
        }

        /// <summary>
        /// Metoda zmienia nagłówki kolumn po załadowaniu danych
        /// </summary>
        private void dataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (_dataPresenter != null)
            {
                var indexPrefix = _dataPresenter.GetColumnHeaderPrefixIndex();

                foreach (DataGridViewColumn column in dataGridView.Columns)
                {
                    if(column.HeaderText.Contains("Id"))
                        column.HeaderText = TranslationsGridHeaders.Default.Id;
                    else{
                        try
                        {
                            column.HeaderText = TranslationsGridHeaders.Default[indexPrefix + column.HeaderText].ToString();

                            // Ustawiamy znacznik sortowania na kolumnie
                            if (_dataPresenter.Ordering.Column == column.Name)
                            {
                                if (_dataPresenter.Ordering.Direction == AppLogic.Core.SortDirection.Ascending)
                                    column.HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                                else
                                    column.HeaderCell.SortGlyphDirection = SortOrder.Descending;
                            }
                        }
                        catch (Exception){}
                    }
                }
            }
        }

        /// <summary>
        /// Metoda obsługuje zmianę parametrów sortowania
        /// </summary>
        /// <param name="e">Kolumna po której należy sortować</param>
        private void dataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewColumn column = dataGridView.Columns[e.ColumnIndex];

            // Ustawiamy kolumnę po której sortujemy
            _dataPresenter.Ordering.Column = column.Name;

            //column.SortMode = DataGridViewColumnSortMode.Programmatic;
            
            // Ustawiamy kierunek sortowania
            if (_dataPresenter.Ordering.Direction == AppLogic.Core.SortDirection.Ascending)
                _dataPresenter.Ordering.Direction = AppLogic.Core.SortDirection.Descending;
            else
                _dataPresenter.Ordering.Direction = AppLogic.Core.SortDirection.Ascending;

            // Ładujemy posortowane dane
            _dataPresenter.LoadData();

            // Ustawiamy znacznik sortowania na kolumnie
            if (_dataPresenter.Ordering.Direction == AppLogic.Core.SortDirection.Ascending)
                dataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            else
                dataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;

        }

        #endregion

        private void buttonClearFilters_Click(object sender, EventArgs e)
        {
            _dataPresenter.FilterFields = null;
            _dataPresenter.LoadData();
        }

    }
}
