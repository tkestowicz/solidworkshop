﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters;
using AppLogic.Interfaces;
using AppLogic.Presenters.Control;
using AppLogic.Interfaces.Controls;

namespace FormsView.Controls
{
    public partial class AddEditSearchControls : Base.BaseUserControl, IAddEditSearchControlsView
    {

        public AddEditSearchControls()
        {
            InitializeComponent();

            _presenter = new AddEditSearchControlsPresenter(this);
        }

        public void AddEditHide()
        {
            buttonAdd.Hide();
            buttonEdit.Hide();
        }

        public void AddEditShow()
        {
            buttonAdd.Show();
            buttonEdit.Show();
        }

        #region Fields

        AddEditSearchControlsPresenter _presenter;
        object _sender;

        #endregion

        #region IAddEditControlsView

        public bool Enable
        {
            set 
            {
                Enabled = value;

                buttonAdd.Enabled = value;
                buttonEdit.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get { return Enabled; }
        }

        public bool SearchButtonVisible
        {
            get
            {
                return buttonSearch.Visible;
            }
            set
            {
                buttonSearch.Visible = value;
            }
        }

        #endregion

        #region Events

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            _presenter.OnEdit();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            _presenter.OnAdd();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            _presenter.OnSearch();
        }

        #endregion

        public object Sender
        {
            get
            {
                return _sender;
            }
            set
            {
                _sender = value;
            }
        }


        public AddEditSearchControlsPresenter.SearchEventHandler AddHandler
        {
            set 
            {
                _presenter.SearchEvent += value;
            }
        }


        public AddEditSearchControlsPresenter.SearchEventHandler RemoveHandler
        {
            set 
            {
                _presenter.SearchEvent -= value;
            }
        }
    }
}
