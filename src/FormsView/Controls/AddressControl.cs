﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters;
using FormsView.Helpers;
using AppLogic.Interfaces.Controls;
using AppLogic.Presenters.Control;
using AppLogic.Presenters.DicitionaryEditor;

namespace FormsView.Controls
{
    public partial class AddressControl : Base.BaseUserControl, IAddressControlView
    {

        AddressControlPresenter _presenter;

        public AddressControl()
        {
            InitializeComponent();

            _presenter = new AddressControlPresenter(this);
        }

        #region IAddressEditorView Members

        public List<Model.Projection.ComboBox> Cities
        {
            set
            {
                comboBoxAddressCity.Items.Clear();
                comboBoxAddressCity.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Provinces
        {
            set
            {
                comboBoxAddressProvince.Items.Clear();
                comboBoxAddressProvince.Items.AddRange(value.ToArray());
            }
        }

        #endregion

        #region IAddress Members

        public int CityId
        {
            get
            {
                try
                {
                    return (comboBoxAddressCity.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                comboBoxAddressCity.SelectedIndex = comboBoxAddressCity.FindIndex(value);
            }
        }

        public string CityByName
        {
            set
            {
                comboBoxAddressCity.SelectedIndex = comboBoxAddressCity.FindIndex(value);
            }
        }

        public int ProvinceId
        {
            get
            {
                try
                {
                    return (comboBoxAddressProvince.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                try
                {
                    comboBoxAddressProvince.SelectedIndex = comboBoxAddressProvince.FindIndex(value);

                    // Ładujemy miasta dla ustawionego wojewódtwa
                    LoadCitiesByProvince(this, new EventArgs());

                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    comboBoxAddressProvince.SelectedIndex = -1;
                }
            }
        }

        public string Province
        {
            get
            {
                try
                {
                    return (comboBoxAddressProvince.SelectedItem as Model.Projection.ComboBox).Value;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                try
                {
                    comboBoxAddressProvince.SelectedIndex = comboBoxAddressProvince.FindIndex(value);

                    // Ładujemy miasta dla ustawionego wojewódtwa
                    LoadCitiesByProvince(this, new EventArgs());

                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    comboBoxAddressProvince.SelectedIndex = -1;
                }
            }
        }

        public string Postcode
        {
            get
            {
                return maskedTextBoxAddressPostcode.Text;
            }
            set
            {
                maskedTextBoxAddressPostcode.Text = value;
            }
        }

        public string Street
        {
            get
            {
                return textBoxAddressStreet.Text;
            }
            set
            {
                textBoxAddressStreet.Text = value;
            }
        }

        public string HouseNumber
        {
            get
            {
                return textBoxAddressHouseNumber.Text;
            }
            set
            {
                textBoxAddressHouseNumber.Text = value;
            }
        }

        public string ApartmentNumber
        {
            get
            {
                return textBoxAddressApartmentNumber.Text;
            }
            set
            {
                textBoxAddressApartmentNumber.Text = value;
            }
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set 
            {
                groupBoxAddress.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return groupBoxAddress.Enabled;
            }
        }

        #endregion

        #region Events

        private void LoadCitiesByProvince(object sender, EventArgs e)
        {
            _presenter.LoadCitiesByProvince(this.Province);
        }

        /// <summary>
        /// Metoda ustawia dodane do bazy miasto.
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        /// <param name="newCity">Obiekt z wartościami nowego rekordu</param>
        private void ReloadCity(object sender, KeyValuePair<int, string> newCity)
        {
            ProvinceId = newCity.Key;
            CityByName = newCity.Value;
        }

        /// <summary>
        /// Otworzenie okienka do dodawania miasta
        /// </summary>
        private void buttonAddressAddCity_Click(object sender, EventArgs e)
        {
            // Tworzymy adapter widoku na kontrolke edytora miasta
            var viewAdapter = new FormsView.Popups.Adapters.CityEditorViewToPopupAdapter
            {
                // Widok, który będzie faktycznie wyświetlał dane
                View = new Popups.WithDropdownListEditor()
                {
                    LabelValue = TranslationsStatic.Default.labelCity,
                    LabelDropdownList = TranslationsStatic.Default.labelProvince
                }
            };

            // Tworzymy logikę, która będzie faktycznie obsługiwać widok
            var logic = new CityPresenter()
            {
                View = viewAdapter  // Przypinamy adapter widoku
            };

            // Wiążemy widok adaptera z logiką właściwą logiką
            viewAdapter.View.Initialize(logic);

            // Tworzymy adapter dla logiki
            var logicAdapter = new AppLogic.Presenters.Popup.EditorPresenterToPopupAdapter
            {
                Presenter = logic,         // Właściwa logika dla kontrolki
                View = viewAdapter.View    // Właściwy widok kontrolki                 
            };

            // Podpinamy obsługę zdarzenia
            logicAdapter.OnReload += new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCity);

            // Inicjujemy logikę
            logicAdapter.Initialize();

            // Dopiero po zainicjowaniu logiki można ustawić element w liście rozwijanej
            viewAdapter.View.CategoryId = ProvinceId;

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(viewAdapter.View, logicAdapter);
            popup.Text = TranslationsStatic.Default.titleAddCity;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia

            // Usuwamy obsługę zdarzenia
            logicAdapter.OnReload -= new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCity);
            
        }

        #endregion

    }
}
