﻿namespace FormsView.Controls
{
    partial class AddressControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressControl));
            this.groupBoxAddress = new System.Windows.Forms.GroupBox();
            this.maskedTextBoxAddressPostcode = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxAddressProvince = new System.Windows.Forms.ComboBox();
            this.labelAddressProvince = new System.Windows.Forms.Label();
            this.textBoxAddressApartmentNumber = new System.Windows.Forms.TextBox();
            this.textBoxAddressHouseNumber = new System.Windows.Forms.TextBox();
            this.labelAddressApartmentNumber = new System.Windows.Forms.Label();
            this.labelAddressHouseNumber = new System.Windows.Forms.Label();
            this.textBoxAddressStreet = new System.Windows.Forms.TextBox();
            this.labelAddressStreet = new System.Windows.Forms.Label();
            this.labelAddressPostcode = new System.Windows.Forms.Label();
            this.buttonAddressAddCity = new System.Windows.Forms.Button();
            this.comboBoxAddressCity = new System.Windows.Forms.ComboBox();
            this.labelAddressCity = new System.Windows.Forms.Label();
            this.groupBoxAddress.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxAddress
            // 
            this.groupBoxAddress.Controls.Add(this.maskedTextBoxAddressPostcode);
            this.groupBoxAddress.Controls.Add(this.comboBoxAddressProvince);
            this.groupBoxAddress.Controls.Add(this.labelAddressProvince);
            this.groupBoxAddress.Controls.Add(this.textBoxAddressApartmentNumber);
            this.groupBoxAddress.Controls.Add(this.textBoxAddressHouseNumber);
            this.groupBoxAddress.Controls.Add(this.labelAddressApartmentNumber);
            this.groupBoxAddress.Controls.Add(this.labelAddressHouseNumber);
            this.groupBoxAddress.Controls.Add(this.textBoxAddressStreet);
            this.groupBoxAddress.Controls.Add(this.labelAddressStreet);
            this.groupBoxAddress.Controls.Add(this.labelAddressPostcode);
            this.groupBoxAddress.Controls.Add(this.buttonAddressAddCity);
            this.groupBoxAddress.Controls.Add(this.comboBoxAddressCity);
            this.groupBoxAddress.Controls.Add(this.labelAddressCity);
            this.groupBoxAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxAddress.Location = new System.Drawing.Point(0, 0);
            this.groupBoxAddress.Name = "groupBoxAddress";
            this.groupBoxAddress.Size = new System.Drawing.Size(289, 250);
            this.groupBoxAddress.TabIndex = 20;
            this.groupBoxAddress.TabStop = false;
            this.groupBoxAddress.Text = global::FormsView.TranslationsStatic.Default.titleAddress;
            // 
            // maskedTextBoxAddressPostcode
            // 
            this.maskedTextBoxAddressPostcode.Location = new System.Drawing.Point(95, 99);
            this.maskedTextBoxAddressPostcode.Mask = "00-000";
            this.maskedTextBoxAddressPostcode.Name = "maskedTextBoxAddressPostcode";
            this.maskedTextBoxAddressPostcode.Size = new System.Drawing.Size(43, 20);
            this.maskedTextBoxAddressPostcode.TabIndex = 53;
            // 
            // comboBoxAddressProvince
            // 
            this.comboBoxAddressProvince.DisplayMember = "Value";
            this.comboBoxAddressProvince.FormattingEnabled = true;
            this.comboBoxAddressProvince.Location = new System.Drawing.Point(95, 25);
            this.comboBoxAddressProvince.Name = "comboBoxAddressProvince";
            this.comboBoxAddressProvince.Size = new System.Drawing.Size(149, 21);
            this.comboBoxAddressProvince.TabIndex = 52;
            this.comboBoxAddressProvince.ValueMember = "Value";
            this.comboBoxAddressProvince.SelectedIndexChanged += new System.EventHandler(this.LoadCitiesByProvince);
            // 
            // labelAddressProvince
            // 
            this.labelAddressProvince.AutoSize = true;
            this.labelAddressProvince.Location = new System.Drawing.Point(12, 25);
            this.labelAddressProvince.Name = "labelAddressProvince";
            this.labelAddressProvince.Size = new System.Drawing.Size(77, 13);
            this.labelAddressProvince.TabIndex = 51;
            this.labelAddressProvince.Text = global::FormsView.TranslationsStatic.Default.labelProvince;
            // 
            // textBoxAddressApartmentNumber
            // 
            this.textBoxAddressApartmentNumber.Location = new System.Drawing.Point(95, 205);
            this.textBoxAddressApartmentNumber.MaxLength = 4;
            this.textBoxAddressApartmentNumber.Name = "textBoxAddressApartmentNumber";
            this.textBoxAddressApartmentNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddressApartmentNumber.Size = new System.Drawing.Size(43, 20);
            this.textBoxAddressApartmentNumber.TabIndex = 50;
            // 
            // textBoxAddressHouseNumber
            // 
            this.textBoxAddressHouseNumber.Location = new System.Drawing.Point(95, 168);
            this.textBoxAddressHouseNumber.MaxLength = 5;
            this.textBoxAddressHouseNumber.Name = "textBoxAddressHouseNumber";
            this.textBoxAddressHouseNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddressHouseNumber.Size = new System.Drawing.Size(43, 20);
            this.textBoxAddressHouseNumber.TabIndex = 49;
            // 
            // labelAddressApartmentNumber
            // 
            this.labelAddressApartmentNumber.AutoSize = true;
            this.labelAddressApartmentNumber.Location = new System.Drawing.Point(12, 205);
            this.labelAddressApartmentNumber.Name = "labelAddressApartmentNumber";
            this.labelAddressApartmentNumber.Size = new System.Drawing.Size(72, 13);
            this.labelAddressApartmentNumber.TabIndex = 48;
            this.labelAddressApartmentNumber.Text = global::FormsView.TranslationsStatic.Default.labelApartmentNumber;
            // 
            // labelAddressHouseNumber
            // 
            this.labelAddressHouseNumber.AutoSize = true;
            this.labelAddressHouseNumber.Location = new System.Drawing.Point(12, 168);
            this.labelAddressHouseNumber.Name = "labelAddressHouseNumber";
            this.labelAddressHouseNumber.Size = new System.Drawing.Size(70, 13);
            this.labelAddressHouseNumber.TabIndex = 47;
            this.labelAddressHouseNumber.Text = global::FormsView.TranslationsStatic.Default.labelHouseNumber;
            // 
            // textBoxAddressStreet
            // 
            this.textBoxAddressStreet.Location = new System.Drawing.Point(95, 133);
            this.textBoxAddressStreet.Name = "textBoxAddressStreet";
            this.textBoxAddressStreet.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddressStreet.Size = new System.Drawing.Size(149, 20);
            this.textBoxAddressStreet.TabIndex = 46;
            // 
            // labelAddressStreet
            // 
            this.labelAddressStreet.AutoSize = true;
            this.labelAddressStreet.Location = new System.Drawing.Point(12, 133);
            this.labelAddressStreet.Name = "labelAddressStreet";
            this.labelAddressStreet.Size = new System.Drawing.Size(34, 13);
            this.labelAddressStreet.TabIndex = 45;
            this.labelAddressStreet.Text = global::FormsView.TranslationsStatic.Default.labelStreet;
            // 
            // labelAddressPostcode
            // 
            this.labelAddressPostcode.AutoSize = true;
            this.labelAddressPostcode.Location = new System.Drawing.Point(12, 99);
            this.labelAddressPostcode.Name = "labelAddressPostcode";
            this.labelAddressPostcode.Size = new System.Drawing.Size(77, 13);
            this.labelAddressPostcode.TabIndex = 43;
            this.labelAddressPostcode.Text = global::FormsView.TranslationsStatic.Default.labelPostcode;
            // 
            // buttonAddressAddCity
            // 
            this.buttonAddressAddCity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonAddressAddCity.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.buttonAddressAddCity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddressAddCity.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddressAddCity.Image")));
            this.buttonAddressAddCity.Location = new System.Drawing.Point(250, 62);
            this.buttonAddressAddCity.Name = "buttonAddressAddCity";
            this.buttonAddressAddCity.Size = new System.Drawing.Size(23, 20);
            this.buttonAddressAddCity.TabIndex = 42;
            this.buttonAddressAddCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddressAddCity.UseVisualStyleBackColor = true;
            this.buttonAddressAddCity.Click += new System.EventHandler(this.buttonAddressAddCity_Click);
            // 
            // comboBoxAddressCity
            // 
            this.comboBoxAddressCity.DisplayMember = "Value";
            this.comboBoxAddressCity.FormattingEnabled = true;
            this.comboBoxAddressCity.Location = new System.Drawing.Point(95, 62);
            this.comboBoxAddressCity.Name = "comboBoxAddressCity";
            this.comboBoxAddressCity.Size = new System.Drawing.Size(149, 21);
            this.comboBoxAddressCity.TabIndex = 34;
            this.comboBoxAddressCity.ValueMember = "Value";
            // 
            // labelAddressCity
            // 
            this.labelAddressCity.AutoSize = true;
            this.labelAddressCity.Location = new System.Drawing.Point(12, 62);
            this.labelAddressCity.Name = "labelAddressCity";
            this.labelAddressCity.Size = new System.Drawing.Size(41, 13);
            this.labelAddressCity.TabIndex = 10;
            this.labelAddressCity.Text = global::FormsView.TranslationsStatic.Default.labelCity;
            // 
            // AddressControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxAddress);
            this.Name = "AddressControl";
            this.Size = new System.Drawing.Size(289, 250);
            this.groupBoxAddress.ResumeLayout(false);
            this.groupBoxAddress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxAddress;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxAddressPostcode;
        private System.Windows.Forms.ComboBox comboBoxAddressProvince;
        private System.Windows.Forms.Label labelAddressProvince;
        private System.Windows.Forms.TextBox textBoxAddressHouseNumber;
        private System.Windows.Forms.Label labelAddressApartmentNumber;
        private System.Windows.Forms.Label labelAddressHouseNumber;
        private System.Windows.Forms.TextBox textBoxAddressStreet;
        private System.Windows.Forms.Label labelAddressStreet;
        private System.Windows.Forms.Label labelAddressPostcode;
        private System.Windows.Forms.Button buttonAddressAddCity;
        private System.Windows.Forms.ComboBox comboBoxAddressCity;
        private System.Windows.Forms.Label labelAddressCity;
        private System.Windows.Forms.TextBox textBoxAddressApartmentNumber;
    }
}
