﻿namespace FormsView.Controls
{
    partial class CarControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarControl));
            this.panelCarData = new System.Windows.Forms.Panel();
            this.maskedProductionYear = new System.Windows.Forms.MaskedTextBox();
            this.labelProductionYear = new System.Windows.Forms.Label();
            this.buttonAddFuel = new System.Windows.Forms.Button();
            this.buttonAddColor = new System.Windows.Forms.Button();
            this.buttonAddModel = new System.Windows.Forms.Button();
            this.buttonAddBrand = new System.Windows.Forms.Button();
            this.comboBoxCarFuel = new System.Windows.Forms.ComboBox();
            this.comboBoxCarColor = new System.Windows.Forms.ComboBox();
            this.comboBoxCarModel = new System.Windows.Forms.ComboBox();
            this.comboBoxCarBrand = new System.Windows.Forms.ComboBox();
            this.textBoxEngineCapacity = new System.Windows.Forms.TextBox();
            this.textBoxBodyNumber = new System.Windows.Forms.TextBox();
            this.textBoxRegistrationNumber = new System.Windows.Forms.TextBox();
            this.labelEngineCapacity = new System.Windows.Forms.Label();
            this.labelCarFuel = new System.Windows.Forms.Label();
            this.labelCarColor = new System.Windows.Forms.Label();
            this.labelCarBrand = new System.Windows.Forms.Label();
            this.labelCarModel = new System.Windows.Forms.Label();
            this.labelCarBodyNumber = new System.Windows.Forms.Label();
            this.labelCarRegistrationNumber = new System.Windows.Forms.Label();
            this.panelCarData.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCarData
            // 
            this.panelCarData.Controls.Add(this.maskedProductionYear);
            this.panelCarData.Controls.Add(this.labelProductionYear);
            this.panelCarData.Controls.Add(this.buttonAddFuel);
            this.panelCarData.Controls.Add(this.buttonAddColor);
            this.panelCarData.Controls.Add(this.buttonAddModel);
            this.panelCarData.Controls.Add(this.buttonAddBrand);
            this.panelCarData.Controls.Add(this.comboBoxCarFuel);
            this.panelCarData.Controls.Add(this.comboBoxCarColor);
            this.panelCarData.Controls.Add(this.comboBoxCarModel);
            this.panelCarData.Controls.Add(this.comboBoxCarBrand);
            this.panelCarData.Controls.Add(this.textBoxEngineCapacity);
            this.panelCarData.Controls.Add(this.textBoxBodyNumber);
            this.panelCarData.Controls.Add(this.textBoxRegistrationNumber);
            this.panelCarData.Controls.Add(this.labelEngineCapacity);
            this.panelCarData.Controls.Add(this.labelCarFuel);
            this.panelCarData.Controls.Add(this.labelCarColor);
            this.panelCarData.Controls.Add(this.labelCarBrand);
            this.panelCarData.Controls.Add(this.labelCarModel);
            this.panelCarData.Controls.Add(this.labelCarBodyNumber);
            this.panelCarData.Controls.Add(this.labelCarRegistrationNumber);
            this.panelCarData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCarData.Enabled = false;
            this.panelCarData.Location = new System.Drawing.Point(0, 0);
            this.panelCarData.Name = "panelCarData";
            this.panelCarData.Size = new System.Drawing.Size(304, 311);
            this.panelCarData.TabIndex = 47;
            this.panelCarData.Tag = "1";
            // 
            // maskedProductionYear
            // 
            this.maskedProductionYear.Location = new System.Drawing.Point(114, 276);
            this.maskedProductionYear.Mask = "0000";
            this.maskedProductionYear.Name = "maskedProductionYear";
            this.maskedProductionYear.Size = new System.Drawing.Size(34, 20);
            this.maskedProductionYear.TabIndex = 64;
            // 
            // labelProductionYear
            // 
            this.labelProductionYear.AutoSize = true;
            this.labelProductionYear.Location = new System.Drawing.Point(10, 276);
            this.labelProductionYear.Name = "labelProductionYear";
            this.labelProductionYear.Size = new System.Drawing.Size(76, 13);
            this.labelProductionYear.TabIndex = 63;
            this.labelProductionYear.Text = "Rok produkcji:";
            // 
            // buttonAddFuel
            // 
            this.buttonAddFuel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonAddFuel.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.buttonAddFuel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddFuel.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddFuel.Image")));
            this.buttonAddFuel.Location = new System.Drawing.Point(269, 123);
            this.buttonAddFuel.Name = "buttonAddFuel";
            this.buttonAddFuel.Size = new System.Drawing.Size(23, 20);
            this.buttonAddFuel.TabIndex = 62;
            this.buttonAddFuel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddFuel.UseVisualStyleBackColor = true;
            this.buttonAddFuel.Click += new System.EventHandler(this.buttonAddFuel_Click);
            // 
            // buttonAddColor
            // 
            this.buttonAddColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonAddColor.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.buttonAddColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddColor.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddColor.Image")));
            this.buttonAddColor.Location = new System.Drawing.Point(269, 88);
            this.buttonAddColor.Name = "buttonAddColor";
            this.buttonAddColor.Size = new System.Drawing.Size(23, 20);
            this.buttonAddColor.TabIndex = 61;
            this.buttonAddColor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddColor.UseVisualStyleBackColor = true;
            this.buttonAddColor.Click += new System.EventHandler(this.buttonAddColor_Click);
            // 
            // buttonAddModel
            // 
            this.buttonAddModel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonAddModel.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.buttonAddModel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddModel.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddModel.Image")));
            this.buttonAddModel.Location = new System.Drawing.Point(269, 52);
            this.buttonAddModel.Name = "buttonAddModel";
            this.buttonAddModel.Size = new System.Drawing.Size(23, 20);
            this.buttonAddModel.TabIndex = 60;
            this.buttonAddModel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddModel.UseVisualStyleBackColor = true;
            this.buttonAddModel.Click += new System.EventHandler(this.buttonAddModel_Click);
            // 
            // buttonAddBrand
            // 
            this.buttonAddBrand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonAddBrand.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.buttonAddBrand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddBrand.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddBrand.Image")));
            this.buttonAddBrand.Location = new System.Drawing.Point(269, 15);
            this.buttonAddBrand.Name = "buttonAddBrand";
            this.buttonAddBrand.Size = new System.Drawing.Size(23, 20);
            this.buttonAddBrand.TabIndex = 59;
            this.buttonAddBrand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddBrand.UseVisualStyleBackColor = true;
            this.buttonAddBrand.Click += new System.EventHandler(this.buttonAddBrand_Click);
            // 
            // comboBoxCarFuel
            // 
            this.comboBoxCarFuel.DisplayMember = "Value";
            this.comboBoxCarFuel.FormattingEnabled = true;
            this.comboBoxCarFuel.Location = new System.Drawing.Point(114, 122);
            this.comboBoxCarFuel.Name = "comboBoxCarFuel";
            this.comboBoxCarFuel.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarFuel.TabIndex = 58;
            this.comboBoxCarFuel.ValueMember = "Value";
            // 
            // comboBoxCarColor
            // 
            this.comboBoxCarColor.DisplayMember = "Value";
            this.comboBoxCarColor.FormattingEnabled = true;
            this.comboBoxCarColor.Location = new System.Drawing.Point(114, 87);
            this.comboBoxCarColor.Name = "comboBoxCarColor";
            this.comboBoxCarColor.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarColor.TabIndex = 57;
            this.comboBoxCarColor.TabStop = false;
            this.comboBoxCarColor.ValueMember = "Value";
            // 
            // comboBoxCarModel
            // 
            this.comboBoxCarModel.DisplayMember = "Value";
            this.comboBoxCarModel.FormattingEnabled = true;
            this.comboBoxCarModel.Location = new System.Drawing.Point(114, 51);
            this.comboBoxCarModel.Name = "comboBoxCarModel";
            this.comboBoxCarModel.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarModel.TabIndex = 56;
            this.comboBoxCarModel.ValueMember = "Value";
            // 
            // comboBoxCarBrand
            // 
            this.comboBoxCarBrand.DisplayMember = "Value";
            this.comboBoxCarBrand.FormattingEnabled = true;
            this.comboBoxCarBrand.Location = new System.Drawing.Point(114, 14);
            this.comboBoxCarBrand.Name = "comboBoxCarBrand";
            this.comboBoxCarBrand.Size = new System.Drawing.Size(149, 21);
            this.comboBoxCarBrand.TabIndex = 55;
            this.comboBoxCarBrand.ValueMember = "Value";
            this.comboBoxCarBrand.SelectedIndexChanged += new System.EventHandler(this.LoadModelsByBrand);
            // 
            // textBoxEngineCapacity
            // 
            this.textBoxEngineCapacity.Location = new System.Drawing.Point(114, 237);
            this.textBoxEngineCapacity.MaxLength = 4;
            this.textBoxEngineCapacity.Name = "textBoxEngineCapacity";
            this.textBoxEngineCapacity.Size = new System.Drawing.Size(34, 20);
            this.textBoxEngineCapacity.TabIndex = 54;
            // 
            // textBoxBodyNumber
            // 
            this.textBoxBodyNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxBodyNumber.Location = new System.Drawing.Point(114, 198);
            this.textBoxBodyNumber.MaxLength = 17;
            this.textBoxBodyNumber.Name = "textBoxBodyNumber";
            this.textBoxBodyNumber.Size = new System.Drawing.Size(149, 20);
            this.textBoxBodyNumber.TabIndex = 50;
            // 
            // textBoxRegistrationNumber
            // 
            this.textBoxRegistrationNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxRegistrationNumber.Location = new System.Drawing.Point(114, 158);
            this.textBoxRegistrationNumber.MaxLength = 7;
            this.textBoxRegistrationNumber.Name = "textBoxRegistrationNumber";
            this.textBoxRegistrationNumber.Size = new System.Drawing.Size(57, 20);
            this.textBoxRegistrationNumber.TabIndex = 49;
            // 
            // labelEngineCapacity
            // 
            this.labelEngineCapacity.AutoSize = true;
            this.labelEngineCapacity.Location = new System.Drawing.Point(10, 237);
            this.labelEngineCapacity.Name = "labelEngineCapacity";
            this.labelEngineCapacity.Size = new System.Drawing.Size(94, 13);
            this.labelEngineCapacity.TabIndex = 53;
            this.labelEngineCapacity.Text = "Pojemność silnika:";
            // 
            // labelCarFuel
            // 
            this.labelCarFuel.AutoSize = true;
            this.labelCarFuel.Location = new System.Drawing.Point(8, 122);
            this.labelCarFuel.Name = "labelCarFuel";
            this.labelCarFuel.Size = new System.Drawing.Size(41, 13);
            this.labelCarFuel.TabIndex = 52;
            this.labelCarFuel.Text = "Paliwo:";
            // 
            // labelCarColor
            // 
            this.labelCarColor.AutoSize = true;
            this.labelCarColor.Location = new System.Drawing.Point(8, 87);
            this.labelCarColor.Name = "labelCarColor";
            this.labelCarColor.Size = new System.Drawing.Size(34, 13);
            this.labelCarColor.TabIndex = 51;
            this.labelCarColor.Text = "Kolor:";
            // 
            // labelCarBrand
            // 
            this.labelCarBrand.AutoSize = true;
            this.labelCarBrand.Location = new System.Drawing.Point(9, 14);
            this.labelCarBrand.Name = "labelCarBrand";
            this.labelCarBrand.Size = new System.Drawing.Size(40, 13);
            this.labelCarBrand.TabIndex = 45;
            this.labelCarBrand.Text = "Marka:";
            // 
            // labelCarModel
            // 
            this.labelCarModel.AutoSize = true;
            this.labelCarModel.Location = new System.Drawing.Point(9, 51);
            this.labelCarModel.Name = "labelCarModel";
            this.labelCarModel.Size = new System.Drawing.Size(39, 13);
            this.labelCarModel.TabIndex = 46;
            this.labelCarModel.Text = "Model:";
            // 
            // labelCarBodyNumber
            // 
            this.labelCarBodyNumber.AutoSize = true;
            this.labelCarBodyNumber.Location = new System.Drawing.Point(10, 198);
            this.labelCarBodyNumber.Name = "labelCarBodyNumber";
            this.labelCarBodyNumber.Size = new System.Drawing.Size(89, 13);
            this.labelCarBodyNumber.TabIndex = 48;
            this.labelCarBodyNumber.Text = "Numer nadwozia:";
            // 
            // labelCarRegistrationNumber
            // 
            this.labelCarRegistrationNumber.AutoSize = true;
            this.labelCarRegistrationNumber.Location = new System.Drawing.Point(10, 158);
            this.labelCarRegistrationNumber.Name = "labelCarRegistrationNumber";
            this.labelCarRegistrationNumber.Size = new System.Drawing.Size(102, 13);
            this.labelCarRegistrationNumber.TabIndex = 47;
            this.labelCarRegistrationNumber.Text = "Numer rejestracyjny:";
            // 
            // CarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelCarData);
            this.Name = "CarControl";
            this.Size = new System.Drawing.Size(304, 311);
            this.panelCarData.ResumeLayout(false);
            this.panelCarData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCarData;
        private System.Windows.Forms.MaskedTextBox maskedProductionYear;
        private System.Windows.Forms.Label labelProductionYear;
        private System.Windows.Forms.Button buttonAddFuel;
        private System.Windows.Forms.Button buttonAddColor;
        private System.Windows.Forms.Button buttonAddModel;
        private System.Windows.Forms.Button buttonAddBrand;
        private System.Windows.Forms.ComboBox comboBoxCarFuel;
        private System.Windows.Forms.ComboBox comboBoxCarColor;
        private System.Windows.Forms.ComboBox comboBoxCarModel;
        private System.Windows.Forms.ComboBox comboBoxCarBrand;
        private System.Windows.Forms.TextBox textBoxEngineCapacity;
        private System.Windows.Forms.TextBox textBoxBodyNumber;
        private System.Windows.Forms.TextBox textBoxRegistrationNumber;
        private System.Windows.Forms.Label labelEngineCapacity;
        private System.Windows.Forms.Label labelCarFuel;
        private System.Windows.Forms.Label labelCarColor;
        private System.Windows.Forms.Label labelCarBrand;
        private System.Windows.Forms.Label labelCarModel;
        private System.Windows.Forms.Label labelCarBodyNumber;
        private System.Windows.Forms.Label labelCarRegistrationNumber;
    }
}
