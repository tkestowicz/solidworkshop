﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Controls;
using AppLogic.Presenters.Control;

namespace FormsView.Controls
{
    public partial class FindClearControls : Base.BaseUserControl, IFindClearControlsView
    {

        FindClearControlsPresenter _presenter;
        object _sender;

        public FindClearControls()
        {
            InitializeComponent();

            _presenter = new FindClearControlsPresenter(this);
        }

        public object Sender
        {
            get
            {
                return _sender;
            }
            set
            {
                _sender = value;
            }
        }

        public void Clear()
        {
            _presenter.OnClear();
        }

        public void Find()
        {
            _presenter.OnFind();
        }

        public bool Enable
        {
            set 
            {
                buttonClear.Enabled = value;
                buttonFind.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return buttonFind.Enabled && buttonClear.Enabled; 
            }
        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
            Find();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}
