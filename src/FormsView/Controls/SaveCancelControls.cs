﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces;
using AppLogic.Presenters;
using AppLogic.Interfaces.Controls;
using AppLogic.Presenters.Control;

namespace FormsView.Controls
{
    public partial class SaveCancelControls : Base.BaseUserControl, ISaveCancelControlsView
    {

        public SaveCancelControls()
        {
            InitializeComponent();

            _presenter = new SaveCancelControlsPresenter(this);
        }

        #region Fields

        SaveCancelControlsPresenter _presenter;
        object _sender;

        #endregion

        #region ISaveCancelControlsView

        public object Sender
        {
            get
            {
                return _sender;
            }
            set
            {
                _sender = value;
            }
        }

        public bool Enable
        {
            set
            {
                Enabled = value;

                buttonCancel.Enabled = value;
                buttonSave.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get { return Enabled; }
        }

        public void Cancel()
        {
            _presenter.OnCancel();
        }

        public void Save()
        {
            _presenter.OnSave();
        }

        #endregion

        #region Events

        private void buttonSave_Click(object sender, EventArgs e)
        {
           Save();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        #endregion
    }
}
