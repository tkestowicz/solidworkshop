﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormsView.Helpers;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Controls;
using AppLogic.Presenters.Control;
using AppLogic.Presenters.DicitionaryEditor;
using AppLogic.Interfaces.Editors;

namespace FormsView.Controls
{
    public partial class CarControl : Base.BaseUserControl, ICarControlView
    {

        CarControlPresenter _presenter;

        public CarControl()
        {
            InitializeComponent();

            _presenter = new CarControlPresenter(this);
        }


        #region ICarControl Members

        public int BrandId
        {
            get
            {
                try
                {
                    return (comboBoxCarBrand.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
               comboBoxCarBrand.SelectedIndex = comboBoxCarBrand.FindIndex(value);
            }
        }

        public int ModelId
        {
            get
            {
                try
                {
                    return (comboBoxCarModel.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
               comboBoxCarModel.SelectedIndex = comboBoxCarModel.FindIndex(value);
            }
        }

        public int ColorId
        {
            get
            {
                try
                {
                    return (comboBoxCarColor.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                 comboBoxCarColor.SelectedIndex = comboBoxCarColor.FindIndex(value);
            }
        }

        public int FuelId
        {
            get
            {
                try
                {
                    return (comboBoxCarFuel.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                comboBoxCarFuel.SelectedIndex = comboBoxCarFuel.FindIndex(value);
            }
        }

        public string RegistrationNumber
        {
            get
            {
                return textBoxRegistrationNumber.Text;
            }
            set
            {
                textBoxRegistrationNumber.Text = value;
            }
        }

        public string BodyNumber
        {
            get
            {
                return textBoxBodyNumber.Text;
            }
            set
            {
                textBoxBodyNumber.Text = value;
            }
        }

        public short? EngineCapacity
        {
            get
            {
                try
                {
                    return short.Parse(textBoxEngineCapacity.Text);
                }
                catch (Exception) { return null; }
            }
            set
            {
                textBoxEngineCapacity.Text = (value == 0)? null : value.ToString();
            }
        }

        public short? ProductionYear
        {
            get
            {
                try
                {
                    return short.Parse(maskedProductionYear.Text);
                }
                catch (Exception){ return null; }
            }
            set
            {
                maskedProductionYear.Text = (value == 0)? null : value.ToString();
            }
        }

        public List<Model.Projection.ComboBox> Brands
        {
            set
            {
                comboBoxCarBrand.Items.Clear();
                comboBoxCarBrand.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Models
        {
            set
            {
                comboBoxCarModel.Items.Clear();
                comboBoxCarModel.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Colors
        {
            set
            {
                comboBoxCarColor.Items.Clear();
                comboBoxCarColor.Items.AddRange(value.ToArray());
            }
        }

        public List<Model.Projection.ComboBox> Fuels
        {
            set
            {
                comboBoxCarFuel.Items.Clear();
                comboBoxCarFuel.Items.AddRange(value.ToArray());
            }
        }

        public string ModelByName
        {
            set 
            {
                comboBoxCarModel.SelectedIndex = comboBoxCarModel.FindIndex(value);
            }
        }

        public string BrandByName
        {
            set
            {
                comboBoxCarBrand.SelectedIndex = comboBoxCarBrand.FindIndex(value);
            }
        }

        public string ColorByName
        {
            set
            {
                comboBoxCarColor.SelectedIndex = comboBoxCarColor.FindIndex(value);
            }
        }

        public string FuelByName
        {
            set
            {
                comboBoxCarFuel.SelectedIndex = comboBoxCarFuel.FindIndex(value);
            }
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                panelCarData.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return panelCarData.Enabled;
            }
        }

        #endregion

        #region Events

        private void LoadModelsByBrand(object sender, EventArgs e)
        {
            _presenter.LoadModelsByBrand(this.BrandId);
        }

        /// <summary>
        /// Metoda ustawia dodany do bazy model samochodu.
        /// Lista modeli zostanie przeładowana.
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        /// <param name="newModel">Obiekt z wartościami nowego rekordu</param>
        private void ReloadCarModel(object sender, KeyValuePair<int, string> newModel)
        {
            BrandId = newModel.Key;
            _presenter.LoadModelsByBrand(BrandId);
            ModelByName = newModel.Value;
        }

        /// <summary>
        /// Metoda ustawia dodaną do bazy markę samochodu.
        /// Lista rozwijana zostanie przeładowana.
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        /// <param name="newModel">Obiekt z wartościami nowego rekordu</param>
        private void ReloadCarBrand(object sender, KeyValuePair<int, string> newBrand)
        {
            _presenter.LoadBrands();
            BrandByName = newBrand.Value;
        }

        /// <summary>
        /// Metoda ustawia dodany do bazy kolor samochodu.
        /// Lista rozwijana zostanie przeładowana.
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        /// <param name="newModel">Obiekt z wartościami nowego rekordu</param>
        private void ReloadCarColor(object sender, KeyValuePair<int, string> newColor)
        {
            _presenter.LoadColors();
            ColorByName = newColor.Value;
        }

        /// <summary>
        /// Metoda ustawia dodany do bazy rodzaj paliwa.
        /// Lista rozwijana zostanie przeładowana.
        /// </summary>
        /// <param name="sender">Obiekt, który wywołał zdarzenie</param>
        /// <param name="newModel">Obiekt z wartościami nowego rekordu</param>
        private void ReloadCarFuel(object sender, KeyValuePair<int, string> newFuel)
        {
            _presenter.LoadFuels();
            FuelByName = newFuel.Value;
        }

        #endregion

        private void buttonAddModel_Click(object sender, EventArgs e)
        {
            
            // Tworzymy adapter widoku na kontrolke edytora modelu samochodu
            var viewAdapter = new FormsView.Popups.Adapters.CarModelEditorViewToPopupAdapter
            {
                // Widok, który będzie faktycznie wyświetlał dane
                View = new Popups.WithDropdownListEditor()
                {
                    LabelValue = TranslationsStatic.Default.labelModel,
                    LabelDropdownList = TranslationsStatic.Default.labelBrand
                }
            };

            // Tworzymy logikę, która będzie faktycznie obsługiwać widok
            var logic = new CarModelPresenter()
                {
                    View = viewAdapter  // Przypinamy adapter widoku
                };

            // Wiążemy widok adaptera z logiką właściwą logiką
            viewAdapter.View.Initialize(logic);

            // Tworzymy adapter dla logiki
            var logicAdapter = new AppLogic.Presenters.Popup.EditorPresenterToPopupAdapter
            {
                 Presenter = logic,         // Właściwa logika dla kontrolki
                 View = viewAdapter.View    // Właściwy widok kontrolki                 
            };

            // Podpinamy obsługę zdarzenia
            logicAdapter.OnReload += new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarModel);

            // Inicjujemy logikę
            logicAdapter.Initialize();

            // Dopiero po zainicjowaniu logiki można ustawić element w liście rozwijanej
            viewAdapter.View.CategoryId = BrandId;

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(viewAdapter.View, logicAdapter);
            popup.Text = TranslationsStatic.Default.titleAddModel;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia

            // Usuwamy obsługę zdarzenia
            logicAdapter.OnReload -= new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarModel);
        }

        private void buttonAddBrand_Click(object sender, EventArgs e)
        {
            // Tworzymy adapter widoku na kontrolke edytora marki
            var viewAdapter = new Popups.Adapters.DictionaryEditorViewToPopupAdapter
            {
                // Widok, który będzie faktycznie wyświetlał dane
                View = new FormsView.Popups.WithTextboxEditor()
                {
                    LabelValue = TranslationsStatic.Default.labelBrand
                }
            };

            // Tworzymy logikę, która będzie faktycznie obsługiwać widok
            var logic = new CarBrandPresenter()
                {
                    View = viewAdapter // Przypinamy adapter widoku
                };


            // Wiążemy widok adaptera z logiką właściwą logiką
            viewAdapter.View.Initialize(logic);

            // Tworzymy adapter dla logiki
            var logicAdapter = new AppLogic.Presenters.Popup.EditorPresenterToPopupAdapter
            {
                Presenter = logic,         // Właściwa logika dla kontrolki
                View = viewAdapter.View    // Właściwy widok kontrolki                 
            };


            // Podpinamy obsługę zdarzenia
            logicAdapter.OnReload += new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarBrand);

            // Inicjujemy logikę
            logicAdapter.Initialize();

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(viewAdapter.View, logicAdapter);
            popup.Text = TranslationsStatic.Default.titleAddBrand;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia

            // Usuwamy obsługę zdarzenia
            logicAdapter.OnReload -= new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarBrand);

        }

        private void buttonAddColor_Click(object sender, EventArgs e)
        {
            // Tworzymy adapter widoku na kontrolke edytora marki
            var viewAdapter = new Popups.Adapters.DictionaryEditorViewToPopupAdapter
            {
                // Widok, który będzie faktycznie wyświetlał dane
                View = new FormsView.Popups.WithTextboxEditor()
                {
                    LabelValue = TranslationsStatic.Default.labelColor
                }
            };

            // Tworzymy logikę, która będzie faktycznie obsługiwać widok
            var logic = new ColorPresenter()
            {
                View = viewAdapter // Przypinamy adapter widoku
            };


            // Wiążemy widok adaptera z logiką właściwą logiką
            viewAdapter.View.Initialize(logic);

            // Tworzymy adapter dla logiki
            var logicAdapter = new AppLogic.Presenters.Popup.EditorPresenterToPopupAdapter
            {
                Presenter = logic,         // Właściwa logika dla kontrolki
                View = viewAdapter.View    // Właściwy widok kontrolki                 
            };


            // Podpinamy obsługę zdarzenia
            logicAdapter.OnReload += new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarColor);

            // Inicjujemy logikę
            logicAdapter.Initialize();

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(viewAdapter.View, logicAdapter);
            popup.Text = TranslationsStatic.Default.titleAddColor;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia

            // Usuwamy obsługę zdarzenia
            logicAdapter.OnReload -= new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarColor);

        }

        private void buttonAddFuel_Click(object sender, EventArgs e)
        {
            // Tworzymy adapter widoku na kontrolke edytora marki
            var viewAdapter = new Popups.Adapters.DictionaryEditorViewToPopupAdapter
            {
                // Widok, który będzie faktycznie wyświetlał dane
                View = new FormsView.Popups.WithTextboxEditor()
                {
                    LabelValue = TranslationsStatic.Default.labelFuel
                }
            };

            // Tworzymy logikę, która będzie faktycznie obsługiwać widok
            var logic = new FuelPresenter()
            {
                View = viewAdapter // Przypinamy adapter widoku
            };


            // Wiążemy widok adaptera z logiką właściwą logiką
            viewAdapter.View.Initialize(logic);

            // Tworzymy adapter dla logiki
            var logicAdapter = new AppLogic.Presenters.Popup.EditorPresenterToPopupAdapter
            {
                Presenter = logic,         // Właściwa logika dla kontrolki
                View = viewAdapter.View    // Właściwy widok kontrolki                 
            };


            // Podpinamy obsługę zdarzenia
            logicAdapter.OnReload += new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarFuel);

            // Inicjujemy logikę
            logicAdapter.Initialize();

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(viewAdapter.View, logicAdapter);
            popup.Text = TranslationsStatic.Default.titleAddFuel;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia

            // Usuwamy obsługę zdarzenia
            logicAdapter.OnReload -= new AppLogic.Core.EventCollection.ReloadDropdownListHandler(ReloadCarFuel);
        }
    }
}
