﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using AppLogic.Interfaces;

namespace FormsView.Base
{
    public partial class BaseForm : Form, IBaseView
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Właściwość do ustawiania tytułu formularza
        /// </summary>
        public string Title{

            set
            {
                // Pobranie numeru wersji za pomocą refleksji
                string version = Assembly.GetExecutingAssembly().GetName().Version.ToString(3);

                //Text = String.Format("{0} (wersja {1}) : {2}", Properties.Settings.Default.AppName, version, value);
                Text = String.Format("{0} : {1}", Properties.Settings.Default.AppName, value);
                
            }
        }


        #region Implementacja interfejsu IBaseView

        public new void Show()
        {
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        #endregion

        #region Implementacja interfejsu INotifier

        public void ShowInfo(string content, string caption = null)
        {
            Helpers.Notifier.ShowInfo(content, caption);
        }

        public void ShowWarning(string content, string caption = null)
        {
            Helpers.Notifier.ShowWarning(content, caption);
        }

        public void ShowError(string content, string caption = null)
        {
            Helpers.Notifier.ShowError(content, caption);
        }

        public void ShowException(Exception ex, string caption = null)
        {
            Helpers.Notifier.ShowException(ex, caption);
        }

        public bool ShowQuestion(string question, string caption = null)
        {
            return Helpers.Notifier.ShowQuestion(question, caption);
        }

        #endregion

        /// <summary>
        /// Metoda służy do definiowania ustawień formularza, jest podpięta pod zdarzenie OnLoad.
        /// Nie można zrobić tego w konstruktorze, gdyż ustawione właściwości dotyczyłyby tylko formularza bazowego.
        /// Powyższa uwaga wynika z kolejności wykonywania konstruktorów podczas dziedziczenia.
        /// </summary>
        private void CustomInit(object sender, EventArgs e)
        {
            // Ta konstrukcja doklei nazwę aplikacji do tytułu formularza
            Title = Text;
        }
    }
}
