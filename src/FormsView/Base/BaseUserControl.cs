﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces;

namespace FormsView.Base
{
    public partial class BaseUserControl : UserControl, IBaseView
    {
        public BaseUserControl()
        {
            InitializeComponent();
        }

        #region Implementacja interfejsu IBaseView

        public void Close()
        {
            throw new NotImplementedException();
        }

        public void ShowInfo(string content, string caption = null)
        {
            Helpers.Notifier.ShowInfo(content, caption);
        }

        public void ShowWarning(string content, string caption = null)
        {
            Helpers.Notifier.ShowWarning(content, caption);
        }

        public void ShowError(string content, string caption = null)
        {
            Helpers.Notifier.ShowError(content, caption);
        }

        public void ShowException(Exception ex, string caption = null)
        {
            Helpers.Notifier.ShowException(ex, caption);
        }

        public bool ShowQuestion(string question, string caption = null)
        {
            return Helpers.Notifier.ShowQuestion(question, caption);
        }

        #endregion
    }
}
