﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters.Editor;
using AppLogic.Presenters.DicitionaryEditor;
using AppLogic.Interfaces.Editors;
using FormsView.Helpers;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Popups;

namespace FormsView.Popups
{
    /// <summary>
    /// Klasa kontrolki edytora z polem tekstowym i listą rozwijaną.
    /// </summary>
    public partial class WithDropdownListEditor : Base.BaseUserControl, IPopupEditorView
    {
        public WithDropdownListEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
        }

        #region Fields

        /// <summary cref="HoldData">
        /// </summary>
        bool _holdData = false;

        #endregion

        #region IEditorView

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            saveCancelControls1.Sender = presenter;
        }

        public bool Enable
        {
            set 
            {
                groupBoxData.Enabled = value;
                saveCancelControls1.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return groupBoxData.Enabled;
            }
        }

        #endregion

        #region IPopupEditorView

        public bool HoldData
        {
            get
            {
                return _holdData;
            }
            set
            {
                _holdData = value;
            }
        }

        public string Value
        {
            get
            {
                return textBoxValue.Text;
            }
            set
            {
                if(HoldData == false)
                    textBoxValue.Text = value;
            }
        }

        public int? CategoryId
        {
            get
            {
                try
                {
                    return (comboBoxCategory.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                if (HoldData == false)
                    comboBoxCategory.SelectedIndex = comboBoxCategory.FindIndex(value ?? 0);
            }
        }

        public string ByName
        {
            get
            {
                try
                {
                    return (comboBoxCategory.SelectedItem as Model.Projection.ComboBox).Value;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                if (HoldData == false)
                    comboBoxCategory.SelectedIndex = comboBoxCategory.FindIndex(value);
            }
        }

        public List<Model.Projection.ComboBox> DropdownList
        {
            set
            {
                if (HoldData == false)
                {
                    comboBoxCategory.Items.Clear();
                    comboBoxCategory.Items.AddRange(value.ToArray());
                }
            }
        }

        public string LabelValue
        {
            set 
            {
                if (HoldData == false)
                    labelValue.Text = value;
            }
        }

        public string LabelDropdownList
        {
            set
            {
                if (HoldData == false)
                    labelCategory.Text = value;
            }
        }

        public void Initialize(IPopupPresenter presenter)
        {
            saveCancelControls1.Sender = presenter;
        }

        #endregion
    }
}
