﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters.Editor;
using AppLogic.Presenters.DicitionaryEditor;
using AppLogic.Interfaces.Editors;
using FormsView.Helpers;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Popups;

namespace FormsView.Popups
{
    /// <summary>
    /// Klasa kontrolki edytora z polem tekstowym i listą rozwijaną.
    /// </summary>
    public partial class WithTextboxEditor : Base.BaseUserControl, IPopupEditorView
    {
        public WithTextboxEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
        }

        #region Fields

        /// <summary cref="HoldData">
        /// </summary>
        bool _holdData = false;

        #endregion

        #region IEditorView

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            saveCancelControls1.Sender = presenter;
        }

        public bool Enable
        {
            set 
            {
                groupBoxData.Enabled = value;
                saveCancelControls1.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return groupBoxData.Enabled;
            }
        }

        #endregion

        #region IPopupEditorView

        public bool HoldData
        {
            get
            {
                return _holdData;
            }
            set
            {
                _holdData = value;
            }
        }

        public string Value
        {
            get
            {
                return textBoxValue.Text;
            }
            set
            {
                if(HoldData == false)
                    textBoxValue.Text = value;
            }
        }

        public int? CategoryId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ByName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public List<Model.Projection.ComboBox> DropdownList
        {
            set
            {
                throw new NotImplementedException();
            }
        }

        public string LabelValue
        {
            set 
            {
                labelValue.Text = value;
            }
        }

        public string LabelDropdownList
        {
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(IPopupPresenter presenter)
        {
            saveCancelControls1.Sender = presenter;
        }

        #endregion
    }
}
