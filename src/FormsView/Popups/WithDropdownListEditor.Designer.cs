﻿namespace FormsView.Popups
{
    public partial class WithDropdownListEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.panelControls = new System.Windows.Forms.Panel();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.labelValue = new System.Windows.Forms.Label();
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.comboBoxCategory = new System.Windows.Forms.ComboBox();
            this.labelCategory = new System.Windows.Forms.Label();
            this.panelControls.SuspendLayout();
            this.groupBoxData.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.AutoSize = true;
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 10);
            this.saveCancelControls1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 1;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.saveCancelControls1);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControls.Location = new System.Drawing.Point(0, 153);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(314, 35);
            this.panelControls.TabIndex = 3;
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(116, 39);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(174, 20);
            this.textBoxValue.TabIndex = 1;
            // 
            // labelValue
            // 
            this.labelValue.AutoSize = true;
            this.labelValue.Location = new System.Drawing.Point(22, 42);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(50, 13);
            this.labelValue.TabIndex = 0;
            this.labelValue.Text = "Wartość:";
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.comboBoxCategory);
            this.groupBoxData.Controls.Add(this.labelCategory);
            this.groupBoxData.Controls.Add(this.textBoxValue);
            this.groupBoxData.Controls.Add(this.labelValue);
            this.groupBoxData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxData.Enabled = false;
            this.groupBoxData.Location = new System.Drawing.Point(0, 0);
            this.groupBoxData.Margin = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Size = new System.Drawing.Size(314, 153);
            this.groupBoxData.TabIndex = 4;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = global::FormsView.TranslationsStatic.Default.labelDictionaryEditorTitle;
            // 
            // comboBoxCategory
            // 
            this.comboBoxCategory.DisplayMember = "Value";
            this.comboBoxCategory.FormattingEnabled = true;
            this.comboBoxCategory.Location = new System.Drawing.Point(116, 89);
            this.comboBoxCategory.Name = "comboBoxCategory";
            this.comboBoxCategory.Size = new System.Drawing.Size(174, 21);
            this.comboBoxCategory.TabIndex = 27;
            this.comboBoxCategory.ValueMember = "Value";
            // 
            // labelCategory
            // 
            this.labelCategory.AutoSize = true;
            this.labelCategory.Location = new System.Drawing.Point(22, 92);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(55, 13);
            this.labelCategory.TabIndex = 26;
            this.labelCategory.Text = "Kategoria:";
            // 
            // WithDropdownListEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxData);
            this.Controls.Add(this.panelControls);
            this.MinimumSize = new System.Drawing.Size(314, 0);
            this.Name = "WithDropdownListEditor";
            this.Size = new System.Drawing.Size(314, 188);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SaveCancelControls saveCancelControls1;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.Label labelValue;
        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.ComboBox comboBoxCategory;
        private System.Windows.Forms.Label labelCategory;
    }
}
