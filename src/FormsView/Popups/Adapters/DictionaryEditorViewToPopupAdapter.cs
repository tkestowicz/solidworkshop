﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Editors;
using AppLogic.Interfaces;

namespace FormsView.Popups.Adapters
{
    /// <summary cref="IDictionaryEditorView">
    /// Klasa adoptuje widok edytora słownika do szablonu wsadzonego w wyskakujące okienko.
    /// Stanowi nakładke na szablon ICarModelEditorView żeby logika dodawania modelu mogła odwoływać się do właściwości widoku.
    /// </summary>
    public class DictionaryEditorViewToPopupAdapter : BasePopupViewAdapter, IDictionaryEditorView
    {

        public string Data
        {
            get
            {
                return base.View.Value;
            }
            set
            {
                base.View.Value = value;
            }
        }
    }
}
