﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Editors;
using AppLogic.Interfaces;

namespace FormsView.Popups.Adapters
{
    /// <summary cref="ICityEditorView">
    /// Klasa adoptuje widok dodawania miasta do szablonu wsadzonego w wyskakujące okienko.
    /// Stanowi nakładke na szablon <cref="ICityEditorView"> żeby logika dodawania miasta mogła odwoływać się do właściwości widoku.
    /// </summary>
    public class CityEditorViewToPopupAdapter : BasePopupViewAdapter, ICityEditorView
    {
        #region ICityEditorView

        public int ProvinceId
        {
            get
            {
                return (int)base.View.CategoryId;
            }
            set
            {
                base.View.CategoryId = value;
            }
        }

        public string Province
        {
            get
            {
                return base.View.ByName;
            }
            set
            {
                base.View.ByName = value;
            }
        }

        public string City
        {
            get
            {
                return base.View.Value;
            }
            set
            {
                base.View.Value = value;
            }
        }

        public List<Model.Projection.ComboBox> Provinces
        {
            set 
            {
                base.View.DropdownList = value;
            }
        }

        #endregion
    }
}
