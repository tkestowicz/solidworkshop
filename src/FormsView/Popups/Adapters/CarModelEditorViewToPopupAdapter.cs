﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces.Editors;
using AppLogic.Interfaces;

namespace FormsView.Popups.Adapters
{
    /// <summary cref="ICarModelEditorView">
    /// Klasa adoptuje widok dodawania modelu samochodów do szablonu wsadzonego w wyskakujące okienko.
    /// Stanowi nakładke na szablon <cref="ICarModelEditorView"> żeby logika dodawania modelu mogła odwoływać się do właściwości widoku.
    /// </summary>
    public class CarModelEditorViewToPopupAdapter : BasePopupViewAdapter, ICarModelEditorView
    {

        #region ICarModelEditorView

        public string ModelName
        {
            get
            {
                return View.Value;
            }
            set
            {
                View.Value = value;
            }
        }

        public int? BrandId
        {
            get
            {
                return View.CategoryId;
            }
            set
            {
                View.CategoryId = value;
            }
        }

        public string BrandName
        {
            set 
            {
                View.ByName = value;
            }
        }

        public List<Model.Projection.ComboBox> CarBrands
        {
            set 
            {
                View.DropdownList = value;
            }
        }

        #endregion

    }
}
