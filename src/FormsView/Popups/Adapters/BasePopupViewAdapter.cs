﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Popups;

namespace FormsView.Popups.Adapters
{
    /// <summary>
    /// Klasa zbiera część wspólną adapterów związanych z widokiem dla wyskakujących okienek
    /// </summary>
    public abstract class BasePopupViewAdapter : IEditorView, IPopupViewAdapter
    {
        #region Fields
        
        IPopupEditorView _view;
        IPopupPresenter _presenter;

        #endregion

        #region IEditorView

        public void HideSaveCancelControls()
        {
            throw new NotImplementedException();
        }

        public void ShowSaveCancelControls()
        {
            throw new NotImplementedException();
        }

        public void Initialize(IEditorPresenter presenter)
        {
            throw new NotImplementedException();
        }

        public void Show()
        {
            _view.Show();
        }

        public void Close()
        {
            _view.Close();
        }

        public void ShowInfo(string content, string caption = null)
        {
            _view.ShowInfo(content, caption);
        }

        public void ShowWarning(string content, string caption = null)
        {
            _view.ShowWarning(content, caption);
        }

        public void ShowError(string content, string caption = null)
        {
            _view.ShowError(content, caption);
        }

        public bool ShowQuestion(string question, string caption = null)
        {
            return _view.ShowQuestion(question, caption);
        }

        public void ShowException(Exception ex, string caption = null)
        {
            _view.ShowException(ex, caption);
        }

        public bool Enable
        {
            set
            {
                _view.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _view.IsEnabled;
            }
        }

        #endregion

        #region IPopupViewAdapter

        public IPopupEditorView View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        public IPopupPresenter Presenter
        {
            get
            {
                return _presenter;
            }
            set
            {
                _presenter = value;
            }
        }

        #endregion
    }
}
