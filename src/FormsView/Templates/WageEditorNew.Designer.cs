﻿namespace FormsView.Templates
{
    partial class WageEditorNew
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addEditControls1 = new FormsView.Controls.AddEditControls();
            this.SuspendLayout();
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 545);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(225, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 0;
            // 
            // WageEditorNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.addEditControls1);
            this.Name = "WageEditorNew";
            this.Size = new System.Drawing.Size(314, 570);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.AddEditControls addEditControls1;
    }
}
