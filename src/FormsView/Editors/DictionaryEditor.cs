﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters;
using AppLogic.Interfaces;
using AppLogic.Interfaces.Editors;

namespace FormsView.Editors
{
    /// <summary>
    /// Kontrolka edytora dla tabel słownikowych
    /// </summary>
    public partial class DictionaryEditor: Base.BaseUserControl, IDictionaryEditorView
    {

        /// <summary>
        /// Domyślny kontruktor kontrolki
        /// </summary>
        public DictionaryEditor()
        {
            InitializeComponent();

            // Dla słowników ukrywamy przycisk wyszukiwania
            addEditControls1.SearchButtonVisible = false;
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        IEditorPresenter _presenter;

        #endregion

        #region IDictionaryEditorView

        

        public bool Enable
        {
            set
            {
                saveCancelControls1.Enable = value;
                textBoxRecordData.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return textBoxRecordData.Enabled;
            }
        }

        public string Data
        {
            get
            {
                return textBoxRecordData.Text;
            }
            set
            {
                textBoxRecordData.Text = value;
            }
        }

        public void Initialize(IEditorPresenter presenter)
        {
            presenter.Initialize();

            (presenter as DictionaryEditorPresenter).View = this;
            _presenter = presenter;
            _presenter.Reset();
            saveCancelControls1.Sender = _presenter;
        }

        #endregion

    }
}
