﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;
using FormsView.Helpers;
using AppLogic.Presenters;
using AppLogic.Interfaces.Search;
using AppLogic.Presenters.Popup.Search;
using AppLogic.Interfaces.Events;

namespace FormsView.Editors
{
    public partial class StoreEditor : Base.BaseUserControl, IStoreEditorView, IOpenPopupSearchEvent
    {
        public StoreEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        StorePresenter _presenter;

        /// <summary>
        /// Trzymanie logiki wyszukiwania, żeby zapamiętać aktywne filtry
        /// </summary>
        public SearchLogicContainer<IStoreSearchView> _searchLogic;

        #endregion

        #region Properties

        public string PartName
        {
            get
            {
                return textBoxPartName.Text;
            }
            set
            {
                textBoxPartName.Text = value;
            }
        }

        public int? PartClassId
        {
            get
            {
                try
                {
                    return (comboBoxStorePartClass.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                comboBoxStorePartClass.SelectedIndex = comboBoxStorePartClass.FindIndex(value ?? 0);
            }
        }

        public string PartClassName
        {
            set
            {
                comboBoxStorePartClass.SelectedIndex = comboBoxStorePartClass.FindIndex(value);
            }
        }

        public string PartDescription
        {
            get
            {
                return textBoxPartDescription.Text;
            }
            set
            {
                textBoxPartDescription.Text = value;
            }
        }

        public int AvailableAmount
        {
            get
            {
                return Int32.Parse(textBoxPartAvailableAmount.Text);
            }
            set
            {
                textBoxPartAvailableAmount.Text = value.ToString();
            }
        }

        public double UnitPrice
        {
            get
            {
                return Double.Parse(textBoxPartUnitPrice.Text.Replace(".", ","));
            }
            set
            {
                textBoxPartUnitPrice.Text = value.ToString();
            }
        }

        public bool Enable
        {
            set 
            {
                groupBoxStore.Enabled = value;
                saveCancelControls1.Enable = value;
                addEditControls1.Enable = true;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return groupBoxStore.Enabled;
            }
        }

        public List<Model.Projection.ComboBox> PartClasses
        {
            set
            {
                comboBoxStorePartClass.Items.Clear();
                comboBoxStorePartClass.Items.AddRange(value.ToArray());
            }
        }

        #endregion

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as StorePresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;

            // Podpinamy metodę, która reaguje na przycisk "Wyszukaj"
            addEditControls1.AddHandler = OnOpenPopupSearch;
        }

        /// <summary>
        /// Metoda odpala w dodatkowym okienku formatkę wyszukiwania klienta
        /// </summary>
        /// <param name="sender"></param>
        public void OnOpenPopupSearch(object sender)
        {
            if (_searchLogic == null)
                _searchLogic = new SearchLogicContainer<IStoreSearchView>(new StoreSearchPresenter(new SearchControls.StoreSearchControl()));

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(_searchLogic.View, _searchLogic);
            popup.Text = TranslationsStatic.Default.titleSearchStore;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia
        }

        public void ClearSearchFilters()
        {
            _searchLogic = null;
        }
    }
}
