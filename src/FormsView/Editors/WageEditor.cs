﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;

namespace FormsView.Editors
{
    public partial class WageEditor : Base.BaseUserControl, IWageEditorView
    {
        public WageEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
        }

        #region Fields

        WagePresenter _presenter;

        #endregion

        #region IEditorView Members

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as WagePresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                groupBoxWage.Enabled = value;
                saveCancelControls1.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return groupBoxWage.Enabled;
            }
        }

        #endregion

        #region IWageEditorView Members

        public string Month
        {
            get
            {
                return maskedTextBoxWagePeriod.Text;
            }
            set
            {
                maskedTextBoxWagePeriod.Text = value;
            }
        }

        public double Salary
        {
            get
            {
                return double.Parse(textBoxWageSalary.Text);
            }
            set
            {
                textBoxWageSalary.Text = value.ToString();
            }
        }

        public double Bonus
        {
            get
            {
                return double.Parse(textBoxWageBonus.Text);
            }
            set
            {
                textBoxWageBonus.Text = value.ToString();
            }
        }

        #endregion


    }
}
