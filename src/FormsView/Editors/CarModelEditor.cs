﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters.Editor;
using AppLogic.Presenters.DicitionaryEditor;
using AppLogic.Interfaces.Editors;
using FormsView.Helpers;

namespace FormsView.Editors
{
    public partial class CarModelEditor : Base.BaseUserControl, ICarModelEditorView
    {
        public CarModelEditor()
        {
            InitializeComponent();

            // Dla to jest słownik więc ukrywamy przycisk wyszukiwania
            addEditControls1.SearchButtonVisible = false;
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        CarModelPresenter _presenter;

        #endregion

        #region ICarModelEditorView

        public string ModelName
        {
            get
            {
                return textBoxCarModel.Text;
            }
            set
            {
                textBoxCarModel.Text = value;
            }
        }

        public int? BrandId
        {
            get
            {
                try
                {
                    return (comboBoxCarBrand.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {

                comboBoxCarBrand.SelectedIndex = comboBoxCarBrand.FindIndex(value ?? 0);
            }
        }

        public string BrandName
        {
            set 
            {
                comboBoxCarBrand.SelectedIndex = comboBoxCarBrand.FindIndex(value);
            }
        }

        public List<Model.Projection.ComboBox> CarBrands
        {
            set 
            {
                comboBoxCarBrand.Items.Clear();
                comboBoxCarBrand.Items.AddRange(value.ToArray());
            }
        }

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as CarModelPresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;
        }

        public bool Enable
        {
            set 
            {
                groupBoxData.Enabled = value;
                saveCancelControls1.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return groupBoxData.Enabled;
            }
        }

        #endregion

    }
}
