﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Popups;
using AppLogic.Presenters.Popup;

namespace FormsView.Editors
{
    public partial class PasswordEditor : Base.BaseUserControl, IChangePasswordView
    {
        IPopupPresenter _presenter;

        public PasswordEditor()
        {
            InitializeComponent();
        }

        public string ActualPassword
        {
            get 
            {
                return textBoxPasswordPresent.Text;
            }
        }

        public string NewPassword
        {
            get 
            {
                return textBoxPasswordNew.Text;
            }
        }

        public string RepeatPassword
        {
            get 
            {
                return textBoxPasswordNewRepeat.Text;
            }
        }

        public bool HoldData
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Initialize(IPopupPresenter presenter)
        {
            _presenter = presenter;
            saveCancelControls1.Enable = true;
            saveCancelControls1.Sender = presenter;
        }
    }
}
