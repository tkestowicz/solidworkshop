﻿namespace FormsView.Editors
{
    partial class StoreEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.labelPartAvailableAmount = new System.Windows.Forms.Label();
            this.labelPartUnitPrice = new System.Windows.Forms.Label();
            this.textBoxPartAvailableAmount = new System.Windows.Forms.TextBox();
            this.labelPartDescription = new System.Windows.Forms.Label();
            this.textBoxPartUnitPrice = new System.Windows.Forms.TextBox();
            this.textBoxPartName = new System.Windows.Forms.TextBox();
            this.labelPartName = new System.Windows.Forms.Label();
            this.groupBoxStore = new System.Windows.Forms.GroupBox();
            this.comboBoxStorePartClass = new System.Windows.Forms.ComboBox();
            this.labelStorePartClass = new System.Windows.Forms.Label();
            this.textBoxPartDescription = new System.Windows.Forms.TextBox();
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.groupBoxStore.SuspendLayout();
            this.SuspendLayout();
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 606);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 8;
            // 
            // labelPartAvailableAmount
            // 
            this.labelPartAvailableAmount.AutoSize = true;
            this.labelPartAvailableAmount.Location = new System.Drawing.Point(20, 224);
            this.labelPartAvailableAmount.Name = "labelPartAvailableAmount";
            this.labelPartAvailableAmount.Size = new System.Drawing.Size(80, 13);
            this.labelPartAvailableAmount.TabIndex = 2;
            this.labelPartAvailableAmount.Text = "Dostępna ilość:";
            // 
            // labelPartUnitPrice
            // 
            this.labelPartUnitPrice.AutoSize = true;
            this.labelPartUnitPrice.Location = new System.Drawing.Point(20, 264);
            this.labelPartUnitPrice.Name = "labelPartUnitPrice";
            this.labelPartUnitPrice.Size = new System.Drawing.Size(98, 13);
            this.labelPartUnitPrice.TabIndex = 3;
            this.labelPartUnitPrice.Text = "Cena jednostkowa:";
            // 
            // textBoxPartAvailableAmount
            // 
            this.textBoxPartAvailableAmount.Location = new System.Drawing.Point(120, 224);
            this.textBoxPartAvailableAmount.MaxLength = 6;
            this.textBoxPartAvailableAmount.Name = "textBoxPartAvailableAmount";
            this.textBoxPartAvailableAmount.Size = new System.Drawing.Size(71, 20);
            this.textBoxPartAvailableAmount.TabIndex = 6;
            // 
            // labelPartDescription
            // 
            this.labelPartDescription.AutoSize = true;
            this.labelPartDescription.Location = new System.Drawing.Point(20, 117);
            this.labelPartDescription.Name = "labelPartDescription";
            this.labelPartDescription.Size = new System.Drawing.Size(31, 13);
            this.labelPartDescription.TabIndex = 1;
            this.labelPartDescription.Text = "Opis:";
            // 
            // textBoxPartUnitPrice
            // 
            this.textBoxPartUnitPrice.Location = new System.Drawing.Point(120, 264);
            this.textBoxPartUnitPrice.Name = "textBoxPartUnitPrice";
            this.textBoxPartUnitPrice.Size = new System.Drawing.Size(174, 20);
            this.textBoxPartUnitPrice.TabIndex = 7;
            // 
            // textBoxPartName
            // 
            this.textBoxPartName.Location = new System.Drawing.Point(120, 25);
            this.textBoxPartName.Name = "textBoxPartName";
            this.textBoxPartName.Size = new System.Drawing.Size(174, 20);
            this.textBoxPartName.TabIndex = 4;
            // 
            // labelPartName
            // 
            this.labelPartName.AutoSize = true;
            this.labelPartName.Location = new System.Drawing.Point(20, 25);
            this.labelPartName.Name = "labelPartName";
            this.labelPartName.Size = new System.Drawing.Size(43, 13);
            this.labelPartName.TabIndex = 0;
            this.labelPartName.Text = "Nazwa:";
            // 
            // groupBoxStore
            // 
            this.groupBoxStore.Controls.Add(this.comboBoxStorePartClass);
            this.groupBoxStore.Controls.Add(this.labelStorePartClass);
            this.groupBoxStore.Controls.Add(this.labelPartName);
            this.groupBoxStore.Controls.Add(this.textBoxPartName);
            this.groupBoxStore.Controls.Add(this.textBoxPartUnitPrice);
            this.groupBoxStore.Controls.Add(this.labelPartDescription);
            this.groupBoxStore.Controls.Add(this.textBoxPartAvailableAmount);
            this.groupBoxStore.Controls.Add(this.textBoxPartDescription);
            this.groupBoxStore.Controls.Add(this.labelPartUnitPrice);
            this.groupBoxStore.Controls.Add(this.labelPartAvailableAmount);
            this.groupBoxStore.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelFrom", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.groupBoxStore.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxStore.Enabled = false;
            this.groupBoxStore.Location = new System.Drawing.Point(0, 0);
            this.groupBoxStore.Name = "groupBoxStore";
            this.groupBoxStore.Size = new System.Drawing.Size(314, 305);
            this.groupBoxStore.TabIndex = 9;
            this.groupBoxStore.TabStop = false;
            this.groupBoxStore.Text = global::FormsView.TranslationsStatic.Default.titleParts;
            // 
            // comboBoxStorePartClass
            // 
            this.comboBoxStorePartClass.DisplayMember = "Value";
            this.comboBoxStorePartClass.FormattingEnabled = true;
            this.comboBoxStorePartClass.Location = new System.Drawing.Point(120, 69);
            this.comboBoxStorePartClass.Name = "comboBoxStorePartClass";
            this.comboBoxStorePartClass.Size = new System.Drawing.Size(174, 21);
            this.comboBoxStorePartClass.TabIndex = 25;
            this.comboBoxStorePartClass.ValueMember = "Value";
            // 
            // labelStorePartClass
            // 
            this.labelStorePartClass.AutoSize = true;
            this.labelStorePartClass.Location = new System.Drawing.Point(20, 69);
            this.labelStorePartClass.Name = "labelStorePartClass";
            this.labelStorePartClass.Size = new System.Drawing.Size(88, 13);
            this.labelStorePartClass.TabIndex = 8;
            this.labelStorePartClass.Text = "Kategoria części:";
            // 
            // textBoxPartDescription
            // 
            this.textBoxPartDescription.Location = new System.Drawing.Point(120, 117);
            this.textBoxPartDescription.Multiline = true;
            this.textBoxPartDescription.Name = "textBoxPartDescription";
            this.textBoxPartDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxPartDescription.Size = new System.Drawing.Size(174, 88);
            this.textBoxPartDescription.TabIndex = 5;
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 581);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 10;
            // 
            // StoreEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.groupBoxStore);
            this.Controls.Add(this.addEditControls1);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "StoreEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.groupBoxStore.ResumeLayout(false);
            this.groupBoxStore.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.AddEditSearchControls addEditControls1;
        private System.Windows.Forms.Label labelPartAvailableAmount;
        private System.Windows.Forms.Label labelPartUnitPrice;
        private System.Windows.Forms.TextBox textBoxPartAvailableAmount;
        private System.Windows.Forms.Label labelPartDescription;
        private System.Windows.Forms.TextBox textBoxPartUnitPrice;
        private System.Windows.Forms.TextBox textBoxPartName;
        private System.Windows.Forms.Label labelPartName;
        private System.Windows.Forms.GroupBox groupBoxStore;
        private System.Windows.Forms.TextBox textBoxPartDescription;
        private Controls.SaveCancelControls saveCancelControls1;
        private System.Windows.Forms.Label labelStorePartClass;
        private System.Windows.Forms.ComboBox comboBoxStorePartClass;

    }
}
