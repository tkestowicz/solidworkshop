﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;
using AppLogic.Interfaces.Controls;
using FormsView.Helpers;
using AppLogic.Interfaces;

namespace FormsView.Editors
{
    public partial class GroupEditor : Base.BaseUserControl, IGroupEditorView
    {
        public GroupEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        GroupPresenter _presenter;

        #endregion

        #region IGroupEditorView Members

        string IGroupEditorView.Name
        {
            get
            {
                return textBoxGroupName.Text;
            }
            set
            {
                textBoxGroupName.Text = value;
            }
        }

        string IGroupEditorView.Description
        {
            get
            {
                return textBoxGroupDescription.Text;
            }
            set
            {
                textBoxGroupDescription.Text = value;
            }
        }

        IList<Model.Projection.Permission> IGroupEditorView.Permissions 
        {
            get
            {
                return dataGridView1.DataSource as IList<Model.Projection.Permission>;
            }
            set
            {
                dataGridView1.DataSource = value;
            }
        }

        #endregion

        #region IEditorView Members

        public void Initialize(IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as GroupPresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                dataGridView1.ReadOnly = !value;
                groupBoxGroup.Enabled = value;
                saveCancelControls1.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return groupBoxGroup.Enabled;
            }
        }

        #endregion

        #region IGroupEditorView Members


        #endregion

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var indexPrefix = "Permission";

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.HeaderText.Contains("Id"))
                    column.HeaderText = TranslationsGridHeaders.Default.Id;
                else
                {
                    try
                    {
                        column.HeaderText = TranslationsGridHeaders.Default[indexPrefix + column.HeaderText].ToString();
                    }
                    catch (Exception) { }
                }
            }
        }

    }
}
