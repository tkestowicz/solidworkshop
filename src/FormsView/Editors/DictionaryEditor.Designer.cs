﻿namespace FormsView.Editors
{
    partial class DictionaryEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.panelControls = new System.Windows.Forms.Panel();
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.textBoxRecordData = new System.Windows.Forms.TextBox();
            this.labelRecordDataLabel = new System.Windows.Forms.Label();
            this.panelControls.SuspendLayout();
            this.groupBoxData.SuspendLayout();
            this.SuspendLayout();
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 35);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 0;
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.AutoSize = true;
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 10);
            this.saveCancelControls1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 1;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.saveCancelControls1);
            this.panelControls.Controls.Add(this.addEditControls1);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControls.Location = new System.Drawing.Point(0, 571);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(314, 60);
            this.panelControls.TabIndex = 2;
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.textBoxRecordData);
            this.groupBoxData.Controls.Add(this.labelRecordDataLabel);
            this.groupBoxData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxData.Location = new System.Drawing.Point(0, 0);
            this.groupBoxData.Margin = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Size = new System.Drawing.Size(314, 571);
            this.groupBoxData.TabIndex = 3;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = global::FormsView.TranslationsStatic.Default.labelDictionaryEditorTitle;
            // 
            // textBoxRecordData
            // 
            this.textBoxRecordData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxRecordData.Enabled = false;
            this.textBoxRecordData.Location = new System.Drawing.Point(29, 58);
            this.textBoxRecordData.Name = "textBoxRecordData";
            this.textBoxRecordData.Size = new System.Drawing.Size(209, 20);
            this.textBoxRecordData.TabIndex = 1;
            // 
            // labelRecordDataLabel
            // 
            this.labelRecordDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRecordDataLabel.AutoSize = true;
            this.labelRecordDataLabel.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FormsView.TranslationsStatic.Default, "labelDictionaryEditorLabel", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labelRecordDataLabel.Location = new System.Drawing.Point(11, 31);
            this.labelRecordDataLabel.Name = "labelRecordDataLabel";
            this.labelRecordDataLabel.Size = new System.Drawing.Size(50, 13);
            this.labelRecordDataLabel.TabIndex = 0;
            this.labelRecordDataLabel.Text = global::FormsView.TranslationsStatic.Default.labelDictionaryEditorLabel;
            // 
            // DictionaryEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxData);
            this.Controls.Add(this.panelControls);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "DictionaryEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.AddEditSearchControls addEditControls1;
        private Controls.SaveCancelControls saveCancelControls1;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.TextBox textBoxRecordData;
        private System.Windows.Forms.Label labelRecordDataLabel;
    }
}
