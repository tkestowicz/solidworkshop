﻿namespace FormsView.Editors
{
    partial class WageEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxWage = new System.Windows.Forms.GroupBox();
            this.maskedTextBoxWagePeriod = new System.Windows.Forms.MaskedTextBox();
            this.labelWagePeriod = new System.Windows.Forms.Label();
            this.textBoxWageBonus = new System.Windows.Forms.TextBox();
            this.textBoxWageSalary = new System.Windows.Forms.TextBox();
            this.labelWageBonus = new System.Windows.Forms.Label();
            this.labelWageSalary = new System.Windows.Forms.Label();
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.groupBoxWage.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxWage
            // 
            this.groupBoxWage.Controls.Add(this.maskedTextBoxWagePeriod);
            this.groupBoxWage.Controls.Add(this.labelWagePeriod);
            this.groupBoxWage.Controls.Add(this.textBoxWageBonus);
            this.groupBoxWage.Controls.Add(this.textBoxWageSalary);
            this.groupBoxWage.Controls.Add(this.labelWageBonus);
            this.groupBoxWage.Controls.Add(this.labelWageSalary);
            this.groupBoxWage.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxWage.Enabled = false;
            this.groupBoxWage.Location = new System.Drawing.Point(0, 0);
            this.groupBoxWage.Name = "groupBoxWage";
            this.groupBoxWage.Size = new System.Drawing.Size(314, 145);
            this.groupBoxWage.TabIndex = 12;
            this.groupBoxWage.TabStop = false;
            this.groupBoxWage.Text = global::FormsView.TranslationsStatic.Default.titleWage;
            // 
            // maskedTextBoxWagePeriod
            // 
            this.maskedTextBoxWagePeriod.Location = new System.Drawing.Point(93, 21);
            this.maskedTextBoxWagePeriod.Mask = "00/0000";
            this.maskedTextBoxWagePeriod.Name = "maskedTextBoxWagePeriod";
            this.maskedTextBoxWagePeriod.Size = new System.Drawing.Size(54, 20);
            this.maskedTextBoxWagePeriod.TabIndex = 8;
            // 
            // labelWagePeriod
            // 
            this.labelWagePeriod.AutoSize = true;
            this.labelWagePeriod.Location = new System.Drawing.Point(20, 25);
            this.labelWagePeriod.Name = "labelWagePeriod";
            this.labelWagePeriod.Size = new System.Drawing.Size(38, 13);
            this.labelWagePeriod.TabIndex = 0;
            this.labelWagePeriod.Text = "Okres:";
            // 
            // textBoxWageBonus
            // 
            this.textBoxWageBonus.Location = new System.Drawing.Point(93, 100);
            this.textBoxWageBonus.Name = "textBoxWageBonus";
            this.textBoxWageBonus.Size = new System.Drawing.Size(150, 20);
            this.textBoxWageBonus.TabIndex = 7;
            // 
            // textBoxWageSalary
            // 
            this.textBoxWageSalary.Location = new System.Drawing.Point(93, 60);
            this.textBoxWageSalary.Name = "textBoxWageSalary";
            this.textBoxWageSalary.Size = new System.Drawing.Size(150, 20);
            this.textBoxWageSalary.TabIndex = 6;
            // 
            // labelWageBonus
            // 
            this.labelWageBonus.AutoSize = true;
            this.labelWageBonus.Location = new System.Drawing.Point(20, 100);
            this.labelWageBonus.Name = "labelWageBonus";
            this.labelWageBonus.Size = new System.Drawing.Size(42, 13);
            this.labelWageBonus.TabIndex = 3;
            this.labelWageBonus.Text = "Premia:";
            // 
            // labelWageSalary
            // 
            this.labelWageSalary.AutoSize = true;
            this.labelWageSalary.Location = new System.Drawing.Point(20, 60);
            this.labelWageSalary.Name = "labelWageSalary";
            this.labelWageSalary.Size = new System.Drawing.Size(42, 13);
            this.labelWageSalary.TabIndex = 2;
            this.labelWageSalary.Text = "Pensja:";
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 175);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 13;
            // 
            // WageEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.groupBoxWage);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 200);
            this.Name = "WageEditor";
            this.Size = new System.Drawing.Size(314, 200);
            this.groupBoxWage.ResumeLayout(false);
            this.groupBoxWage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelWagePeriod;
        private Controls.SaveCancelControls saveCancelControls1;
        private System.Windows.Forms.Label labelWageBonus;
        private System.Windows.Forms.GroupBox groupBoxWage;
        private System.Windows.Forms.TextBox textBoxWageBonus;
        private System.Windows.Forms.TextBox textBoxWageSalary;
        private System.Windows.Forms.Label labelWageSalary;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxWagePeriod;
    }
}
