﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;
using AppLogic.Interfaces.Controls;
using FormsView.Helpers;
using AppLogic.Interfaces;
using AppLogic.Presenters;
using AppLogic.Interfaces.Search;
using AppLogic.Presenters.Popup.Search;
using AppLogic.Interfaces.Events;

namespace FormsView.Editors
{
    public partial class EmployeeEditor : Base.BaseUserControl, IEmployeeEditorView, IOpenPopupSearchEvent
    {
        public EmployeeEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        EmployeePresenter _presenter;

        /// <summary>
        /// Trzymanie logiki wyszukiwania, żeby zapamiętać aktywne filtry
        /// </summary>
        public SearchLogicContainer<IEmployeeSearchView> _searchLogic;

        #endregion

        #region IEditorView Members

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as EmployeePresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;

            // Podpinamy metodę, która reaguje na przycisk "Wyszukaj"
            addEditControls1.AddHandler = OnOpenPopupSearch;
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                panelEmployeeData.Enabled = value;
                addressEditorEmployee.Enabled = value;
                saveCancelControls1.Enable = value;
                panelSytemUserData.Enabled = value;
            }
        }

        public bool IsEnabled
        {
            get 
            {
                return panelEmployeeData.Enabled || addressEditorEmployee.Enabled || panelSytemUserData.Enabled;
            }
        }

        #endregion

        #region IEmployeeEditorView Members

        public string EmployeeName
        {
            get
            {
                return textBoxEmployeeFirstName.Text;
            }
            set
            {
                textBoxEmployeeFirstName.Text = value;
            }
        }

        public string EmployeeSurname
        {
            get
            {
                return textBoxEmployeeSurname.Text;
            }
            set
            {
                textBoxEmployeeSurname.Text = value;
            }
        }

        public List<Model.Projection.ComboBox> EmployeeGroups
        {
            set
            {
                comboBoxEmployeeGroup.Items.Clear();
                comboBoxEmployeeGroup.Items.AddRange(value.ToArray());
            }
        }

        public int EmployeeGroupId
        {
            get
            {
                try
                {
                    return (comboBoxEmployeeGroup.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                comboBoxEmployeeGroup.Text = null;
                comboBoxEmployeeGroup.SelectedIndex = comboBoxEmployeeGroup.FindIndex(value);
            }
        }

        public string EmployeePesel
        {
            get
            {
                return maskedTextBoxEmployeePesel.Text;
            }
            set
            {
                maskedTextBoxEmployeePesel.Text = value;
            }
        }

        public string EmployeeNIP
        {
            get
            {
                return maskedTextBoxEmployeeNIP.Text;
            }
            set
            {
                maskedTextBoxEmployeeNIP.Text = value;
            }
        }

        public string EmployeePhoneNumber
        {
            get
            {
                return maskedTextBoxEmployeePhone.Text;
            }
            set
            {
                maskedTextBoxEmployeePhone.Text = value;
            }
        }

        public string EmployeeMail
        {
            get
            {
                return textBoxEmployeeMail.Text;
            }
            set
            {
                textBoxEmployeeMail.Text = value;
            }
        }

        public List<Model.Projection.ComboBox> EmployeeStates
        {
            set
            {
                comboBoxEmploymentState.Items.Clear();
                comboBoxEmploymentState.Items.AddRange(value.ToArray());
            }
        }

        public int EmployeeStateId
        {
            get
            {
                return 0;
            }
            set
            {
                
            }
        }

        public Model.Projection.EmployeeState EmployeeState
        {
            get
            {
                try
                {
                    Model.Projection.EmployeeState state = new Model.Projection.EmployeeState();
                    state.Id = (comboBoxEmploymentState.SelectedItem as Model.Projection.ComboBox).Id;
                    state.Name = (comboBoxEmploymentState.SelectedItem as Model.Projection.ComboBox).Value;
                    return state;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                comboBoxEmploymentState.SelectedIndex = comboBoxEmploymentState.FindIndex((int)value.Id);
            }
        }

        public string EmployeeQualifications
        {
            get
            {
                return textBoxEmployeeQualifications.Text;
            }
            set
            {
                textBoxEmployeeQualifications.Text = value;
            }
        }

        public DateTime? EmployeeHireDate
        {
            get
            {
                return dateTimePickerHireDate.Value;
            }
            set
            {
                if (value != null)
                    dateTimePickerHireDate.Value = (DateTime)value;
                else
                    dateTimePickerHireDate.Value = DateTime.Now;
            }
        }

        public DateTime? EmployeeReleaseDate
        {
            get
            {
                if (dateTimePickerReleaseDate.Enabled == true)
                    return dateTimePickerReleaseDate.Value;
                else
                    return null;
                
            }
            set
            {
                if (value != null)
                {
                    dateTimePickerReleaseDate.Value = (DateTime)value;
                    checkBoxReleaseDate.Checked = true; // Jeżeli mamy datę zakończenia umowy musimy aktywować kontrolke
                }
                else
                    checkBoxReleaseDate.Checked = false;
            }
        }

        public IAddress EmployeeAddress
        {
            get 
            {
                return addressEditorEmployee as IAddress;
            }
        }

        public string EmployeeLogin
        {
            get
            {
                return textBoxLogin.Text;
            }
            set
            {
                textBoxLogin.Text = value;
            }
        }

        public string EmployeePassword
        {
            get
            {
                return textBoxPassword.Text;
            }
            set
            {
                textBoxPassword.Text = value;
            }
        }

        public string EmployeeRepeatPassword
        {
            get
            {
                return textBoxPasswordNewRepeat.Text;
            }
            set
            {
                textBoxPasswordNewRepeat.Text = value;
            }
        }

        #endregion

        private void CheckItem(object sender, EventArgs e)
        {
            // Kontrolka daty zwolnienia aktywna tylko wtedy, gdy status zatrudnienia == zwolniony || emerytowany
            if (checkBoxReleaseDate.Checked)
                dateTimePickerReleaseDate.Enabled = true;
            else
                dateTimePickerReleaseDate.Enabled = false;
        }

        private void buttonGeneratePassword_Click(object sender, EventArgs e)
        {
            _presenter.GeneratePassword();
        }

        private void PrepareLogin(object sender, TabControlEventArgs e)
        {
            _presenter.GenerateLogin();
            // Generujemy propozycję loginu

        }

        /// <summary>
        /// Metoda odpala w dodatkowym okienku formatkę wyszukiwania klienta
        /// </summary>
        /// <param name="sender"></param>
        public void OnOpenPopupSearch(object sender)
        {
            if (_searchLogic == null)
                _searchLogic = new SearchLogicContainer<IEmployeeSearchView>(new EmployeeSearchPresenter(new SearchControls.EmployeeSearchControl()));

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(_searchLogic.View, _searchLogic);
            popup.Text = TranslationsStatic.Default.titleSearchEmployee;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia
        }

        public void ClearSearchFilters()
        {
            _searchLogic = null;
        }
    }
}
