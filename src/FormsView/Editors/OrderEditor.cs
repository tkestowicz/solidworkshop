﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;
using AppLogic.Core;

namespace FormsView.Editors
{
    public partial class OrderEditor : Base.BaseUserControl, IOrderEditorView
    {
        public OrderEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        OrderPresenter _presenter;

        #endregion


        #region Properties

        public string OrderStatus
        {
            get
            {
                return comboBoxOrderState.Text;
            }
            set
            {
                comboBoxOrderState.Text = value;
            }
        }

        public List<string> OrderStatuses
        {
            get
            {
                return comboBoxOrderState.Items.Cast<string>().ToList();
            }
            set
            {
                comboBoxOrderState.Items.Clear();
                comboBoxOrderState.Items.AddRange(value.ToArray<string>());
            }
        }

        int carId_;
        public int CarId
        {
            get
            {
                return carId_;
            }
            set
            {
                carId_ = value;
            }
        }

        public string Car
        {
            get
            {
                return textBoxOrderCar.Text;
            }
            set
            {
                textBoxOrderCar.Text = value;
            }
        }

        public string OrderSupervisor
        {
            get
            {
                return comboBoxOrderSupervisor.Text;
            }
            set
            {
                comboBoxOrderSupervisor.Text = value;
            }
        }

        public List<string> OrderSupervisores
        {
            get
            {
                return comboBoxOrderSupervisor.Items.Cast<string>().ToList();
            }
            set
            {
                comboBoxOrderSupervisor.Items.Clear();
                comboBoxOrderSupervisor.Items.AddRange(value.ToArray<string>());
            }
        }

        public string OrderDescription
        {
            get
            {
                return textBoxOrderDescription.Text;
            }
            set
            {
                textBoxOrderDescription.Text = value;
            }
        }

        public double? OrderPrice
        {
            get
            {
                return Double.Parse(textBoxOrderPrice.Text.Replace(".", ","));
            }
            set
            {
                textBoxOrderPrice.Text = value.ToString();
            }
        }

        #endregion

        #region IEditorView Members

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as OrderPresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                groupBoxOrder.Enabled = value;
                saveCancelControls1.Enable = value;
                addEditControls1.Enable = true;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return groupBoxOrder.Enabled;
            }
        }

        #endregion

        private void buttonSelectCar_Click(object sender, EventArgs e)
        {
            Helpers.CarChooseForm form = new Helpers.CarChooseForm();
            carId_ = form.Run(this);

            var db = AppLogic.Core.ServiceManager.Database;

            if (carId_ == 0)
                textBoxOrderCar.Text = "";
            else
                textBoxOrderCar.Text = (from c in db.Cars where c.CarId == carId_ select c).FirstOrDefault().CarModels.CarBrand.BrandName + " " +
                                        (from c in db.Cars where c.CarId == carId_ select c).FirstOrDefault().CarModels.ModelName + ", " +
                                        (from c in db.Cars where c.CarId == carId_ select c).FirstOrDefault().RegistrationNumber;
        }

        #region IOrderEditorView Members


        #endregion
    }
}
