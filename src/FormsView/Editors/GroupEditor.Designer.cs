﻿namespace FormsView.Editors
{
    partial class GroupEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.tabControlGroup = new System.Windows.Forms.TabControl();
            this.tabPageGroup = new System.Windows.Forms.TabPage();
            this.groupBoxGroup = new System.Windows.Forms.GroupBox();
            this.labelGroupName = new System.Windows.Forms.Label();
            this.textBoxGroupName = new System.Windows.Forms.TextBox();
            this.labelGroupDescription = new System.Windows.Forms.Label();
            this.textBoxGroupDescription = new System.Windows.Forms.TextBox();
            this.tabPagePermitions = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControlGroup.SuspendLayout();
            this.tabPageGroup.SuspendLayout();
            this.groupBoxGroup.SuspendLayout();
            this.tabPagePermitions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 581);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 13;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 606);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 11;
            // 
            // tabControlGroup
            // 
            this.tabControlGroup.Controls.Add(this.tabPageGroup);
            this.tabControlGroup.Controls.Add(this.tabPagePermitions);
            this.tabControlGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlGroup.Location = new System.Drawing.Point(0, 0);
            this.tabControlGroup.Name = "tabControlGroup";
            this.tabControlGroup.SelectedIndex = 0;
            this.tabControlGroup.Size = new System.Drawing.Size(314, 507);
            this.tabControlGroup.TabIndex = 15;
            // 
            // tabPageGroup
            // 
            this.tabPageGroup.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageGroup.Controls.Add(this.groupBoxGroup);
            this.tabPageGroup.Location = new System.Drawing.Point(4, 22);
            this.tabPageGroup.Name = "tabPageGroup";
            this.tabPageGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGroup.Size = new System.Drawing.Size(306, 481);
            this.tabPageGroup.TabIndex = 0;
            this.tabPageGroup.Text = "Grupa";
            // 
            // groupBoxGroup
            // 
            this.groupBoxGroup.Controls.Add(this.labelGroupName);
            this.groupBoxGroup.Controls.Add(this.textBoxGroupName);
            this.groupBoxGroup.Controls.Add(this.labelGroupDescription);
            this.groupBoxGroup.Controls.Add(this.textBoxGroupDescription);
            this.groupBoxGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxGroup.Enabled = false;
            this.groupBoxGroup.Location = new System.Drawing.Point(3, 3);
            this.groupBoxGroup.Name = "groupBoxGroup";
            this.groupBoxGroup.Size = new System.Drawing.Size(300, 475);
            this.groupBoxGroup.TabIndex = 13;
            this.groupBoxGroup.TabStop = false;
            this.groupBoxGroup.Text = global::FormsView.TranslationsStatic.Default.titleGroup;
            // 
            // labelGroupName
            // 
            this.labelGroupName.AutoSize = true;
            this.labelGroupName.Location = new System.Drawing.Point(20, 25);
            this.labelGroupName.Name = "labelGroupName";
            this.labelGroupName.Size = new System.Drawing.Size(43, 13);
            this.labelGroupName.TabIndex = 0;
            this.labelGroupName.Text = "Nazwa:";
            // 
            // textBoxGroupName
            // 
            this.textBoxGroupName.Location = new System.Drawing.Point(82, 25);
            this.textBoxGroupName.Name = "textBoxGroupName";
            this.textBoxGroupName.Size = new System.Drawing.Size(193, 20);
            this.textBoxGroupName.TabIndex = 4;
            // 
            // labelGroupDescription
            // 
            this.labelGroupDescription.AutoSize = true;
            this.labelGroupDescription.Location = new System.Drawing.Point(20, 62);
            this.labelGroupDescription.Name = "labelGroupDescription";
            this.labelGroupDescription.Size = new System.Drawing.Size(31, 13);
            this.labelGroupDescription.TabIndex = 1;
            this.labelGroupDescription.Text = "Opis:";
            // 
            // textBoxGroupDescription
            // 
            this.textBoxGroupDescription.Location = new System.Drawing.Point(82, 62);
            this.textBoxGroupDescription.Multiline = true;
            this.textBoxGroupDescription.Name = "textBoxGroupDescription";
            this.textBoxGroupDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxGroupDescription.Size = new System.Drawing.Size(193, 153);
            this.textBoxGroupDescription.TabIndex = 5;
            // 
            // tabPagePermitions
            // 
            this.tabPagePermitions.BackColor = System.Drawing.SystemColors.Control;
            this.tabPagePermitions.Controls.Add(this.dataGridView1);
            this.tabPagePermitions.Location = new System.Drawing.Point(4, 22);
            this.tabPagePermitions.Name = "tabPagePermitions";
            this.tabPagePermitions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePermitions.Size = new System.Drawing.Size(306, 481);
            this.tabPagePermitions.TabIndex = 1;
            this.tabPagePermitions.Text = "Uprawnienia";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(300, 475);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            // 
            // GroupEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlGroup);
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.addEditControls1);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "GroupEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.tabControlGroup.ResumeLayout(false);
            this.tabPageGroup.ResumeLayout(false);
            this.groupBoxGroup.ResumeLayout(false);
            this.groupBoxGroup.PerformLayout();
            this.tabPagePermitions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
        private System.Windows.Forms.TabControl tabControlGroup;
        private System.Windows.Forms.TabPage tabPageGroup;
        private System.Windows.Forms.GroupBox groupBoxGroup;
        private System.Windows.Forms.Label labelGroupName;
        private System.Windows.Forms.TextBox textBoxGroupName;
        private System.Windows.Forms.Label labelGroupDescription;
        private System.Windows.Forms.TextBox textBoxGroupDescription;
        private System.Windows.Forms.TabPage tabPagePermitions;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}
