﻿namespace FormsView.Editors
{
    partial class ServiceEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.textBoxServiceName = new System.Windows.Forms.TextBox();
            this.labelServiceName = new System.Windows.Forms.Label();
            this.labelServiceDescription = new System.Windows.Forms.Label();
            this.textBoxServicePrice = new System.Windows.Forms.TextBox();
            this.textBoxServiceDescription = new System.Windows.Forms.TextBox();
            this.labelServicePrice = new System.Windows.Forms.Label();
            this.comboBoxClasses = new System.Windows.Forms.ComboBox();
            this.labelClass = new System.Windows.Forms.Label();
            this.panelControls = new System.Windows.Forms.Panel();
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.groupBoxData.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.textBoxServiceName);
            this.groupBoxData.Controls.Add(this.labelServiceName);
            this.groupBoxData.Controls.Add(this.labelServiceDescription);
            this.groupBoxData.Controls.Add(this.textBoxServicePrice);
            this.groupBoxData.Controls.Add(this.textBoxServiceDescription);
            this.groupBoxData.Controls.Add(this.labelServicePrice);
            this.groupBoxData.Controls.Add(this.comboBoxClasses);
            this.groupBoxData.Controls.Add(this.labelClass);
            this.groupBoxData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxData.Enabled = false;
            this.groupBoxData.Location = new System.Drawing.Point(0, 0);
            this.groupBoxData.Margin = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Size = new System.Drawing.Size(314, 571);
            this.groupBoxData.TabIndex = 6;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = global::FormsView.TranslationsStatic.Default.labelDictionaryEditorTitle;
            // 
            // textBoxServiceName
            // 
            this.textBoxServiceName.Location = new System.Drawing.Point(88, 45);
            this.textBoxServiceName.Name = "textBoxServiceName";
            this.textBoxServiceName.Size = new System.Drawing.Size(198, 20);
            this.textBoxServiceName.TabIndex = 44;
            // 
            // labelServiceName
            // 
            this.labelServiceName.AutoSize = true;
            this.labelServiceName.Location = new System.Drawing.Point(21, 45);
            this.labelServiceName.Name = "labelServiceName";
            this.labelServiceName.Size = new System.Drawing.Size(43, 13);
            this.labelServiceName.TabIndex = 43;
            this.labelServiceName.Text = "Nazwa:";
            // 
            // labelServiceDescription
            // 
            this.labelServiceDescription.AutoSize = true;
            this.labelServiceDescription.Location = new System.Drawing.Point(21, 84);
            this.labelServiceDescription.Name = "labelServiceDescription";
            this.labelServiceDescription.Size = new System.Drawing.Size(31, 13);
            this.labelServiceDescription.TabIndex = 39;
            this.labelServiceDescription.Text = "Opis:";
            // 
            // textBoxServicePrice
            // 
            this.textBoxServicePrice.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxServicePrice.Location = new System.Drawing.Point(88, 191);
            this.textBoxServicePrice.Name = "textBoxServicePrice";
            this.textBoxServicePrice.Size = new System.Drawing.Size(198, 20);
            this.textBoxServicePrice.TabIndex = 42;
            // 
            // textBoxServiceDescription
            // 
            this.textBoxServiceDescription.Location = new System.Drawing.Point(88, 84);
            this.textBoxServiceDescription.Multiline = true;
            this.textBoxServiceDescription.Name = "textBoxServiceDescription";
            this.textBoxServiceDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxServiceDescription.Size = new System.Drawing.Size(198, 88);
            this.textBoxServiceDescription.TabIndex = 41;
            // 
            // labelServicePrice
            // 
            this.labelServicePrice.AutoSize = true;
            this.labelServicePrice.Location = new System.Drawing.Point(21, 191);
            this.labelServicePrice.Name = "labelServicePrice";
            this.labelServicePrice.Size = new System.Drawing.Size(35, 13);
            this.labelServicePrice.TabIndex = 40;
            this.labelServicePrice.Text = "Cena:";
            // 
            // comboBoxClasses
            // 
            this.comboBoxClasses.DisplayMember = "Value";
            this.comboBoxClasses.FormattingEnabled = true;
            this.comboBoxClasses.Location = new System.Drawing.Point(88, 229);
            this.comboBoxClasses.Name = "comboBoxClasses";
            this.comboBoxClasses.Size = new System.Drawing.Size(198, 21);
            this.comboBoxClasses.TabIndex = 27;
            this.comboBoxClasses.ValueMember = "Value";
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Location = new System.Drawing.Point(21, 232);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(28, 13);
            this.labelClass.TabIndex = 26;
            this.labelClass.Text = global::FormsView.TranslationsStatic.Default.labelBrand;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.saveCancelControls1);
            this.panelControls.Controls.Add(this.addEditControls1);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControls.Location = new System.Drawing.Point(0, 571);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(314, 60);
            this.panelControls.TabIndex = 5;
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.AutoSize = true;
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 10);
            this.saveCancelControls1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 3;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 35);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 2;
            // 
            // ServiceEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxData);
            this.Controls.Add(this.panelControls);
            this.MaximumSize = new System.Drawing.Size(314, 631);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "ServiceEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxData.PerformLayout();
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.ComboBox comboBoxClasses;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.TextBox textBoxServiceName;
        private System.Windows.Forms.Label labelServiceName;
        private System.Windows.Forms.Label labelServiceDescription;
        private System.Windows.Forms.TextBox textBoxServicePrice;
        private System.Windows.Forms.TextBox textBoxServiceDescription;
        private System.Windows.Forms.Label labelServicePrice;
        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
    }
}
