﻿namespace FormsView.Editors
{
    partial class CarModelEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.panelControls = new System.Windows.Forms.Panel();
            this.textBoxCarModel = new System.Windows.Forms.TextBox();
            this.labelCarModel = new System.Windows.Forms.Label();
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.comboBoxCarBrand = new System.Windows.Forms.ComboBox();
            this.labelCarBrand = new System.Windows.Forms.Label();
            this.panelControls.SuspendLayout();
            this.groupBoxData.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.AutoSize = true;
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 10);
            this.saveCancelControls1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 1;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 35);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 0;
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.saveCancelControls1);
            this.panelControls.Controls.Add(this.addEditControls1);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControls.Location = new System.Drawing.Point(0, 571);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(314, 60);
            this.panelControls.TabIndex = 3;
            // 
            // textBoxCarModel
            // 
            this.textBoxCarModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxCarModel.Location = new System.Drawing.Point(116, 39);
            this.textBoxCarModel.Name = "textBoxCarModel";
            this.textBoxCarModel.Size = new System.Drawing.Size(174, 20);
            this.textBoxCarModel.TabIndex = 1;
            // 
            // labelCarModel
            // 
            this.labelCarModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCarModel.AutoSize = true;
            this.labelCarModel.Location = new System.Drawing.Point(22, 42);
            this.labelCarModel.Name = "labelCarModel";
            this.labelCarModel.Size = new System.Drawing.Size(39, 13);
            this.labelCarModel.TabIndex = 0;
            this.labelCarModel.Text = global::FormsView.TranslationsStatic.Default.labelModel;
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.comboBoxCarBrand);
            this.groupBoxData.Controls.Add(this.labelCarBrand);
            this.groupBoxData.Controls.Add(this.textBoxCarModel);
            this.groupBoxData.Controls.Add(this.labelCarModel);
            this.groupBoxData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxData.Enabled = false;
            this.groupBoxData.Location = new System.Drawing.Point(0, 0);
            this.groupBoxData.Margin = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxData.Size = new System.Drawing.Size(314, 571);
            this.groupBoxData.TabIndex = 4;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = global::FormsView.TranslationsStatic.Default.labelDictionaryEditorTitle;
            // 
            // comboBoxCarBrand
            // 
            this.comboBoxCarBrand.DisplayMember = "Value";
            this.comboBoxCarBrand.FormattingEnabled = true;
            this.comboBoxCarBrand.Location = new System.Drawing.Point(116, 89);
            this.comboBoxCarBrand.Name = "comboBoxCarBrand";
            this.comboBoxCarBrand.Size = new System.Drawing.Size(174, 21);
            this.comboBoxCarBrand.TabIndex = 27;
            this.comboBoxCarBrand.ValueMember = "Value";
            // 
            // labelCarBrand
            // 
            this.labelCarBrand.AutoSize = true;
            this.labelCarBrand.Location = new System.Drawing.Point(22, 92);
            this.labelCarBrand.Name = "labelCarBrand";
            this.labelCarBrand.Size = new System.Drawing.Size(28, 13);
            this.labelCarBrand.TabIndex = 26;
            this.labelCarBrand.Text = global::FormsView.TranslationsStatic.Default.labelBrand;
            // 
            // CarModelEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxData);
            this.Controls.Add(this.panelControls);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "CarModelEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.TextBox textBoxCarModel;
        private System.Windows.Forms.Label labelCarModel;
        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.ComboBox comboBoxCarBrand;
        private System.Windows.Forms.Label labelCarBrand;
    }
}
