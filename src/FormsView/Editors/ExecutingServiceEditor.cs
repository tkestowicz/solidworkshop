﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;
using FormsView.Helpers;
using AppLogic.Presenters;
using AppLogic.Interfaces.Search;
using AppLogic.Presenters.Popup.Search;
using AppLogic.Interfaces.Events;

namespace FormsView.Editors
{
    public partial class ExecutingServiceEditor : Base.BaseUserControl, IExecutingServiceEditorView, IOpenPopupSearchEvent
    {
        public ExecutingServiceEditor()
        {
            InitializeComponent();
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        ExecutingServicePresenterEditor _presenter;

        /// <summary>
        /// Trzymanie logiki wyszukiwania, żeby zapamiętać aktywne filtry
        /// </summary>
        public SearchLogicContainer<IStoreSearchView> _searchLogic;

        #endregion

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as ExecutingServicePresenterEditor);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;

            // Podpinamy metodę, która reaguje na przycisk "Wyszukaj"
            addEditControls1.AddHandler = OnOpenPopupSearch;
        }

        /// <summary>
        /// Metoda odpala w dodatkowym okienku formatkę wyszukiwania klienta
        /// </summary>
        /// <param name="sender"></param>
        public void OnOpenPopupSearch(object sender)
        {
            if (_searchLogic == null)
                _searchLogic = new SearchLogicContainer<IStoreSearchView>(new StoreSearchPresenter(new SearchControls.StoreSearchControl()));

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(_searchLogic.View, _searchLogic);
            popup.Text = TranslationsStatic.Default.titleSearchStore;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia
        }

        public void ClearSearchFilters()
        {
            _searchLogic = null;
        }

        #region IExecutingServiceEditorView Members

        public int StatusesId
        {
            get
            {
                foreach (var l in statusesList_)
                    if (l.Value == (comboBoxStatus.SelectedItem as string))
                        return l.Id;
                return 0;
            }
            set
            {
                foreach (var s in statusesList_)
                {
                    if ((s as Model.Projection.ComboBox).Id == value)
                    {
                        comboBoxStatus.SelectedItem = s.Value;
                        break;
                    }
                }
            }
        }

        List<Model.Projection.ComboBox> statusesList_;
        public List<Model.Projection.ComboBox> StatusesClasses
        {
            get
            {
                return statusesList_;
            }
            set
            {
                statusesList_ = value;
                comboBoxStatus.Items.Clear();
                foreach (var s in value)
                    if (s.Value != null)
                        comboBoxStatus.Items.Add(s.Value);
            }
        }

        public int EmployeeId
        {
            get
            {
                foreach (var l in employeeList_)
                    if (l.Value == (comboBoxEmployee.SelectedItem as string))
                        return l.Id;
                return 0;
            }
            set
            {
                foreach (var s in employeeList_)
                {
                    if ((s as Model.Projection.ComboBox).Id == value)
                    {
                        comboBoxEmployee.SelectedItem = s.Value;
                        break;
                    }
                }
            }
        }

        private List<Model.Projection.ComboBox> employeeList_;
        public List<Model.Projection.ComboBox> EmployeeClasses
        {
            get
            {
                return employeeList_;
            }
            set
            {
                employeeList_ = value;
                comboBoxEmployee.Items.Clear();
                foreach (var e in value)
                    if (e.Value != null)
                        comboBoxEmployee.Items.Add(e.Value);
            }
        }

        public int OrderId
        {
            get
            {
                foreach (var l in orderList_)
                    if (l.Value == (comboBoxOrder.SelectedItem as string))
                        return l.Id;
                return 0;
            }
            set
            {
                foreach (var s in orderList_)
                {
                    if ((s as Model.Projection.ComboBox).Id == value)
                    {
                        comboBoxOrder.SelectedItem = s.Value;
                        break;
                    }
                }
            }
        }

        private List<Model.Projection.ComboBox> orderList_;
        public List<Model.Projection.ComboBox> OrderClasses
        {
            get
            {
                return orderList_;
            }
            set
            {
                orderList_ = value;
                comboBoxOrder.Items.Clear();
                foreach (var o in value)
                    if (o.Value != null)
                        comboBoxOrder.Items.Add(o.Value);
            }
        }


        public int ServiceId
        {
            get
            {
                foreach (var l in servicesList_)
                    if (l.Value == (comboBoxService.SelectedItem as string))
                        return l.Id;
                return 0;
            }
            set
            {
                foreach (var s in servicesList_)
                {
                    if ((s as Model.Projection.ComboBox).Id == value)
                    {
                        comboBoxService.SelectedItem = s.Value;
                        break;
                    }
                }
            }
        }

        private List<Model.Projection.ComboBox> servicesList_;
        public List<Model.Projection.ComboBox> ServiceClasses
        {
            get
            {
                return servicesList_;
            }
            set
            {
                servicesList_ = value;
                comboBoxService.Items.Clear();
                foreach (var o in value)
                    if (o.Value != null)
                        comboBoxService.Items.Add(o.Value);
            }
        }



        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                panelService.Enabled = value;
                saveCancelControls1.Enable = value;
                addEditControls1.Enable = true;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return panelService.Enabled;
            }
        }

        #endregion

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (_searchLogic == null)
                _searchLogic = new SearchLogicContainer<IStoreSearchView>(new StoreSearchPresenter(new SearchControls.StoreSearchControl()));

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(_searchLogic.View, _searchLogic);
            popup.Text = TranslationsStatic.Default.titleSearchStore;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia
        }
    }
}
