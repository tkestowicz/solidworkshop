﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Presenters.Editor;
using AppLogic.Presenters.DicitionaryEditor;
using AppLogic.Interfaces.Editors;
using FormsView.Helpers;

namespace FormsView.Editors
{
    public partial class ServiceEditor : Base.BaseUserControl, IServiceEditorView
    {
        public ServiceEditor()
        {
            InitializeComponent();

            // Dla to jest słownik więc ukrywamy przycisk wyszukiwania
            addEditControls1.SearchButtonVisible = false;
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        ServicePresenter _presenter;

        #endregion

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as ServicePresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;
        }

        public bool Enable
        {
            set
            {
                groupBoxData.Enabled = value;
                saveCancelControls1.Enable = value;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return groupBoxData.Enabled;
            }
        }

        #region IServiceEditorView Members

        public string ServiceName
        {
            get
            {
                return textBoxServiceName.Text;
            }
            set
            {
                textBoxServiceName.Text = value;
            }
        }

        public string Description
        {
            get
            {
                return textBoxServiceDescription.Text;
            }
            set
            {
                textBoxServiceDescription.Text = value;
            }
        }

        public double Price
        {
            get
            {
                return double.Parse(textBoxServicePrice.Text);
            }
            set
            {
                textBoxServicePrice.Text = value.ToString();
            }
        }

        public int? ClassId
        {
            get
            {
                try
                {
                    return (comboBoxClasses.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                comboBoxClasses.SelectedIndex = comboBoxClasses.FindIndex(value ?? 0);
            }
        }

        public string ClassName
        {
            set
            {
                comboBoxClasses.SelectedIndex = comboBoxClasses.FindIndex(value);
            }
        }

        public List<Model.Projection.ComboBox> Classes
        {
            set
            {
                comboBoxClasses.Items.Clear();
                comboBoxClasses.Items.AddRange(value.ToArray());
            }
        }

        #endregion
    }
}
