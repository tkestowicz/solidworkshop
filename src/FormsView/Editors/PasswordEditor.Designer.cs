﻿namespace FormsView.Editors
{
    partial class PasswordEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.groupBoxPassword = new System.Windows.Forms.GroupBox();
            this.textBoxPasswordNewRepeat = new System.Windows.Forms.TextBox();
            this.labePasswordNewRepeat = new System.Windows.Forms.Label();
            this.textBoxPasswordNew = new System.Windows.Forms.TextBox();
            this.labelPasswordNew = new System.Windows.Forms.Label();
            this.textBoxPasswordPresent = new System.Windows.Forms.TextBox();
            this.labelPasswordPresent = new System.Windows.Forms.Label();
            this.groupBoxPassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 176);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 0;
            // 
            // groupBoxPassword
            // 
            this.groupBoxPassword.Controls.Add(this.textBoxPasswordNewRepeat);
            this.groupBoxPassword.Controls.Add(this.labePasswordNewRepeat);
            this.groupBoxPassword.Controls.Add(this.textBoxPasswordNew);
            this.groupBoxPassword.Controls.Add(this.labelPasswordNew);
            this.groupBoxPassword.Controls.Add(this.textBoxPasswordPresent);
            this.groupBoxPassword.Controls.Add(this.labelPasswordPresent);
            this.groupBoxPassword.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxPassword.Location = new System.Drawing.Point(0, 0);
            this.groupBoxPassword.Name = "groupBoxPassword";
            this.groupBoxPassword.Size = new System.Drawing.Size(314, 158);
            this.groupBoxPassword.TabIndex = 1;
            this.groupBoxPassword.TabStop = false;
            this.groupBoxPassword.Text = global::FormsView.TranslationsStatic.Default.titlePassword;
            // 
            // textBoxPasswordNewRepeat
            // 
            this.textBoxPasswordNewRepeat.Location = new System.Drawing.Point(141, 110);
            this.textBoxPasswordNewRepeat.Name = "textBoxPasswordNewRepeat";
            this.textBoxPasswordNewRepeat.PasswordChar = '*';
            this.textBoxPasswordNewRepeat.Size = new System.Drawing.Size(149, 20);
            this.textBoxPasswordNewRepeat.TabIndex = 55;
            // 
            // labePasswordNewRepeat
            // 
            this.labePasswordNewRepeat.AutoSize = true;
            this.labePasswordNewRepeat.Location = new System.Drawing.Point(17, 110);
            this.labePasswordNewRepeat.Name = "labePasswordNewRepeat";
            this.labePasswordNewRepeat.Size = new System.Drawing.Size(107, 13);
            this.labePasswordNewRepeat.TabIndex = 54;
            this.labePasswordNewRepeat.Text = global::FormsView.TranslationsStatic.Default.labelPasswordRepeatNew;
            // 
            // textBoxPasswordNew
            // 
            this.textBoxPasswordNew.Location = new System.Drawing.Point(141, 72);
            this.textBoxPasswordNew.Name = "textBoxPasswordNew";
            this.textBoxPasswordNew.PasswordChar = '*';
            this.textBoxPasswordNew.Size = new System.Drawing.Size(149, 20);
            this.textBoxPasswordNew.TabIndex = 53;
            // 
            // labelPasswordNew
            // 
            this.labelPasswordNew.AutoSize = true;
            this.labelPasswordNew.Location = new System.Drawing.Point(17, 72);
            this.labelPasswordNew.Name = "labelPasswordNew";
            this.labelPasswordNew.Size = new System.Drawing.Size(68, 13);
            this.labelPasswordNew.TabIndex = 52;
            this.labelPasswordNew.Text = global::FormsView.TranslationsStatic.Default.labelPasswordNew;
            // 
            // textBoxPasswordPresent
            // 
            this.textBoxPasswordPresent.Location = new System.Drawing.Point(141, 32);
            this.textBoxPasswordPresent.Name = "textBoxPasswordPresent";
            this.textBoxPasswordPresent.PasswordChar = '*';
            this.textBoxPasswordPresent.Size = new System.Drawing.Size(149, 20);
            this.textBoxPasswordPresent.TabIndex = 51;
            // 
            // labelPasswordPresent
            // 
            this.labelPasswordPresent.AutoSize = true;
            this.labelPasswordPresent.Location = new System.Drawing.Point(17, 32);
            this.labelPasswordPresent.Name = "labelPasswordPresent";
            this.labelPasswordPresent.Size = new System.Drawing.Size(82, 13);
            this.labelPasswordPresent.TabIndex = 50;
            this.labelPasswordPresent.Text = global::FormsView.TranslationsStatic.Default.labelPasswordPresent;
            // 
            // PasswordEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxPassword);
            this.Controls.Add(this.saveCancelControls1);
            this.Name = "PasswordEditor";
            this.Size = new System.Drawing.Size(314, 201);
            this.groupBoxPassword.ResumeLayout(false);
            this.groupBoxPassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SaveCancelControls saveCancelControls1;
        private System.Windows.Forms.GroupBox groupBoxPassword;
        private System.Windows.Forms.TextBox textBoxPasswordPresent;
        private System.Windows.Forms.Label labelPasswordPresent;
        private System.Windows.Forms.TextBox textBoxPasswordNewRepeat;
        private System.Windows.Forms.Label labePasswordNewRepeat;
        private System.Windows.Forms.TextBox textBoxPasswordNew;
        private System.Windows.Forms.Label labelPasswordNew;
    }
}
