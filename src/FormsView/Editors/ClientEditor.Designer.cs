﻿using FormsView.Controls;
namespace FormsView.Editors
{
    partial class ClientEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabsClientData = new System.Windows.Forms.TabControl();
            this.tabPageClient = new System.Windows.Forms.TabPage();
            this.panelClientData = new System.Windows.Forms.Panel();
            this.maskedTextBoxClientPesel = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxClientPhone = new System.Windows.Forms.MaskedTextBox();
            this.comboBoxClientDiscount = new System.Windows.Forms.ComboBox();
            this.labelClientDiscount = new System.Windows.Forms.Label();
            this.textBoxClientMail = new System.Windows.Forms.TextBox();
            this.textBoxClientFirstName = new System.Windows.Forms.TextBox();
            this.textBoxClientSurname = new System.Windows.Forms.TextBox();
            this.labelClientMail = new System.Windows.Forms.Label();
            this.labelClientFirstName = new System.Windows.Forms.Label();
            this.labelClientSurname = new System.Windows.Forms.Label();
            this.labelClientPhone = new System.Windows.Forms.Label();
            this.labelClientPesel = new System.Windows.Forms.Label();
            this.tabPageClientAddress = new System.Windows.Forms.TabPage();
            this.addressEditorClient = new FormsView.Controls.AddressControl();
            this.buttonClientAddCar = new System.Windows.Forms.Button();
            this.buttonClientRemoveCar = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.panel1.SuspendLayout();
            this.tabsClientData.SuspendLayout();
            this.tabPageClient.SuspendLayout();
            this.panelClientData.SuspendLayout();
            this.tabPageClientAddress.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 581);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 1;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 606);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabsClientData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 331);
            this.panel1.TabIndex = 3;
            // 
            // tabsClientData
            // 
            this.tabsClientData.Controls.Add(this.tabPageClient);
            this.tabsClientData.Controls.Add(this.tabPageClientAddress);
            this.tabsClientData.Dock = System.Windows.Forms.DockStyle.Top;
            this.helpProvider1.SetHelpKeyword(this.tabsClientData, "client");
            this.tabsClientData.Location = new System.Drawing.Point(0, 0);
            this.tabsClientData.Name = "tabsClientData";
            this.tabsClientData.SelectedIndex = 0;
            this.helpProvider1.SetShowHelp(this.tabsClientData, true);
            this.tabsClientData.Size = new System.Drawing.Size(314, 331);
            this.tabsClientData.TabIndex = 0;
            this.tabsClientData.Text = "Klient";
            this.tabsClientData.SelectedIndexChanged += new System.EventHandler(this.TabChanged);
            // 
            // tabPageClient
            // 
            this.tabPageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageClient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tabPageClient.Controls.Add(this.panelClientData);
            this.tabPageClient.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabPageClient.Location = new System.Drawing.Point(4, 22);
            this.tabPageClient.Name = "tabPageClient";
            this.tabPageClient.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageClient.Size = new System.Drawing.Size(306, 305);
            this.tabPageClient.TabIndex = 0;
            this.tabPageClient.Text = "Klient - dane";
            // 
            // panelClientData
            // 
            this.panelClientData.Controls.Add(this.maskedTextBoxClientPesel);
            this.panelClientData.Controls.Add(this.maskedTextBoxClientPhone);
            this.panelClientData.Controls.Add(this.comboBoxClientDiscount);
            this.panelClientData.Controls.Add(this.labelClientDiscount);
            this.panelClientData.Controls.Add(this.textBoxClientMail);
            this.panelClientData.Controls.Add(this.textBoxClientFirstName);
            this.panelClientData.Controls.Add(this.textBoxClientSurname);
            this.panelClientData.Controls.Add(this.labelClientMail);
            this.panelClientData.Controls.Add(this.labelClientFirstName);
            this.panelClientData.Controls.Add(this.labelClientSurname);
            this.panelClientData.Controls.Add(this.labelClientPhone);
            this.panelClientData.Controls.Add(this.labelClientPesel);
            this.panelClientData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelClientData.Enabled = false;
            this.panelClientData.Location = new System.Drawing.Point(3, 3);
            this.panelClientData.Name = "panelClientData";
            this.panelClientData.Size = new System.Drawing.Size(300, 299);
            this.panelClientData.TabIndex = 0;
            // 
            // maskedTextBoxClientPesel
            // 
            this.maskedTextBoxClientPesel.Location = new System.Drawing.Point(102, 128);
            this.maskedTextBoxClientPesel.Mask = "00000000000";
            this.maskedTextBoxClientPesel.Name = "maskedTextBoxClientPesel";
            this.maskedTextBoxClientPesel.Size = new System.Drawing.Size(72, 20);
            this.maskedTextBoxClientPesel.TabIndex = 50;
            // 
            // maskedTextBoxClientPhone
            // 
            this.maskedTextBoxClientPhone.Location = new System.Drawing.Point(102, 168);
            this.maskedTextBoxClientPhone.Mask = "000-000-000";
            this.maskedTextBoxClientPhone.Name = "maskedTextBoxClientPhone";
            this.maskedTextBoxClientPhone.Size = new System.Drawing.Size(72, 20);
            this.maskedTextBoxClientPhone.TabIndex = 49;
            this.maskedTextBoxClientPhone.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // comboBoxClientDiscount
            // 
            this.comboBoxClientDiscount.DisplayMember = "Value";
            this.comboBoxClientDiscount.FormattingEnabled = true;
            this.comboBoxClientDiscount.Location = new System.Drawing.Point(103, 89);
            this.comboBoxClientDiscount.Name = "comboBoxClientDiscount";
            this.comboBoxClientDiscount.Size = new System.Drawing.Size(182, 21);
            this.comboBoxClientDiscount.TabIndex = 48;
            this.comboBoxClientDiscount.ValueMember = "Value";
            // 
            // labelClientDiscount
            // 
            this.labelClientDiscount.AutoSize = true;
            this.labelClientDiscount.Location = new System.Drawing.Point(17, 89);
            this.labelClientDiscount.Name = "labelClientDiscount";
            this.labelClientDiscount.Size = new System.Drawing.Size(42, 13);
            this.labelClientDiscount.TabIndex = 47;
            this.labelClientDiscount.Text = "Zniżka:";
            // 
            // textBoxClientMail
            // 
            this.textBoxClientMail.Location = new System.Drawing.Point(102, 206);
            this.textBoxClientMail.Name = "textBoxClientMail";
            this.textBoxClientMail.Size = new System.Drawing.Size(182, 20);
            this.textBoxClientMail.TabIndex = 45;
            // 
            // textBoxClientFirstName
            // 
            this.textBoxClientFirstName.Location = new System.Drawing.Point(103, 15);
            this.textBoxClientFirstName.Name = "textBoxClientFirstName";
            this.textBoxClientFirstName.Size = new System.Drawing.Size(182, 20);
            this.textBoxClientFirstName.TabIndex = 42;
            // 
            // textBoxClientSurname
            // 
            this.textBoxClientSurname.Location = new System.Drawing.Point(103, 52);
            this.textBoxClientSurname.Name = "textBoxClientSurname";
            this.textBoxClientSurname.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxClientSurname.Size = new System.Drawing.Size(182, 20);
            this.textBoxClientSurname.TabIndex = 43;
            // 
            // labelClientMail
            // 
            this.labelClientMail.AutoSize = true;
            this.labelClientMail.Location = new System.Drawing.Point(16, 206);
            this.labelClientMail.Name = "labelClientMail";
            this.labelClientMail.Size = new System.Drawing.Size(38, 13);
            this.labelClientMail.TabIndex = 44;
            this.labelClientMail.Text = "E-mail:";
            // 
            // labelClientFirstName
            // 
            this.labelClientFirstName.AutoSize = true;
            this.labelClientFirstName.Location = new System.Drawing.Point(17, 15);
            this.labelClientFirstName.Name = "labelClientFirstName";
            this.labelClientFirstName.Size = new System.Drawing.Size(29, 13);
            this.labelClientFirstName.TabIndex = 38;
            this.labelClientFirstName.Text = "Imię:";
            // 
            // labelClientSurname
            // 
            this.labelClientSurname.AutoSize = true;
            this.labelClientSurname.Location = new System.Drawing.Point(17, 52);
            this.labelClientSurname.Name = "labelClientSurname";
            this.labelClientSurname.Size = new System.Drawing.Size(56, 13);
            this.labelClientSurname.TabIndex = 39;
            this.labelClientSurname.Text = "Nazwisko:";
            // 
            // labelClientPhone
            // 
            this.labelClientPhone.AutoSize = true;
            this.labelClientPhone.Location = new System.Drawing.Point(16, 168);
            this.labelClientPhone.Name = "labelClientPhone";
            this.labelClientPhone.Size = new System.Drawing.Size(82, 13);
            this.labelClientPhone.TabIndex = 41;
            this.labelClientPhone.Text = "Numer telefonu:";
            // 
            // labelClientPesel
            // 
            this.labelClientPesel.AutoSize = true;
            this.labelClientPesel.Location = new System.Drawing.Point(16, 131);
            this.labelClientPesel.Name = "labelClientPesel";
            this.labelClientPesel.Size = new System.Drawing.Size(44, 13);
            this.labelClientPesel.TabIndex = 40;
            this.labelClientPesel.Text = "PESEL:";
            // 
            // tabPageClientAddress
            // 
            this.tabPageClientAddress.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageClientAddress.Controls.Add(this.addressEditorClient);
            this.tabPageClientAddress.Location = new System.Drawing.Point(4, 22);
            this.tabPageClientAddress.Name = "tabPageClientAddress";
            this.tabPageClientAddress.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageClientAddress.Size = new System.Drawing.Size(306, 305);
            this.tabPageClientAddress.TabIndex = 2;
            this.tabPageClientAddress.Text = "Klient - adres";
            // 
            // addressEditorClient
            // 
            this.addressEditorClient.ApartmentNumber = "";
            this.addressEditorClient.CityId = -1;
            this.addressEditorClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressEditorClient.Enabled = false;
            this.addressEditorClient.HouseNumber = "";
            this.addressEditorClient.Location = new System.Drawing.Point(3, 3);
            this.addressEditorClient.Name = "addressEditorClient";
            this.addressEditorClient.Postcode = "  -";
            this.addressEditorClient.Province = null;
            this.addressEditorClient.ProvinceId = -1;
            this.addressEditorClient.Size = new System.Drawing.Size(300, 299);
            this.addressEditorClient.Street = "";
            this.addressEditorClient.TabIndex = 39;
            // 
            // buttonClientAddCar
            // 
            this.buttonClientAddCar.Enabled = false;
            this.buttonClientAddCar.Location = new System.Drawing.Point(7, 337);
            this.buttonClientAddCar.Name = "buttonClientAddCar";
            this.buttonClientAddCar.Size = new System.Drawing.Size(136, 23);
            this.buttonClientAddCar.TabIndex = 47;
            this.buttonClientAddCar.Text = "Dodaj samochód";
            this.buttonClientAddCar.UseVisualStyleBackColor = true;
            this.buttonClientAddCar.Click += new System.EventHandler(this.AddNewCar);
            // 
            // buttonClientRemoveCar
            // 
            this.buttonClientRemoveCar.Enabled = false;
            this.buttonClientRemoveCar.Location = new System.Drawing.Point(171, 337);
            this.buttonClientRemoveCar.Name = "buttonClientRemoveCar";
            this.buttonClientRemoveCar.Size = new System.Drawing.Size(136, 23);
            this.buttonClientRemoveCar.TabIndex = 48;
            this.buttonClientRemoveCar.Text = global::FormsView.TranslationsStatic.Default.buttonRemoveCar;
            this.buttonClientRemoveCar.UseVisualStyleBackColor = true;
            this.buttonClientRemoveCar.Click += new System.EventHandler(this.buttonClientRemoveCar_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "client.chm";
            // 
            // ClientEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonClientRemoveCar);
            this.Controls.Add(this.buttonClientAddCar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.addEditControls1);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "ClientEditor";
            this.helpProvider1.SetShowHelp(this, true);
            this.Size = new System.Drawing.Size(314, 631);
            this.panel1.ResumeLayout(false);
            this.tabsClientData.ResumeLayout(false);
            this.tabPageClient.ResumeLayout(false);
            this.panelClientData.ResumeLayout(false);
            this.panelClientData.PerformLayout();
            this.tabPageClientAddress.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabsClientData;
        private System.Windows.Forms.TabPage tabPageClient;
        private System.Windows.Forms.TabPage tabPageClientAddress;
        private AddressControl addressEditorClient;
        private System.Windows.Forms.Panel panelClientData;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxClientPesel;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxClientPhone;
        private System.Windows.Forms.ComboBox comboBoxClientDiscount;
        private System.Windows.Forms.Label labelClientDiscount;
        private System.Windows.Forms.TextBox textBoxClientMail;
        private System.Windows.Forms.TextBox textBoxClientFirstName;
        private System.Windows.Forms.TextBox textBoxClientSurname;
        private System.Windows.Forms.Label labelClientMail;
        private System.Windows.Forms.Label labelClientFirstName;
        private System.Windows.Forms.Label labelClientSurname;
        private System.Windows.Forms.Label labelClientPhone;
        private System.Windows.Forms.Label labelClientPesel;
        private System.Windows.Forms.Button buttonClientAddCar;
        private System.Windows.Forms.Button buttonClientRemoveCar;
        private System.Windows.Forms.HelpProvider helpProvider1;

    }
}
