﻿namespace FormsView.Editors
{
    partial class ExecutingServiceEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlService = new System.Windows.Forms.TabControl();
            this.tabPageService = new System.Windows.Forms.TabPage();
            this.panelService = new System.Windows.Forms.Panel();
            this.comboBoxService = new System.Windows.Forms.ComboBox();
            this.labelServce = new System.Windows.Forms.Label();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.labelStatus = new System.Windows.Forms.Label();
            this.comboBoxOrder = new System.Windows.Forms.ComboBox();
            this.labelOrder = new System.Windows.Forms.Label();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.labelEmployee = new System.Windows.Forms.Label();
            this.tabPageServiceParts = new System.Windows.Forms.TabPage();
            this.panelUsedParts = new System.Windows.Forms.Panel();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.dataGridViewServiceParts = new System.Windows.Forms.DataGridView();
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.tabControlService.SuspendLayout();
            this.tabPageService.SuspendLayout();
            this.panelService.SuspendLayout();
            this.tabPageServiceParts.SuspendLayout();
            this.panelUsedParts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServiceParts)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlService
            // 
            this.tabControlService.Controls.Add(this.tabPageService);
            this.tabControlService.Controls.Add(this.tabPageServiceParts);
            this.tabControlService.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlService.Location = new System.Drawing.Point(0, 0);
            this.tabControlService.Name = "tabControlService";
            this.tabControlService.SelectedIndex = 0;
            this.tabControlService.Size = new System.Drawing.Size(314, 489);
            this.tabControlService.TabIndex = 17;
            this.tabControlService.Text = "Użyte części:";
            // 
            // tabPageService
            // 
            this.tabPageService.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageService.Controls.Add(this.panelService);
            this.tabPageService.Location = new System.Drawing.Point(4, 22);
            this.tabPageService.Name = "tabPageService";
            this.tabPageService.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageService.Size = new System.Drawing.Size(306, 463);
            this.tabPageService.TabIndex = 0;
            this.tabPageService.Text = "Usługa";
            // 
            // panelService
            // 
            this.panelService.Controls.Add(this.comboBoxService);
            this.panelService.Controls.Add(this.labelServce);
            this.panelService.Controls.Add(this.comboBoxStatus);
            this.panelService.Controls.Add(this.labelStatus);
            this.panelService.Controls.Add(this.comboBoxOrder);
            this.panelService.Controls.Add(this.labelOrder);
            this.panelService.Controls.Add(this.comboBoxEmployee);
            this.panelService.Controls.Add(this.labelEmployee);
            this.panelService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelService.Enabled = false;
            this.panelService.Location = new System.Drawing.Point(3, 3);
            this.panelService.Name = "panelService";
            this.panelService.Size = new System.Drawing.Size(300, 457);
            this.panelService.TabIndex = 0;
            // 
            // comboBoxService
            // 
            this.comboBoxService.FormattingEnabled = true;
            this.comboBoxService.Location = new System.Drawing.Point(91, 148);
            this.comboBoxService.Name = "comboBoxService";
            this.comboBoxService.Size = new System.Drawing.Size(162, 21);
            this.comboBoxService.TabIndex = 7;
            // 
            // labelServce
            // 
            this.labelServce.AutoSize = true;
            this.labelServce.Location = new System.Drawing.Point(25, 151);
            this.labelServce.Name = "labelServce";
            this.labelServce.Size = new System.Drawing.Size(62, 13);
            this.labelServce.TabIndex = 6;
            this.labelServce.Text = "Typ Usługi:";
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(91, 47);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(162, 21);
            this.comboBoxStatus.TabIndex = 5;
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(25, 50);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(40, 13);
            this.labelStatus.TabIndex = 4;
            this.labelStatus.Text = "Status:";
            // 
            // comboBoxOrder
            // 
            this.comboBoxOrder.FormattingEnabled = true;
            this.comboBoxOrder.Location = new System.Drawing.Point(91, 112);
            this.comboBoxOrder.Name = "comboBoxOrder";
            this.comboBoxOrder.Size = new System.Drawing.Size(162, 21);
            this.comboBoxOrder.TabIndex = 3;
            // 
            // labelOrder
            // 
            this.labelOrder.AutoSize = true;
            this.labelOrder.Location = new System.Drawing.Point(25, 115);
            this.labelOrder.Name = "labelOrder";
            this.labelOrder.Size = new System.Drawing.Size(51, 13);
            this.labelOrder.TabIndex = 2;
            this.labelOrder.Text = "Zlecenie:";
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Location = new System.Drawing.Point(91, 79);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(162, 21);
            this.comboBoxEmployee.TabIndex = 1;
            // 
            // labelEmployee
            // 
            this.labelEmployee.AutoSize = true;
            this.labelEmployee.Location = new System.Drawing.Point(25, 82);
            this.labelEmployee.Name = "labelEmployee";
            this.labelEmployee.Size = new System.Drawing.Size(60, 13);
            this.labelEmployee.TabIndex = 0;
            this.labelEmployee.Text = "Pracownik:";
            // 
            // tabPageServiceParts
            // 
            this.tabPageServiceParts.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageServiceParts.Controls.Add(this.panelUsedParts);
            this.tabPageServiceParts.Location = new System.Drawing.Point(4, 22);
            this.tabPageServiceParts.Name = "tabPageServiceParts";
            this.tabPageServiceParts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageServiceParts.Size = new System.Drawing.Size(306, 463);
            this.tabPageServiceParts.TabIndex = 1;
            this.tabPageServiceParts.Text = "Użyte części";
            // 
            // panelUsedParts
            // 
            this.panelUsedParts.Controls.Add(this.buttonAdd);
            this.panelUsedParts.Controls.Add(this.dataGridViewServiceParts);
            this.panelUsedParts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUsedParts.Enabled = false;
            this.panelUsedParts.Location = new System.Drawing.Point(3, 3);
            this.panelUsedParts.Name = "panelUsedParts";
            this.panelUsedParts.Size = new System.Drawing.Size(300, 457);
            this.panelUsedParts.TabIndex = 0;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(26, 414);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 27;
            this.buttonAdd.Text = "Dodaj";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // dataGridViewServiceParts
            // 
            this.dataGridViewServiceParts.AllowUserToAddRows = false;
            this.dataGridViewServiceParts.AllowUserToDeleteRows = false;
            this.dataGridViewServiceParts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewServiceParts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewServiceParts.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewServiceParts.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewServiceParts.Name = "dataGridViewServiceParts";
            this.dataGridViewServiceParts.Size = new System.Drawing.Size(300, 395);
            this.dataGridViewServiceParts.TabIndex = 26;
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 581);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 19;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 606);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 18;
            // 
            // ExecutingServiceEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.addEditControls1);
            this.Controls.Add(this.tabControlService);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "ExecutingServiceEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.tabControlService.ResumeLayout(false);
            this.tabPageService.ResumeLayout(false);
            this.panelService.ResumeLayout(false);
            this.panelService.PerformLayout();
            this.tabPageServiceParts.ResumeLayout(false);
            this.panelUsedParts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServiceParts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlService;
        private System.Windows.Forms.TabPage tabPageService;
        private System.Windows.Forms.TabPage tabPageServiceParts;
        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
        private System.Windows.Forms.Panel panelService;
        private System.Windows.Forms.Panel panelUsedParts;
        private System.Windows.Forms.DataGridView dataGridViewServiceParts;
        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Label labelEmployee;
        private System.Windows.Forms.ComboBox comboBoxOrder;
        private System.Windows.Forms.Label labelOrder;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.ComboBox comboBoxService;
        private System.Windows.Forms.Label labelServce;
    }
}
