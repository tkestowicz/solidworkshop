﻿namespace FormsView.Editors
{
    partial class OrderEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.groupBoxOrder = new System.Windows.Forms.GroupBox();
            this.comboBoxOrderState = new System.Windows.Forms.ComboBox();
            this.labelOrderState = new System.Windows.Forms.Label();
            this.comboBoxOrderSupervisor = new System.Windows.Forms.ComboBox();
            this.buttonSelectCar = new System.Windows.Forms.Button();
            this.labelOrderCar = new System.Windows.Forms.Label();
            this.textBoxOrderCar = new System.Windows.Forms.TextBox();
            this.labelOrderDescription = new System.Windows.Forms.Label();
            this.textBoxOrderPrice = new System.Windows.Forms.TextBox();
            this.textBoxOrderDescription = new System.Windows.Forms.TextBox();
            this.labelOrderSupervisor = new System.Windows.Forms.Label();
            this.labelOrderPrice = new System.Windows.Forms.Label();
            this.groupBoxOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 581);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 13;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 606);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 11;
            // 
            // groupBoxOrder
            // 
            this.groupBoxOrder.Controls.Add(this.comboBoxOrderState);
            this.groupBoxOrder.Controls.Add(this.labelOrderState);
            this.groupBoxOrder.Controls.Add(this.comboBoxOrderSupervisor);
            this.groupBoxOrder.Controls.Add(this.buttonSelectCar);
            this.groupBoxOrder.Controls.Add(this.labelOrderCar);
            this.groupBoxOrder.Controls.Add(this.textBoxOrderCar);
            this.groupBoxOrder.Controls.Add(this.labelOrderDescription);
            this.groupBoxOrder.Controls.Add(this.textBoxOrderPrice);
            this.groupBoxOrder.Controls.Add(this.textBoxOrderDescription);
            this.groupBoxOrder.Controls.Add(this.labelOrderSupervisor);
            this.groupBoxOrder.Controls.Add(this.labelOrderPrice);
            this.groupBoxOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxOrder.Enabled = false;
            this.groupBoxOrder.Location = new System.Drawing.Point(0, 0);
            this.groupBoxOrder.Name = "groupBoxOrder";
            this.groupBoxOrder.Size = new System.Drawing.Size(314, 472);
            this.groupBoxOrder.TabIndex = 12;
            this.groupBoxOrder.TabStop = false;
            this.groupBoxOrder.Text = global::FormsView.TranslationsStatic.Default.titleOrder;
            // 
            // comboBoxOrderState
            // 
            this.comboBoxOrderState.FormattingEnabled = true;
            this.comboBoxOrderState.Location = new System.Drawing.Point(89, 31);
            this.comboBoxOrderState.Name = "comboBoxOrderState";
            this.comboBoxOrderState.Size = new System.Drawing.Size(200, 21);
            this.comboBoxOrderState.TabIndex = 11;
            // 
            // labelOrderState
            // 
            this.labelOrderState.AutoSize = true;
            this.labelOrderState.Location = new System.Drawing.Point(20, 31);
            this.labelOrderState.Name = "labelOrderState";
            this.labelOrderState.Size = new System.Drawing.Size(40, 13);
            this.labelOrderState.TabIndex = 10;
            this.labelOrderState.Text = "Status:";
            // 
            // comboBoxOrderSupervisor
            // 
            this.comboBoxOrderSupervisor.FormattingEnabled = true;
            this.comboBoxOrderSupervisor.Location = new System.Drawing.Point(89, 192);
            this.comboBoxOrderSupervisor.Name = "comboBoxOrderSupervisor";
            this.comboBoxOrderSupervisor.Size = new System.Drawing.Size(200, 21);
            this.comboBoxOrderSupervisor.TabIndex = 9;
            // 
            // buttonSelectCar
            // 
            this.buttonSelectCar.Location = new System.Drawing.Point(93, 153);
            this.buttonSelectCar.Name = "buttonSelectCar";
            this.buttonSelectCar.Size = new System.Drawing.Size(129, 23);
            this.buttonSelectCar.TabIndex = 8;
            this.buttonSelectCar.Text = "Wybierz samochód";
            this.buttonSelectCar.UseVisualStyleBackColor = true;
            this.buttonSelectCar.Click += new System.EventHandler(this.buttonSelectCar_Click);
            // 
            // labelOrderCar
            // 
            this.labelOrderCar.AutoSize = true;
            this.labelOrderCar.Location = new System.Drawing.Point(20, 70);
            this.labelOrderCar.Name = "labelOrderCar";
            this.labelOrderCar.Size = new System.Drawing.Size(61, 13);
            this.labelOrderCar.TabIndex = 0;
            this.labelOrderCar.Text = "Samochód:";
            // 
            // textBoxOrderCar
            // 
            this.textBoxOrderCar.Location = new System.Drawing.Point(89, 70);
            this.textBoxOrderCar.Multiline = true;
            this.textBoxOrderCar.Name = "textBoxOrderCar";
            this.textBoxOrderCar.ReadOnly = true;
            this.textBoxOrderCar.Size = new System.Drawing.Size(200, 65);
            this.textBoxOrderCar.TabIndex = 4;
            // 
            // labelOrderDescription
            // 
            this.labelOrderDescription.AutoSize = true;
            this.labelOrderDescription.Location = new System.Drawing.Point(20, 234);
            this.labelOrderDescription.Name = "labelOrderDescription";
            this.labelOrderDescription.Size = new System.Drawing.Size(31, 13);
            this.labelOrderDescription.TabIndex = 1;
            this.labelOrderDescription.Text = "Opis:";
            // 
            // textBoxOrderPrice
            // 
            this.textBoxOrderPrice.Location = new System.Drawing.Point(89, 383);
            this.textBoxOrderPrice.Name = "textBoxOrderPrice";
            this.textBoxOrderPrice.ReadOnly = true;
            this.textBoxOrderPrice.Size = new System.Drawing.Size(200, 20);
            this.textBoxOrderPrice.TabIndex = 6;
            // 
            // textBoxOrderDescription
            // 
            this.textBoxOrderDescription.Location = new System.Drawing.Point(89, 234);
            this.textBoxOrderDescription.Multiline = true;
            this.textBoxOrderDescription.Name = "textBoxOrderDescription";
            this.textBoxOrderDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOrderDescription.Size = new System.Drawing.Size(200, 133);
            this.textBoxOrderDescription.TabIndex = 5;
            // 
            // labelOrderSupervisor
            // 
            this.labelOrderSupervisor.AutoSize = true;
            this.labelOrderSupervisor.Location = new System.Drawing.Point(20, 192);
            this.labelOrderSupervisor.Name = "labelOrderSupervisor";
            this.labelOrderSupervisor.Size = new System.Drawing.Size(56, 13);
            this.labelOrderSupervisor.TabIndex = 3;
            this.labelOrderSupervisor.Text = "Nadzorca:";
            // 
            // labelOrderPrice
            // 
            this.labelOrderPrice.AutoSize = true;
            this.labelOrderPrice.Location = new System.Drawing.Point(20, 383);
            this.labelOrderPrice.Name = "labelOrderPrice";
            this.labelOrderPrice.Size = new System.Drawing.Size(35, 13);
            this.labelOrderPrice.TabIndex = 2;
            this.labelOrderPrice.Text = "Cena:";
            // 
            // OrderEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxOrder);
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.addEditControls1);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "OrderEditor";
            this.Size = new System.Drawing.Size(314, 631);
            this.groupBoxOrder.ResumeLayout(false);
            this.groupBoxOrder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelOrderCar;
        private System.Windows.Forms.TextBox textBoxOrderCar;
        private System.Windows.Forms.GroupBox groupBoxOrder;
        private System.Windows.Forms.Label labelOrderDescription;
        private System.Windows.Forms.TextBox textBoxOrderPrice;
        private System.Windows.Forms.TextBox textBoxOrderDescription;
        private System.Windows.Forms.Label labelOrderSupervisor;
        private System.Windows.Forms.Label labelOrderPrice;
        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
        private System.Windows.Forms.Button buttonSelectCar;
        private System.Windows.Forms.ComboBox comboBoxOrderSupervisor;
        private System.Windows.Forms.Label labelOrderState;
        private System.Windows.Forms.ComboBox comboBoxOrderState;

    }
}
