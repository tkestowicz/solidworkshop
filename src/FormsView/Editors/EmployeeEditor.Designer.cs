﻿namespace FormsView.Editors
{
    partial class EmployeeEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlEmployee = new System.Windows.Forms.TabControl();
            this.tabPageEmployeeData = new System.Windows.Forms.TabPage();
            this.panelEmployeeData = new System.Windows.Forms.Panel();
            this.checkBoxReleaseDate = new System.Windows.Forms.CheckBox();
            this.dateTimePickerReleaseDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerHireDate = new System.Windows.Forms.DateTimePicker();
            this.comboBoxEmployeeGroup = new System.Windows.Forms.ComboBox();
            this.labelEmployeeGroup = new System.Windows.Forms.Label();
            this.labelEmployeeFirstName = new System.Windows.Forms.Label();
            this.labelEmployeeHireTime = new System.Windows.Forms.Label();
            this.textBoxEmployeeSurname = new System.Windows.Forms.TextBox();
            this.textBoxEmployeeQualifications = new System.Windows.Forms.TextBox();
            this.labelEmployeeSurname = new System.Windows.Forms.Label();
            this.labelEmployeeQualifications = new System.Windows.Forms.Label();
            this.textBoxEmployeeFirstName = new System.Windows.Forms.TextBox();
            this.comboBoxEmploymentState = new System.Windows.Forms.ComboBox();
            this.labelEmployeePesel = new System.Windows.Forms.Label();
            this.labelEmploymentStatus = new System.Windows.Forms.Label();
            this.labelEmployeePhone = new System.Windows.Forms.Label();
            this.maskedTextBoxEmployeeNIP = new System.Windows.Forms.MaskedTextBox();
            this.labelEmployeeMail = new System.Windows.Forms.Label();
            this.labelEmployeeNIP = new System.Windows.Forms.Label();
            this.textBoxEmployeeMail = new System.Windows.Forms.TextBox();
            this.maskedTextBoxEmployeePesel = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxEmployeePhone = new System.Windows.Forms.MaskedTextBox();
            this.tabPageEmployeeAddress = new System.Windows.Forms.TabPage();
            this.addressEditorEmployee = new FormsView.Controls.AddressControl();
            this.tabPageEmployeeUserData = new System.Windows.Forms.TabPage();
            this.panelSytemUserData = new System.Windows.Forms.Panel();
            this.buttonGeneratePassword = new System.Windows.Forms.Button();
            this.textBoxPasswordNewRepeat = new System.Windows.Forms.TextBox();
            this.labePasswordRepeat = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.labelLogin = new System.Windows.Forms.Label();
            this.saveCancelControls1 = new FormsView.Controls.SaveCancelControls();
            this.addEditControls1 = new FormsView.Controls.AddEditSearchControls();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.tabControlEmployee.SuspendLayout();
            this.tabPageEmployeeData.SuspendLayout();
            this.panelEmployeeData.SuspendLayout();
            this.tabPageEmployeeAddress.SuspendLayout();
            this.tabPageEmployeeUserData.SuspendLayout();
            this.panelSytemUserData.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlEmployee
            // 
            this.tabControlEmployee.Controls.Add(this.tabPageEmployeeData);
            this.tabControlEmployee.Controls.Add(this.tabPageEmployeeAddress);
            this.tabControlEmployee.Controls.Add(this.tabPageEmployeeUserData);
            this.tabControlEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.helpProvider1.SetHelpKeyword(this.tabControlEmployee, "employee");
            this.tabControlEmployee.Location = new System.Drawing.Point(0, 0);
            this.tabControlEmployee.Name = "tabControlEmployee";
            this.tabControlEmployee.SelectedIndex = 0;
            this.helpProvider1.SetShowHelp(this.tabControlEmployee, true);
            this.tabControlEmployee.Size = new System.Drawing.Size(314, 479);
            this.tabControlEmployee.TabIndex = 14;
            this.tabControlEmployee.Text = "Pracownik - dane";
            this.tabControlEmployee.Selected += new System.Windows.Forms.TabControlEventHandler(this.PrepareLogin);
            // 
            // tabPageEmployeeData
            // 
            this.tabPageEmployeeData.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageEmployeeData.Controls.Add(this.panelEmployeeData);
            this.tabPageEmployeeData.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmployeeData.Name = "tabPageEmployeeData";
            this.tabPageEmployeeData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmployeeData.Size = new System.Drawing.Size(306, 453);
            this.tabPageEmployeeData.TabIndex = 0;
            this.tabPageEmployeeData.Text = "Dane personalne";
            // 
            // panelEmployeeData
            // 
            this.panelEmployeeData.Controls.Add(this.checkBoxReleaseDate);
            this.panelEmployeeData.Controls.Add(this.dateTimePickerReleaseDate);
            this.panelEmployeeData.Controls.Add(this.dateTimePickerHireDate);
            this.panelEmployeeData.Controls.Add(this.comboBoxEmployeeGroup);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeGroup);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeFirstName);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeHireTime);
            this.panelEmployeeData.Controls.Add(this.textBoxEmployeeSurname);
            this.panelEmployeeData.Controls.Add(this.textBoxEmployeeQualifications);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeSurname);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeQualifications);
            this.panelEmployeeData.Controls.Add(this.textBoxEmployeeFirstName);
            this.panelEmployeeData.Controls.Add(this.comboBoxEmploymentState);
            this.panelEmployeeData.Controls.Add(this.labelEmployeePesel);
            this.panelEmployeeData.Controls.Add(this.labelEmploymentStatus);
            this.panelEmployeeData.Controls.Add(this.labelEmployeePhone);
            this.panelEmployeeData.Controls.Add(this.maskedTextBoxEmployeeNIP);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeMail);
            this.panelEmployeeData.Controls.Add(this.labelEmployeeNIP);
            this.panelEmployeeData.Controls.Add(this.textBoxEmployeeMail);
            this.panelEmployeeData.Controls.Add(this.maskedTextBoxEmployeePesel);
            this.panelEmployeeData.Controls.Add(this.maskedTextBoxEmployeePhone);
            this.panelEmployeeData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEmployeeData.Enabled = false;
            this.panelEmployeeData.Location = new System.Drawing.Point(3, 3);
            this.panelEmployeeData.Name = "panelEmployeeData";
            this.panelEmployeeData.Size = new System.Drawing.Size(300, 447);
            this.panelEmployeeData.TabIndex = 0;
            // 
            // checkBoxReleaseDate
            // 
            this.checkBoxReleaseDate.AutoSize = true;
            this.checkBoxReleaseDate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxReleaseDate.Location = new System.Drawing.Point(14, 415);
            this.checkBoxReleaseDate.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.checkBoxReleaseDate.Name = "checkBoxReleaseDate";
            this.checkBoxReleaseDate.Size = new System.Drawing.Size(151, 17);
            this.checkBoxReleaseDate.TabIndex = 80;
            this.checkBoxReleaseDate.Text = global::FormsView.TranslationsStatic.Default.labelReleaseTime;
            this.checkBoxReleaseDate.UseVisualStyleBackColor = true;
            this.checkBoxReleaseDate.CheckedChanged += new System.EventHandler(this.CheckItem);
            // 
            // dateTimePickerReleaseDate
            // 
            this.dateTimePickerReleaseDate.Enabled = false;
            this.dateTimePickerReleaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerReleaseDate.Location = new System.Drawing.Point(171, 413);
            this.dateTimePickerReleaseDate.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerReleaseDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerReleaseDate.Name = "dateTimePickerReleaseDate";
            this.dateTimePickerReleaseDate.Size = new System.Drawing.Size(112, 20);
            this.dateTimePickerReleaseDate.TabIndex = 79;
            this.dateTimePickerReleaseDate.Value = new System.DateTime(2012, 6, 30, 15, 0, 16, 0);
            // 
            // dateTimePickerHireDate
            // 
            this.dateTimePickerHireDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerHireDate.Location = new System.Drawing.Point(171, 374);
            this.dateTimePickerHireDate.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerHireDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerHireDate.Name = "dateTimePickerHireDate";
            this.dateTimePickerHireDate.Size = new System.Drawing.Size(112, 20);
            this.dateTimePickerHireDate.TabIndex = 78;
            this.dateTimePickerHireDate.Value = new System.DateTime(2012, 6, 30, 15, 0, 16, 0);
            // 
            // comboBoxEmployeeGroup
            // 
            this.comboBoxEmployeeGroup.DisplayMember = "Value";
            this.comboBoxEmployeeGroup.FormattingEnabled = true;
            this.comboBoxEmployeeGroup.Location = new System.Drawing.Point(134, 87);
            this.comboBoxEmployeeGroup.Name = "comboBoxEmployeeGroup";
            this.comboBoxEmployeeGroup.Size = new System.Drawing.Size(151, 21);
            this.comboBoxEmployeeGroup.TabIndex = 71;
            this.comboBoxEmployeeGroup.ValueMember = "Value";
            // 
            // labelEmployeeGroup
            // 
            this.labelEmployeeGroup.AutoSize = true;
            this.labelEmployeeGroup.Location = new System.Drawing.Point(16, 87);
            this.labelEmployeeGroup.Name = "labelEmployeeGroup";
            this.labelEmployeeGroup.Size = new System.Drawing.Size(39, 13);
            this.labelEmployeeGroup.TabIndex = 60;
            this.labelEmployeeGroup.Text = global::FormsView.TranslationsStatic.Default.labelGroup;
            // 
            // labelEmployeeFirstName
            // 
            this.labelEmployeeFirstName.AutoSize = true;
            this.labelEmployeeFirstName.Location = new System.Drawing.Point(16, 10);
            this.labelEmployeeFirstName.Name = "labelEmployeeFirstName";
            this.labelEmployeeFirstName.Size = new System.Drawing.Size(29, 13);
            this.labelEmployeeFirstName.TabIndex = 56;
            this.labelEmployeeFirstName.Text = global::FormsView.TranslationsStatic.Default.labelPersonName;
            // 
            // labelEmployeeHireTime
            // 
            this.labelEmployeeHireTime.AutoSize = true;
            this.labelEmployeeHireTime.Location = new System.Drawing.Point(16, 377);
            this.labelEmployeeHireTime.Name = "labelEmployeeHireTime";
            this.labelEmployeeHireTime.Size = new System.Drawing.Size(123, 13);
            this.labelEmployeeHireTime.TabIndex = 74;
            this.labelEmployeeHireTime.Text = global::FormsView.TranslationsStatic.Default.labelHireTime;
            // 
            // textBoxEmployeeSurname
            // 
            this.textBoxEmployeeSurname.Location = new System.Drawing.Point(134, 47);
            this.textBoxEmployeeSurname.Name = "textBoxEmployeeSurname";
            this.textBoxEmployeeSurname.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxEmployeeSurname.Size = new System.Drawing.Size(151, 20);
            this.textBoxEmployeeSurname.TabIndex = 59;
            // 
            // textBoxEmployeeQualifications
            // 
            this.textBoxEmployeeQualifications.Location = new System.Drawing.Point(134, 302);
            this.textBoxEmployeeQualifications.Multiline = true;
            this.textBoxEmployeeQualifications.Name = "textBoxEmployeeQualifications";
            this.textBoxEmployeeQualifications.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxEmployeeQualifications.Size = new System.Drawing.Size(151, 61);
            this.textBoxEmployeeQualifications.TabIndex = 73;
            // 
            // labelEmployeeSurname
            // 
            this.labelEmployeeSurname.AutoSize = true;
            this.labelEmployeeSurname.Location = new System.Drawing.Point(16, 47);
            this.labelEmployeeSurname.Name = "labelEmployeeSurname";
            this.labelEmployeeSurname.Size = new System.Drawing.Size(56, 13);
            this.labelEmployeeSurname.TabIndex = 57;
            this.labelEmployeeSurname.Text = global::FormsView.TranslationsStatic.Default.labelPersonSurname;
            // 
            // labelEmployeeQualifications
            // 
            this.labelEmployeeQualifications.AutoSize = true;
            this.labelEmployeeQualifications.Location = new System.Drawing.Point(16, 302);
            this.labelEmployeeQualifications.Name = "labelEmployeeQualifications";
            this.labelEmployeeQualifications.Size = new System.Drawing.Size(66, 13);
            this.labelEmployeeQualifications.TabIndex = 72;
            this.labelEmployeeQualifications.Text = global::FormsView.TranslationsStatic.Default.labelQualifications;
            // 
            // textBoxEmployeeFirstName
            // 
            this.textBoxEmployeeFirstName.Location = new System.Drawing.Point(134, 10);
            this.textBoxEmployeeFirstName.Name = "textBoxEmployeeFirstName";
            this.textBoxEmployeeFirstName.Size = new System.Drawing.Size(151, 20);
            this.textBoxEmployeeFirstName.TabIndex = 58;
            // 
            // comboBoxEmploymentState
            // 
            this.comboBoxEmploymentState.DisplayMember = "Value";
            this.comboBoxEmploymentState.FormattingEnabled = true;
            this.comboBoxEmploymentState.Location = new System.Drawing.Point(132, 270);
            this.comboBoxEmploymentState.Name = "comboBoxEmploymentState";
            this.comboBoxEmploymentState.Size = new System.Drawing.Size(151, 21);
            this.comboBoxEmploymentState.TabIndex = 70;
            this.comboBoxEmploymentState.ValueMember = "Value";
            this.comboBoxEmploymentState.SelectedIndexChanged += new System.EventHandler(this.CheckItem);
            // 
            // labelEmployeePesel
            // 
            this.labelEmployeePesel.AutoSize = true;
            this.labelEmployeePesel.Location = new System.Drawing.Point(16, 125);
            this.labelEmployeePesel.Name = "labelEmployeePesel";
            this.labelEmployeePesel.Size = new System.Drawing.Size(44, 13);
            this.labelEmployeePesel.TabIndex = 61;
            this.labelEmployeePesel.Text = global::FormsView.TranslationsStatic.Default.labelPesel;
            // 
            // labelEmploymentStatus
            // 
            this.labelEmploymentStatus.AutoSize = true;
            this.labelEmploymentStatus.Location = new System.Drawing.Point(16, 270);
            this.labelEmploymentStatus.Name = "labelEmploymentStatus";
            this.labelEmploymentStatus.Size = new System.Drawing.Size(100, 13);
            this.labelEmploymentStatus.TabIndex = 69;
            this.labelEmploymentStatus.Text = global::FormsView.TranslationsStatic.Default.labelEmploymentState;
            // 
            // labelEmployeePhone
            // 
            this.labelEmployeePhone.AutoSize = true;
            this.labelEmployeePhone.Location = new System.Drawing.Point(16, 197);
            this.labelEmployeePhone.Name = "labelEmployeePhone";
            this.labelEmployeePhone.Size = new System.Drawing.Size(82, 13);
            this.labelEmployeePhone.TabIndex = 62;
            this.labelEmployeePhone.Text = global::FormsView.TranslationsStatic.Default.labelTelephone;
            // 
            // maskedTextBoxEmployeeNIP
            // 
            this.maskedTextBoxEmployeeNIP.Location = new System.Drawing.Point(134, 161);
            this.maskedTextBoxEmployeeNIP.Mask = "000-000-00-00";
            this.maskedTextBoxEmployeeNIP.Name = "maskedTextBoxEmployeeNIP";
            this.maskedTextBoxEmployeeNIP.Size = new System.Drawing.Size(77, 20);
            this.maskedTextBoxEmployeeNIP.TabIndex = 68;
            // 
            // labelEmployeeMail
            // 
            this.labelEmployeeMail.AutoSize = true;
            this.labelEmployeeMail.Location = new System.Drawing.Point(16, 235);
            this.labelEmployeeMail.Name = "labelEmployeeMail";
            this.labelEmployeeMail.Size = new System.Drawing.Size(38, 13);
            this.labelEmployeeMail.TabIndex = 63;
            this.labelEmployeeMail.Text = global::FormsView.TranslationsStatic.Default.labelMail;
            // 
            // labelEmployeeNIP
            // 
            this.labelEmployeeNIP.AutoSize = true;
            this.labelEmployeeNIP.Location = new System.Drawing.Point(16, 161);
            this.labelEmployeeNIP.Name = "labelEmployeeNIP";
            this.labelEmployeeNIP.Size = new System.Drawing.Size(28, 13);
            this.labelEmployeeNIP.TabIndex = 67;
            this.labelEmployeeNIP.Text = global::FormsView.TranslationsStatic.Default.labelNIP;
            // 
            // textBoxEmployeeMail
            // 
            this.textBoxEmployeeMail.Location = new System.Drawing.Point(134, 235);
            this.textBoxEmployeeMail.Name = "textBoxEmployeeMail";
            this.textBoxEmployeeMail.Size = new System.Drawing.Size(151, 20);
            this.textBoxEmployeeMail.TabIndex = 64;
            // 
            // maskedTextBoxEmployeePesel
            // 
            this.maskedTextBoxEmployeePesel.Location = new System.Drawing.Point(134, 125);
            this.maskedTextBoxEmployeePesel.Mask = "00000000000";
            this.maskedTextBoxEmployeePesel.Name = "maskedTextBoxEmployeePesel";
            this.maskedTextBoxEmployeePesel.Size = new System.Drawing.Size(77, 20);
            this.maskedTextBoxEmployeePesel.TabIndex = 66;
            // 
            // maskedTextBoxEmployeePhone
            // 
            this.maskedTextBoxEmployeePhone.Location = new System.Drawing.Point(134, 197);
            this.maskedTextBoxEmployeePhone.Mask = "000-000-000";
            this.maskedTextBoxEmployeePhone.Name = "maskedTextBoxEmployeePhone";
            this.maskedTextBoxEmployeePhone.Size = new System.Drawing.Size(77, 20);
            this.maskedTextBoxEmployeePhone.TabIndex = 65;
            this.maskedTextBoxEmployeePhone.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // tabPageEmployeeAddress
            // 
            this.tabPageEmployeeAddress.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageEmployeeAddress.Controls.Add(this.addressEditorEmployee);
            this.tabPageEmployeeAddress.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmployeeAddress.Name = "tabPageEmployeeAddress";
            this.tabPageEmployeeAddress.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmployeeAddress.Size = new System.Drawing.Size(306, 453);
            this.tabPageEmployeeAddress.TabIndex = 1;
            this.tabPageEmployeeAddress.Text = "Dane adresowe";
            // 
            // addressEditorEmployee
            // 
            this.addressEditorEmployee.ApartmentNumber = "";
            this.addressEditorEmployee.CityId = -1;
            this.addressEditorEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressEditorEmployee.Enabled = false;
            this.addressEditorEmployee.HouseNumber = "";
            this.addressEditorEmployee.Location = new System.Drawing.Point(3, 3);
            this.addressEditorEmployee.Name = "addressEditorEmployee";
            this.addressEditorEmployee.Postcode = "  -";
            this.addressEditorEmployee.Province = null;
            this.addressEditorEmployee.ProvinceId = -1;
            this.addressEditorEmployee.Size = new System.Drawing.Size(300, 447);
            this.addressEditorEmployee.Street = "";
            this.addressEditorEmployee.TabIndex = 45;
            // 
            // tabPageEmployeeUserData
            // 
            this.tabPageEmployeeUserData.Controls.Add(this.panelSytemUserData);
            this.tabPageEmployeeUserData.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmployeeUserData.Name = "tabPageEmployeeUserData";
            this.tabPageEmployeeUserData.Size = new System.Drawing.Size(306, 453);
            this.tabPageEmployeeUserData.TabIndex = 2;
            this.tabPageEmployeeUserData.Text = "Konto systemowe";
            this.tabPageEmployeeUserData.UseVisualStyleBackColor = true;
            // 
            // panelSytemUserData
            // 
            this.panelSytemUserData.BackColor = System.Drawing.SystemColors.Control;
            this.panelSytemUserData.Controls.Add(this.buttonGeneratePassword);
            this.panelSytemUserData.Controls.Add(this.textBoxPasswordNewRepeat);
            this.panelSytemUserData.Controls.Add(this.labePasswordRepeat);
            this.panelSytemUserData.Controls.Add(this.textBoxPassword);
            this.panelSytemUserData.Controls.Add(this.labelPassword);
            this.panelSytemUserData.Controls.Add(this.textBoxLogin);
            this.panelSytemUserData.Controls.Add(this.labelLogin);
            this.panelSytemUserData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSytemUserData.Enabled = false;
            this.panelSytemUserData.Location = new System.Drawing.Point(0, 0);
            this.panelSytemUserData.Name = "panelSytemUserData";
            this.panelSytemUserData.Size = new System.Drawing.Size(306, 453);
            this.panelSytemUserData.TabIndex = 0;
            // 
            // buttonGeneratePassword
            // 
            this.buttonGeneratePassword.Location = new System.Drawing.Point(86, 132);
            this.buttonGeneratePassword.Name = "buttonGeneratePassword";
            this.buttonGeneratePassword.Size = new System.Drawing.Size(134, 23);
            this.buttonGeneratePassword.TabIndex = 68;
            this.buttonGeneratePassword.Text = global::FormsView.TranslationsStatic.Default.buttonGeneratePassword;
            this.buttonGeneratePassword.UseVisualStyleBackColor = true;
            this.buttonGeneratePassword.Click += new System.EventHandler(this.buttonGeneratePassword_Click);
            // 
            // textBoxPasswordNewRepeat
            // 
            this.textBoxPasswordNewRepeat.Location = new System.Drawing.Point(137, 88);
            this.textBoxPasswordNewRepeat.MaxLength = 12;
            this.textBoxPasswordNewRepeat.Name = "textBoxPasswordNewRepeat";
            this.textBoxPasswordNewRepeat.Size = new System.Drawing.Size(149, 20);
            this.textBoxPasswordNewRepeat.TabIndex = 67;
            // 
            // labePasswordRepeat
            // 
            this.labePasswordRepeat.AutoSize = true;
            this.labePasswordRepeat.Location = new System.Drawing.Point(16, 88);
            this.labePasswordRepeat.Name = "labePasswordRepeat";
            this.labePasswordRepeat.Size = new System.Drawing.Size(78, 13);
            this.labePasswordRepeat.TabIndex = 66;
            this.labePasswordRepeat.Text = global::FormsView.TranslationsStatic.Default.labelPasswordRepeatPassword;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(137, 50);
            this.textBoxPassword.MaxLength = 12;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(149, 20);
            this.textBoxPassword.TabIndex = 65;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(16, 50);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(39, 13);
            this.labelPassword.TabIndex = 64;
            this.labelPassword.Text = global::FormsView.TranslationsStatic.Default.labelPassword;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(137, 15);
            this.textBoxLogin.MaxLength = 25;
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(149, 20);
            this.textBoxLogin.TabIndex = 63;
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(16, 18);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(105, 13);
            this.labelLogin.TabIndex = 62;
            this.labelLogin.Text = global::FormsView.TranslationsStatic.Default.labelLogin;
            // 
            // saveCancelControls1
            // 
            this.saveCancelControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.saveCancelControls1.Enabled = false;
            this.saveCancelControls1.Location = new System.Drawing.Point(0, 581);
            this.saveCancelControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.saveCancelControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.saveCancelControls1.Name = "saveCancelControls1";
            this.saveCancelControls1.Sender = null;
            this.saveCancelControls1.Size = new System.Drawing.Size(314, 25);
            this.saveCancelControls1.TabIndex = 13;
            // 
            // addEditControls1
            // 
            this.addEditControls1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addEditControls1.Location = new System.Drawing.Point(0, 606);
            this.addEditControls1.MaximumSize = new System.Drawing.Size(0, 25);
            this.addEditControls1.MinimumSize = new System.Drawing.Size(150, 25);
            this.addEditControls1.Name = "addEditControls1";
            this.addEditControls1.SearchButtonVisible = true;
            this.addEditControls1.Sender = null;
            this.addEditControls1.Size = new System.Drawing.Size(314, 25);
            this.addEditControls1.TabIndex = 11;
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "employee.chm";
            // 
            // EmployeeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlEmployee);
            this.Controls.Add(this.saveCancelControls1);
            this.Controls.Add(this.addEditControls1);
            this.MaximumSize = new System.Drawing.Size(314, 0);
            this.MinimumSize = new System.Drawing.Size(314, 631);
            this.Name = "EmployeeEditor";
            this.helpProvider1.SetShowHelp(this, true);
            this.Size = new System.Drawing.Size(314, 631);
            this.tabControlEmployee.ResumeLayout(false);
            this.tabPageEmployeeData.ResumeLayout(false);
            this.panelEmployeeData.ResumeLayout(false);
            this.panelEmployeeData.PerformLayout();
            this.tabPageEmployeeAddress.ResumeLayout(false);
            this.tabPageEmployeeUserData.ResumeLayout(false);
            this.panelSytemUserData.ResumeLayout(false);
            this.panelSytemUserData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.SaveCancelControls saveCancelControls1;
        private Controls.AddEditSearchControls addEditControls1;
        private FormsView.Controls.AddressControl addressEditorEmployee;
        private System.Windows.Forms.TabControl tabControlEmployee;
        private System.Windows.Forms.TabPage tabPageEmployeeAddress;
        private System.Windows.Forms.TabPage tabPageEmployeeData;
        private System.Windows.Forms.Panel panelEmployeeData;
        private System.Windows.Forms.ComboBox comboBoxEmployeeGroup;
        private System.Windows.Forms.Label labelEmployeeGroup;
        private System.Windows.Forms.Label labelEmployeeFirstName;
        private System.Windows.Forms.Label labelEmployeeHireTime;
        private System.Windows.Forms.TextBox textBoxEmployeeSurname;
        private System.Windows.Forms.TextBox textBoxEmployeeQualifications;
        private System.Windows.Forms.Label labelEmployeeSurname;
        private System.Windows.Forms.Label labelEmployeeQualifications;
        private System.Windows.Forms.TextBox textBoxEmployeeFirstName;
        private System.Windows.Forms.ComboBox comboBoxEmploymentState;
        private System.Windows.Forms.Label labelEmployeePesel;
        private System.Windows.Forms.Label labelEmploymentStatus;
        private System.Windows.Forms.Label labelEmployeePhone;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxEmployeeNIP;
        private System.Windows.Forms.Label labelEmployeeMail;
        private System.Windows.Forms.Label labelEmployeeNIP;
        private System.Windows.Forms.TextBox textBoxEmployeeMail;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxEmployeePesel;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxEmployeePhone;
        private System.Windows.Forms.DateTimePicker dateTimePickerHireDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerReleaseDate;
        private System.Windows.Forms.CheckBox checkBoxReleaseDate;
        private System.Windows.Forms.TabPage tabPageEmployeeUserData;
        private System.Windows.Forms.Panel panelSytemUserData;
        private System.Windows.Forms.TextBox textBoxPasswordNewRepeat;
        private System.Windows.Forms.Label labePasswordRepeat;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Button buttonGeneratePassword;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}
