﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AppLogic.Interfaces.Editors;
using AppLogic.Presenters.Editor;
using AppLogic.Interfaces;
using FormsView.Helpers;
using AppLogic.Interfaces.Controls;
using AppLogic.Interfaces.Events;
using AppLogic.Presenters;
using AppLogic.Presenters.Popup.Search;
using AppLogic.Interfaces.Popups.Search;

namespace FormsView.Editors
{
    public partial class ClientEditor : Base.BaseUserControl, IClientEditorView, IOpenPopupSearchEvent
    {
        public ClientEditor()
        {
            InitializeComponent();

            _AddCarPage(new Model.Projection.Car());
        }

        public void HideSaveCancelControls()
        {
            saveCancelControls1.Hide();
            addEditControls1.AddEditHide();
        }

        public void ShowSaveCancelControls()
        {
            saveCancelControls1.Show();
            addEditControls1.AddEditShow();
        }

        #region Fields

        ClientPresenter _presenter;

        /// <summary>
        /// Trzymanie logiki wyszukiwania, żeby zapamiętać aktywne filtry
        /// </summary>
        public SearchLogicContainer<IClientSearchView> _searchLogic;

        #endregion

        #region IEditorView Members

        public void Initialize(AppLogic.Interfaces.IEditorPresenter presenter)
        {
            presenter.Initialize();
            _presenter = (presenter as ClientPresenter);
            _presenter.View = this;
            saveCancelControls1.Sender = _presenter;

            // Podpinamy metodę, która reaguje na przycisk "Wyszukaj"
            addEditControls1.AddHandler = OnOpenPopupSearch;
        }

        #endregion

        #region IControlView Members

        public bool Enable
        {
            set
            {
                buttonClientAddCar.Enabled = value;
                panelClientData.Enabled = value;
                addressEditorClient.Enabled = value;
                saveCancelControls1.Enable = value;

                _EnableCarControls(value);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return panelClientData.Enabled || addressEditorClient.Enabled;
            }
        }

        #endregion

        #region IClientEditorView Members

        public string ClientName
        {
            get
            {
                return textBoxClientFirstName.Text;
            }
            set
            {
                textBoxClientFirstName.Text = value;
            }
        }

        public string ClientSurname
        {
            get
            {
                return textBoxClientSurname.Text;
            }
            set
            {
                textBoxClientSurname.Text = value;
            }
        }

        public List<Model.Projection.ComboBox> ClientDiscounts
        {
            set
            {
                comboBoxClientDiscount.Items.Clear();
                comboBoxClientDiscount.Items.Add(new Model.Projection.ComboBox()
                {
                    Id = 0,
                    Value = TranslationsStatic.Default.comboBoxEmpty
                });
                comboBoxClientDiscount.Items.AddRange(value.ToArray());
            }
        }

        public int? ClientDiscountId
        {
            get
            {
                try
                {
                    return (comboBoxClientDiscount.SelectedItem as Model.Projection.ComboBox).Id;
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                try
                {
                    comboBoxClientDiscount.SelectedIndex = comboBoxClientDiscount.FindIndex(value ?? 0);
                } // Object reference not set on to an instance of an object
                catch (Exception)
                {
                    comboBoxClientDiscount.SelectedIndex = -1;
                }
            }
        }

        public string ClientPesel
        {
            get
            {
                return maskedTextBoxClientPesel.Text;
            }
            set
            {
                maskedTextBoxClientPesel.Text = value;
            }
        }

        public string ClientPhoneNumber
        {
            get
            {
                return maskedTextBoxClientPhone.Text;
            }
            set
            {
                maskedTextBoxClientPhone.Text = value;
            }
        }

        public List<Model.Interfaces.ICar> ClientCars
        {
            get
            {
                return _GetCars();
            }
            set
            {
                _RemoveAllCars();
                value.ForEach(o => _AddCarPage(o));
            }
        }

        public string ClientMail
        {
            get
            {
                return textBoxClientMail.Text;
            }
            set
            {
                textBoxClientMail.Text = value;
            }
        }

        public IAddress ClientAddress
        {
            get 
            {
                return (addressEditorClient as IAddress);
            }
        }

        #endregion

        #region AdditionalMethods

        /// <summary>
        /// Metoda włącza / wyłącza kontrolki samochodów
        /// </summary>
        /// <param name="state">Flaga określająca, czy kontrolka ma być włączona, czy wyłączona</param>
        private void _EnableCarControls(bool state)
        {
            // Przeszukujemy wszystkie zakładki żeby zebrać dane samochodów
            foreach (TabPage page in tabsClientData.TabPages)
            {
                // Jeżeli znaleziono zakładkę z danymi samochodu
                if (page.Name.Contains("tabPageCar"))
                {
                    // Ustawienie aktywności kontrolki
                    (page.Controls[0] as ICarControlView).Enable = state;
                }
            }
        }

        /// <summary>
        /// Metoda usuwa wszystkie samochody
        /// </summary>
        private void _RemoveAllCars()
        {
            tabsClientData.TabPages.Clear();
            tabsClientData.TabPages.Add(tabPageClient);
            tabsClientData.TabPages.Add(tabPageClientAddress);
        }

        /// <summary>
        /// Usuwa zakładkę samochodu o określonym indeksie
        /// </summary>
        /// <param name="index">Indeks zakładki z samochodem</param>
        private void _RemoveCarPage(int index)
        {
            // Zapewnienie, że będą usuwane tylko zakładki samochodów
            if (!tabsClientData.TabPages[index].Name.Contains("tabPageCar"))
                return;

            tabsClientData.TabPages.RemoveAt(index);

            // Numer zakładki auta
            int tid = 1;
            foreach(TabPage page in tabsClientData.TabPages)
            {
                if (page.Name.Contains("tabPageCar"))
                {
                    page.Text = TranslationsStatic.Default.tabTitleCar + " #" + tid.ToString();
                    page.Name = "tabPageCar" + tid.ToString();
                    tid++;
                }
            }
        }

        /// <summary>
        /// Tworzy zakładke z danymi samochodu
        /// </summary>
        /// <param name="car">Obiekt z danymi samochodu, które będą widoczne w zakładce</param>
        /// <param name="enable">Flaga określa, czy zakładka ma być aktywna</param>
        private void _AddCarPage(Model.Interfaces.ICar car, bool enable = false)
        {
            // Numer zakładki auta
            var tid = (tabsClientData.TabPages.Count - 1);
            
            // Nowa zakładka
            var panel = new TabPage()
            {
                Text = TranslationsStatic.Default.tabTitleCar + " #" + tid.ToString(),
                Name = "tabPageCar"+tid.ToString()
            };

            var cc = new Controls.CarControl()
            {
                BrandId = car.BrandId,
                ModelId = car.ModelId,
                ColorId = car.ColorId,
                FuelId = car.FuelId,
                RegistrationNumber = car.RegistrationNumber,
                BodyNumber = car.BodyNumber,
                EngineCapacity = car.EngineCapacity ?? 0,
                ProductionYear = car.ProductionYear ?? 0,
                Enable = enable
            };

            // Dodanie kontrolki z danymi samochodu do zakładki
            panel.Controls.Add(cc);

            // Dodanie zakładki do kontrolki
            tabsClientData.Controls.Add(panel);
        }

        /// <summary>
        /// Zwraca dane samochodów w kontrolce w postaci listy.
        /// </summary>
        /// <returns>Lista z danymi samochodów</returns>
        private List<Model.Interfaces.ICar> _GetCars()
        {
            var list = new List<Model.Interfaces.ICar>();

            // Przeszukujemy wszystkie zakładki żeby zebrać dane samochodów
            foreach(TabPage page in tabsClientData.TabPages)
            {
                // Jeżeli znaleziono zakładkę z danymi samochodu
                if(page.Name.Contains("tabPageCar"))
                {
                    // Pobranie kontrolki z danymi samochodu z aktualnej zakładki
                    // I wstawienie danych samochodu do listy
                    list.Add(page.Controls[0] as Model.Interfaces.ICar);
                }
            }

            return list;
        }

        #endregion

        #region Events

        private void AddNewCar(object sender, EventArgs e)
        {
            _AddCarPage(new Model.Projection.Car(), true);

            // Ustawiamy nową zakładke jako aktywną
            tabsClientData.SelectedIndex = tabsClientData.TabPages.Count - 1;
        }


        private void buttonClientRemoveCar_Click(object sender, EventArgs e)
        {
            _RemoveCarPage(tabsClientData.SelectedIndex);

            // Ustawiamy ostatnią zakładke jako aktywną
            tabsClientData.SelectedIndex = tabsClientData.TabPages.Count - 1;
        }

        private void TabChanged(object sender, EventArgs e)
        {
            // Włączamy przycisk "usuń samochód" dla aktywnej zakładki samochodu
            if (tabsClientData.SelectedTab != null && tabsClientData.SelectedTab.Name.Contains("tabPageCar"))
                buttonClientRemoveCar.Enabled = IsEnabled && true;
            else
                buttonClientRemoveCar.Enabled = false;
        }

        #endregion

        /// <summary>
        /// Metoda odpala w dodatkowym okienku formatkę wyszukiwania klienta
        /// </summary>
        /// <param name="sender"></param>
        public void OnOpenPopupSearch(object sender)
        {
            if(_searchLogic == null)
                _searchLogic = new SearchLogicContainer<IClientSearchView>(new ClientSearchPresenter(new SearchControls.ClientSearchControl()));

            // Tworzymy okienko kontenera
            var popup = new Forms.PopupContainer();

            popup.Initialize(_searchLogic.View, _searchLogic);
            popup.Text = TranslationsStatic.Default.titleSearchClient;
            popup.ShowDialog(); // Metoda blokująca, dlatego możemy od razu potem usunąć obsługę zdarzenia
        }

        public void ClearSearchFilters()
        {
            _searchLogic = null;
        }
    }
}
